# [etke.cc](https://etke.cc)

matrix homeservers setup & maintenance

### shortcuts

> managed on CDN

* https://etke.cc/wiki
* https://etke.cc/bmc
* https://etke.cc/kofi
* https://etke.cc/setup
* https://etke.cc/membership
* https://etke.cc/consultation
