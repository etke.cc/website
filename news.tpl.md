---
id: '{{ .ID }}'
date: '{{ .CreatedAtFull }}'
title: '{{ .CreatedAt }}{{ .ReplacedNote }}'
author: '{{ .Author }}'
draft: false
layout: singlenews
description: 'New message published on {{ .CreatedAt }}'
---

{{ .Text }}


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/{{ .ID }}?via=etke.cc&via=matrix.org)
