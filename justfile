# Shows help
default:
    @just --list --justfile {{ justfile() }}

# Pulls news from the news Matrix room (#news:etke.cc) and generates articles in content/news
news:
    @echo "exporting messages from #news:etke.cc..."
    @-emm -hs ${EMM_HOMESERVER} -u ${EMM_LOGIN} -p ${EMM_PASSWORD} -r "#news:etke.cc" -i "@_neb_rssbot_=40aine=3aetke.cc:matrix.org,@hookshot:etke.cc" -t news.tpl.md -o content/news/%s.md -l 10000
    @echo "done"

# Runs hugo server
run:
    @hugo server

# Builds the website and runs a production-like server on port 1313 (same as `just run`)
run-prod:
    {{ just_executable() }} svelte
    @hugo --ignoreCache --minify -b http://localhost:1313
    cp svelte/src/lib/provider/dataConfig.json public/order/components.json
    @python -m http.server -d public 1313

# Builds Svelte components to static files
svelte: __svelte_deps
    cd svelte; yarn run build
    mkdir -p assets/order
    mv svelte/dist/assets/* assets/order/

# install svelte deps
__svelte_deps:
    cd svelte; yarn install --non-interactive

# Runs a development server (http://localhost:5173) for Svelte development (live reload, etc.)
svelte-dev: __svelte_deps
    cd svelte; yarn run dev --host=0.0.0.0 --port=5173

# Builds the hugo website, and copies components.json to the public folder
build: svelte news
    @hugo --ignoreCache --minify
    cp svelte/src/lib/provider/dataConfig.json public/order/components.json

deploy:
    @echo "uploading website..."
    @bunny-upload -c ${BUNNY_CONFIG}
    @echo "done"
