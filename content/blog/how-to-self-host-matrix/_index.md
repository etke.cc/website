---
id: how-to-self-host-matrix
title: How to self-host Matrix and why you probably shouldn't
draft: true
layout: blog-entry
---

[Matrix](https://matrix.org) is [federated](https://en.wikipedia.org/wiki/Federation_(information_technology)) communication network.

Federated networks (like email or the [fediverse](https://en.wikipedia.org/wiki/Fediverse)) thrive on diversity - the more interconnected and split-out the network, the more resilient it is. Individual nodes may experience failures, downtime or censorship, but the network as a whole continues to run unaffected.

There are [various way to join the Matrix chat network](/blog/how-to-join-matrix).

...
