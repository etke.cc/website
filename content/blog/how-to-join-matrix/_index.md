---
id: how-to-join-matrix
title: How to join the Matrix chat network
draft: true
layout: blog-entry
---

## Introduction

[Matrix](https://matrix.org) is [federated](https://en.wikipedia.org/wiki/Federation_(information_technology)) communication network. Federated networks are a collection of servers that can all communicate with one another.

**Email** systems work the same way - `john@example.com` can send emails to `peter@gmail.com` or `frank@yahoo.com`. The inboxes for all these people live on completely separate systems, yet they can all communicate with one another seamlessly. This is only possible, because all these different systems speak a common protocol (in the case of [email](https://en.wikipedia.org/wiki/Email), a collection of various arcane protocols).

**Matrix** is also a protocol. The Matrix network is comprised of **thousands of servers** across the world, which host **millions of users**.
All Matrix users can communicate with one another regardless of which Matrix server they use.

**Matrix user identifiers** (also called Matrix IDs or MXIDs) resemble email identifiers with some minor differences. Here's one: `@john:example.com` - note the `@` at the start, and the `:` character separating the username (called localpart) from the domain name.


## Joining Matrix

To join the Matrix network, you need to:

1. [Choose a Matrix client](#choosing-a-client-application) application, which you will use to to connect to the Matrix network
2. [Choose a Matrix server](#choosing-a-matrix-server-to-host-your-data) which will host your data (messages, photos, videos, etc.)


## Choosing a Matrix client application

The Matrix homeserver is **your** gateway to the Matrix network. To reach this gateway, you'll need to have a Matrix Client application. To continue the email analogy we started earlier - this is like Outlook or Mozilla Thunderbird - an application which talks "email". In our case, we're looking for an application which talks "matrix".

You can find **a list of Matrix client applications** for various platforms (web, desktop, mobile) [on the matrix.org website](https://matrix.org/clients/).

If you're new to Matrix (which we assume you are, given that you're reading this article), we recommend that you go with **Element**. Element may not be the fastest or lightest application available, but is currently the most fully-featured one.

You can get Element the following ways:

- by using a web browser and accessing:
  - the [`app.element.io`](https://app.element.io) website (maintained by the Element developers)
  - the [app.etke.cc](https://app.etke.cc) website (maintained by us)
- on your desktop, by [downloading the Element application](https://element.io/get-started#download)
- on your Android mobile device:
  - by installing [Element from the Google Play store](https://play.google.com/store/apps/details?id=im.vector.app)
  - by installing [Element from the F-Droid store](https://f-droid.org/packages/im.vector.app/)
- on your iOS mobile device, by installing [Element from the Apple App Store](https://apps.apple.com/app/vector/id1083446067)

Whichever method you go with, know that we've only solved the first part - choosing a Matrix client application.
For this application to actually be of any use, you'd need to connect it to a Matrix server. We'll let you know how to [choose a Matrix server](#choosing-a-matrix-server-to-host-your-data) below.


## Choosing a Matrix server to host your data

Matrix is not a [Peer-to-peer](https://en.wikipedia.org/wiki/Peer-to-peer) network. As such, your data does not live on the client.
This has a few benefits, such as:

- your data is less likely to get lost when your computer (phone) is broken or lost
- your server continues being reachable even when you're offline
- all the heavy communication work is handled by a more powerful computer than your mobile device
- your various devices (computers, phones, etc.) can all easily download the same messages, photos, etc. from a central location

Your Matrix server is your **gateway** to the Matrix network.

You can join Matrix a few different ways:

- by [using a public Matrix server](#using-a-public-matrix-server)
  - ✅ completely free
  - ✅ quick
  - ✅ a great way to try it out as first-time user
  - 🚫 not very private or reliable

- by [self-hosting the Matrix server yourself](#self-hosting-a-matrix-homeserver-yourself)
  - ✅ gives you complete data sovereignty
  - 🚫 you need to obtain a VPS server yourself
  - 🚫 you need to bare the server-cost yourself
  - 🚫 you need to learn how to do it (technical skills required)
  - 🚫 you need to bare all the server administration burden and security risks yourself
  - 🚫 likely not as fully-featured (may lack bridges or other services) depending on your self-hosting method of choice

- by [getting a managed Matrix server, operated by etke.cc](#managed-matrix-servers-operated-by-etkecc)
  - ✅ gives you complete data sovereignty (data lives on your own server or on one provided by us - it's your choice)
  - ✅ we can obtain a VPS server on your behalf, if you'd rather not do it yourself
  - ✅ quick
  - ✅ no technical skills required on your part
  - ✅ all server administration burden and security risks fall on us
  - ✅ you get a huge list of [bridges](/help/bridges), [bots](/help/bots) and [extra services](/help/extras)
  - 🚫 you need to bare the server-cost yourself
  - 🚫 we charge a small server maintenance fee (which we think is fair)


### Using a public Matrix server

The easiest way to join Matrix is to use someone else's Matrix server.

There are various public Matrix servers which you can join for free. You can claim a free username there (e.g. `@someone:matrix.org`) and start chatting with anyone else on the Matrix network.

If you're new to Matrix, we recommend that you give it a try from a public server. Later on, you can start anew at a more permanent Matrix home for you - your [very own self-hosted Matrix server](#self-hosting-a-matrix-homeserver-yourself) or a [managed Matrix server operated by etke.cc](#managed-matrix-servers-operated-by-etkecc).

#### Choosing a public Matrix server

You can choose any public Matrix server from an unofficial list such as [this one](https://www.hello-matrix.net/public_servers.php).

If you'd rather not dig through such lists, we recommend that you use `matrix.org` - a public Matrix server maintained by the Matrix-protocol developers. The `matrix.org` homeserver. This is the largest public server and as such is often overloaded or unstable.


### Self-hosting a Matrix homeserver yourself

If you'd rather not use someone else's server and instead **run your own Matrix server**, you can do that by self-hosting the full Matrix stack.

In fact, that's how we at etke.cc started using Matrix a few years ago. Back in those days, the only way to use Matrix was to either join the public `matrix.org` server or to self-host Matrix by yourself with all the pain involved in that.

Out of necessity, we developed what we believe is **the only sane way to deploy Matrix** - the [matrix-docker-ansible-deploy](https://github.com/spantaleev/matrix-docker-ansible-deploy) Ansible playbook.

The documentation for that [Ansible](https://www.ansible.com/) playbook ([available here](https://github.com/spantaleev/matrix-docker-ansible-deploy/tree/master/docs)) is extensive and thousands of people use it to deploy their Matrix servers.

With that, **technically-inclined people with some skill and time on their hands** can get a **fully-featured** Matrix server running in a matter of hours.

We say **fully-featured**, because running Matrix (just like running an email server system) is a complicated endeavour, which requires running multiple components which work together. It's not just the Matrix homeserver that needs to be installed. For a serious and useful deployment, you'd need a [Postgres](https://www.postgresql.org/) database server, a reverse-proxy server, SSL certificates, a client application (like Element), a TURN/STUN server for audio/video calls, possibly a few Matrix bridges, etc.

If you're looking to self-host Matrix yourself or if you've had troubles self-hosting Matrix in the past, check out [matrix-docker-ansible-deploy](https://github.com/spantaleev/matrix-docker-ansible-deploy) - you'd be surprised how (relatively) easy it is!

If you'd rather not maintain your own servers (an ongoing endeavour), you can always get a [managed Matrix server operated by etke.cc](#managed-matrix-servers-operated-by-etkecc).


### Managed Matrix servers operated by etke.cc
