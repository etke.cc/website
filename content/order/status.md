---
title: Order Status
noindex: true
draft: false
layout: single
---

{{< partial "order_status.html" >}}

## FAQ

### Delegation Redirects

We typically host your Matrix server on a subdomain, like `matrix.your-server.com`. This normally results in full usernames which include the `matrix.` prefix (e.g. `@someone:matrix.your-server.com`).

To host Matrix on a subdomain, but have cleaner usernames which do not contain the subdomain (e.g. `@someone:your-server.com`), so-called **delegation is necessary**. It's a similar concept to email services, where you might have an email server at `mail.your-server.com`, but your mailbox addresses are still like `someone@your-server.com`.

Matrix server delegation can be done either with DNS [SRV records](https://en.wikipedia.org/wiki/SRV_record) or with well-known files. DNS SRV record delegation is not supported by us, because its SSL certificate setup is peculiar.

Our setup only supports the well-known file delegation method, which involves you **setting up a few redirects** (`/.well-known/matrix/*`) **on your base domain**. Setting up these redirects allows for shorter, more user-friendly usernames, while still keeping your base domain available for other purposes (such as hosting a website).

Here is the **full list of redirects you need to configure** (assuming your base domain is `your-server.com`):

* `https://your-server.com/.well-known/matrix/server` -> `https://matrix.your-server.com/.well-known/matrix/server`
* `https://your-server.com/.well-known/matrix/client` -> `https://matrix.your-server.com/.well-known/matrix/client`
* `https://your-server.com/.well-known/matrix/support` -> `https://matrix.your-server.com/.well-known/matrix/support`

**Alternatively**: if you are unable or unwilling to configure these `/.well-known/matrix/*` redirects on your base domain, we can set up your server exclusively on `matrix.your-server.com`. In this case, usernames will be longer, such as `@someone:matrix.your-server.com`. Please, indicate your preference during the order discussion!

**Also**: if you do not need to make use of your base domain for hosting a website right now, it's easiest if you point the base domain's DNS record to the Matrix server's IP address and have us handle the redirections on your behalf.

### Ports and Firewalls

Matrix server components require various ports for operation.
In case of on-premises setups, you may need to adjust your firewall settings to allow traffic on the ports described below (if you are using our managed hosting, we will take care of this for you).
The mandatory ports for the base components of the Matrix stack are as follows:

* `22/tcp` - SSH
* `80/tcp` - HTTP
* `443/tcp` - HTTPS
* `443/udp` - HTTPS over [HTTP/3](https://en.wikipedia.org/wiki/HTTP/3)
* `8448/tcp` - Matrix Federation (mandatory for [The Scheduler](/help/extras/scheduler/), [Matrix Support Channel](/contacts/), [Updates Announcements](/news/), Integration Manager, even bridges' and bots' avatars or any other form of cross-server communication)
* `8448/udp` - Matrix Federation over [HTTP/3](https://en.wikipedia.org/wiki/HTTP/3)
* `3478/tcp+udp` - TURN (for audio/video calls)
* `5349/tcp+udp` - TURN (for audio/video calls)
* `49152-49573/udp` (port range) - TURN (for audio/video calls)

**Jitsi**:

These ports are required only if you want to install [Jitsi](/help/extras/jitsi) on your server:

* `4443/tcp`
* `10000/udp`

**Email bridge**:

These ports are required only if you want to install [Postmoogle](/help/bridges/postmoogle) on your server:

* `25/tcp`
* `587/tcp`

**IRC bridge**:

These ports are required only if you want to install [Heisenbridge](/help/bridges/heisenbridge) on your server:

* `113/tcp`

**Firezone**:

These ports are required only if you want [Firezone](/help/extras/firezone) on your server:

* `51820/udp`

**Peertube**:

These ports are required only if you want [Peertube](/help/extras/peertube) on your server:

* `1935/udp`
