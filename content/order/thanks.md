---
title: Order received!
noindex: true
draft: false
layout: special
---
You should have received an **email with further questions** about your order (don't forget to check your Spam folder).

We're looking forward to your answers, so we can proceed to set up your server!<br>
Please be advised that orders that are older than 7 days are eligible for automatic removal on our end.<br>
If your order is not fulfilled before automatic removal, it will be erased.
