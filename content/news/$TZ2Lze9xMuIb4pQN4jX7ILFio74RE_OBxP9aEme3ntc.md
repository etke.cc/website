---
id: '$TZ2Lze9xMuIb4pQN4jX7ILFio74RE_OBxP9aEme3ntc'
date: '2023-07-14 07:25:03.182 +0000 UTC'
title: '2023-07-14 07:25 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-07-14 07:25 UTC'
---

**Stable Updates Published**

- Grafana 10.0.1 -> 10.0.2

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

**Service Updates**

- [etke.cc](https://etke.cc/)'s ssh keys have been rotated, published on the website ([etke.cc/ssh.key](https://etke.cc/ssh.key)) and updated on all customers' servers

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$TZ2Lze9xMuIb4pQN4jX7ILFio74RE_OBxP9aEme3ntc?via=etke.cc&via=matrix.org)
