---
id: '$IRzqIeBmp4UltVpQAlE0_91hjwP4V5vYsJ1xfjhbp2o'
date: '2021-11-30 16:04:42.116 +0000 UTC'
title: '2021-11-30 16:04 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-11-30 16:04 UTC'
---

**servers update in progress**
- IRC (heisenbridge) 1.7.0 -> 1.7.1
- Mailer (exim) 4.94.2-r0-5 -> 4.95-r0
- Telegram 0.10.1 -> 0.10.2
- Synapse 1.47.1 -> 1.48.0

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$IRzqIeBmp4UltVpQAlE0_91hjwP4V5vYsJ1xfjhbp2o?via=etke.cc&via=matrix.org)
