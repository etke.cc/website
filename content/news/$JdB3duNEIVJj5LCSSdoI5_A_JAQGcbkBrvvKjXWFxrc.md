---
id: '$JdB3duNEIVJj5LCSSdoI5_A_JAQGcbkBrvvKjXWFxrc'
date: '2021-12-14 19:01:20.452 +0000 UTC'
title: '2021-12-14 19:01 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-12-14 19:01 UTC'
---

**servers update in progress**
- Facebook bridge latest _will be updated with latest version again, because of important bugfixes, not included in pinned version yet_
- Jitsi 6173 -> 6726 _some changes were added to the auth flow, should not cause any issues_
- Mjolnir 1.1.20 -> 1.2.1
- #miounne:etke.cc 2.2.0 -> 2.2.1
- Synapse 1.48.0 -> 1.49.0

previous updates (already installed during off cycle deploys):
- Element web 1.9.6 -> 1.9.7
- Grafana 8.3.0 -> 8.3.1

___

Automatic database backups enabled by default for all subscribers' servers (configuration: 7 daily, 0 weekly, 0 monthly = 7 total/max), including turnkey/hosting. That was a pretty common request because not all hosting providers have automatic backup feature. Please, keep in mind that backups will require some disk space.

_If you don't want the automatic backups and/or you want to change the configuration for it - just send me a PM_

___

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$JdB3duNEIVJj5LCSSdoI5_A_JAQGcbkBrvvKjXWFxrc?via=etke.cc&via=matrix.org)
