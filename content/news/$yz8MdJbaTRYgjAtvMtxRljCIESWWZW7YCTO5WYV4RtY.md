---
id: '$yz8MdJbaTRYgjAtvMtxRljCIESWWZW7YCTO5WYV4RtY'
date: '2022-03-29 19:22:39.372 +0000 UTC'
title: '2022-03-29 19:22 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-03-29 19:22 UTC'
---

**servers have been updated**

- Mjolnir 1.3.2 -> 1.4.1
- Element 1.10.7 -> 1.10.8
- Kuma 1.12.1 -> 1.13.1
- Googlechat latest -> 0.3.1 _version pin_
- Synapse 1.55.0 -> 1.55.2

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$yz8MdJbaTRYgjAtvMtxRljCIESWWZW7YCTO5WYV4RtY?via=etke.cc&via=matrix.org)
