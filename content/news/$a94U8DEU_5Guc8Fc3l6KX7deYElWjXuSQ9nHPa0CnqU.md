---
id: '$a94U8DEU_5Guc8Fc3l6KX7deYElWjXuSQ9nHPa0CnqU'
date: '2023-04-06 19:59:41.479 +0000 UTC'
title: '2023-04-06 19:59 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-04-06 19:59 UTC'
---

**Stable Updates Published**

- Borgmatic 1.7.10 -> 1.7.11
- Coturn 4.6.1-r2 -> 4.6.1-r3
- Element / [app.etke.cc](https://app.etke.cc/) 1.11.26 -> 1.11.28
- Hookshot 3.1.1 -> 3.2.0
- Languagetool 6.0 -> 6.1
- Ntfy 2.3.0 -> 2.3.1
- Uptime Kuma 1.21.1 -> 1.21.2

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

**Service Updates**

- Encrypted Bridges option has been added to the order form for new customers. Existing customers can request it by [contacting support](https://etke.cc/contacts/). Please, note that it's **unstable** and may fail at any time without an easy way to fix it.
- A new service is now being offered: [Custom Development](https://etke.cc/services/customdev). If you have a feature you want to implement within one of our open-source projects, but for some reason there are no MRs from the open-source community, and we don't need it for our own services, you can pay us to get it done. Just to be on the same page: when we develop a project for our own needs, we implement specific features useful for our operations, but we don't have spare resources to invest into features outside that scope. Of course, we totally understand that others use cases may be different from ours and that somebody will need a bit different set of features. We want to be fair, so we added that custom development service. If somebody finds it useful - that will be wonderful!

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$a94U8DEU_5Guc8Fc3l6KX7deYElWjXuSQ9nHPa0CnqU?via=etke.cc&via=matrix.org)
