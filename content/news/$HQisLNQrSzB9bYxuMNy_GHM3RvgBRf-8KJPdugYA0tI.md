---
id: '$HQisLNQrSzB9bYxuMNy_GHM3RvgBRf-8KJPdugYA0tI'
date: '2024-06-13 20:00:11.333 +0000 UTC'
title: '2024-06-13 20:00 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-06-13 20:00 UTC'
---

**Stable Updates Published**

* [Radicale](https://github.com/tomsquest/docker-radicale): [3.2.0.0](https://github.com/tomsquest/docker-radicale/releases/tag/3.2.0.0) -> [3.2.1.0](https://github.com/tomsquest/docker-radicale/releases/tag/3.2.1.0)
* [Traefik](https://github.com/traefik/traefik): [v2.11.2](https://github.com/traefik/traefik/releases/tag/v2.11.2) -> [v2.11.4](https://github.com/traefik/traefik/releases/tag/v2.11.4)

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$HQisLNQrSzB9bYxuMNy_GHM3RvgBRf-8KJPdugYA0tI?via=etke.cc&via=matrix.org)
