---
id: '$oakLufbP4xWvbd7tC6BIb-T6veK_BhrFSL_fSCMUaIs'
date: '2023-06-22 21:25:44.803 +0000 UTC'
title: '2023-06-22 21:25 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-06-22 21:25 UTC'
---

**Stable Updates Published**

- Coturn 4.6.1-r3 -> 4.6.2-r3
- Discord bridge 0.4.0 -> 0.5.0
- Element 1.11.33 -> 1.11.34
- Etherpad 1.8.18 -> 1.9.0
- Google Chat bridge 0.4.0 -> 0.5.0 _authorization works again, this will require all users to relogin_
- Hookshot 4.2.0 -> 4.3.0
- Hydrogen 0.3.8 -> 0.4.0
- Nginx 1.23.3 -> 1.25.1
- Synapse 1.85.2 -> 1.86.0
- Traefik 2.10.1 -> 2.10.3
- WhatsApp bridge 0.8.5 -> 0.8.6

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$oakLufbP4xWvbd7tC6BIb-T6veK_BhrFSL_fSCMUaIs?via=etke.cc&via=matrix.org)
