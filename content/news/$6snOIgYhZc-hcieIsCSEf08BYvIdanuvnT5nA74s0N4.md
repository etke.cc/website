---
id: '$6snOIgYhZc-hcieIsCSEf08BYvIdanuvnT5nA74s0N4'
date: '2024-05-30 20:01:07.442 +0000 UTC'
title: '2024-05-30 20:01 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-05-30 20:01 UTC'
---

**Stable Updates Published**

* [Coturn](https://github.com/coturn/coturn): [4.6.2-r5](https://github.com/coturn/coturn/releases/tag/4.6.2-r5) -> [4.6.2-r9](https://github.com/coturn/coturn/releases/tag/4.6.2-r9)
* [Synapse](https://github.com/element-hq/synapse): [v1.107.0](https://github.com/element-hq/synapse/releases/tag/v1.107.0) -> [v1.108.0](https://github.com/element-hq/synapse/releases/tag/v1.108.0)

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$6snOIgYhZc-hcieIsCSEf08BYvIdanuvnT5nA74s0N4?via=etke.cc&via=matrix.org)
