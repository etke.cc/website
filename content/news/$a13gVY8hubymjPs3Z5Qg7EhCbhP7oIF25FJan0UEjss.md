---
id: '$a13gVY8hubymjPs3Z5Qg7EhCbhP7oIF25FJan0UEjss'
date: '2023-05-18 21:14:42.06 +0000 UTC'
title: '2023-05-18 21:14 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-05-18 21:14 UTC'
---

**Stable Updates Published**

- Borgmatic 1.7.12 -> 1.7.13
- Discord bridge 0.3.0 -> 0.4.0
- Prometheus 2.43.0 -> 2.44.0
- Signal bridge 0.4.2 -> 0.4.3
- WhatsApp bridge 0.8.4 -> 0.8.5

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$a13gVY8hubymjPs3Z5Qg7EhCbhP7oIF25FJan0UEjss?via=etke.cc&via=matrix.org)
