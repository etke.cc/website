---
id: '$Whgtut1OW6OCmV__sgh-XU773Q2LbIay_coGpuLLtiU'
date: '2022-11-24 22:32:54.242 +0000 UTC'
title: '2022-11-24 22:32 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-11-24 22:32 UTC'
---

**stable updates published**

- Element / [app.etke.cc](https://app.etke.cc/) 1.11.14 -> 1.11.15
- Grafana 9.2.5 -> 9.2.6
- Ntfy 1.29.0 -> 1.29.1
- #postmoogle:etke.cc 0.9.8 -> 0.9.10
- Soft-Serve 0.4.1 -> 0.4.4
- Synapse 1.71.0 -> 1.72.0

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

**service updates**

Support email has been changed on [contacts page](https://etke.cc/contacts) - the new email powered by postmoogle

---

**#postmoogle:etke.cc updates**

Postmoogle is email \<-> matrix bridge (actual SMTP server) developed by [etke.cc](https://etke.cc/) and it got the biggest update so far. The most notable changes are:

- automatic ban list (can be enabled by postmoogle admin) - no more nasty spammers will ever bother your host and if they will - Postmoogle will drop TCP connections before even talking with them
- automatic greylisting (can be configured by postmoogle admin)
- internal email queue to re-send failed emails (with 45x errors) automatically
- bridging email threads ⇿ matrix threads/reply-tos (reply to an email in a matrix room, either by matrix thread or matrix reply-to, = email reply on email side, works both ways)
- multi-domain mode with the ability to select a domain for sending emails per room/per mailbox (all other domains will act as aliases)
- `!pm send` and thread replies send multipart emails by default (both HTML from formatted markdown and plaintext in the same email)
- lots of other enhancements and fixes under the hood

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$Whgtut1OW6OCmV__sgh-XU773Q2LbIay_coGpuLLtiU?via=etke.cc&via=matrix.org)
