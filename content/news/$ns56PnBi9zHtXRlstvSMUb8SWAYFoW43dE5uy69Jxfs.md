---
id: '$ns56PnBi9zHtXRlstvSMUb8SWAYFoW43dE5uy69Jxfs'
date: '2021-07-23 16:28:46.218 +0000 UTC'
title: '2021-07-23 16:28 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-07-23 16:28 UTC'
---

**servers updates**

* Synapse 1.38.0 -> 1.38.1

_bugfix update. Fixes message encryption issues for new users, especially on Android_


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$ns56PnBi9zHtXRlstvSMUb8SWAYFoW43dE5uy69Jxfs?via=etke.cc&via=matrix.org)
