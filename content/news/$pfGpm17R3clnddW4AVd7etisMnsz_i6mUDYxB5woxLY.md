---
id: '$pfGpm17R3clnddW4AVd7etisMnsz_i6mUDYxB5woxLY'
date: '2023-03-29 11:31:52.77 +0000 UTC'
title: '2023-03-29 11:31 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-03-29 11:31 UTC'
---

**Servers Update In Progress (Security Fix)**

- Cinny 2.2.4 → 2.2.6

> Cinny security fix was postponed due to build issues, so the usable fixed version was released 15-20mins ago

from: [matrix.org security announcement](https://matrix.org/blog/2023/03/28/security-releases-matrix-js-sdk-24-0-0-and-matrix-react-sdk-3-69-0)

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$pfGpm17R3clnddW4AVd7etisMnsz_i6mUDYxB5woxLY?via=etke.cc&via=matrix.org)
