---
id: '$mxX3MwD2jITRTaSrVGaNXsT5o_6YqUtIw-Z1RVyqDc8'
date: '2022-07-01 09:19:37.389 +0000 UTC'
title: '2022-07-01 09:19 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-07-01 09:19 UTC'
---

**stable updates published**

- Grafana 8.5.3 -> 9.0.2
- Mailer 4.95-r0-2 -> 4.95-r0-4
- Prometheus 2.33.3 -> 2.36.2
- Synapse 1.61.0 -> 1.61.1 _security fix was installed on Tuesday on all customers' servers_

---

**service updates**

- To more proactively respond to service troubles for the servers we manage, we've installed internal monitoring to all subscribers' servers (powered by [prometheus-node-exporter](https://prometheus.io/docs/guides/node-exporter/)). Collected information: CPU, RAM, Disk and systemd service status. Example of collected information is available in the attached file (that's real data from the the etke.cc homeserver, without any redactions). Metrics are collected by our system from authenticated endpoints on each Matrix server (no public access). We retain collected monitoring metrics on our side for 7 days. Some of our customers have already received monitoring system emails about high disk utilization, misconfigured DNS and misconfigured external integrations. We plan to release a customer-facing monitoring tool in the future
- We've added new hosting plans: `Turnkey: huge` (4vCPU, 16GB RAM, 160GB Disk) and `Turnkey: monster` (8vCPU, 32GB RAM, 240GB Disk). Existing Turnkey customers may upgrade to larger Turnkey plans without data loss and with 2-5 minutes of total downtime.

___

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$mxX3MwD2jITRTaSrVGaNXsT5o_6YqUtIw-Z1RVyqDc8?via=etke.cc&via=matrix.org)
