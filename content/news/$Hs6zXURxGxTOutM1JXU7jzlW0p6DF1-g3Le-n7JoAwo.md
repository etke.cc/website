---
id: '$Hs6zXURxGxTOutM1JXU7jzlW0p6DF1-g3Le-n7JoAwo'
date: '2021-09-13 16:11:33.039 +0000 UTC'
title: '2021-09-13 16:11 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-09-13 16:11 UTC'
---

**servers update in progress**

new versions:

* Element 1.8.2 -> 1.8.4 _security fix_
* Grafana 8.1.2 -> 8.1.3
* #miounne:etke.cc 1.0.0 -> 1.0.2

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$Hs6zXURxGxTOutM1JXU7jzlW0p6DF1-g3Le-n7JoAwo?via=etke.cc&via=matrix.org)
