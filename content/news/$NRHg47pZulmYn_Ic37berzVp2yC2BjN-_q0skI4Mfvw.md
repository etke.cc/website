---
id: '$NRHg47pZulmYn_Ic37berzVp2yC2BjN-_q0skI4Mfvw'
date: '2022-09-02 04:56:56.321 +0000 UTC'
title: '2022-09-02 04:56 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-09-02 04:56 UTC'
---

**stable updates published**

- Cinny 2.1.2 -> 2.1.3 _already installed_
- Element 1.11.3 -> 1.11.4 _already installed_
- Grafana 9.1.1 -> 9.1.2
- Hydrogen 0.3.1 -> 0.3.2
- Instagram bridge 0.1.3 -> 0.2.0
- Jitsi stable-7648-3 -> stable-7648-4
- #postmoogle:etke.cc latest -> 0.9.0

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/scheduler#fresh-testing)_

---

**postmoogle updates**

#postmoogle:etke.cc got ACLs feature (you may limit usage to specific users or homeservers), admin users, ability to list and remove mailboxes and manage allowed users directly in matrix chats, without any server side config changes. More display options added as well (show/hide sender, show/hide subject, html or text-only emails, enable/disable email -> matrix threads, enable/disable attachments support).

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$NRHg47pZulmYn_Ic37berzVp2yC2BjN-_q0skI4Mfvw?via=etke.cc&via=matrix.org)
