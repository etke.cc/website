---
id: '$nFdoUEe0ST7ZxaaH6rl3ZQb4rP65Ra6IA-7GC3_aPSs'
date: '2022-04-12 18:09:01.042 +0000 UTC'
title: '2022-04-12 18:09 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-04-12 18:09 UTC'
---

**servers update in progress**

* coturn 4.5.2-r8 -> 4.5.2-r11
* element 1.10.8 -> 1.10.9
* facebook 0.3.3 -> 0.4.0
* #honoroit:etke.cc 0.9.5 -> 0.9.6
* instagram 0.1.2 -> 0.1.3
* jitsi 6865 -> 7001
* uptime kuma 1.13.1 -> 1.14.0

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$nFdoUEe0ST7ZxaaH6rl3ZQb4rP65Ra6IA-7GC3_aPSs?via=etke.cc&via=matrix.org)
