---
id: '$7QSy_xpt1L4VP78fS8v__0WsMnoYRernV1j8DgkzNYA'
date: '2022-07-08 10:36:10.208 +0000 UTC'
title: '2022-07-08 10:36 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-07-08 10:36 UTC'
---

**stable updates published**

- Element 1.10.15 -> 1.11.0
- Jitsi 7001 -> 7439-2
- LanguageTool 5.7 -> 5.8
- Ntfy 1.27.2 _new_
- Synapse 1.61.1 -> 1.62.0


_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/scheduler#fresh-testing)_

---

**service updates**

- [ntfy](https://ntfy.sh/) is an open source self-hosted UnifiedPush provider that may be used instead of Google FCM/GCM on Android and works with SchildiChat and FluffyChat (Element support coming soon in next Element releases). It's available for order on the [etke.cc](https://etke.cc/)
- etke.cc's ssh keys have been rotated. The new keys are available on website. That's automated process, you don't need to do anything.

---

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$7QSy_xpt1L4VP78fS8v__0WsMnoYRernV1j8DgkzNYA?via=etke.cc&via=matrix.org)
