---
id: '$NsjRp5dwDiV9sTBmIFzaQv_tcp9E8ettrmCfx8UsR6s'
date: '2021-06-04 19:43:10.055 +0000 UTC'
title: '2021-06-04 19:43 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-06-04 19:43 UTC'
---

This Week in Matrix 2021-06-04 https://matrix.org/blog/2021/06/04/this-week-in-matrix-2021-06-04


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$NsjRp5dwDiV9sTBmIFzaQv_tcp9E8ettrmCfx8UsR6s?via=etke.cc&via=matrix.org)
