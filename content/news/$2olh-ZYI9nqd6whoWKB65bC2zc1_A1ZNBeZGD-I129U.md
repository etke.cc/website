---
id: '$2olh-ZYI9nqd6whoWKB65bC2zc1_A1ZNBeZGD-I129U'
date: '2021-08-25 15:50:47.993 +0000 UTC'
title: '2021-08-25 15:50 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-08-25 15:50 UTC'
---

**servers updates**
- IRC 0.29.0 -> 0.30.0
- Linkedin 0.5.0 _new_
- Element 1.8.0 -> 1.8.1
- Corporal 2.1.1 -> 2.1.2
- Coturn 4.5.2-r2 -> 4.5.2-r3
- Mailer 4.94.2-r0-2 -> 4.94.2-r0-3
- Telegram 0.9.0 -> 0.10.1
- Prometheus (node exporter) 1.2.0 -> 1.2.2
- Prometheus (postgres exporter) 0.9.0 -> 0.10.0
- Prometheus 2.28.1 -> 2.29.1
- Sygnal 0.9.0 -> 0.10.1
- Synapse 1.40.0 -> 1.41.0

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$2olh-ZYI9nqd6whoWKB65bC2zc1_A1ZNBeZGD-I129U?via=etke.cc&via=matrix.org)
