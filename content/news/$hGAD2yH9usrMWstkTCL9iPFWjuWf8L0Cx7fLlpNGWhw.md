---
id: '$hGAD2yH9usrMWstkTCL9iPFWjuWf8L0Cx7fLlpNGWhw'
date: '2022-05-06 09:58:36.101 +0000 UTC'
title: '2022-05-06 09:58 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-05-06 09:58 UTC'
---

etke.cc customers are **not** affected


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$hGAD2yH9usrMWstkTCL9iPFWjuWf8L0Cx7fLlpNGWhw?via=etke.cc&via=matrix.org)
