---
id: '$039s1IvET7IWEnd-LehdlqKmJ8ufMNrwxfWdmc_10EY'
date: '2024-04-25 20:03:53.596 +0000 UTC'
title: '2024-04-25 20:03 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-04-25 20:03 UTC'
---

**Stable Updates Published**

- [Element](https://github.com/element-hq/element-web): [v1.11.64](https://github.com/element-hq/element-web/releases/tag/v1.11.64) -> [v1.11.65](https://github.com/element-hq/element-web/releases/tag/v1.11.65)
- [Jitsi](https://github.com/jitsi/docker-jitsi-meet): [stable-9364-1](https://github.com/jitsi/docker-jitsi-meet/releases/tag/stable-9364-1) -> [stable-9457-1](https://github.com/jitsi/docker-jitsi-meet/releases/tag/stable-9457-1)
- [Prometheus Node Exporter](https://github.com/prometheus/node_exporter): [v1.7.0](https://github.com/prometheus/node_exporter/releases/tag/v1.7.0) -> [v1.8.0](https://github.com/prometheus/node_exporter/releases/tag/v1.8.0)
- [Synapse](https://github.com/element-hq/synapse): [v1.105.0](https://github.com/element-hq/synapse/releases/tag/v1.105.0) -> [v1.105.1](https://github.com/element-hq/synapse/releases/tag/v1.105.1)
- [Synapse Admin](https://github.com/Awesome-Technologies/synapse-admin): [0.9.2](https://github.com/Awesome-Technologies/synapse-admin/releases/tag/0.9.2) -> [0.10.1](https://github.com/Awesome-Technologies/synapse-admin/releases/tag/0.10.1)
- [Uptime Kuma](https://github.com/louislam/uptime-kuma): [1.23.12](https://github.com/louislam/uptime-kuma/releases/tag/1.23.12) -> [1.23.13](https://github.com/louislam/uptime-kuma/releases/tag/1.23.13)

**Service Updates: [Monitoring](https://etke.cc/services/monitoring/) Update**

The monitoring report now includes an additional check - [Metrics](https://etke.cc/services/monitoring/#metrics-failures). This check will only be visible when the [VPS Metrics](https://etke.cc/services/monitoring/#vps-metrics) cannot be read. Such a failure indicates a critical issue with the server, typically severe overload, to the extent that even metrics cannot be checked.

**Service Updates: RAM utilization adjustments**

Due to a significant number of cases with high RAM utilization, we've implemented adjustments to decrease the usage, the most considerable changes are: decreased the max allowed memory for database, and more aggressive swapping. Those changes lead to a decrease in RAM utilization from 10% to 30% of total RAM. Unfortunately, they come with the cost of slightly decreased performance of the Matrix stack. If you'd like to have "performance-first" configuration instead, please, [contact us](https://etke.cc/contacts/)

**Service Updates: [Order Form](https://etke.cc/order/) Update**

With the implementation of the [new monitoring system](https://etke.cc/services/monitoring/), an increasing number of customers are becoming aware of overloads on their servers. In most cases, these are the Small-sized servers with additional components installed. Unfortunately, the notes regarding the underpowered nature of these servers on [the order form](https://etke.cc/order/) were insufficient. Consequently, many servers were overloaded from the start, leading to confusion among their owners when they received alerts regarding high CPU/RAM utilization. We apologize for any confusion caused and are taking steps to prevent such situations in the future.

Regrettably, for existing customers, the only recourse is to inform them of the situation, which the new monitoring service accomplishes effectively.

However, for new customers, we are implementing changes. Henceforth, the order form will automatically disable any resource-intensive components for the Small server size, accompanied by a clear explanation of why and what happened. It's important to note that these disabled components are unusable on Small server sizes anyway, and their inclusion often results in unresponsive servers rather quickly. Additionally, certain components (e.g., Synapse Workers) will be disabled for Medium-sized servers as well due to the same reason.

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$039s1IvET7IWEnd-LehdlqKmJ8ufMNrwxfWdmc_10EY?via=etke.cc&via=matrix.org)
