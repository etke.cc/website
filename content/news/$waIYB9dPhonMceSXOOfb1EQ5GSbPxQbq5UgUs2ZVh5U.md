---
id: '$waIYB9dPhonMceSXOOfb1EQ5GSbPxQbq5UgUs2ZVh5U'
date: '2022-05-31 15:43:00.597 +0000 UTC'
title: '2022-05-31 15:43 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-05-31 15:43 UTC'
---

**reminder**

next scheduled maintenance will happen on June 3rd. As the result [of that poll](https://matrix.to/#/!dKLcIwBGprWVoAskgY:etke.cc/$CFsGdGCpxIml2dieJ3KQ3XT8aszuCXkOHapPubyY9SA?via=etke.cc&via=matrix.org&via=clup.chat), we change the default maintenance day to Friday, starting from the June 3rd


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$waIYB9dPhonMceSXOOfb1EQ5GSbPxQbq5UgUs2ZVh5U?via=etke.cc&via=matrix.org)
