---
id: '$6Nm6HPxy1zhxByuBHgBkZBOG3XCp5kLAqPt5pd63p2s'
date: '2022-11-04 06:49:50.967 +0000 UTC'
title: '2022-11-04 06:49 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-11-04 06:49 UTC'
---

**stable updates published**

- Element / [app.etke.cc](https://app.etke.cc/) 1.11.12 -> 1.11.13
- Grafana 9.2.2 -> 9.2.3
- Instagram bridge latest -> 0.2.2 _pinned release with fixes_
- Signal bridge 0.4.0 -> 0.4.1
- Synapse 1.69.0 -> 1.70.1

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

**service updates**

- @scheduler:etke.cc received some QoL updates, like visual indication of running commands

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$6Nm6HPxy1zhxByuBHgBkZBOG3XCp5kLAqPt5pd63p2s?via=etke.cc&via=matrix.org)
