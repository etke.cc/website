---
id: '$Lr-tIMrkM0yWXguMONFadmeiDCn-A7tbj36J5aICYno'
date: '2022-09-09 06:50:07.615 +0000 UTC'
title: '2022-09-09 06:50 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-09-09 06:50 UTC'
---

**stable updates published**

* Grafana 9.1.2 -> 9.1.3
* Kuma 1.17.1 -> 1.18.0

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/scheduler#fresh-testing)_

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$Lr-tIMrkM0yWXguMONFadmeiDCn-A7tbj36J5aICYno?via=etke.cc&via=matrix.org)
