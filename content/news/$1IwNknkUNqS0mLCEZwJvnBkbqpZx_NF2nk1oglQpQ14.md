---
id: '$1IwNknkUNqS0mLCEZwJvnBkbqpZx_NF2nk1oglQpQ14'
date: '2021-10-12 17:22:35.252 +0000 UTC'
title: '2021-10-12 17:22 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-10-12 17:22 UTC'
---

**servers update in progress**

_no versioned updates yet, consider today maintenance only_

**service updates**
- added http://etke.cc/bridges with auth instructions for each bridge

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$1IwNknkUNqS0mLCEZwJvnBkbqpZx_NF2nk1oglQpQ14?via=etke.cc&via=matrix.org)
