---
id: '$dzE9QL0gH4pCCTCBwiDeglf5ng7R3YO4CaknHa2UWcI'
date: '2024-03-28 21:03:48.158 +0000 UTC'
title: '2024-03-28 21:03 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-03-28 21:03 UTC'
---

**Stable Updates Published**

- Element / [app.etke.cc](https://app.etke.cc/) 1.11.61 -> 1.11.63
- Grafana 10.4.0 -> 10.4.1
- Jitsi 9364 -> 9364-1
- KeyDB 6.3.4 _new_
- Ntfy 2.9.0 -> 2.10.0
- Radicale 3.1.9.0 -> 3.1.9.1
- Redis 7.2.3 -> 7.2.4

**Service Updates: [LanguageTool](https://etke.cc/help/extras/languagetool/) is back**

Previously, we've offered [LanguageTool](https://languagetool.org/), but it was taken down for refactoring. It returns for the price of +$1/month, with optional [extended list of supported n-grams](https://etke.cc/help/extras/languagetool/#n-grams) and [a quick start guide](https://etke.cc/help/extras/languagetool/).

**Service Updates: [Redis](https://redis.io/) has been replaced with [KeyDB](https://keydb.dev/) due to upcoming license changes**

Starting from v7.4.x, Redis is no longer Open Source, and switch to source-available model, it was [announced in their blog](https://redis.com/blog/redis-adopts-dual-source-available-licensing/). As a result, we've decided to replace Redis with KeyDB - an Open Source drop-in replacement with additional features (like multi-threading). That change affects all customers with Synapse Workers, but you don't need to do anything - it will be fully automatic. The only possible impact is longer downtime during the maintenance (only once, to apply the migration)

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$dzE9QL0gH4pCCTCBwiDeglf5ng7R3YO4CaknHa2UWcI?via=etke.cc&via=matrix.org)
