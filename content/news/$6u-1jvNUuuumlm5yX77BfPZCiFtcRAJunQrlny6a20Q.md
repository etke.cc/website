---
id: '$6u-1jvNUuuumlm5yX77BfPZCiFtcRAJunQrlny6a20Q'
date: '2021-11-23 13:02:25.128 +0000 UTC'
title: '2021-11-23 13:02 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-11-23 13:02 UTC'
---

**servers update in progress**

new versions:
- Element 1.9.4 -> 1.9.5
- Exim 4.94.2-r0-4 -> 4.94.2-r0-5
- IRC (heisenbridge) 1.6.0 -> 1.7.0
- Synapse 1.46.0 -> 1.47.1
- Matrix Corporal 2.1.4 -> 2.2.1

**configuration changes**

all servers:
- log level set to `warning` for all actual bridges to prevent sensitive messages leaks in logs and decrease disk utilization

servers with #miounne:etke.cc  and matrix-registration:
- notification about the used token has been enabled. Keep in mind that it's an experimental feature (fully covered by tests, but still), so in case of any problems post them to the #miounne:etke.cc  room and/or directly to the https://etke.cc/miounne issues

___

only for  _NEW_ customers:
- identity server not installed by default
- exim mailer not installed by default
- synapse (matrix homeserver) will be used to send emails in case of SMTP-relay

Both identity server (ma1sd) and mailer (exim) are battle-tested and work well, but nobody needs them. In 99% of cases, you want email verification and notifications, that's it. Of course, for specific cases, like phone number verification we will install identity server and related services and of course, we continue to support them on existing installations. Just keep in mind that the latest identity server release was on April 16, and it's not ready for Matrix 1.1 spec release. So we will not set up ma1sd and exim (used by ma1sd only in most cases) by default. We will continue to support them and install them on request.

_These changes are NOT applied to existing customers and will be used for new homeservers only. But if you want to apply them on your server (eg: to free some compute power) - send me a PM._

___

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$6u-1jvNUuuumlm5yX77BfPZCiFtcRAJunQrlny6a20Q?via=etke.cc&via=matrix.org)
