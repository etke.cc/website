---
id: '$3eLQkVR1hZdnySjddRRKEvxNH1aXbgJhL-H6zBSq1T4'
date: '2021-06-18 18:51:24.732 +0000 UTC'
title: '2021-06-18 18:51 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-06-18 18:51 UTC'
---

PS: forgot about that (yep, every time)! This Week in Matrix discussion & preparation room is... in Matrix: #thisweekinmatrix:matrix.org 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$3eLQkVR1hZdnySjddRRKEvxNH1aXbgJhL-H6zBSq1T4?via=etke.cc&via=matrix.org)
