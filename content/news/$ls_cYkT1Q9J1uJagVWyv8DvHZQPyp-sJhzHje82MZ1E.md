---
id: '$ls_cYkT1Q9J1uJagVWyv8DvHZQPyp-sJhzHje82MZ1E'
date: '2022-01-11 19:19:00.971 +0000 UTC'
title: '2022-01-11 19:19 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-01-11 19:19 UTC'
---

**servers update in progress**
- Uptime Kuma 1.11.1 -> 1.11.3
- #honoroit:etke.cc 0.9.0 -> 0.9.1
- Webhooks latest -> 1.0.2-01

due to instability during previous updates, we focused on stability improvements instead of new features for this update.

**service updates**
- Added news section: [etke.cc/news](https://etke.cc/news/) - an automated copy of messages in that room, generated by [emm](https://gitlab.com/etke.cc/emm)
- Subscribers now should use [etke.cc/add](https://etke.cc/add/) if they want to add or change something on homeserver. PMs to a developer is no longer an option
- @support:etke.cc is available in case of any problems
- By customers' requests: we start accepting PayPal payment on [ko-fi.com/etkecc](https://ko-fi.com/etkecc) for customers who prefer to pay with PayPal and/or can't pay with a credit card directly. Consider that as an additional payment method, it doesn't affect existing customers.

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$ls_cYkT1Q9J1uJagVWyv8DvHZQPyp-sJhzHje82MZ1E?via=etke.cc&via=matrix.org)
