---
id: '$jB-ww50b7bz87t1Nf2FOh_BSpFVhTCEcaojiUYJAhIc'
date: '2024-02-29 21:03:31.643 +0000 UTC'
title: '2024-02-29 21:03 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-02-29 21:03 UTC'
---

**Stable Updates Published**

- Element / [app.etke.cc](https://app.etke.cc/) 1.11.58 -> 1.11.59
- Prometheus 2.49.1 -> 2.50.1
- SchildiChat v1.11.30-sc.2 -> v1.11.36-sc.6

**Service Updates: New Facebook and Instagram bridges**

[mautrix-instagram](https://etke.cc/help/bridges/mautrix-instagram) and [mautrix-facebook](https://etke.cc/help/bridges/mautrix-facebook) bridges were superseded by the [mautrix-meta](https://github.com/mautrix/meta/) bridge. Quick Start documentation has been updated both for the [Facebook / Messenger](https://etke.cc/help/bridges/mautrix-meta-messenger) and the [Instagram](https://etke.cc/help/bridges/mautrix-meta-instagram) bridges.

Existing customers are not affected, but if you wish to migrate to the new bridges - just follow the [Facebook](https://etke.cc/help/bridges/mautrix-facebook#migrating-to-the-new-mautrix-meta-bridge) and [Instagram](https://etke.cc/help/bridges/mautrix-instagram#migrating-to-the-new-mautrix-meta-bridge) migration guides

**Service Updates: Registry Updates**

To prevent abuse of our private Docker registry, we've made internal changes to how access is controlled/secured. The new flow has been enabled for customers using the [`fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing). If you encounter any issues with the new flow, please, do not hesitate to [contact us](https://etke.cc/contacts)

**Service Updates: [Demo Server](https://etke.cc/demo/) is Up&Running**

To offer a way to test components before ordering them (both for new & existing customers), we've deployed a demo server that you could use to decide. It has a few limitations, and not all components are available for usage yet, but we hope you will find it useful! Details: [etke.cc/demo](https://etke.cc/demo)

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$jB-ww50b7bz87t1Nf2FOh_BSpFVhTCEcaojiUYJAhIc?via=etke.cc&via=matrix.org)
