---
id: '$lnYoRH4gGeh9XadRob_RTuUqu_Jj-S-ojfVvfZLmXPM'
date: '2023-12-11 08:43:45.885 +0000 UTC'
title: '2023-12-11 08:43 UTC'
author: '@slavi:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-12-11 08:43 UTC'
---

# New order form, pricing model changes, extra services and better servers in the EU and US

The end of 2023 is approaching and we'd like to announce several **large changes that we're launching today**:

1. We have a **brand new [order form](https://etke.cc/order/)**. It's completely redesigned and allows for order processing to go more smoothly, both for customers and for us.
2. We're switching to **a new pricing model**, allowing us to offer additional services (see below) while keeping things fair. Thanks to it, **we can now offer [a basic Matrix server setup](https://etke.cc/help/faq#what-are-the-base-matrix-components-installed-on-the-server) for as little as $5/month** (installed against a server of your own).
3. We're **bringing back support for additional non-Matrix services**. We're **launching 6 new add-on services** (charged extra) and plan on growing the list in the future.
4. For those renting VPS servers from us (Turnkey), we're launching **support for more powerful servers** (backed by [Hetzner Cloud](https://hetzner.cloud/?ref=1yg3ZZmtrg5A)'s AMD-powered `CPX` line) and **support for hosting in the US region**.

Below, we'll go into details for each of these changes and how they affect existing customers.


## Brand new Order Form

Our previous order form was very simple - built with HTML and some JavaScript sprinkled on top for interactivity.

With time, we've added more and more components, each of which could benefit from additional form fields for its configuration, validation logic, etc.
We've noticed that redoing the form completely would bring a lot of benefits, so we've finally gotten around to launching the new form.

For the technically-inclined, we'll briefly mention that [our new order form](https://etke.cc/order/) is powered by [Svelte](https://svelte.dev/). Anything we do is [free-software](https://en.wikipedia.org/wiki/Free_software) and our [website](https://gitlab.com/etke.cc/website) is no exception. Feel free to learn from it, contribute to it and [report issues on Gitlab](https://gitlab.com/etke.cc/website/-/issues) or via our other [contact methods](https://etke.cc/contacts/).

Here are **some of the things we aimed to accomplish** with the order form rewrite:

- **improve the design and User Experience** of the form - hopefully this decreases the number of incorrect orders we receive
- **expose more configuration fields** right in the form - this avoids having to clear up the order requirements with slow back-and-forth communication over email
- **automate more parts of the server setup process**, aiming for 100% automation - less work for us and quicker order processing for you
- expand to **offer additional services besides just Matrix**
- switch to a **new pricing model** and **make the prices of every component easy to grasp**


## Pricing model changes

We're **switching from a flat-fee pricing model to one where you pay-by-complexity**.

Until now, our services have always been offered for a flat fee.
The fee would be different based on the product you chose - Maintenace on your own server (BYOS) was $10/month, while a Turnkey:Small order (Maintenance + a small server rented from us) was $19/month.
These prices remained the same no matter how many components we were installing, maintaining and providing you with support for.

We've always been **trying to establish a fair price**, but the number of services we offered varied. Some people were ordering [a basic Matrix server](https://etke.cc/help/faq#what-are-the-base-matrix-components-installed-on-the-server) - one that doesn't even include any bridges. Others were ordering everything we've got to offer (Matrix, plus all bridges, plus all bots, plus various other services). **Both types of customer were paying exactly the same** (e.g. $10/month). It's clear that **one was potentially overpaying** (for only getting a very basic server), **while the other was paying too little** (for the large amount of services and increased support attention we were affording them).

Starting today, **new orders will be charged according to a new pricing model** that we call **pay-by-complexity**.
People who order few services will be charged little. People who order many services will be charged extra.
We **calculate a customer's monthly subscription price based on the selected services** and the complexity level we associate with providing each service. We believe that this way, everyone can pay fairly.

In a way, we've split out the $10-for-anything-you-wish Maintenance tier into its base components (base Matrix server, bridges, bots, additional services).
This **allows us to offer a new type of service - a [base Matrix server](https://etke.cc/help/faq#what-are-the-base-matrix-components-installed-on-the-server) (just Matrix, no bridges/bots) for as little as $5/month**. This is a **50% price reduction** compared to our previous $10/month Maintenance tier (used against a Bring-Your-Own-Server type of server).

Some of the **services we used provide for free (as part of the Maintenance tier) have now become paid add-ons**.

For Turnkey orders, we've also split the total price into its constituents (server hosting fee + various components). You can now see how much we'll charge you for the [Hetzner Cloud](https://hetzner.cloud/?ref=1yg3ZZmtrg5A) servers we rent on your behalf and can decide whether you'd like to get a server from us or elsewhere.

With the new pay-by-complexity pricing model, we're in a better position to offer additional services besides Matrix. See the **New services** section below for more details.

We're **making Matrix hosting services more affordable** not only by now offering a [base Matrix server](https://etke.cc/help/faq#what-are-the-base-matrix-components-installed-on-the-server) for a cheaper price (as low as $5/month), but also by offering additional services (charged extra) that can co-exist with Matrix on the same server. The server itself, no matter who you're renting it from, is a large component of the total price you're paying each month. **Running more services (besides just Matrix) on the same server brings the server's utility value up** and allows you to justify paying the server price.

**Prices we've set for our services may change in the future**. We've tried to come up with component prices which are fair: **affordable to you and sustainable for us**.
We may have misestimated some and made them too high or too low. We reserve the right to iterate on component prices in the near future and are sorry for the inconvenience it may bring.
Nevertheless, we promise not to make abrupt changes for the worse.

**For existing customers, we're not forcing any pricing changes yet** - at least until the 1st of July 2024 you can continue paying the same price as before.
We explain all the details about this in a dedicated FAQ entry: [When will old subscriptions (Maintenance and Turnkeys) be deprecated?](https://etke.cc/help/faq/#when-will-old-subscriptions-maintenance-and-turnkeys-be-deprecated).

We understand that **fairness is always subjective and forever elusive**. Some people may find themselves having to pay more under our new pricing model.
Until now, they may have been getting 15 different services (e.g.: Matrix, a bunch of Matrix bridges, [Ntfy](https://ntfy.sh/), [Etherpad](https://etherpad.org/), [Jitsi](https://jitsi.org/), etc.) from us for just $10/month. We think this **incredible-generosity model is unsustainable** and need to take action to correct it. Still, we believe our prices continue to be affordable and people wishing to run a minimal setup may find them even better now.


## New services

We've previously been offering various add-on services besides Matrix - the [Miniflux](https://miniflux.app/) RSS reader, the [Uptime Kuma](https://uptime.kuma.pet/) monitoring system, the [Radicale](https://radicale.org/) CalDAV/CardDAV server.
With time, we've had to discontinue these services, because they weren't our main focus and offering them for free (alongside Matrix) was costly and unsustainable.

Over the years, we've had various people ask us to install and maintain other services for them. It's to be expected, because running more services on the same server allows people to **extract more value from the server they're already paying for**. Some technical people were setting up additional services themselves via the [SSH](https://en.wikipedia.org/wiki/Secure_Shell) `root` access we allow. This is risky compared to delegating service hosting to us, and puts you into [self-hosting](https://en.wikipedia.org/wiki/Self-hosting_(web_services)) territory. We think of etke.cc as positioned as a sort of "assisted self-hosting service" - one that brings you the benefits of self-hosting and supporting free-software, but without the high risk and hard work.

Now that we have a new and more fair pricing model (explained in detail above), **we can afford to bring back these services (and others) as paid add-ons**.
Our new free-software [mash-playbook](https://github.com/mother-of-all-self-hosting/mash-playbook) Ansible playbook has been gaining popularity and support for a growing [set of supported services](https://github.com/mother-of-all-self-hosting/mash-playbook/blob/main/docs/supported-services.md). With time, **we'd like to offer most of these as add-ons available to our customers**.

For now, we're starting small, with only a few of these services being available to order. We rely on [your feedback](https://etke.cc/contacts/) to tell us which other services you'll find useful.

**We're launching the following additional services right now**:

- (+$1/mo) [previously-offered and making a comeback] the [Miniflux](https://miniflux.app/) RSS reader
- (+$1/mo) [previously-offered and making a comeback] the [Radicale](https://radicale.org/) CalDAV/CardDAV server
- (+$1/mo) [previously-offered and making a comeback] the [Uptime Kuma](https://uptime.kuma.pet/) monitoring system
- (+$3/mo) [brand new] the [GoToSocial](https://gotosocial.org/) ActivityPub server (a lightweight [Mastodon](https://joinmastodon.org/) alternative)
- (+$1/mo) [brand new] the [Linkding](https://github.com/sissbruecker/linkding) bookmark manager
- (+$2/mo) [brand new] the [Vaultwarden](https://github.com/dani-garcia/vaultwarden) password manager (a lightweight [Bitwarden](https://bitwarden.com/)-compatible server which can be used with the Bitwarden client apps)

Each service requires an additional charge, so we believe that the new model can be sustainable and would allow us to expand the list of services we're offering.

There are no technical difficulties which prevent us from launching support for other services we already have automation for, but we don't wish to spread ourselves too thin and complicate our [customer support](https://etke.cc/services/support) staff too much. Offering a new service (and being around to support it) requires much more than just including it in our installation/upgrade scripts, so **expanding our additional services portfolio will happen gradually**.

New customers can order the new services from our [new order form](https://etke.cc/order/).

Existing customers who wish to make use of these new services can do so by [contacting our support](https://etke.cc/contacts/) and agreeing to switching to our new pay-by-complexity pricing model.


## Hello, America and new AMD-powered servers

We [encourage people to use their own servers](https://etke.cc/news/pxdtbg_vzrjleg-2cddw8xcdea-x8j3gaznd5fbmbwo/) against which we can install components. With these so-called Bring-Your-Own-Server types of orders, you can use any hosting provider anywhere in the world.

However, for people who don't wish to deal with the hassle of renting a server from another service provider, we try to offer affordable VPS servers (previously referred to as Turnkey).Customers who have been ordering our Turnkey servers have been getting [Hetzner Cloud](https://hetzner.cloud/?ref=1yg3ZZmtrg5A) VPS servers that we rent on their behalf.

For a long time already, Hetzner Cloud has supported a new `CPX` line of servers which use powerful [AMD](https://www.amd.com/) CPUs.
These servers are both **cheaper in terms of performance-per-dollar** compared to their existing `CX` server line and also offer **double the disk space**.

Best of all, **these servers are also available not only in Europe, but also in the US**. In the future, Hetzner Cloud may also offer availability in other regions on other continents.

Because of the flat-fee Turnkey tiers we've been offering (e.g. $19/mo for Turnkey:small), it was difficult for us to introduce support for the new `CPX` server line and for the US region, because server prices in the `CPX` line were somewhat different (higher).
With our new pricing model, we can assign a fair resale price to each server type and calculate your final monthly subscription fee accordingly.

This means, **we're now launching support for these new server instances** and also for **servers hosted in the US** (Ashburn, Virginia and Hillsboro, Oregon).
New orders will automatically use the new `CPX` server line and increased disk space it offers.

**Existing customers' servers remain untouched** on the `CX` line.

As discussed in the [When will old subscriptions (Maintenance and Turnkeys) be deprecated?](https://etke.cc/help/faq/#when-will-old-subscriptions-maintenance-and-turnkeys-be-deprecated) FAQ entry, existing customers can remain on the their existing service (server-wise and component-wise) for the same price they were paying until now.

Should they wish to upgrade their server to the new `CPX` server line, they can do so by [contacting our support](https://etke.cc/contacts/) and agreeing to switching to our new pay-by-complexity pricing model.


## FAQ

### Are your services becoming more expensive now?

For customers who are used to getting everything we have to offer for a flat fee of (e.g. $10), yes - getting the same kind of service will be more expensive. Our previous pricing model proved to be unsustainable and we need to switch away from it.

For customers who wish to pay little, we now offer a [base Matrix server](https://etke.cc/help/faq#what-are-the-base-matrix-components-installed-on-the-server) (just Matrix, no bridges/bots), which operates against your own server and is cheaper than what we've had before ($10/month -> $5/month).

For everyone in between these 2 extremes, you get to pay according to the services you order. We believe we've set fair prices, but may adjust them over time.

Because we're on the path of offering additional services (co-existing with Matrix on the same server and bringing up the utility value of the server), we believe our service as a whole may actually become more affordable (or at least more worth it for roughly the same price).

### Do existing customers on the old pricing model need to switch to the new one?

Not yet.

Below is an excerpt from our [When will old subscriptions (Maintenance and Turnkeys) be deprecated?](https://etke.cc/help/faq/#when-will-old-subscriptions-maintenance-and-turnkeys-be-deprecated) FAQ entry which goes into the full details.

**Until 1st of July 2024**, existing customers can remain on their old subscription tier as long as the services we provide to them remain unchanged. That is:

- if customers continue to use the same services, they can remain on the same subscription tier and we won't charge them differently

- if customers request additional services from us, we'll need to ask them to switch to the new *By Complexity* subscription tier, so we can charge them fairly according to our new pricing model

**After 1st of July 2024**, any existing customer who contacts our support will be asked to switch to the new *By Complexity* subscription plan. If customers wish to continue using our services without support, they can remain on the same subscription tier for longer. We have not yet established a final date after which we'll completely abandon the old subscription tiers.

### Can you upgrade my existing Turnkey server to the new CPX server line?

Yes. However, each server instance in new server line is a few dollars more expensive (even if it's cheaper in terms of performance-per-dollar), so we cannot do it while you're on the old pricing model.

If you wish to switch to the `CPX` server line, you can request such changes by [contacting our support](https://etke.cc/contacts/) and agreeing to switching to our new pay-by-complexity pricing model.

If you wish for your new server instance to be in a different region, please refer to the next FAQ entry below.

### Can you move my existing Turnkey server to a different region?

Your existing Turnkey server is likely on the old `CX` server line we've been offering until now. These servers are only available in the EU region.

Only the new AMD-powered `CPX` server line of [Hetzner Cloud](https://hetzner.cloud/?ref=1yg3ZZmtrg5A) is available in the US region.

Automated region-to-region migrations are not supported by our hosting provider, so we'll need to migrate your server manually.
Doing it requires coordination with you as to when you'd like us to do it and for you to experience some short downtime. DNS record changes may be necessary as well.
For us to do the migration, you'll need to make use of our [**Migration (VPS to VPS)** service](https://ko-fi.com/etkecc/shop) (charged as a one-time-fee of $100).

We do not offer `CPX` servers as part of the old pricing model, so **this change requires that you switch to our new pricing model**.

### What happens if I want you to install additional services to my existing server?

Any of the services we offer on our [new order form](https://etke.cc/order/) can be installed to existing servers.

However, adding additional services (be it some of the new add-on services we've launched, or an existing component that you don't have on your server already) requires that we start charging you according to our new pricing model.

You can request service changes by [contacting our support](https://etke.cc/contacts/) and agreeing to switching to our new pay-by-complexity pricing model.

### As an existing customer on the old pricing model, can I still use your support service?

**Until 1st of July 2024**, you can [contact our support](https://etke.cc/contacts/) at any time about issues which are covered by our [Customer Support service](https://etke.cc/services/support). However, if you ask us for new components, we'll need to ask you to switch to our new pay-by-complexity pricing model.

**After 1st of July 2024**, you cannot use our support service unless you switch to our new pay-by-complexity pricing model.

For more details, see the [When will old subscriptions (Maintenance and Turnkeys) be deprecated?](https://etke.cc/help/faq/#when-will-old-subscriptions-maintenance-and-turnkeys-be-deprecated) FAQ entry.

### As an existing customer who will be switching to the new pricing model, how do I figure out my new subscription price?

You can use our [new order form](https://etke.cc/order/) as a calculator - enter some dummy values and select the services you're using (or wish to be using) and see what the **Total** value amounts to.

Alternatively, you can also [contact our support](https://etke.cc/contacts/) and ask. We'd prefer if you calculate using the [new order form](https://etke.cc/order/) by yourself though.

### I'll be switching to the new pricing model. How do I do it?

Given that you're either on one of our old **Maintenance** tiers or on some **Turnkey** tier, you'll first need to [Cancel Your Existing Subscription](https://etke.cc/help/payments/#how-to-cancel-your-subscription).

After this, you can [subscribe to our new By Complexity tier](https://etke.cc/membership/) using a custom price that matches your order.
If you're not sure what your subscription price ought to be (despite our [new order form](https://etke.cc/order/) as a calculator), you can confirm the price with us by [contacting our support](https://etke.cc/contacts/).



[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$lnYoRH4gGeh9XadRob_RTuUqu_Jj-S-ojfVvfZLmXPM?via=etke.cc&via=matrix.org)
