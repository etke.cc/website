---
id: '$FXtHk3d4UbQ-p6wh01IN_aZDvoxB733w3HmAV7Vn0Go'
date: '2021-10-05 17:54:23.288 +0000 UTC'
title: '2021-10-05 17:54 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-10-05 17:54 UTC'
---

**servers update in progress**
- Linkedin 0.5.0 -> 0.5.1
- IRC (heisenbridge) 1.2.0 -> 1.2.1
- Languagetool 5.4 -> 5.5
- Synapse 1.43.0 -> 1.44.0

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$FXtHk3d4UbQ-p6wh01IN_aZDvoxB733w3HmAV7Vn0Go?via=etke.cc&via=matrix.org)
