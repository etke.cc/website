---
id: '$F6iRKor5VzZbAWNyxJqOnuZcLxfrsLJ_9YTEQuEY16w'
date: '2024-04-11 20:05:09.204 +0000 UTC'
title: '2024-04-11 20:05 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-04-11 20:05 UTC'
---

**Stable Updates Published**

- Container Socket Proxy 0.1.1 -> 0.1.2
- Element 1.11.63 -> 1.11.64
- Exim 4.97-r0-0 -> 4.97.1-r0-0
- GoToSocial 0.14.0 -> 0.15.0
- Peertube 6.0.3 -> 6.0.4
- Sygnal 0.14.0 -> 0.14.1

**Service Updates: Email Deliverability**

We continue working on improving email deliverability from your matrix servers. With this update, any new order will have DKIM configured by default, and so new matrix servers will send signed emails by default.

Existing customers may request DKIM integration just by [contacting us](https://etke.cc/contacts/)

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$F6iRKor5VzZbAWNyxJqOnuZcLxfrsLJ_9YTEQuEY16w?via=etke.cc&via=matrix.org)
