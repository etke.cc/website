---
id: '$iSgZzgazVibTtq5b_gP2hLz-I84HbBccNphSfIk_5C4'
date: '2023-07-21 07:16:09.893 +0000 UTC'
title: '2023-07-21 07:16 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-07-21 07:16 UTC'
---

**Stable Updates Published**

- Discord bridge 0.5.0 -> 0.6.0
- Element 1.11.35 -> 1.11.36
- IRC bridge 1.14.2 -> 1.14.3
- Radicale 3.1.8.2 -> 3.1.8.3
- Synapse 1.87.0 -> 1.88.0
- WhatsApp bridge 0.8.6 -> 0.9.0

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$iSgZzgazVibTtq5b_gP2hLz-I84HbBccNphSfIk_5C4?via=etke.cc&via=matrix.org)
