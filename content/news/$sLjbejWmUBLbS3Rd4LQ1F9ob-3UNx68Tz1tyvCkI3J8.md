---
id: '$sLjbejWmUBLbS3Rd4LQ1F9ob-3UNx68Tz1tyvCkI3J8'
date: '2022-12-30 08:05:52.737 +0000 UTC'
title: '2022-12-30 08:05 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-12-30 08:05 UTC'
---

**Stable Updates Published**

- Linkedin bridge 0.5.3 -> 0.5.4
- Uptime Kuma 1.18.5 -> 1.19.2
- Ntfy 1.29.1 -> 1.30.1
- Signal (daemon) 0.23.0 -> 0.23.1

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

**Service Updates**

- [etke.cc](https://etke.cc) services are no longer available in Russian language

---

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$sLjbejWmUBLbS3Rd4LQ1F9ob-3UNx68Tz1tyvCkI3J8?via=etke.cc&via=matrix.org)
