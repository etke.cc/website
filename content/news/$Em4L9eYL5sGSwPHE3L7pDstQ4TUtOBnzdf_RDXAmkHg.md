---
id: '$Em4L9eYL5sGSwPHE3L7pDstQ4TUtOBnzdf_RDXAmkHg'
date: '2021-10-19 18:24:22.626 +0000 UTC'
title: '2021-10-19 18:24 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-10-19 18:24 UTC'
---

**servers updates in progress**

* Element 1.9.0 -> 1.9.2
* Synapse 1.44.0 -> 1.45.0

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$Em4L9eYL5sGSwPHE3L7pDstQ4TUtOBnzdf_RDXAmkHg?via=etke.cc&via=matrix.org)
