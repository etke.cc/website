---
id: '$VAvSMVpwun9qqlMphCRpZbH1kzUZ8vyIJ0Cpjzv7DDQ'
date: '2022-06-17 11:14:50.162 +0000 UTC'
title: '2022-06-17 11:14 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-06-17 11:14 UTC'
---

**servers update in progress**

- Element 1.10.14 -> 1.10.15
- Soft-Serve 0.3.1 -> 0.3.2
- Synapse 1.60.0 -> 1.61.0
- WhatsApp _latest_/0.4.0 -> 0.5.0
- Skype bridge _latest_ _new_

---

**service updates**

- The new Skype bridge will be available to order after the maintenance ends
- We're configuring new maintenance process and slowly applying it to existing customers' servers. More information will be shared in upcoming updates. First 10 servers will be assigned random maintenance day and time after the maintenance ends, server owners will be invited to the special feedback room

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$VAvSMVpwun9qqlMphCRpZbH1kzUZ8vyIJ0Cpjzv7DDQ?via=etke.cc&via=matrix.org)
