---
id: '$wDbyWToJqGUm6LqStxCLpGLV1vmtB_B-TmPBQw8a0CU'
date: '2024-01-18 20:58:25.921 +0000 UTC'
title: '2024-01-18 20:58 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-01-18 20:58 UTC'
---

**Stable Updates Published**

- Discord bridge 0.6.4 -> 0.6.5
- Element / [app.etke.cc](https://app.etke.cc/) 1.11.53 -> 1.11.54
- Google Messages bridge 0.2.3 -> 0.2.4
- Reminder bot 0.2.1 -> 0.3.0
- Prometheus 2.48.1 -> 2.49.1
- Signal bridge _updated pinned commit_
- Synapse 1.98.0 -> 1.99.0

**Service Updates: [etke.cc](https://etke.cc/) SSH Keys Have Been Rotated**
New keys published on [etke.cc/keys.txt](https://etke.cc/keys.txt), all customers servers were updated to accept the new keys only. That's an automatic process, you don't need to do anything

**Service Updates: Element's Identity Server is Enabled By Default**

Due to increased support requests regarding [Element Web bug](https://github.com/element-hq/element-web/issues/26172) that stucks on registration with token and email, we've enabled Element's proprietary identity server by default. That does not mean you are forced to use it; however, there is no other option, as Element Web does not follow the protocol specification and requires an identity server to complete registration with invite token + email

**Service Updates: Huge Networking Refactoring**

With that update, we've redone the networking of the matrix stack to remove unnecessary layers and rearrange private networks to minimize latency and increase throughput of the components.
Unfortunately, such significant changes cannot be completely bug free, so there is a chance something may be broken. Thanks to the customers using [fresh stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing) and open source community, we ensured the new networking setup works correctly on numerous different configurations!

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$wDbyWToJqGUm6LqStxCLpGLV1vmtB_B-TmPBQw8a0CU?via=etke.cc&via=matrix.org)
