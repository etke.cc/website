---
id: '$pP_4caaYKmQRz6H69lTLvsDyc_EK_fMlLIRnsK1J8YY'
date: '2023-07-31 16:28:46.918 +0000 UTC'
title: '2023-07-31 16:28 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-07-31 16:28 UTC'
---

**Component Update Has Been Deployed (Security Fix)**

- Hookshot 4.4.0 -> 4.4.1

> Bridges Security Update

[Details](https://matrix.org/blog/2023/07/bridges-security-updates/)

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$pP_4caaYKmQRz6H69lTLvsDyc_EK_fMlLIRnsK1J8YY?via=etke.cc&via=matrix.org)
