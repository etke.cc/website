---
id: '$CrItjqzUnfj0clR3ikwKv49WaG7J3X89P1AnKZFhFoM'
date: '2023-08-03 19:38:48.351 +0000 UTC'
title: '2023-08-03 19:38 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-08-03 19:38 UTC'
---

**Stable Updates Published**

- Element 1.11.36 -> 1.11.37
- IRC bridge 1.14.3 -> 1.14.4
- #honoroit:etke.cc 0.9.17 -> 0.9.18
- Hookshot 4.4.0 -> 4.4.1 _already installed on all servers_
- Synapse 1.88.0 -> 1.89.0

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$CrItjqzUnfj0clR3ikwKv49WaG7J3X89P1AnKZFhFoM?via=etke.cc&via=matrix.org)
