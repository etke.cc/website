---
title: News
draft: false
layout: list
description: "copy of #news:etke.cc room messages"
---

This page contains copy of [#news:etke.cc](https://matrix.to/#/%23news:etke.cc) room messages,
but you may use [RSS](/news/index.xml), or [Fediverse](https://mastodon.matrix.org/@etkecc) to get them.
Additionally, you may check [News Archive](/news/archive) for full list of messages.

{{< partial "attention.html" >}}
