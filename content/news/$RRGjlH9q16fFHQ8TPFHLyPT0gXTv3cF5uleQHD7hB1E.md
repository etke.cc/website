---
id: '$RRGjlH9q16fFHQ8TPFHLyPT0gXTv3cF5uleQHD7hB1E'
date: '2022-08-12 04:57:26.255 +0000 UTC'
title: '2022-08-12 04:57 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-08-12 04:57 UTC'
---

**stable updates published**

- Cinny 2.0.4 -> 2.1.2
- Coturn 4.5.2-r13 -> 4.5.2-r14
- Email2Matrix 1.0.3 -> 1.1.0
- Grafana 9.0.6 -> 9.0.7
- Jitsi stable-7439-2 -> stable-7577-2
- SoftServe 0.3.3 -> 0.4.0

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/scheduler#fresh-testing)_

---

**whatsapp bridge update**

Spaces support has been enabled for the whatsapp bridge. Existing users (matrix users with a bridged whatsapp account) should follow the following steps:

- server should be updated (either by `run maintenance` with etke.cc/scheduler or by automatic maintenance)
- (any) matrix user with bridged whatsapp account **who wish to get their rooms put into a space** should send the `!wa sync space` command to the whatsapp bridge bot (the `!wa` prefix may be omitted)

Manual migration should be performed only for existing matrix users who have bridged their whatsapp accounts (so that their existing rooms would be grouped into a Matrix space). Any users who bridge their whatsapp account in the future (after this update) will get spaces automatically without having to do anything

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$RRGjlH9q16fFHQ8TPFHLyPT0gXTv3cF5uleQHD7hB1E?via=etke.cc&via=matrix.org)
