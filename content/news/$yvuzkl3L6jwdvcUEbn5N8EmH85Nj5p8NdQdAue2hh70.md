---
id: '$yvuzkl3L6jwdvcUEbn5N8EmH85Nj5p8NdQdAue2hh70'
date: '2022-10-07 05:16:55.775 +0000 UTC'
title: '2022-10-07 05:16 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-10-07 05:16 UTC'
---

**stable updates published**

- Grafana 9.1.6 -> 9.1.7
- Uptime Kuma 1.8.0 -> 1.8.3
- Prometheus 2.38.0 -> 2.39.0
- Signal (daemon) 0.21.1 -> 0.22.2

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/scheduler#fresh-testing)_

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$yvuzkl3L6jwdvcUEbn5N8EmH85Nj5p8NdQdAue2hh70?via=etke.cc&via=matrix.org)
