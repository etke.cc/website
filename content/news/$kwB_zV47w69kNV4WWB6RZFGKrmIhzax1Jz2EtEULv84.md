---
id: '$kwB_zV47w69kNV4WWB6RZFGKrmIhzax1Jz2EtEULv84'
date: '2024-05-09 20:00:40.47 +0000 UTC'
title: '2024-05-09 20:00 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-05-09 20:00 UTC'
---

**Stable Updates Published**

* [Element](https://github.com/element-hq/element-web): [v1.11.65](https://github.com/element-hq/element-web/releases/tag/v1.11.65) -> [v1.11.66](https://github.com/element-hq/element-web/releases/tag/v1.11.66)
* [Etherpad](https://github.com/ether/etherpad-lite): [2.0.1](https://github.com/ether/etherpad-lite/releases/tag/2.0.1) -> [2.0.3](https://github.com/ether/etherpad-lite/releases/tag/2.0.3)
* [Peertube](https://github.com/Chocobozzz/PeerTube): [v6.0.4](https://github.com/Chocobozzz/PeerTube/releases/tag/v6.0.4) -> [v6.1.0](https://github.com/Chocobozzz/PeerTube/releases/tag/v6.1.0)

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$kwB_zV47w69kNV4WWB6RZFGKrmIhzax1Jz2EtEULv84?via=etke.cc&via=matrix.org)
