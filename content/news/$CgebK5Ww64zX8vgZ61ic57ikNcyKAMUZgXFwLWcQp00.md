---
id: '$CgebK5Ww64zX8vgZ61ic57ikNcyKAMUZgXFwLWcQp00'
date: '2022-08-05 10:54:55.402 +0000 UTC'
title: '2022-08-05 10:54 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-08-05 10:54 UTC'
---

**stable updates published**

- Element 1.11.1 -> 1.11.2
- Grafana 9.0.5 -> 9.0.6
- #honoroit:etke.cc 0.9.10 -> 0.9.12
- Hydrogen 0.2.33 -> 0.3.1
- Soft-Serve 0.3.2 -> 0.3.3
- Synapse 1.63.1 -> 1.64.0

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/scheduler#fresh-testing)_

---

**service updates**

- [etke.cc/contacts](https://etke.cc/contacts) page added with multiple (new) contact methods

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$CgebK5Ww64zX8vgZ61ic57ikNcyKAMUZgXFwLWcQp00?via=etke.cc&via=matrix.org)
