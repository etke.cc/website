---
id: '$0ip0Hlpm83hUL-gumN4FQmWEiUxPrSVTHPyDLC5BpjQ'
date: '2021-09-07 19:33:36.013 +0000 UTC'
title: '2021-09-07 19:33 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-09-07 19:33 UTC'
---

**servers update in progress**

new versions:
- Element 1.8.1 -> 1.8.2
- IRC (heisenbridge) 1.0.0 -> 1.0.1
- Synapse 1.41.1 -> 1.42.0
- dnsmasq _new_
- #miounne:etke.cc 1.0.0

WireGuard
- added dnsmasq integration

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$0ip0Hlpm83hUL-gumN4FQmWEiUxPrSVTHPyDLC5BpjQ?via=etke.cc&via=matrix.org)
