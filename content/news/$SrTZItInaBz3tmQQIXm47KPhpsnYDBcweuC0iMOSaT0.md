---
id: '$SrTZItInaBz3tmQQIXm47KPhpsnYDBcweuC0iMOSaT0'
date: '2022-12-09 07:31:01.892 +0000 UTC'
title: '2022-12-09 07:31 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-12-09 07:31 UTC'
---

**stable updates published**

- Coturn 4.6.0 -> 4.6.1
- Element / [app.etke.cc](https://app.etke.cc/) 1.11.15 -> 1.11.16
- Hookshot 2.4.0 -> 2.5.0
- #postmoogle:etke.cc 0.9.9 -> 0.9.10
- Signal bridge 0.4.1 -> 0.4.2
- Synapse 1.72.0 -> 1.73.0

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$SrTZItInaBz3tmQQIXm47KPhpsnYDBcweuC0iMOSaT0?via=etke.cc&via=matrix.org)
