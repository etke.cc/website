---
id: '$XakUz1Cykl_fGW7w0W6E-PM3UYrMCs3P75tKdqM8bVY'
date: '2021-08-17 16:49:58.107 +0000 UTC'
title: '2021-08-17 16:49 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-08-17 16:49 UTC'
---

**servers updates**

* Element 1.7.34 -> 1.8.0
* IRC (heisenbridge) latest -> 1.0.0 _version pinning_
* Facebook bridge latest -> 0.3.1 _version pinning_
* Miniflux 2.0.31 -> 2.0.32

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$XakUz1Cykl_fGW7w0W6E-PM3UYrMCs3P75tKdqM8bVY?via=etke.cc&via=matrix.org)
