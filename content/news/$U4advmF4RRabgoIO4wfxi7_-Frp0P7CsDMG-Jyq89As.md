---
id: '$U4advmF4RRabgoIO4wfxi7_-Frp0P7CsDMG-Jyq89As'
date: '2022-04-14 09:12:52.516 +0000 UTC'
title: '2022-04-14 09:12 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-04-14 09:12 UTC'
---

**servers update in progress** (off cycle)

_due to whatsapp protocol changes, the pinned whatsapp bridge release (v0.3.0) doesn't sync messages for some customers. We're updating all subscribers with whatsapp bridge installed to the latest unpinned version. It may be not stable, but messages sync will work_

- whatsapp 0.3.0 -> _latest_

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$U4advmF4RRabgoIO4wfxi7_-Frp0P7CsDMG-Jyq89As?via=etke.cc&via=matrix.org)
