---
id: '$bWU3qsH_vvhYeQdfVwY3Hw1fQwfcTBXo4plQw1Mxa9M'
date: '2023-09-28 19:41:25.505 +0000 UTC'
title: '2023-09-28 19:41 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-09-28 19:41 UTC'
---

**Stable Updates Published**

- Element / [app.etke.cc](https://app.etke.cc/) 1.11.43 -> 1.11.44
- Etherpad 1.9.2 -> 1.9.3
- Hookshot 4.4.1 -> 4.5.1
- Jitsi 8960 -> 8960-1
- #postmoogle:etke.cc 0.9.14 -> 0.9.15
- Synapse 1.92.3 -> 1.93.0

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$bWU3qsH_vvhYeQdfVwY3Hw1fQwfcTBXo4plQw1Mxa9M?via=etke.cc&via=matrix.org)
