---
id: '$nJ0BZ_L2yfT8aqXIsBLvIze3MattSdDWXNSfh6Q73_s'
date: '2021-11-09 17:11:57.143 +0000 UTC'
title: '2021-11-09 17:11 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-11-09 17:11 UTC'
---

**servers update in progress**

* Element 1.9.3 -> 1.9.4
* IRC (heisenbridge) 1.4.1 -> 1.5.0

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$nJ0BZ_L2yfT8aqXIsBLvIze3MattSdDWXNSfh6Q73_s?via=etke.cc&via=matrix.org)
