---
id: '$76O7T6y5FHgOba_HGUHwuj38VpiU_lH6-2rFxrx9boE'
date: '2022-10-27 21:06:39.083 +0000 UTC'
title: '2022-10-27 21:06 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-10-27 21:06 UTC'
---

**stable updates published**

- #buscarron:etke.cc 1.2.1 -> 1.3.0
- Element 1.11.10 -> 1.11.12
- Grafana 9.2.1 -> 9.2.2
- #honoroit:etke.cc 0.9.15 -> 0.9.16
- Hookshot 2.3.0 -> 2.4.0
- Nginx 1.23.1 -> 1.23.2
- #postmoogle:etke.cc 0.9.7 -> 0.9.8
- Signal daemon 0.22.2 -> 0.23.0

Note on Synapse 1.70 update - it's **not** included within `fresh` and `stable` updates due to serious regressions in that specific release.

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

**service updates**

- [app.etke.cc](https://app.etke.cc/) is the Element web hosted on blazing fast [Bunny CDN](https://bunny.net/?ref=z3pduwxhge), it's intended for customers without element installed on their servers and for people who want to try new features
- we're deploying the same docker image as on [app.etke.cc](https://app.etke.cc/) for all our customers

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$76O7T6y5FHgOba_HGUHwuj38VpiU_lH6-2rFxrx9boE?via=etke.cc&via=matrix.org)
