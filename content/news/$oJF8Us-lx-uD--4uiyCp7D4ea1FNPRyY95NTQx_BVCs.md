---
id: '$oJF8Us-lx-uD--4uiyCp7D4ea1FNPRyY95NTQx_BVCs'
date: '2022-10-27 08:32:21.7 +0000 UTC'
title: '2022-10-27 08:32 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-10-27 08:32 UTC'
---

**hotfix update published**

* Signal daemon 0.22.2 -> 0.23.0

Due to changes on signal's server side ([details](https://gitlab.com/signald/signald/-/issues/343)), signal bride doesn't work anymore.
Affected customers: everyone who use signal bridge

Please, use [etke.cc/scheduler](https://etke.cc/scheduler) to apply hotfix (available both for `fresh` and `stable` branches), otherwise you may wait for the full stable update tomorrow.

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$oJF8Us-lx-uD--4uiyCp7D4ea1FNPRyY95NTQx_BVCs?via=etke.cc&via=matrix.org)
