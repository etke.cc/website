---
id: '$fVW6zoArxganbAapiOhxrmOwEkv_FV4qRi8IulvxJBg'
date: '2021-05-17 17:25:58.469 +0000 UTC'
title: '2021-05-17 17:25 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-05-17 17:25 UTC'
---

The Matrix Space Beta! https://matrix.org/blog/2021/05/17/the-matrix-space-beta


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$fVW6zoArxganbAapiOhxrmOwEkv_FV4qRi8IulvxJBg?via=etke.cc&via=matrix.org)
