---
id: '$N4Zgq1cUrjFnM2REYQ1IChcZINgkQFqvgxNg1SZDxt8'
date: '2024-01-25 21:08:27.198 +0000 UTC'
title: '2024-01-25 21:08 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-01-25 21:08 UTC'
---

**Stable Updates Published**

- Element [app.etke.cc](https://app.etke.cc/) 1.11.54 -> 1.11.55
- Grafana 10.2.3 -> 10.3.1
- Sliding Sync 0.99.14 -> 0.99.15

**Service Updates: Server Emails are sent through Exim from now on**

Previously, when you ordered the [SMTP Relay](https://etke.cc/help/extras/smtp-relay/) component we configured it for Synapse only because it was the only real email sender on matrix servers, but as we've extended the components we offer, some of them can send emails as well. We've reconfigured SMTP integration on all customers servers to use Exim for sending emails, and integrated it with all email-capable components by default

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$N4Zgq1cUrjFnM2REYQ1IChcZINgkQFqvgxNg1SZDxt8?via=etke.cc&via=matrix.org)
