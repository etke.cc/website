---
id: '$JxWnASyzm3LX-L8umZBe2rXenov3XJdS9UUujwWMmYk'
date: '2022-02-15 16:52:39.016 +0000 UTC'
title: '2022-02-15 16:52 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-02-15 16:52 UTC'
---

**servers update in progress**
- Element 1.10.1 -> 1.10.3
- Hydrogen 0.2.25 -> 0.2.26
- Uptime Kuma 1.11.3 -> 1.11.14
- Radicale 3.1.4.0 -> 3.1.5.0

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$JxWnASyzm3LX-L8umZBe2rXenov3XJdS9UUujwWMmYk?via=etke.cc&via=matrix.org)
