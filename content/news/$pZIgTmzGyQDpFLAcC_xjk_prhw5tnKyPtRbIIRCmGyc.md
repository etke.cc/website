---
id: '$pZIgTmzGyQDpFLAcC_xjk_prhw5tnKyPtRbIIRCmGyc'
date: '2022-12-02 07:24:01.464 +0000 UTC'
title: '2022-12-02 07:24 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-12-02 07:24 UTC'
---

**stable updates published**

- Grafana 9.2.6 -> 9.3.1
- Hydrogen 0.3.4 -> 0.3.5
- Prometheus node exporter 1.4.0 -> 1.5.0
- Prometheus 2.40.2 -> 2.40.5
- Telegram bridge 0.12.1 -> 0.12.2

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$pZIgTmzGyQDpFLAcC_xjk_prhw5tnKyPtRbIIRCmGyc?via=etke.cc&via=matrix.org)
