---
id: '$7DS4_ovwd2fgSZGVWBVANJzU__vIZeJqelHoS4I93Rc'
date: '2023-10-12 20:29:13.598 +0000 UTC'
title: '2023-10-12 20:29 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-10-12 20:29 UTC'
---

**Stable Updates Published**

- Coturn 4.6.2-r4 -> 4.6.2-r5
- Element / [app.etke.cc](https://app.etke.cc/) 1.11.45 -> 1.11.46
- Sliding Sync 0.9.10 -> 0.9.11
- Synapse 1.93.0 -> 1.94.0
- Traefik 2.10.4 -> 2.10.5
- Uptime Kuma 1.23.2 -> 1.23.3

---

**Service Updates: [etke.cc](https://etke.cc/)'s Ssh Keys Have Been Rotated**

New keys are published on [etke.cc/ssh.key](https://etke.cc/ssh.key) and already installed on all customers' servers. That's an automatic process, you don't need to do anything

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$7DS4_ovwd2fgSZGVWBVANJzU__vIZeJqelHoS4I93Rc?via=etke.cc&via=matrix.org)
