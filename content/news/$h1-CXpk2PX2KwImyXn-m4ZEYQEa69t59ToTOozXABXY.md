---
id: '$h1-CXpk2PX2KwImyXn-m4ZEYQEa69t59ToTOozXABXY'
date: '2023-03-03 06:15:41.193 +0000 UTC'
title: '2023-03-03 06:15 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-03-03 06:15 UTC'
---

**Stable Updates Published**

- Element 1.11.23 -> 1.11.24
- Grafana 9.3.6 -> 9.4.1
- Redis 7.0.7 -> 7.0.9
- Uptime Kuma 1.20.1 -> 1.20.2
- Ntfy 2.0.1 -> 2.1.0
- Synapse 1.77.0 -> 1.78.0
- Synapse auto compressor 0.1.3 _new_
- Telegram bridge 0.12.2 -> 0.13.0

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

**Services Updates**

- [synapse\_auto\_compressor](https://github.com/matrix-org/rust-synapse-compress-state/) has been added. That tool performs internal housekeeping tasks and helps keep your synapse database clean (infamous `state_groups` table) by recalculating the state events in small chunks every day. The tool was enabled for customers on the `fresh` stability branch

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$h1-CXpk2PX2KwImyXn-m4ZEYQEa69t59ToTOozXABXY?via=etke.cc&via=matrix.org)
