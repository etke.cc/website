---
id: '$MmUQMY5fqUMo6LL68DxblW_25x30pX5ekiFHRB2sLUg'
date: '2021-10-21 13:00:01.384 +0000 UTC'
title: '2021-10-21 13:00 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-10-21 13:00 UTC'
---

> <@_neb_rssbot_=40aine=3aetke.cc:matrix.org> matrix.org: Synapse 1.45.1 released ( https://matrix.org/blog/2021/10/20/synapse-1-45-1-released )

Note: that release contains the only revert of MAU calculations. As no one of etke.cc subscribers use that thing, there will be **no** off cycle update today

All subscribers' servers were updated yesterday with 1.45.0 release


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$MmUQMY5fqUMo6LL68DxblW_25x30pX5ekiFHRB2sLUg?via=etke.cc&via=matrix.org)
