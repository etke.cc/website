---
id: '$PxdtBg_vZRjLEg-2CDDW8XCDEA-X8J3gAzNd5fBmBWo'
date: '2023-08-25 08:52:40.254 +0000 UTC'
title: '2023-08-25 08:52 UTC'
author: '@slavi:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-08-25 08:52 UTC'
---

TLDR: [Bring-your-own-server orders](https://etke.cc/order/#byos) on [etke.cc](https://etke.cc/) no longer incur a one-time setup fee ($50) as part of a pricing change experiment, thus making Matrix hosting with us more accessible and encouraging more people to host on their own server. Additional details are below.

At [etke.cc](https://etke.cc/), we're working on a new ordering form, new services and pricing changes to match. We're always trying to cut down costs and to make prices more fair - smaller/simpler setups costing less, while orders with many components (requiring more work to setup and to support) having a higher cost.

At the same time, we're working on automating more of the order-handling flow and of server maintenace, so that we can keep costs low for everyone.

While all these changes are still being worked on (coming soon!), we'd like to announce another change - the experimental removal of the $50 one-time setup fee that we used to charge for [Bring-your-own-server orders](https://etke.cc/order/#byos). With this fee removed, we're aiming to encourage more people to try Matrix on their own server (on-premise or rented from any provider in any localtion), as opposed to renting [Hetzner Cloud](https://hetzner.cloud/?ref=1yg3ZZmtrg5A) servers out through us via our [Turnkey](https://etke.cc/order/#turnkey) offering.

While our [Turnkey](https://etke.cc/order/#turnkey) offering (optionally combined with hosting on a domain of ours like `YOU.etke.host` or `YOU.onmatrix.chat`) is still the easiest way to get started, we'd like to promote our more-independent and vendor-lock-in-free [Bring-your-own-server](https://etke.cc/order/#byos) offering more.

Bring-your-own-server orders are more difficult/costly for us to process compared to hosting Turnkey servers that we are in complete control of, so removing the one-time setup fee may come back to bite us through an influx of many and difficult to process orders. For now, we've decided to take this risk and see how it goes. If you've been wishing to [host Matrix via etke.cc](https://etke.cc/) but this fee was holding you back, now is your chance!


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$PxdtBg_vZRjLEg-2CDDW8XCDEA-X8J3gAzNd5fBmBWo?via=etke.cc&via=matrix.org)
