---
id: '$1bA1-3M8f8RkvmVFlAsx9j9FhqrqHvYcdh0ALfHOcsM'
date: '2021-08-10 16:11:54.294 +0000 UTC'
title: '2021-08-10 16:11 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-08-10 16:11 UTC'
---

**servers updates**
- IRC (appservice) 0.27.0 -> 0.29.0
- Element 1.7.33 -> 1.7.34
- Hydrogen 0.2.3 -> 0.2.5
- Synapse 1.39.0 -> 1.40.0

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$1bA1-3M8f8RkvmVFlAsx9j9FhqrqHvYcdh0ALfHOcsM?via=etke.cc&via=matrix.org)
