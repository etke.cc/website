---
id: '$U8gXd3vGKhvhc-WwawW9WST9Y-Anblo0r6Cl6-D-Vk0'
date: '2024-05-02 20:54:05.501 +0000 UTC'
title: '2024-05-02 20:54 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-05-02 20:54 UTC'
---

**Stable Updates Published**

* [Honoroit](https://gitlab.com/etke.cc/honoroit): [v0.9.20](https://gitlab.com/etke.cc/honoroit/-/tags/v0.9.20) -> [v0.9.21](https://gitlab.com/etke.cc/honoroit/-/tags/v0.9.21)
* [Jitsi](https://github.com/jitsi/docker-jitsi-meet): [stable-9457-1](https://github.com/jitsi/docker-jitsi-meet/releases/tag/stable-9457-1) -> [stable-9457-2](https://github.com/jitsi/docker-jitsi-meet/releases/tag/stable-9457-2)
* [Languagetool](https://github.com/Erikvl87/docker-languagetool): [6.3](https://github.com/Erikvl87/docker-languagetool/releases/tag/v6.3) -> [6.4](https://github.com/Erikvl87/docker-languagetool/releases/tag/v6.4)
* [Miniflux](https://github.com/miniflux/v2): [2.1.2](https://github.com/miniflux/v2/releases/tag/2.1.2) -> [2.1.3](https://github.com/miniflux/v2/releases/tag/2.1.3)
* [Postmoogle](https://gitlab.com/etke.cc/postmoogle): [v0.9.17](https://gitlab.com/etke.cc/postmoogle/-/tags/v0.9.17) -> [v0.9.18](https://gitlab.com/etke.cc/postmoogle/-/tags/v0.9.18)
* [Sliding Sync](https://github.com/matrix-org/sliding-sync): [v0.99.15](https://github.com/matrix-org/sliding-sync/releases/tag/v0.99.15) -> [v0.99.16](https://github.com/matrix-org/sliding-sync/releases/tag/v0.99.16)
* [Synapse](https://github.com/element-hq/synapse): [v1.105.1](https://github.com/element-hq/synapse/releases/tag/v1.105.1) -> [v1.106.0](https://github.com/element-hq/synapse/releases/tag/v1.106.0)

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$U8gXd3vGKhvhc-WwawW9WST9Y-Anblo0r6Cl6-D-Vk0?via=etke.cc&via=matrix.org)
