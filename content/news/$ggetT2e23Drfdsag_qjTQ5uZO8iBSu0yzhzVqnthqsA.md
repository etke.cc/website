---
id: '$ggetT2e23Drfdsag_qjTQ5uZO8iBSu0yzhzVqnthqsA'
date: '2021-05-17 16:00:08.665 +0000 UTC'
title: '2021-05-17 16:00 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-05-17 16:00 UTC'
---

**servers update**

* Element Web 1.7.27 -> 1.7.28
* Grafana 7.5.5 -> 7.5.6
* LanguageTool latest -> 5.3 _not a real upgrade, just switch to exact version_
* Mailer 4.94.2-r0 -> 4.94.2-r0-1
* Miniflux latest -> 2.0.30 _not a real upgrade, just switch to exact version_
* Prometheus 2.26.0 -> 2.27.0
* Synapse 1.33.2 -> 1.34.0

> **NOTE**: Spaces finally available with that version of Synapse & Element _Web_. On android spaces are available with 1.1.7 pre-relase, on desktop wait for Element Desktop 1.7.28


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$ggetT2e23Drfdsag_qjTQ5uZO8iBSu0yzhzVqnthqsA?via=etke.cc&via=matrix.org)
