---
id: '$Z07QwHg_3rDsp3MSSBbKkq0FV6sq97kUk9dKLuTEDkI'
date: '2022-09-23 06:33:07.35 +0000 UTC'
title: '2022-09-23 06:33 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-09-23 06:33 UTC'
---

**stable updates published**

- Cinny 2.1.3 -> 2.2.0
- Grafana 9.1.5 -> 9.1.6
- #honoroit:etke.cc 0.9.13 -> 0.9.15
- Instagram bridge 0.2.0 -> 0.2.1
- #postmoogle:etke.cc 0.9.2 -> 0.9.3
- Signal bridge 0.3.0 -> 0.4.0
- WhatsApp bridge 0.6.1 -> 0.7.0

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/scheduler#fresh-testing)_

---

**service updates**

- [etke.cc/admin](https://etke.cc/admin) got the [Bulk registration](https://etke.cc/admin#bulk-registration) section ([German](https://etke.cc/de/admin#massen-registrierung), [Russian](https://etke.cc/ru/admin#%D0%BC%D0%B0%D1%81%D1%81%D0%BE%D0%B2%D0%B0%D1%8F-%D1%80%D0%B5%D0%B3%D0%B8%D1%81%D1%82%D1%80%D0%B0%D1%86%D0%B8%D1%8F)) about the users CSV Import feature

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$Z07QwHg_3rDsp3MSSBbKkq0FV6sq97kUk9dKLuTEDkI?via=etke.cc&via=matrix.org)
