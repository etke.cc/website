---
id: '$HjZadWyZptHDIZvKnPtzLeKH9jZ0KdYPQ52n7HmPqZM'
date: '2024-02-15 20:57:39.976 +0000 UTC'
title: '2024-02-15 20:57 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-02-15 20:57 UTC'
---

**Stable Updates Published**

- Element / [app.etke.cc](https://app.etke.cc/) 1.11.57 -> 1.11.58
- Jitsi 9220 -> 9258
- #postmoogle:etke.cc 0.9.16 -> 0.9.17
- Synapse-Admin 0.9.1 -> 0.8.7 _downgrade due to regression issue in 0.9.x versions_
- Synapse 1.100.0 -> 1.101.0
- Traefik 2.10.7 -> 2.11.0

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$HjZadWyZptHDIZvKnPtzLeKH9jZ0KdYPQ52n7HmPqZM?via=etke.cc&via=matrix.org)
