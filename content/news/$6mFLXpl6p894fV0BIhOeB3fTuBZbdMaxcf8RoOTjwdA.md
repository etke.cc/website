---
id: '$6mFLXpl6p894fV0BIhOeB3fTuBZbdMaxcf8RoOTjwdA'
date: '2024-08-01 20:02:33.677 +0000 UTC'
title: '2024-08-01 20:02 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-08-01 20:02 UTC'
---

**Stable Updates Published**

* [Coturn](https://github.com/coturn/coturn): [4.6.2-r10](https://github.com/coturn/coturn/releases/tag/docker%2F4.6.2-r10) -> [4.6.2-r11](https://github.com/coturn/coturn/releases/tag/docker%2F4.6.2-r11)
* [Element](https://github.com/element-hq/element-web): [v1.11.71](https://github.com/element-hq/element-web/releases/tag/v1.11.71) -> [v1.11.72](https://github.com/element-hq/element-web/releases/tag/v1.11.72)
* [Grafana](https://github.com/grafana/grafana): [11.1.1](https://github.com/grafana/grafana/releases/tag/v11.1.1) -> [11.1.3](https://github.com/grafana/grafana/releases/tag/v11.1.3)
* [Honoroit](https://gitlab.com/etke.cc/honoroit): [v0.9.23](https://gitlab.com/etke.cc/honoroit/-/tags/v0.9.23) -> [v0.9.24](https://gitlab.com/etke.cc/honoroit/-/tags/v0.9.24)
* [Peertube](https://github.com/Chocobozzz/PeerTube): [v6.2.0](https://github.com/Chocobozzz/PeerTube/releases/tag/v6.2.0) -> [v6.2.1](https://github.com/Chocobozzz/PeerTube/releases/tag/v6.2.1)
* [Postmoogle](https://gitlab.com/etke.cc/postmoogle): [v0.9.19](https://gitlab.com/etke.cc/postmoogle/-/tags/v0.9.19) -> [v0.9.20](https://gitlab.com/etke.cc/postmoogle/-/tags/v0.9.20)
* [Redis](https://github.com/redis/redis): [7.2.4](https://github.com/redis/redis/releases/tag/7.2.4) -> [7.2.5](https://github.com/redis/redis/releases/tag/7.2.5)
* [Synapse](https://github.com/element-hq/synapse): [v1.111.0](https://github.com/element-hq/synapse/releases/tag/v1.111.0) -> [v1.112.0](https://github.com/element-hq/synapse/releases/tag/v1.112.0)
* [Traefik](https://github.com/traefik/traefik): [v3.1.0](https://github.com/traefik/traefik/releases/tag/v3.1.0) -> [v3.1.1](https://github.com/traefik/traefik/releases/tag/v3.1.1)

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$6mFLXpl6p894fV0BIhOeB3fTuBZbdMaxcf8RoOTjwdA?via=etke.cc&via=matrix.org)
