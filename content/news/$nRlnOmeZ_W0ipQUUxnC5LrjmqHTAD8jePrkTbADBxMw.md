---
id: '$nRlnOmeZ_W0ipQUUxnC5LrjmqHTAD8jePrkTbADBxMw'
date: '2023-09-18 15:19:50.536 +0000 UTC'
title: '2023-09-18 15:19 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-09-18 15:19 UTC'
---

**Servers Update In Progress (Another Security Fix)**

- Synapse 1.92.2 -> 1.92.3

> It turns out that libwebp is bundled statically in Pillow wheels so we need to update this dependency instead of libwebp package at the OS level.

[Details](https://github.com/advisories/GHSA-j7hp-h8jx-5ppr)

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$nRlnOmeZ_W0ipQUUxnC5LrjmqHTAD8jePrkTbADBxMw?via=etke.cc&via=matrix.org)
