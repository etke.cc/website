---
id: '$48KRYwzZnpr2nk2ohHHOA3aYgQKVwsyXKesC2NQBlQQ'
date: '2022-06-10 09:20:10.335 +0000 UTC'
title: '2022-06-10 09:20 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-06-10 09:20 UTC'
---

**servers update in progress**

* Element 1.10.13 -> 1.10.14
* GoogleChat bridge 0.3.2 -> 0.3.3

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$48KRYwzZnpr2nk2ohHHOA3aYgQKVwsyXKesC2NQBlQQ?via=etke.cc&via=matrix.org)
