---
id: '$NZfbQjzJmuMVzLcLmTQj4Eoe4p-vOL1AKltXLEvUCZ0'
date: '2023-09-15 13:30:37.856 +0000 UTC'
title: '2023-09-15 13:30 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-09-15 13:30 UTC'
---

**Servers Update In Progress (Security Fix)**

- Synapse 1.92.1 -> 1.92.2

> a critical vulnerability in `libwebp`

[Details](https://github.com/advisories/GHSA-j7hp-h8jx-5ppr)

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$NZfbQjzJmuMVzLcLmTQj4Eoe4p-vOL1AKltXLEvUCZ0?via=etke.cc&via=matrix.org)
