---
id: '$7l8xLQYxBQl7_z0l8Kp7fCqc_IMTC0bgPNzYLyEffXc'
date: '2023-04-27 20:04:00.691 +0000 UTC'
title: '2023-04-27 20:04 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-04-27 20:04 UTC'
---

**Stable Updates Published**

- Element / [app.etke.cc](https://app.etke.cc/) 1.11.29 -> 1.11.30
- Grafana 9.4.7 -> 9.5.1
- Ntfy 2.3.1 -> 2.4.0
- Synapse 1.81.0 -> 1.82.0

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

**Service Updates:** [MatrixRooms.info](https://matrixrooms.info/)

Lack of rooms discovery for new users is one of the issues we noticed, so today, we welcome you to [MatrixRooms.info](https://matrixrooms.info/), a search engine built for matrix rooms discovery.

The current index contains over 230,000 rooms across 19,000 servers, and we're constantly tweaking it to provide more relevant search results.
Even non-English users may find it useful because it does automatic language detection in room names and topics (during the alpha stage language analysis limited to English, Hindi, Spanish, French, Russian, Portuguese, German)

We plan the following future improvements:

- Searching by language
- Searching by specific field
- Providing a browsable catalog of rooms by server/language/etc
- Implementing a subset of the Matrix Server-Server API to request (and provide!) a Matrix-native rooms directory

[MatrixRooms.info](https://matrixrooms.info/), [Source code](https://gitlab.com/etke.cc/mrs), #mrs:etke.cc

As with anything else we do at [etke.cc](https://etke.cc/), the website, its API backend and all related tools are free software (A/GPL).

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$7l8xLQYxBQl7_z0l8Kp7fCqc_IMTC0bgPNzYLyEffXc?via=etke.cc&via=matrix.org)
