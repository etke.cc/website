---
id: '$6hZaPwpv7DWcYFKbjkMqgzzSYvMse0nEsWfCh_Kklw0'
date: '2024-06-27 20:00:20.452 +0000 UTC'
title: '2024-06-27 20:00 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-06-27 20:00 UTC'
---

**Stable Updates Published**

- [Coturn](https://github.com/coturn/coturn): [4.6.2-r9](https://github.com/coturn/coturn/releases/tag/docker%2F4.6.2-r9) -> [4.6.2-r10](https://github.com/coturn/coturn/releases/tag/docker%2F4.6.2-r10)
- [Gotosocial](https://github.com/superseriousbusiness/gotosocial): [0.15.0](https://github.com/superseriousbusiness/gotosocial/releases/tag/v0.15.0) -> [0.16.0](https://github.com/superseriousbusiness/gotosocial/releases/tag/v0.16.0)
- [Grafana](https://github.com/grafana/grafana): [11.0.0](https://github.com/grafana/grafana/releases/tag/v11.0.0) -> [11.1.0](https://github.com/grafana/grafana/releases/tag/v11.1.0)
- [Hookshot](https://github.com/matrix-org/matrix-hookshot): [5.3.0](https://github.com/matrix-org/matrix-hookshot/releases/tag/5.3.0) -> [5.4.1](https://github.com/matrix-org/matrix-hookshot/releases/tag/5.4.1)
- [Sygnal](https://github.com/matrix-org/sygnal): [v0.14.3](https://github.com/matrix-org/sygnal/releases/tag/v0.14.3) -> [v0.15.0](https://github.com/matrix-org/sygnal/releases/tag/v0.15.0)

**Final Reminder**

_changes will be enforced starting from the **1st July**_

- [In relation to our new pricing model: When will old subscriptions (Maintenance and Turnkeys) be deprecated?](https://etke.cc/help/faq#when-will-old-subscriptions-maintenance-and-turnkeys-be-deprecated)
- [Upcoming removal of long-deprecated components](https://etke.cc/help/faq#what-about-deprecated-components)

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$6hZaPwpv7DWcYFKbjkMqgzzSYvMse0nEsWfCh_Kklw0?via=etke.cc&via=matrix.org)
