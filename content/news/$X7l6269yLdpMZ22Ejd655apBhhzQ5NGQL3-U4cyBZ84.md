---
id: '$X7l6269yLdpMZ22Ejd655apBhhzQ5NGQL3-U4cyBZ84'
date: '2024-08-08 20:00:32.9 +0000 UTC'
title: '2024-08-08 20:00 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-08-08 20:00 UTC'
---

**Stable Updates Published**

- [Cinny](https://github.com/ajbura/cinny): [v4.0.3](https://github.com/ajbura/cinny/releases/tag/v4.0.3) -> [v4.1.0](https://github.com/ajbura/cinny/releases/tag/v4.1.0)
- [Element](https://github.com/element-hq/element-web): [v1.11.72](https://github.com/element-hq/element-web/releases/tag/v1.11.72) -> [v1.11.73](https://github.com/element-hq/element-web/releases/tag/v1.11.73)
- [Exim Relay](https://github.com/devture/exim-relay): [4.98-r0-0](https://github.com/devture/exim-relay/releases/tag/4.98-r0-0) -> [4.98-r0-1](https://github.com/devture/exim-relay/releases/tag/4.98-r0-1)
- [Jitsi](https://github.com/jitsi/docker-jitsi-meet): [stable-9584-1](https://github.com/jitsi/docker-jitsi-meet/releases/tag/stable-9584-1) -> [stable-9646](https://github.com/jitsi/docker-jitsi-meet/releases/tag/stable-9646)
- [Traefik](https://github.com/traefik/traefik): [v3.1.1](https://github.com/traefik/traefik/releases/tag/v3.1.1) -> [v3.1.2](https://github.com/traefik/traefik/releases/tag/v3.1.2)

**Service Update: New Component - [Maubot](https://etke.cc/help/bots/maubot/)**

[Maubot](https://github.com/maubot/maubot) is available for order. Existing customers may request it by [contacting us](https://etke.cc/contacts/)

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$X7l6269yLdpMZ22Ejd655apBhhzQ5NGQL3-U4cyBZ84?via=etke.cc&via=matrix.org)
