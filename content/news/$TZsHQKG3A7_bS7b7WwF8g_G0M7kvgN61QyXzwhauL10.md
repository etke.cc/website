---
id: '$TZsHQKG3A7_bS7b7WwF8g_G0M7kvgN61QyXzwhauL10'
date: '2022-01-18 16:21:20.734 +0000 UTC'
title: '2022-01-18 16:21 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-01-18 16:21 UTC'
---

**servers update in progress**
- IRC (heisenbridge) 1.9.0 -> 1.10.0
- Webhooks 1.0.2-1 -> 1.0.3-1
- Linkedin 0.5.1 -> 0.5.2
- #honoroit:etke.cc 0.9.1 -> 0.9.2
- Element 1.9.8 -> 1.9.9
- Hydrogen 0.2.19 -> 0.2.23
- Jitsi 6726-1 -> 6726-2
- Instagram latest -> 0.1.2
- Signal latest -> 0.2.2
- Signal (daemon) latest -> 0.16.1
- Telegram 0.10.2 -> 0.11.1
- Twitter latest -> 0.1.3
- WhatsApp latest -> 0.2.3
- Prometheus (node exporter) 1.2.2 -> 1.3.1
- Sygnal 0.10.1 -> 0.11.0
- Synapse 1.49.2 -> 1.50.0
- Fixed kernel CVE-2021-4204, CVE-2022-23222 (both exploit unprivileged eBPF access)

**service updates**
- the announcements room was renamed to`etke.cc | news` and got new alias`#news:etke.cc` because it's easier to remember (`#announcements:etke.cc` alias exists and won't be removed)

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$TZsHQKG3A7_bS7b7WwF8g_G0M7kvgN61QyXzwhauL10?via=etke.cc&via=matrix.org)
