---
id: '$gwx5o7BMShzIAT8QKhfgN4pgCEnaGL8LHGCE-cwH77s'
date: '2023-12-21 21:04:45.45 +0000 UTC'
title: '2023-12-21 21:04 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-12-21 21:04 UTC'
---

**Stable Updates Published**

- Element / [app.etke.cc](https://app.etke.cc/) 1.11.51 -> 1.11.52
- Firezone 0.7.35 _new_
- Google Messages bridge 0.2.2 -> 0.2.3
- GoToSocial 0.12.2 -> 0.13.0
- Grafana 10.2.2 -> 10.2.3
- WhatsApp bridge 0.10.4 -> 0.10.5

**Service Updates: Firezone and ChatGPT bot are Available for Order**

A new component is available for ordering - [Firezone](https://firezone.dev/) - it's a VPN server based on [WireGuard](https://wireguard.com/) with Web UI. Existing customers can request it by [contacting us](https://etke.cc/contacts/)

Another available component is [ChatGPT bot](https://etke.cc/help/bots/chatgpt), that became available with the new form, but we've forgotten to mention it previously

**Service Updates: improved Help pages**

We've added details about [Bots](https://etke.cc/help/bots) and [Extra Services](https://etke.cc/help/extras) in the [etke.cc/help](https://etke.cc/help) section, and hope you may find them useful to better understand what exactly each component does, and what features it has

**Service Updates: Fresh Stability Branch Notification Improvements**

If you use [fresh stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing), you can keep an eye on the updates in the #fresh:etke.cc room. Previously, update message was not properly synchronized with the actual availability of the changes because messages were sent before the update was ready to be installed. Starting from this week, the notifications system was redone, so when you see a message about a new update, you can install it immediately.

**Service Updates: Scheduler's Maintenance**

During this week, [etke.cc/scheduler](https://etke.cc/scheduler) was under several maintenance cycles and updates, some of them affected a few customers. We thank you for your patience and apologize for the inconvenience. We will continue updating it for a while, and hope the upcoming updates will affect you only positively 🙂

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$gwx5o7BMShzIAT8QKhfgN4pgCEnaGL8LHGCE-cwH77s?via=etke.cc&via=matrix.org)
