---
id: '$RYj4j8ln4RLk9ethVFudLs5h_mYOGLa959kWnxdtX-A'
date: '2022-06-03 09:28:26.846 +0000 UTC'
title: '2022-06-03 09:28 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-06-03 09:28 UTC'
---

**servers update in progress**

- Cinny 2.0.3 -> 2.0.4
- #honoroit:etke.cc 0.9.7 -> 0.9.9
- Uptime Kuma 1.15.1 -> 1.16.1
- Miniflux 2.0.36 -> 2.0.37
- Signal daemon 0.18.1 -> 0.18.5
- Soft-Serve 0.3.0 -> 0.3.1
- Synapse 1.59.1 -> 1.60.0

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$RYj4j8ln4RLk9ethVFudLs5h_mYOGLa959kWnxdtX-A?via=etke.cc&via=matrix.org)
