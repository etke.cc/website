---
id: '$ZtODYIfSZDcUBnIWhoY8eT1nEdFS1RZHw70pP_n7A0U'
date: '2023-01-12 21:37:11.419 +0000 UTC'
title: '2023-01-12 21:37 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-01-12 21:37 UTC'
---

**Stable Updates Published**

- IRC (heisenbridge) 1.14.0 -> 1.14.1
- Uptime Kuma 1.19.3 -> 1.19.4
- Languagetool 6.0-dockerupdate-1 -> 6.0->dockerupdate-2

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

**Service Updates**

- [The Scheduler](https://etke.cc/scheduler) got new features: an automatic [Alert mode](https://etke.cc/help/extras/scheduler/#alert) that sends alerts into matrix rooms and by email, and the new [disk](https://etke.cc/help/extras/scheduler/#disk) command. The new version was released before that announcement, so you may have already seen it in action and some issues with duplicated alerts and odd summaries. Sorry for the inconvenience, we solved all those problems
- we're rolling out our docker registry to minimize issues with common public registries like GitLab. In the first phase (the current one) we migrated all etke.cc-related docker images into that registry, so during the maintenance they will be downloaded from there, instead of the public registry. During the next phase, we'll configure mirroring of all images used by our customers, so all of them will be downloaded from etke.cc's registry without exceptions

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$ZtODYIfSZDcUBnIWhoY8eT1nEdFS1RZHw70pP_n7A0U?via=etke.cc&via=matrix.org)
