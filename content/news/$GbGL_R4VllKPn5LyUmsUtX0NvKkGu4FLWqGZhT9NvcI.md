---
id: '$GbGL_R4VllKPn5LyUmsUtX0NvKkGu4FLWqGZhT9NvcI'
date: '2024-04-18 20:20:01.001 +0000 UTC'
title: '2024-04-18 20:20 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-04-18 20:20 UTC'
---

**Stable Updates Published**

- #buscarron:etke.cc 1.4.0 -> 1.4.1
- Firezone 0.7.35 -> 0.7.36
- Google Messages bridge 0.3.0 -> 0.4.0
- Hookshot 5.2.1 -> 5.3.0
- Facebook & Instagram bridge 0.2.0 -> 0.3.0
- Signal bridge 0.5.1 -> 0.6.0
- Synapse 1.104.0 -> 1.105.0
- Synapse Admin 0.8.7 -> 0.9.2
- Uptime Kuma 1.23.11 -> 1.23.12
- WhatsApp bridge 0.10.6 -> 0.10.7

**Service Updates: Email Deliverability**

We continue working on improving email deliverability from your matrix servers. With this update, a few bugs were fixed, including an incorrect sender address (it used base domain instead of subdomain). Starting from this update, all outgoing emails will be sent from server@matrix.your-server.com address by default. Sender address change does not affect servers with [Email Service](https://etke.cc/help/extras/email-hosting/), [SMTP Relay](https://etke.cc/help/extras/smtp-relay/), or customized sender

Existing customers may request DKIM integration just by [contacting us](https://etke.cc/contacts/)

**Service Updates: New [Monitoring Service](https://etke.cc/services/monitoring/)**

We continue working on improving stability and awareness of homeserver owners, and starting from this week we've replaced the monitoring service with a new one. The new service provides more detailed and precise alerts, and based on the [ping command](https://etke.cc/help/extras/scheduler/#ping) of the Scheduler. Documentation: [etke.cc/services/monitoring](https://etke.cc/services/monitoring/)

As part of this update, the following changes were made:

- Added system metrics (CPU, RAM, Disk utilization)
- Added details about the problematic items
- Standardized alert time window (from 3am UTC to 4am UTC)
- All alerts are sent within a single email / matrix message

You can run full monitoring check of your server at any moment by using the [ping command](https://etke.cc/help/extras/scheduler/#ping) of the Scheduler.

The new monitoring service is in testing phase, if you have any issues, please [contact us](https://etke.cc/contacts/)

**Service Updates: [etke.cc SSH Keys](https://etke.cc/keys.txt) Have Been Rotated**

New keys have been published on the website (https://etke.cc/keys.txt) and rotated on customers' servers. This is automatic process, you don't need to do anything

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$GbGL_R4VllKPn5LyUmsUtX0NvKkGu4FLWqGZhT9NvcI?via=etke.cc&via=matrix.org)
