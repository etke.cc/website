---
id: '$tB7GmCeSO0f60Cs5KpUVTZmmdLHnvCE8_8O_iApKGSY'
date: '2021-07-14 05:08:21.776 +0000 UTC'
title: '2021-07-14 05:08 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-07-14 05:08 UTC'
---

**servers updates**

* Element 1.7.31 -> 1.7.32
* Corporal 2.1.0 -> 2.1.1
* Grafana 8.0.3 -> 8.0.5
* Nginx 1.21.0 -> 1.21.1
* Prometheus 2.28.0 -> 2.28.1
* Synapse 1.37.1 -> 1.38.0

#discussion:etke.cc to say something about update


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$tB7GmCeSO0f60Cs5KpUVTZmmdLHnvCE8_8O_iApKGSY?via=etke.cc&via=matrix.org)
