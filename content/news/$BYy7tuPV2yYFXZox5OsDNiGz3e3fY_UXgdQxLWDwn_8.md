---
id: '$BYy7tuPV2yYFXZox5OsDNiGz3e3fY_UXgdQxLWDwn_8'
date: '2022-11-11 06:27:21.617 +0000 UTC'
title: '2022-11-11 06:27 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-11-11 06:27 UTC'
---

**stable updates published**

- Element / [app.etke.cc](https://app.etke.cc/) 1.11.13 -> 1.11.14
- Grafana 9.2.3 -> 9.2.4
- Hydrogen 0.3.2 -> 0.3.4
- Prometheus 2.39.1 -> 2.40.1
- Soft-Serve 0.4.0 -> 0.4.1
- Synapse 1.70.1 -> 1.71.0

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

**service updates**

- The Etherpad component got overhaul - dependency on dimension is optional now and etherpad itself got new plugins and features, including microsoft docs import/export support (integration with libreoffice)
- @scheduler:etke.cc got more QoL updates

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$BYy7tuPV2yYFXZox5OsDNiGz3e3fY_UXgdQxLWDwn_8?via=etke.cc&via=matrix.org)
