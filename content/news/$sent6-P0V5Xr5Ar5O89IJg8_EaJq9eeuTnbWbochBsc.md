---
id: '$sent6-P0V5Xr5Ar5O89IJg8_EaJq9eeuTnbWbochBsc'
date: '2022-11-18 06:19:25.094 +0000 UTC'
title: '2022-11-18 06:19 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-11-18 06:19 UTC'
---

**stable updates published**

- Facebook bridge 0.4.0 -> 0.4.1
- Google Chat bridge 0.3.3 -> 0.4.0
- Grafana 9.2.4 -> 9.2.5
- Jitsi stable-7882 -> stable-8044
- Ntfy 1.28.0 -> 1.29.0
- Prometheus 2.40.1 -> 2.40.2
- WhatsApp bridge 0.7.1 -> 0.7.2

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$sent6-P0V5Xr5Ar5O89IJg8_EaJq9eeuTnbWbochBsc?via=etke.cc&via=matrix.org)
