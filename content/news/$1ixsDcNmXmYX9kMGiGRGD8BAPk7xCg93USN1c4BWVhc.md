---
id: '$1ixsDcNmXmYX9kMGiGRGD8BAPk7xCg93USN1c4BWVhc'
date: '2023-02-23 21:17:00.162 +0000 UTC'
title: '2023-02-23 21:17 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-02-23 21:17 UTC'
---

**Stable Updates Published**

- Borgmatic 1.7.6 -> 1.7.7
- Ntfy 1.31.0 -> 2.0.1
- Radicale 3.1.8.0 -> 3.1.8.1
- Soft-Serve 0.4.5 -> 0.4.6

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$1ixsDcNmXmYX9kMGiGRGD8BAPk7xCg93USN1c4BWVhc?via=etke.cc&via=matrix.org)
