---
id: '$Mvkya0Dw0ecUP5m3cnXQyHKSr3TtUAR47_J1ZBZjkyY'
date: '2023-03-16 21:02:16.079 +0000 UTC'
title: '2023-03-16 21:02 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-03-16 21:02 UTC'
---

**Stable Updates Published**

- Element 1.11.24 -> 1.11.25
- Synapse 1.78.0 -> 1.79.0

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$Mvkya0Dw0ecUP5m3cnXQyHKSr3TtUAR47_J1ZBZjkyY?via=etke.cc&via=matrix.org)
