---
id: '$0D8kv-G0lD3oZVp5X9dmxWl3uAbPz4ihkS_G0uXQojw'
date: '2022-08-31 16:34:37.72 +0000 UTC'
title: '2022-08-31 16:34 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-08-31 16:34 UTC'
---

**servers update in progress**

- Element 1.11.3 -> 1.11.4
- Cinny 2.1.2 -> 2.1.3

_off cycle deployment on all customers' servers with the fix of CVE-2022-36059 / CVE-2022-36060_

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$0D8kv-G0lD3oZVp5X9dmxWl3uAbPz4ihkS_G0uXQojw?via=etke.cc&via=matrix.org)
