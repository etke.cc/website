---
id: '$7x0U1N1Gcl5GcgErOD2uFHXRYGivvuWiBgr_nMuGbvE'
date: '2021-07-27 18:10:40.891 +0000 UTC'
title: '2021-07-27 18:10 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-07-27 18:10 UTC'
---

**servers updates**

* Prometheus node exporter 1.1.2 -> 1.2.0

_no synapse release today ☹️ , so consider this as maintenance update_


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$7x0U1N1Gcl5GcgErOD2uFHXRYGivvuWiBgr_nMuGbvE?via=etke.cc&via=matrix.org)
