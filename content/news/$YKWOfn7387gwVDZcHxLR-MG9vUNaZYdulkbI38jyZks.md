---
id: '$YKWOfn7387gwVDZcHxLR-MG9vUNaZYdulkbI38jyZks'
date: '2023-04-03 07:49:15.892 +0000 UTC'
title: '2023-04-03 07:49 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-04-03 07:49 UTC'
---

**Outage is in progress** (update)

- **What**: etke.host domain
- **Impact**: customers using etke.host as their matrix server's domain, etke.cc email communications
- **ETA**: unknown

We got a response from the domain registry operator - they won't do anything, until Google Safe Browsing issue is resolved. Meanwhile, we're updating the website with notes about that outage, and contacting affected customers to give them a clear understanding of the current status.


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$YKWOfn7387gwVDZcHxLR-MG9vUNaZYdulkbI38jyZks?via=etke.cc&via=matrix.org)
