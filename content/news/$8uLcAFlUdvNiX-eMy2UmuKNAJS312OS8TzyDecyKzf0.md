---
id: '$8uLcAFlUdvNiX-eMy2UmuKNAJS312OS8TzyDecyKzf0'
date: '2023-11-09 20:49:31.455 +0000 UTC'
title: '2023-11-09 20:49 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-11-09 20:49 UTC'
---

**Stable Updates Published**

- Sliding Sync 0.99.11 -> 0.99.12
- Vaultwarden 1.29.2 -> 1.30.0

**Service Updates: Concluded [etke.cc](https://etke.cc/) infrastructure maintenance**

During the week, we've updated internal etke.cc services, like [Scheduler](https://etke.cc/help/extras/scheduler/) and docker registry. Downtime was not expected, but it affected several customers. We appreciate your patience and understanding!

As the result of the infrastructure maintenance, the registry storage was migrated from standard on-server filesystem to S3 because we serve 40gb of docker images (just the latest stable versions), and that size grows almost daily

**Service Updates: [MatrixRooms.info](https://matrixrooms.info/) got [MSC3266](https://github.com/matrix-org/matrix-spec-proposals/pull/3266) implementation**

The #mrs:etke.cc project we're building, and its demo instance on [MatrixRooms.info](https://matrixrooms.info/) just got room previews in the form of [MSC3266](https://github.com/matrix-org/matrix-spec-proposals/pull/3266) implementation. With that update, client apps and services like `matrix.to` will show proper room name, topic, avatar, and joined members count when you share a matrix room alias using MRS instance

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$8uLcAFlUdvNiX-eMy2UmuKNAJS312OS8TzyDecyKzf0?via=etke.cc&via=matrix.org)
