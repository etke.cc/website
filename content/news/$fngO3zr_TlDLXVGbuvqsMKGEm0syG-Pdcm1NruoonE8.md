---
id: '$fngO3zr_TlDLXVGbuvqsMKGEm0syG-Pdcm1NruoonE8'
date: '2023-10-19 20:04:35.402 +0000 UTC'
title: '2023-10-19 20:04 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-10-19 20:04 UTC'
---

**Stable Updates Published**

- Borgmatic 1.8.2 -> 1.8.3
- Discord bridge 0.6.2 -> 0.6.3
- Google Messages bridge 0.2.0 -> 0.2.1
- Grafana 10.1.4 -> 10.1.5
- Exim 4.96-r1-0 -> 4.96.2-r0-0
- Miniflux 2.0.48 -> 2.0.49
- Prometheus 2.47.1 -> 2.47.2
- WhatsApp bridge 0.10.2 -> 0.10.3

**Service Updates: Automatic Database Tuning during the Maintenance**

Starting from this update, each maintenance run will perform automatic Postgres tuning, calculated based on customers' server configuration.

**Service Updates: Consultation Service Has Been Retired**

We do not offer Consultations anymore - that service wasn't popular, and it was decided that focus on technical enhancements (like the db tuning above) will provide more benefits to our customers

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$fngO3zr_TlDLXVGbuvqsMKGEm0syG-Pdcm1NruoonE8?via=etke.cc&via=matrix.org)
