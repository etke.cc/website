---
id: '$WAyW6dQf4lYJSqxPBW97cdapdY74Xs9dDlRtASPCm0E'
date: '2023-09-14 20:01:14.849 +0000 UTC'
title: '2023-09-14 20:01 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-09-14 20:01 UTC'
---

**Stable Updates Published**

- Element / [app.etke.cc](https://app.etke.cc/) 1.11.40 -> 1.11.42
- Jitsi stable-8922 -> stable-8922-1
- Synapse 1.91.2 -> 1.92.1

---

**Service Updates: Better Email Alerts**

We've had customer complaints about the poor deliverability of the alert emails, so we switched from a homegrown solution to [Postmark](https://postmarkapp.com/) for sending alert emails, the same way we do for order follow-ups. We hope that this will help avoid issues and reduce downtime

**Service Updates: Enforced Postgres Vacuum during the Maintenance**

A few recent Synapse versions contained various bugs related to excessive disk utilization, including ineffective database usage. To work around the problem, we enforce database vacuuming during the maintenance run by default. Details: [Postgres documentation](https://www.postgresql.org/docs/current/sql-vacuum.html) (lock-free `VACUUM` mode is used)

**Service Updates: Sliding-Sync Enabled by Default**

Element X has become more common nowadays, and one of the special requirements of the app is a backend with sliding sync (sync v3) support. Starting with this update, sliding-sync proxy will be installed by default on all customers servers (including existing customers) automatically

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$WAyW6dQf4lYJSqxPBW97cdapdY74Xs9dDlRtASPCm0E?via=etke.cc&via=matrix.org)
