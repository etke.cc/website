---
id: '$D8ckk3_-fGAMHqbbGJ5PbDYTDWMWBramVwRctxUeVck'
date: '2024-04-04 20:01:25.601 +0000 UTC'
title: '2024-04-04 20:01 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-04-04 20:01 UTC'
---

**Stable Updates Published**

- Borg 1.2.7 -> 1.2.8
- Borgmatic 1.8.6 -> 1.8.9
- Funkwhale 1.4.0 _new_
- Miniflux 2.1.1 -> 2.1.2
- Peertube 6.0.3 _new_
- Synapse 1.103.0 -> 1.104.0

**Service Updates: Email Deliverability**

We continue working on improving email deliverability from your matrix servers. With this update, the outgoing email queue was made persistent, so emails won't be lost due to restarts or temporary downtimes.

**Service Updates: [Extras](https://etke.cc/help/extras) Updates**

We've restructured the [extras](https://etke.cc/help/extras) page, added missing quick start guides, and divided all items into sensible sections

**Service Updates: [List of Deprecated Components](https://etke.cc/help/faq#what-about-deprecated-components) was updated**

The list was extended with [Facebook (mautrix-facebook)](https://etke.cc/help/bridges/mautrix-facebook) and [Instagram (mautrix-instagram)](https://etke.cc/help/bridges/mautrix-instagram) bridges, as they were archived by their devs on March 2024, and superseded by [Facebook (mautrix-meta)](https://etke.cc/help/bridges/mautrix-meta-messenger) and [Instagram (mautrix-meta)](https://etke.cc/help/bridges/mautrix-meta-instagram) bridges.

Quick start guides of the deprecated bridges contain migration guides for your convenience

**Service Updates: New Component - [Funkwhale](https://etke.cc/help/extras/funkwhale)**

A new component is available for order - Funkwhale is a self-hosted audio player and publication platform. It enables users to build libraries of existing content and publish their own. It uses the [ActivityPub protocol](https://www.w3.org/TR/activitypub/) to talk to other apps across the Fediverse. Users can share content between Funkwhale pods or with other Fediverse software.

Funkwhale is available for new and existing customers for +$5/month

**Service Updates: New Component - [Peertube](https://etke.cc/help/extras/peertube)**

A new component is available for order - Peertube is a self-hosted video platform over Fediverse, similar to Funkwhale. And as with Funkwhale it allows building libraries of existing content from Fediverse and publish your own.

Additionally, Peertube supports S3-compatible storage, so you don't need to worry about the disk space utilization

Peertube is available for new and existing customers for +$5/month

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$D8ckk3_-fGAMHqbbGJ5PbDYTDWMWBramVwRctxUeVck?via=etke.cc&via=matrix.org)
