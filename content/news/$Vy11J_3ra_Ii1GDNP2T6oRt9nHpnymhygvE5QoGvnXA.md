---
id: '$Vy11J_3ra_Ii1GDNP2T6oRt9nHpnymhygvE5QoGvnXA'
date: '2023-03-09 21:11:21.762 +0000 UTC'
title: '2023-03-09 21:11 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-03-09 21:11 UTC'
---

**Stable Updates Published**

- Borgmatic 1.7.7 -> 17.8
- Grafana 9.4.1 -> 9.4.3
- #honoroit:etke.cc 0.9.16 -> 0.9.17
- Jitsi stable-8252 -> stable-8319
- Ntfy 2.1.0 -> 2.1.2

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

**Services Updates**

- [synapse\_auto\_compressor](https://github.com/matrix-org/rust-synapse-compress-state/) has been enabled for all customers. That tool performs internal housekeeping tasks and helps keep your synapse database clean (infamous `state_groups` table) by recalculating the state events in small chunks every day.

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$Vy11J_3ra_Ii1GDNP2T6oRt9nHpnymhygvE5QoGvnXA?via=etke.cc&via=matrix.org)
