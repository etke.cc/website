---
id: '$COL9inhBKboKfE80ODy2un2krTjK7F8urvTqgEBEWy4'
date: '2022-05-24 15:23:45.048 +0000 UTC'
title: '2022-05-24 15:23 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-05-24 15:23 UTC'
---

**servers update in progress**

- Coturn 4.5.2-r11 -> 4.5.2-r12
- Element 1.10.12 -> 1.10.13
- Etherpad 1.8.16 -> 1.8.18
- Google chat bridge 0.3.1 -> 0.3.2
- Grafana 8.5.1 -> 8.5.3
- IRC bridge (heisenbridge) 1.12.0 -> 1.13.0
- Hydrogen 0.2.26 -> 0.2.29
- Synapse 1.59.0 -> 1.59.1
- Twitter bridge 0.1.3 -> 0.1.4

---

**service updates**

- turnkey / hosting can be ordered with subdomain yourname.etke.host
- @room  next scheduled maintenance will happen on June 3rd. As the result [of that poll](https://matrix.to/#/!dKLcIwBGprWVoAskgY:etke.cc/$CFsGdGCpxIml2dieJ3KQ3XT8aszuCXkOHapPubyY9SA?via=etke.cc&via=matrix.org&via=clup.chat), we change the default maintenance day to Friday, starting from the June 3rd

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$COL9inhBKboKfE80ODy2un2krTjK7F8urvTqgEBEWy4?via=etke.cc&via=matrix.org)
