---
id: '$IULHqUOFrqbp6cwu-QZV6aL75ekiwwuWBVj3QNryOck'
date: '2023-05-04 20:08:12.077 +0000 UTC'
title: '2023-05-04 20:08 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-05-04 20:08 UTC'
---

**Stable Updates Published**
* Hookshot 3.2.0 -> 4.0.0
* Jitsi stable-8319 -> stable-8615
* Signal (daemon) 0.23.1 -> 0.23.2

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$IULHqUOFrqbp6cwu-QZV6aL75ekiwwuWBVj3QNryOck?via=etke.cc&via=matrix.org)
