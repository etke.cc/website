---
id: '$5VwsjZ-LG0L2fPKlh60vYwcAsGxVmB2K88r0hdRdaC4'
date: '2023-12-07 20:52:53.656 +0000 UTC'
title: '2023-12-07 20:52 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-12-07 20:52 UTC'
---

**Stable Updates Published**

- BorgBackup 1.2.6 -> 1.2.7
- Element Web / [app.etke.cc](https://app.etke.cc/) 1.11.50 -> 1.11.51
- GoToSocial 0.12.1 -> 0.12.2
- Hookshot 4.6.0 -> 4.7.0
- LanguageTool 6.1 -> 6.2
- Redis 7.2.0 -> 7.2.3
- Sliding Sync proxy 0.99.12 -> 0.99.13
- Traefik 2.10.6 -> 2.10.7
- Traefik certs dumper 2.8.1 -> 2.8.3
- Uptime Kuma 1.23.7 -> 1.23.8

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$5VwsjZ-LG0L2fPKlh60vYwcAsGxVmB2K88r0hdRdaC4?via=etke.cc&via=matrix.org)
