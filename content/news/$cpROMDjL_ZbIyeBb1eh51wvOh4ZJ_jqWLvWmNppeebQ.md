---
id: '$cpROMDjL_ZbIyeBb1eh51wvOh4ZJ_jqWLvWmNppeebQ'
date: '2023-08-25 06:14:32.545 +0000 UTC'
title: '2023-08-25 06:14 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-08-25 06:14 UTC'
---

**Stable Updates Published**

- Borgmatic 1.7.15 -> 1.8.2
- Discord bridge 0.6.0 -> 0.6.1
- Etherpad 1.9.0 -> 1.9.2
- Google Messages bridge latest -> 0.1.0 _pin_
- Miniflux 2.0.46 -> 2.0.47
- Ntfy 2.6.2 -> 2.7.0
- Redis 7.0.12 -> 7.2.0
- Uptime Kuma 1.22.1 -> 1.23.0
- WhatsApp bridge 0.9.0 -> 0.10.0

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$cpROMDjL_ZbIyeBb1eh51wvOh4ZJ_jqWLvWmNppeebQ?via=etke.cc&via=matrix.org)
