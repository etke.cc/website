---
id: '$Sw4vdZoXxAXguKeEzfZXn7JjKxhMf1k5hM9LJonI2o4'
date: '2021-05-21 19:04:50.279 +0000 UTC'
title: '2021-05-21 19:04 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-05-21 19:04 UTC'
---

This Week in Matrix 2021-05-21 https://matrix.org/blog/2021/05/21/this-week-in-matrix-2021-05-21


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$Sw4vdZoXxAXguKeEzfZXn7JjKxhMf1k5hM9LJonI2o4?via=etke.cc&via=matrix.org)
