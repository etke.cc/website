---
id: '$zSKSXM9ShxsyZSRs4Z8YTWsqpMEY6IxlLz_rtdW7sB0'
date: '2024-07-18 20:00:49.504 +0000 UTC'
title: '2024-07-18 20:00 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-07-18 20:00 UTC'
---

**Stable Updates Published**

- Borgmatic: 1.8.11 -> 1.8.13
- [Discord](https://github.com/mautrix/discord): [v0.6.5](https://github.com/mautrix/discord/releases/tag/v0.6.5) -> [v0.7.0](https://github.com/mautrix/discord/releases/tag/v0.7.0)
- [Element](https://github.com/element-hq/element-web): [v1.11.70](https://github.com/element-hq/element-web/releases/tag/v1.11.70) -> [v1.11.71](https://github.com/element-hq/element-web/releases/tag/v1.11.71)
- [Exim Relay](https://github.com/devture/exim-relay): [4.97.1-r0-1](https://github.com/devture/exim-relay/releases/tag/4.97.1-r0-1) -> [4.98-r0-0](https://github.com/devture/exim-relay/releases/tag/4.98-r0-0)
- [Gmessages](https://github.com/mautrix/gmessages): [v0.4.2](https://github.com/mautrix/gmessages/releases/tag/v0.4.2) -> [v0.4.3](https://github.com/mautrix/gmessages/releases/tag/v0.4.3)
- [Googlechat](https://github.com/mautrix/googlechat): [v0.5.1](https://github.com/mautrix/googlechat/releases/tag/v0.5.1) -> [v0.5.2](https://github.com/mautrix/googlechat/releases/tag/v0.5.2)
- [Meta Instagram](https://github.com/mautrix/meta): [v0.3.1](https://github.com/mautrix/meta/releases/tag/v0.3.1) -> [v0.3.2](https://github.com/mautrix/meta/releases/tag/v0.3.2)
- [Meta Messenger](https://github.com/mautrix/meta): [v0.3.1](https://github.com/mautrix/meta/releases/tag/v0.3.1) -> [v0.3.2](https://github.com/mautrix/meta/releases/tag/v0.3.2)
- [Peertube](https://github.com/Chocobozzz/PeerTube): [v6.1.0](https://github.com/Chocobozzz/PeerTube/releases/tag/v6.1.0) -> [v6.2.0](https://github.com/Chocobozzz/PeerTube/releases/tag/v6.2.0)
- [Prometheus Node Exporter](https://github.com/prometheus/node_exporter): [v1.8.1](https://github.com/prometheus/node_exporter/releases/tag/v1.8.1) -> [v1.8.2](https://github.com/prometheus/node_exporter/releases/tag/v1.8.2)
- [Signal](https://github.com/mautrix/signal): [v0.6.2](https://github.com/mautrix/signal/releases/tag/v0.6.2) -> [v0.6.3](https://github.com/mautrix/signal/releases/tag/v0.6.3)
- [Synapse](https://github.com/element-hq/synapse): [v1.110.0](https://github.com/element-hq/synapse/releases/tag/v1.110.0) -> [v1.111.0](https://github.com/element-hq/synapse/releases/tag/v1.111.0)
- [Telegram](https://github.com/mautrix/telegram): [v0.15.1](https://github.com/mautrix/telegram/releases/tag/v0.15.1) -> [v0.15.2](https://github.com/mautrix/telegram/releases/tag/v0.15.2)
- [Traefik](https://github.com/traefik/traefik): [v3.0.4](https://github.com/traefik/traefik/releases/tag/v3.0.4) -> [v3.1.0](https://github.com/traefik/traefik/releases/tag/v3.1.0)
- [Twitter](https://github.com/mautrix/twitter): [v0.1.7](https://github.com/mautrix/twitter/releases/tag/v0.1.7) -> [v0.1.8](https://github.com/mautrix/twitter/releases/tag/v0.1.8)
- [Whatsapp](https://github.com/mautrix/whatsapp): [v0.10.8](https://github.com/mautrix/whatsapp/releases/tag/v0.10.8) -> [v0.10.9](https://github.com/mautrix/whatsapp/releases/tag/v0.10.9)

**Service Updates: [etke.cc SSH Keys](https://etke.cc/keys.txt) Have Been Rotated**

New keys have been published on the website (https://etke.cc/keys.txt) and rotated on customers’ servers. This is an automatic process, you don’t need to do anything

**Reminder About Subscriptions Migration**

> [Context](https://etke.cc/help/faq#when-will-old-subscriptions-maintenance-and-turnkeys-be-deprecated). Impacts only customers on the deprecated price model (Maintenance and Turnkey subscriptions). If you got your server after December 2023 or already switched to the new By Complexity model, you are not affected and thus you don't need to do anything.

If you intend to switch from a deprecated subscription plan, you don't need to submit a new order. Just obtain the price for your specific server by using [`run price` of etke.cc/scheduler](https://etke.cc/help/extras/scheduler/#price) and follow the [step-by-step guide](https://etke.cc/help/payments#how-to-become-a-subscriber) to update your payment details

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$zSKSXM9ShxsyZSRs4Z8YTWsqpMEY6IxlLz_rtdW7sB0?via=etke.cc&via=matrix.org)
