---
id: '$o2NPceVUP1-Np8-lOwiPeF789OLd2bz979OcARYeI5I'
date: '2023-01-06 05:38:35.075 +0000 UTC'
title: '2023-01-06 05:38 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-01-06 05:38 UTC'
---

**Stable Updates Published**

- Uptime Kuma 1.19.2 -> 1.19.3
- Languagetool 5.9 -> 6.0

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

**Service Updates**

- [etke.cc](https://etke.cc/) prices were increased for new on-premises orders (existing customers are not affected, it's just a preparation for price model overhaul)

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$o2NPceVUP1-Np8-lOwiPeF789OLd2bz979OcARYeI5I?via=etke.cc&via=matrix.org)
