---
id: '$Ufnib8dPu6YP4NNdxNWPAaPcFugM_ypmUblxbWk7-zs'
date: '2022-02-08 18:27:07.402 +0000 UTC'
title: '2022-02-08 18:27 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-02-08 18:27 UTC'
---

**servers update in progress**
- email2matrix 1.0.1 -> 1.0.3
- grafana 8.3.3 -> 8.3.4
- IRC (heisenbridge) 1.10.0 -> 1.10.1
- nginx 1.21.5 -> 1.21.6
- prometheus 2.31.1 -> 2.33.1
- radicale 3.1.3.0 -> 3.1.4.0
- synapse 1.51.0 -> 1.52.0

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$Ufnib8dPu6YP4NNdxNWPAaPcFugM_ypmUblxbWk7-zs?via=etke.cc&via=matrix.org)
