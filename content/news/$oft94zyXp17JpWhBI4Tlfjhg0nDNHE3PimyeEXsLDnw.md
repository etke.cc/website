---
id: '$oft94zyXp17JpWhBI4Tlfjhg0nDNHE3PimyeEXsLDnw'
date: '2024-03-21 21:02:07.902 +0000 UTC'
title: '2024-03-21 21:02 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-03-21 21:02 UTC'
---

**Stable Updates Published**

- Element / [app.etke.cc](https://app.etke.cc/) 1.11.60 -> 1.11.61
- Etherpad 1.9.6 -> 2.0.1
- Google Messages bridge 0.2.4 -> 0.3.0
- Jitsi 9258 -> 9364
- Facebook & Instagram bridge 0.1.0 -> 0.2.0
- Prometheus 2.50.1 -> 2.51.0
- Radicale 3.1.8.3 -> 3.1.9.0
- Signal bridge 0.5.0 -> 0.5.1
- Sygnal 0.13.0 -> 0.14.0
- Synapse 1.102.0 -> 1.103.0
- WhatsApp bridge 0.10.5 -> 0.10.6

_during this week, there were issues with system packages updates. We apologize for the inconvenience, but can assure you that the issue did not affect Matrix servers, just blocked the update of the system packages for a short time. We've fixed all affected hosts already, and adjusted automation to properly handle such situations. However, if you will notice a failed maintenance, please, try to run it again to apply the fix_

**Service Updates: [About Us](https://etke.cc/about/?utm_source=news&utm_medium=news&utm_campaign=news)**

We've added the [About Us](https://etke.cc/about/?utm_source=news&utm_medium=news&utm_campaign=news) page that contains information about the founders and some stats. It hasn't even been four years 😄

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$oft94zyXp17JpWhBI4Tlfjhg0nDNHE3PimyeEXsLDnw?via=etke.cc&via=matrix.org)
