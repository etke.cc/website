---
id: '$hchfYRHWTip7E7HsrFBuT0GCp2DGC6wYc_z5aHLHiuM'
date: '2021-05-19 14:46:10.706 +0000 UTC'
title: '2021-05-19 14:46 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-05-19 14:46 UTC'
---

**service updates**
- New components available: wireguard, miniflux, languagetool, etc. _check [the order form on website](https://etke.cc/#order) to see a full list_ (if you are a subscriber, just PM me with stuff you want to install)
- New subscriptions available: email services for domain and 2 turnkey solutions (you need just a domain name, hosting and all other stuff is handled by service), [full list available on payments page](https://www.buymeacoffee.com/etkecc)


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$hchfYRHWTip7E7HsrFBuT0GCp2DGC6wYc_z5aHLHiuM?via=etke.cc&via=matrix.org)
