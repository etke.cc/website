---
id: '$kt0wMauXlJhDwl9gRli1qUfG5uUIknpyl2MgTOJYweI'
date: '2023-12-14 20:59:20.104 +0000 UTC'
title: '2023-12-14 20:59 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-12-14 20:59 UTC'
---

**Stable Updates Published**

- Etherpad 1.9.3 -> 1.9.5
- Languagetool 6.2 -> 6.3
- Miniflux 2.0.50 -> 2.0.51
- Prometheus 2.48.0 -> 2.48.1
- Synapse 1.97.0 -> 1.98.0
- Uptime Kuma 1.23.8 -> 1.23.10

**Service Updates: New `By Complexity` pricing model**

It was announced earlier this week. Please, take a look at our news post if you haven't seen it yet: [New order form, pricing model changes, extra services and better servers in the EU and US](https://etke.cc/news/lnyorh4ggeh9xadrob_rtuuqu_jj-s-ojfvvfzlmxpm/)

**Service Updates: New Components**

Apart from the new components that were announced earlier (Miniflux, Radicale, Uptime Kuma, GoToSocial, Linkding, and Vaultwarden) we've added another component: **Synapse S3** storage module that automatically uploads all media from your matrix server to an S3 bucket (supports not only AWS S3, but any S3-compatible provider), keeping disk utilization low

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$kt0wMauXlJhDwl9gRli1qUfG5uUIknpyl2MgTOJYweI?via=etke.cc&via=matrix.org)
