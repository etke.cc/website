---
id: '$simDTXvvekw5hNO4GMqLgtoEq_ffxkETPqWThcfzkMw'
date: '2024-01-11 21:34:01.007 +0000 UTC'
title: '2024-01-11 21:34 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-01-11 21:34 UTC'
---

**Stable Updates Published**

- Borgmatic 1.8.5 -> 1.8.6
- Signal bridge _updated pinned commit_
- Sliding Sync 0.99.13 -> 0.99.14

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$simDTXvvekw5hNO4GMqLgtoEq_ffxkETPqWThcfzkMw?via=etke.cc&via=matrix.org)
