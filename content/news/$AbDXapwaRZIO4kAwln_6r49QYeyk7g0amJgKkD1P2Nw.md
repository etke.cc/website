---
id: '$AbDXapwaRZIO4kAwln_6r49QYeyk7g0amJgKkD1P2Nw'
date: '2021-12-28 19:04:38.325 +0000 UTC'
title: '2021-12-28 19:04 UTC'
author: '@support:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-12-28 19:04 UTC'
---

**servers update in progress**

- Etherpad 1.8.12 -> 1.8.16
- IRC (heisenbridge) 1.7.1 -> 1.8.2
- #honoroit:etke.cc latest
- Twitter latest
- Webhooks switched to [fork](https://github.com/redoonetworks/matrix-appservice-webhooks)

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$AbDXapwaRZIO4kAwln_6r49QYeyk7g0amJgKkD1P2Nw?via=etke.cc&via=matrix.org)
