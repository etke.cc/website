---
id: '$DcuWHZnHfhjgRMs7pA2r7TXfRE0CdKcxwbyd4NY3AeE'
date: '2021-06-24 10:19:49.154 +0000 UTC'
title: '2021-06-24 10:19 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-06-24 10:19 UTC'
---

**question to subscribers**

Synapse by default logs your homeserver users' IPs (you can check them in synapse-admin in user profile) and stores them for 28 days.

If you want to change that (eg for legal reasons), please send me a PM with desired duration


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$DcuWHZnHfhjgRMs7pA2r7TXfRE0CdKcxwbyd4NY3AeE?via=etke.cc&via=matrix.org)
