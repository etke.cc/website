---
id: '$9m0IyWjBC0cZnRzhtA_2ZdWcmGlxHO6LKDeRWCih3Rw'
date: '2022-08-26 07:25:29.291 +0000 UTC'
title: '2022-08-26 07:25 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-08-26 07:25 UTC'
---

**stable updates published**

- Linkedin 0.5.2 -> 0.5.3
- #buscarron:etke.cc  1.2.0 -> 1.2.1
- Grafana 9.1.0 -> 9.1.1
- IRC 1.13.1 -> 1.14.0
- #honoroit:etke.cc  0.9.12 -> 0.9.13
- Jitsi stable-7648-2 -> stable-7648-3
- #postmoogle:etke.cc  _new_
- Prometheus Postgres exporter 0.11.0 -> 0.11.1
- Signal (daemon) 0.21.0 -> 0.21.1
- Twitter 0.1.4 -> 0.1.5

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/scheduler#fresh-testing)_

---

**service updates**

- #postmoogle:etke.cc  is a new component available for order on etke.cc website (existing customers may request it using etke.cc/contacts). Postmoogle is a new Email to Matrix bot that doesn't require any server-side configuration during usage - you can do everything directly in the matrix room using simple commands like `!pm mailbox name`. Current Postmoogle's feature list already exceeds the one for email2matrix, and it will evolve further over time and migrate from "bots" into the "bridges" category. [Roadmap with implemented and planned features](https://gitlab.com/etke.cc/postmoogle#roadmap). Simple manual migration from email2matrix  is also possible

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$9m0IyWjBC0cZnRzhtA_2ZdWcmGlxHO6LKDeRWCih3Rw?via=etke.cc&via=matrix.org)
