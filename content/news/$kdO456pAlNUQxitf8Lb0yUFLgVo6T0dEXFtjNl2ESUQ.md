---
id: '$kdO456pAlNUQxitf8Lb0yUFLgVo6T0dEXFtjNl2ESUQ'
date: '2021-09-28 17:51:43.966 +0000 UTC'
title: '2021-09-28 17:51 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-09-28 17:51 UTC'
---

**servers update in progress**

- Element 1.8.5 -> 1.9.0
- IRC (heisenbridge) 1.1.1 -> 1.2.0
- Miniflux 2.0.32 -> 2.0.33

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$kdO456pAlNUQxitf8Lb0yUFLgVo6T0dEXFtjNl2ESUQ?via=etke.cc&via=matrix.org)
