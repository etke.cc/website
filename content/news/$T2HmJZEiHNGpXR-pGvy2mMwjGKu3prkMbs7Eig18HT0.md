---
id: '$T2HmJZEiHNGpXR-pGvy2mMwjGKu3prkMbs7Eig18HT0'
date: '2024-04-23 16:19:45.415 +0000 UTC'
title: '2024-04-23 16:19 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-04-23 16:19 UTC'
---

**Servers Update In Progress (Security Fix)**

- Synapse 1.105.0 -> 1.105.1

> Weakness in auth chain indexing allows DoS from remote room members through disk fill and high CPU usage.

[Details](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-31208)

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$T2HmJZEiHNGpXR-pGvy2mMwjGKu3prkMbs7Eig18HT0?via=etke.cc&via=matrix.org)
