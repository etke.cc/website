---
id: '$1YzuDIv8zXXgsDxJRdl3eHIHMsaW37Y-H1FWC9-vaIE'
date: '2021-11-18 12:02:29.103 +0000 UTC'
title: '2021-11-18 12:02 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-11-18 12:02 UTC'
---

> <@_neb_rssbot_=40aine=3aetke.cc:matrix.org> matrix.org: Synapse 1.47.0 released ( https://matrix.org/blog/2021/11/17/synapse-1-47-0-released )

Due to late release and planned security fix release on the next Tuesday, **no** off cycle deployment will be done.


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$1YzuDIv8zXXgsDxJRdl3eHIHMsaW37Y-H1FWC9-vaIE?via=etke.cc&via=matrix.org)
