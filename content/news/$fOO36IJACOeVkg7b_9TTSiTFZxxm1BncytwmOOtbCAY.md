---
id: '$fOO36IJACOeVkg7b_9TTSiTFZxxm1BncytwmOOtbCAY'
date: '2021-12-07 18:38:12.026 +0000 UTC'
title: '2021-12-07 18:38 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-12-07 18:38 UTC'
---

@room **service updates**

_sorry for that notification/mention, but the information below is quite important and we want to be sure that every customer will get it as soon as possible. Subscribers will receive a copy of that message through BMC notification as well._

Due to recent changes with payment processing options and discounting of PayPal there were some concerns and questions we had. Now we have answers.

* Will minimal prices and PWYW model change to address new payment processor fees? - **NO**. Prices will stay the same. Of course, fees are higher, but they are still manageable and have a way to somehow optimize the processing cost, so there will be no changes in minimal pricing.
* Should subscribers to do something due to payment processor changes? **YES**. Unfortunately, PayPal stops payments processing on their side, so your existing subscriptions are affected. Please, [follow the BMC guide to change payment method from PayPal to Credit/Debit Card, Apple Pay or Google Pay](https://help.buymeacoffee.com/en/articles/5787433-how-to-change-my-membership-payment-method-from-paypal). Good news - no more throwaway accounts, because you have ability to change your payment settings without re-creating BMC account
* Will subscribers receive updates today even if subscription failed/automatically canceled due to PayPal issues? **YES**. All subscribers' servers are updating right now, no need to worry.

[Updated "discounting PayPal" FAQ on BMC](https://help.buymeacoffee.com/en/articles/5777776-discontinuing-paypal-faq) if you want to get more info.

If you have any questions - please, ask them in #discussion:etke.cc room 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$fOO36IJACOeVkg7b_9TTSiTFZxxm1BncytwmOOtbCAY?via=etke.cc&via=matrix.org)
