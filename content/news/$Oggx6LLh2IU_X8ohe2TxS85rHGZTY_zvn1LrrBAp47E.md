---
id: '$Oggx6LLh2IU_X8ohe2TxS85rHGZTY_zvn1LrrBAp47E'
date: '2022-09-16 06:35:49.949 +0000 UTC'
title: '2022-09-16 06:35 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-09-16 06:35 UTC'
---

**stable updates published**

- Coturn 4.5.2-r14 -> 4.6.0-r0
- Element 1.11.4 -> 1.11.5
- Grafana 9.1.3 -> 9.1.5
- #postmoogle:etke.cc 0.9.0 -> 0.9.2
- Synapse 1.66.0 -> 1.67.0

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/scheduler#fresh-testing)_

---

**service updates**

- etke.cc ist jetzt auch in deutscher Sprache verfügbar, einschließlich der Bestellabwicklung und Supportanfragen. Bitte beachten Sie, dass die Bearbeitung von Anfragen auf Deutsch etwas mehr Zeit in Anspruch nehmen kann
- Due to our increased support burden for providing consultation services (which shortens the time we have for working on the etke.cc service itself), we're increasing the hourly price for consultation services to $50/h for customers (for service-related questions) and $100/h for anybody else (for more technical Matrix-related questions).
- #postmoogle:etke.cc  can now send emails from matrix

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$Oggx6LLh2IU_X8ohe2TxS85rHGZTY_zvn1LrrBAp47E?via=etke.cc&via=matrix.org)
