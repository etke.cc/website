---
id: '$cH7w7D73mfCPrwF3cMpwD81kEauWlJnm9fPBwHJW4H8'
date: '2021-05-28 21:21:11.284 +0000 UTC'
title: '2021-05-28 21:21 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-05-28 21:21 UTC'
---

This Week in Matrix 2021-05-28 https://matrix.org/blog/2021/05/28/this-week-in-matrix-2021-05-28


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$cH7w7D73mfCPrwF3cMpwD81kEauWlJnm9fPBwHJW4H8?via=etke.cc&via=matrix.org)
