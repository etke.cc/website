---
id: '$EdDEPH9qJEThNa8VyqNGlkl4iaDWC7FVr2QUCw-XpyM'
date: '2021-11-16 19:19:03.009 +0000 UTC'
title: '2021-11-16 19:19 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-11-16 19:19 UTC'
---

**servers update in progress**
- Matrix Corporal 2.1.2 -> 2.1.4
- IRC (heisenbridge) 1.5.0 -> 1.6.0
- Facebook bridge 0.3.1 -> 0.3.2
- Nginx 1.21.3 -> 1.21.4
- #miounne:etke.cc 2.1.0 -> 2.2.0

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$EdDEPH9qJEThNa8VyqNGlkl4iaDWC7FVr2QUCw-XpyM?via=etke.cc&via=matrix.org)
