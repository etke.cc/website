---
id: '$ao52DKLlHZFuxHFv9tPxYJfevqkQPkSRkqT9K42DX2A'
date: '2023-02-09 21:07:12.304 +0000 UTC'
title: '2023-02-09 21:07 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-02-09 21:07 UTC'
---

**Stable Updates Published**

- IRC (heisenbridge) 1.14.1 -> 1.14.2
- #postmoogle:etke.cc  0.9.11 -> 0.9.12
- Synapse Admin 0.8.6 -> 0.8.7

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

**Service Update**

- automatic daily sync of `latest` (non-versioned) images added to the [etke.cc](https://etke.cc/)'s registry, providing a way to get the latest changes for the components without pinned versions

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$ao52DKLlHZFuxHFv9tPxYJfevqkQPkSRkqT9K42DX2A?via=etke.cc&via=matrix.org)
