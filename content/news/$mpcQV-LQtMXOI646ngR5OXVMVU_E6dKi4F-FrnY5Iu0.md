---
id: '$mpcQV-LQtMXOI646ngR5OXVMVU_E6dKi4F-FrnY5Iu0'
date: '2022-04-19 21:11:31.74 +0000 UTC'
title: '2022-04-19 21:11 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-04-19 21:11 UTC'
---

> <@aine:etke.cc> **servers update in progress** (off cycle)
> 
> _due to whatsapp protocol changes, the pinned whatsapp bridge release (v0.3.0) doesn't sync messages for some customers. We're updating all subscribers with whatsapp bridge installed to the latest unpinned version. It may be not stable, but messages sync will work_
> 
> - whatsapp 0.3.0 -> _latest_
> 
> #discussion:etke.cc

off cycle deploy again, 0.3.1 has version mismatch with database migrations, so the _latest_ version will be installed


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$mpcQV-LQtMXOI646ngR5OXVMVU_E6dKi4F-FrnY5Iu0?via=etke.cc&via=matrix.org)
