---
id: '$MRKpuQhpyPK6aIBiYarXurVAJ6OgXJSVWXVbB3mgiyM'
date: '2021-12-21 16:24:30.989 +0000 UTC'
title: '2021-12-21 16:24 UTC'
author: '@support:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-12-21 16:24 UTC'
---

**servers update in progress**

- Element 1.9.7 -> 1.9.8
- Coturn 4.5.2-r4 -> 4.5.2-r8
- Grafana 8.3.1 -> 8.3.3
- Jitsi 6726 -> 6726-1
- Miniflux 2.0.33 -> 2.0.34
- Synapse 1.49.0 -> 1.49.1
- Synapse Admin 0.8.1 -> 0.8.4

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$MRKpuQhpyPK6aIBiYarXurVAJ6OgXJSVWXVbB3mgiyM?via=etke.cc&via=matrix.org)
