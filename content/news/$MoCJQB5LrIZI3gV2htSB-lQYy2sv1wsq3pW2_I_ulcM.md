---
id: '$MoCJQB5LrIZI3gV2htSB-lQYy2sv1wsq3pW2_I_ulcM'
date: '2022-04-26 18:02:50.883 +0000 UTC'
title: '2022-04-26 18:02 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-04-26 18:02 UTC'
---

**servers update in progress**

- Element 1.10.10 -> 1.10.11
- IRC (heisenbridge) 1.10.1 -> 1.12.0
- Uptime Kuma 1.14.1 -> 1.15.0
- Radicale 3.1.5.1 -> 3.1.7.0
- Signal 0.2.3 -> 0.3.0
- Signal daemon 0.17.0 -> 0.18.1
- Soft-Serve 0.3.0 _new_
- Synapse 1.57.0 -> 1.57.1

---

**service updates**

- [soft-serve](https://github.com/charmbracelet/soft-serve) ssh-git server will be available for order after the maintenance

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$MoCJQB5LrIZI3gV2htSB-lQYy2sv1wsq3pW2_I_ulcM?via=etke.cc&via=matrix.org)
