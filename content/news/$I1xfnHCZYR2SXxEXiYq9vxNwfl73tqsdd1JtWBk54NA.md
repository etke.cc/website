---
id: '$I1xfnHCZYR2SXxEXiYq9vxNwfl73tqsdd1JtWBk54NA'
date: '2024-03-07 21:24:16.515 +0000 UTC'
title: '2024-03-07 21:24 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-03-07 21:24 UTC'
---

**Stable Updates Published**

- GoToSocial 0.13.3 -> 0.14.0
- Grafana 10.3.1 -> 10.4.0
- Synapse 1.101.0 -> 1.102.0
- Vaultwarden 1.30.3 -> 1.30.5

**Service Updates: Registry Updates**

To prevent abuse of our private Docker registry, we've made internal changes to how access is controlled/secured. The new flow has been enabled for all customers. If you encounter any issues with the new flow, please, do not hesitate to [contact us](https://etke.cc/contacts)

As with anything we do, [the changes are open source under AGPL-3.0](https://gitlab.com/etke.cc/docker-registry-proxy/)

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$I1xfnHCZYR2SXxEXiYq9vxNwfl73tqsdd1JtWBk54NA?via=etke.cc&via=matrix.org)
