---
id: '$4uBzz9TqzaxAMsycLi2FUNx3bXZOQhk4zKLwAvid9cI'
date: '2023-06-02 07:14:27.322 +0000 UTC'
title: '2023-06-02 07:14 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-06-02 07:14 UTC'
---

**Stable Updates Published**

- Facebook bridge 0.4.1 -> 0.5.0
- Instagram bridge 0.2.3 -> 0.3.0
- Prometheus node exporter 1.5.0 -> 1.6.0
- Synapse 1.84.0 -> 1.84.1
- Telegram bridge 0.13.0 -> 0.14.0
- Traefik 2.9.10 -> 2.10.1

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$4uBzz9TqzaxAMsycLi2FUNx3bXZOQhk4zKLwAvid9cI?via=etke.cc&via=matrix.org)
