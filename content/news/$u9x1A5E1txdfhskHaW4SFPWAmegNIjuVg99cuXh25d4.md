---
id: '$u9x1A5E1txdfhskHaW4SFPWAmegNIjuVg99cuXh25d4'
date: '2021-06-29 17:10:32.568 +0000 UTC'
title: '2021-06-29 17:10 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-06-29 17:10 UTC'
---

**servers update**
- IRC 0.26.1 -> 0.27.0
- Hydrogen 0.1.57 -> 0.2.0
- Coturn 4.5.2 -> 4.5.2-r2
- Prometheus 2.27.1 -> 2.28.0
- Synapse 1.36.0 -> 1.37.0


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$u9x1A5E1txdfhskHaW4SFPWAmegNIjuVg99cuXh25d4?via=etke.cc&via=matrix.org)
