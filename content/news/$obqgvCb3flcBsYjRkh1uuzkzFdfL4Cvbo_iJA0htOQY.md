---
id: '$obqgvCb3flcBsYjRkh1uuzkzFdfL4Cvbo_iJA0htOQY'
date: '2023-08-17 22:25:32.304 +0000 UTC'
title: '2023-08-17 22:25 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-08-17 22:25 UTC'
---

**Stable Updates Published**

- Element 1.11.38 -> 1.11.39
- IRC bridge 1.14.4 -> 1.14.5
- Nginx 1.25.1 -> 1.25.2
- Prometheus Node Exporter 1.6.0 -> 1.6.1
- Redis 7.0.10 -> 7.0.12

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$obqgvCb3flcBsYjRkh1uuzkzFdfL4Cvbo_iJA0htOQY?via=etke.cc&via=matrix.org)
