---
id: '$kklroHpfGiIUx3KS5ahU8MtNL9o-Wk85DdR1MKSfR1U'
date: '2022-01-05 16:51:48.29 +0000 UTC'
title: '2022-01-05 16:51 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-01-05 16:51 UTC'
---

**servers update in progress**
- Languagetool 5.5 -> 5.6
- IRC (heisenbridge) 1.8.2 -> 1.9.0
- Nginx 1.21.4 -> 1.21.5
- #honoroit:etke.cc latest
- Uptime Kuma 1.11.1
- Cinny 1.6.1
- Radicale 3.0.0

**service updates**
- etke.cc SSH keys rotating during this update, new publc key available on website (https://etke.cc/ssh.key). That's an automated process, you don't need to do anything
- [uptime-kuma](https://github.com/louislam/uptime-kuma) available to order
- [cinny](https://github.com/ajbura/cinny) available to order
- [radicale](https://radicale.org/) available to order

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$kklroHpfGiIUx3KS5ahU8MtNL9o-Wk85DdR1MKSfR1U?via=etke.cc&via=matrix.org)
