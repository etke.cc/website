---
id: '$U9HCiaHcuzxeZ9q0BEMK-Ps2rTqCDMEQEqYKmdd4b4o'
date: '2022-09-26 11:39:24.055 +0000 UTC'
title: '2022-09-26 11:39 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-09-26 11:39 UTC'
---

Note about the [recent security release of the appservice irc](https://github.com/matrix-org/matrix-appservice-irc/releases/tag/0.35.1)

etke.cc customers are **not** affected


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$U9HCiaHcuzxeZ9q0BEMK-Ps2rTqCDMEQEqYKmdd4b4o?via=etke.cc&via=matrix.org)
