---
id: '$mcnnNUNGhJ2qg1MMJauKor-YJCTfeZAn514ERxcK6g8'
date: '2022-03-08 20:09:35.866 +0000 UTC'
title: '2022-03-08 20:09 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-03-08 20:09 UTC'
---

**servers update in progress**
- Mjolnir 1.3.1 -> 1.3.2
- Cinny 1.7.0 -> 1.8.0
- Element 1.10.5 -> 1.10.6
- Radicale 3.1.5.0 -> 3.1.5.1
- Synapse 1.53.0 -> 1.54.0

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$mcnnNUNGhJ2qg1MMJauKor-YJCTfeZAn514ERxcK6g8?via=etke.cc&via=matrix.org)
