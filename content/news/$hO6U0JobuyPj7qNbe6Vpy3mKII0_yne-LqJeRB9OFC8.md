---
id: '$hO6U0JobuyPj7qNbe6Vpy3mKII0_yne-LqJeRB9OFC8'
date: '2023-07-28 07:20:09.784 +0000 UTC'
title: '2023-07-28 07:20 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-07-28 07:20 UTC'
---

**Stable Updates Published**

- Google Messages bridge latest _new_
- Grafana 10.0.2 -> 10.0.3
- Miniflux 2.0.45 -> 2.0.46
- Prometheus postgres exporter 0.13.1 -> 0.13.2
- Traefik 2.10.3 -> 2.10.4

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

**Service Updates**

- The new Google Messages bridge is available for order for new customers, existing customers can request it using [etke.cc/contacts](https://etke.cc/contacts). Documentation is available on [etke.cc/help/bridges/mautrix-gmessages](https://etke.cc/help/bridges/mautrix-gmessages)

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$hO6U0JobuyPj7qNbe6Vpy3mKII0_yne-LqJeRB9OFC8?via=etke.cc&via=matrix.org)
