---
id: '$22YcwHdVmoJBUReWPvj0Dxwpo3uzuY224DVrVT1lCtc'
date: '2021-05-25 16:09:56.046 +0000 UTC'
title: '2021-05-25 16:09 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-05-25 16:09 UTC'
---

**servers updates**
are postponed - Synapse got only release candidate today, so waiting for stable 1.35 release (I suppose it will happen in few days)


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$22YcwHdVmoJBUReWPvj0Dxwpo3uzuY224DVrVT1lCtc?via=etke.cc&via=matrix.org)
