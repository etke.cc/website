---
id: '$14N8rd3zeeNyBKTEoLyn-VJ9HjcxA1UvLjhhy1SOFIc'
date: '2024-03-14 20:41:05.875 +0000 UTC'
title: '2024-03-14 20:41 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-03-14 20:41 UTC'
---

**Stable Updates Published**

- Element / [app.etke.cc](https://app.etke.cc/) 1.11.59 -> 1.11.60
- Miniflux 2.1.0 -> 2.1.1
- Ntfy 2.8.0 -> 2.9.0

**Service Updates: Support Updates**

Previously, we've announced the [Customer Support](https://etke.cc/services/support) service and described the limits for each support level. Now we continue to enhance it, and will tell the [current request's number](https://etke.cc/services/support#how-requests-are-counted) (counted for the last 30 days) in the greetings message when you [contact us](https://etke.cc/contacts) via Matrix

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$14N8rd3zeeNyBKTEoLyn-VJ9HjcxA1UvLjhhy1SOFIc?via=etke.cc&via=matrix.org)
