---
id: '$BdmKwquTdhMpYyWYZa8Hx8WbuMmsI3EcqnIvwa-prnE'
date: '2022-07-15 11:02:45.941 +0000 UTC'
title: '2022-07-15 11:02 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-07-15 11:02 UTC'
---

**stable updates published**

- Signal (daemon) 0.18.5 -> 0.20.0

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/scheduler#fresh-testing)_

---

**service updates**

- added [etke.cc/admin](https://etke.cc/admin) documentation with common synapse-admin issues and a small guide on how-to remove media

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$BdmKwquTdhMpYyWYZa8Hx8WbuMmsI3EcqnIvwa-prnE?via=etke.cc&via=matrix.org)
