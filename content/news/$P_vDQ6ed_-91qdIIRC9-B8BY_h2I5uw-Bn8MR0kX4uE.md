---
id: '$P_vDQ6ed_-91qdIIRC9-B8BY_h2I5uw-Bn8MR0kX4uE'
date: '2021-05-28 21:23:31.6 +0000 UTC'
title: '2021-05-28 21:23 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-05-28 21:23 UTC'
---

> <@aine:etke.cc> This Week in Matrix 2021-05-28 https://matrix.org/blog/2021/05/28/this-week-in-matrix-2021-05-28

> Lastly, we're looking forward to the release of Synapse 1.35 which will bring significant improvements to memory use during room joins, but that's for next week. 😉

Waiting for it


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$P_vDQ6ed_-91qdIIRC9-B8BY_h2I5uw-Bn8MR0kX4uE?via=etke.cc&via=matrix.org)
