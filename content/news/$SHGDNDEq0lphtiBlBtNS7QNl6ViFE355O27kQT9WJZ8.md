---
id: '$SHGDNDEq0lphtiBlBtNS7QNl6ViFE355O27kQT9WJZ8'
date: '2023-05-26 07:03:42.077 +0000 UTC'
title: '2023-05-26 07:03 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-05-26 07:03 UTC'
---

**Stable Updates Published**

- Ntfy 2.4.0 -> 2.5.0
- Synapse 1.83.0 -> 1.84.0
- Twitter bridge 0.1.5 -> 0.1.6

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

**Service Updates**

- [The Scheduler](https://etke.cc/scheduler) supports multiple admin users from now on. If you want to add more admins for your homeserver, [contact us](https://etke.cc/contacts/)
- [FAQ page](https://etke.cc/help/faq) was updated with new useful sections

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$SHGDNDEq0lphtiBlBtNS7QNl6ViFE355O27kQT9WJZ8?via=etke.cc&via=matrix.org)
