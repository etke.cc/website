---
id: '$9KOBGW3tPGEncJ3bdGdIBDMcvqQhHjnLa1v1aFCNF7U'
date: '2022-09-13 15:09:32.57 +0000 UTC'
title: '2022-09-13 15:09 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-09-13 15:09 UTC'
---

> <@_neb_rssbot_=40aine=3aetke.cc:matrix.org> matrix.org: Security release of matrix-appservice-irc 0.35.0 (High severity) ( https://matrix.org/blog/2022/09/13/security-release-of-matrix-appservice-irc-0-35-0-high-severity )

etke.cc customers are **not** affected


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$9KOBGW3tPGEncJ3bdGdIBDMcvqQhHjnLa1v1aFCNF7U?via=etke.cc&via=matrix.org)
