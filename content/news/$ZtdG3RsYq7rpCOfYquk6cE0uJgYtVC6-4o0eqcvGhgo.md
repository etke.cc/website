---
id: '$ZtdG3RsYq7rpCOfYquk6cE0uJgYtVC6-4o0eqcvGhgo'
date: '2024-05-16 20:00:55.026 +0000 UTC'
title: '2024-05-16 20:00 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-05-16 20:00 UTC'
---

**Stable Updates Published**

* [Grafana](https://github.com/grafana/grafana): [10.4.2](https://github.com/grafana/grafana/releases/tag/v10.4.2) -> [11.0.0](https://github.com/grafana/grafana/releases/tag/v11.0.0)
* [Prometheus](https://github.com/prometheus/prometheus): [v2.51.2](https://github.com/prometheus/prometheus/releases/tag/v2.51.2) -> [v2.52.0](https://github.com/prometheus/prometheus/releases/tag/v2.52.0)
* [Sliding Sync](https://github.com/matrix-org/sliding-sync): [v0.99.16](https://github.com/matrix-org/sliding-sync/releases/tag/v0.99.16) -> [v0.99.17](https://github.com/matrix-org/sliding-sync/releases/tag/v0.99.17)
* [Synapse](https://github.com/element-hq/synapse): [v1.106.0](https://github.com/element-hq/synapse/releases/tag/v1.106.0) -> [v1.107.0](https://github.com/element-hq/synapse/releases/tag/v1.107.0)

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$ZtdG3RsYq7rpCOfYquk6cE0uJgYtVC6-4o0eqcvGhgo?via=etke.cc&via=matrix.org)
