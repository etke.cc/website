---
id: '$iVzyTlO4v1gMcF6As112EwEbFle4pMdYS2h3dBQ2Ncc'
date: '2022-03-24 19:28:44.339 +0000 UTC'
title: '2022-03-24 19:28 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-03-24 19:28 UTC'
---

Note about **1.55.1** bugfix release: etke.cc subscribers are **not** affected.

Same for the 1.55.2


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$iVzyTlO4v1gMcF6As112EwEbFle4pMdYS2h3dBQ2Ncc?via=etke.cc&via=matrix.org)
