---
id: '$MGVrahRTbgP1YJ4XczEu_-4sfi1hTfdk0CKLfCbUSQM'
date: '2022-07-22 08:33:32.327 +0000 UTC'
title: '2022-07-22 08:33 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-07-22 08:33 UTC'
---

**stable updated published**

- Coturn 4.5.2-r12 -> 4.5.2-r13
- Grafana 9.0.2 -> 9.0.4
- IRC 1.13.0 -> 1.13.1
- #honoroit:etke.cc 0.9.9 -> 0.9.10
- Hydrogen 0.2.29 -> 0.2.33
- Nginx 1.21.6 -> 1.23.0
- Prometheus 2.36.2 -> 2.37.0
- Radicale 3.1.7.0 -> 3.1.8.0
- Redis 6.2.6 -> 7.0.4
- Sygnal 0.11.0 -> 0.12.0
- Synapse 1.62.0 -> 1.63.1
- Whatsapp 0.5.0 -> 0.6.0

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/scheduler#fresh-testing)_

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$MGVrahRTbgP1YJ4XczEu_-4sfi1hTfdk0CKLfCbUSQM?via=etke.cc&via=matrix.org)
