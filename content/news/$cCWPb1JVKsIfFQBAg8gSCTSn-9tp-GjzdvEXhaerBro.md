---
id: '$cCWPb1JVKsIfFQBAg8gSCTSn-9tp-GjzdvEXhaerBro'
date: '2024-07-02 08:11:56.457 +0000 UTC'
title: '2024-07-02 08:11 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-07-02 08:11 UTC'
---

**Servers Have Been Updated (Vulnerability Mitigation)**

>  It was discovered that OpenSSH incorrectly handled signal management. A remote attacker could use this issue to bypass authentication and remotely access systems without proper credentials.

[Details (Ubuntu)](https://ubuntu.com/security/CVE-2024-6387), [Details (Debian)](https://security-tracker.debian.org/tracker/CVE-2024-6387)

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$cCWPb1JVKsIfFQBAg8gSCTSn-9tp-GjzdvEXhaerBro?via=etke.cc&via=matrix.org)
