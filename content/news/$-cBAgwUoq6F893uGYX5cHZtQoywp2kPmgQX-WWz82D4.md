---
id: '$-cBAgwUoq6F893uGYX5cHZtQoywp2kPmgQX-WWz82D4'
date: '2023-05-11 20:24:51.167 +0000 UTC'
title: '2023-05-11 20:24 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-05-11 20:24 UTC'
---

**Stable Updates Published**

- Element / [app.etke.cc](https://app.etke.cc/) 1.11.30 -> 1.11.31
- Grafana 9.5.1 -> 9.5.2
- Miniflux 2.0.43 -> 2.0.44
- Synapse 1.82.0 -> 1.83.0
- Uptime Kuma 1.21.2 -> 1.21.3

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

**Service Updates**

- We've developed new Radicale (contacts, calendars server) [matrix auth](https://gitlab.com/etke.cc/radicale-auth-matrix) module, enabled by default for new installations. If you already use Radicale and want to switch from manual user management to automatic matrix-based, [contact us](https://etke.cc/contacts/)

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$-cBAgwUoq6F893uGYX5cHZtQoywp2kPmgQX-WWz82D4?via=etke.cc&via=matrix.org)
