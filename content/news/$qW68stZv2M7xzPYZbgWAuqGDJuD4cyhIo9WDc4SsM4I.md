---
id: '$qW68stZv2M7xzPYZbgWAuqGDJuD4cyhIo9WDc4SsM4I'
date: '2023-01-19 21:05:29.262 +0000 UTC'
title: '2023-01-19 21:05 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-01-19 21:05 UTC'
---

**Stable Updates Published**

- Borg backups / Borg 1.2.3 _version pinning_
- Borg backups / Borgmatic 1.7.5 _version pinning_
- #buscarron:etke.cc  1.3.0 -> 1.3.1
- Cinny 2.2.2 -> 2.2.3
- Element 1.11.17 -> 1.11.19
- Hookshot 2.5.0 -> 2.6.1
- Jitsi stable-8138-1 -> stable-8218
- Uptime Kuma 1.19.4 -> 1.19.6
- #postmoogle:etke.cc  0.9.10 -> 0.9.11
- Synapse 1.74.0 -> 1.75.0
- WhatsApp bridge 0.8.0 -> 0.8.1

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

**Service Updates**

- [etke.cc](https://etke.cc/)'s ssh keys have been rotated. It's automatic process, you don't need to do anything
- The second phase of [etke.cc](https://etke.cc/) docker registry rollout is completed, from now on all our customers' servers will download images from our registry instead of public registries by default.
- We've switched from the old unmaintained Slack bridge ([mx-puppet-slack](https://etke.cc/help/bridges/mx-puppet-slack)) to a new better maintained one ([mautrix-slack](https://etke.cc/help/bridges/mautrix-slack)) for all new customers by default. If you want to switch the bridge on your existing server, please, contact @support:etke.cc

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$qW68stZv2M7xzPYZbgWAuqGDJuD4cyhIo9WDc4SsM4I?via=etke.cc&via=matrix.org)
