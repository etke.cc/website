---
id: '$Wk-ixiNsn72yE1aND5L9iGdY7m2LkzzgeuuT3cpAg7M'
date: '2021-06-15 19:23:31.685 +0000 UTC'
title: '2021-06-15 19:23 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-06-15 19:23 UTC'
---

**servers update** _in progress_
- Hydrogen 0.1.56 -> 0.1.57
- Grafana 7.5.7 -> 8.0.2
- Redis 6.0.10 -> 6.2.4
- Synapse 1.35.1 -> 1.36.0

_no Element update today, because only release candidate of new version is available_
PS: If you want to discuss this update - #discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$Wk-ixiNsn72yE1aND5L9iGdY7m2LkzzgeuuT3cpAg7M?via=etke.cc&via=matrix.org)
