---
id: '$KyN2O2HcCqNB0YFGv2zMmm5wdv-s2j8XWTZE6U5WF0c'
date: '2024-02-12 09:21:55.717 +0000 UTC'
title: '2024-02-12 09:21 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-02-12 09:21 UTC'
---

# 🎉 Today marks 3 years of etke.cc! 🎉

The etke.cc managed Matrix hosting service was launched on the 12th of February 2021 and has turned 3 years old today!

Below, we'd like to show you what improvements have happened over the last year and what's coming ahead for us.

## New payment model, more services, more server regions

In case you've missed [our large news announcement](https://etke.cc/news/lnyorh4ggeh9xadrob_rtuuqu_jj-s-ojfvvfzlmxpm/) from December 2023, it's not too late to learn about it now.

To summarize, we have:

*   switched to a **new payment/pricing model**, which is more sustainable
*   launched **many of new components and services** (see below) and will continue to launch new ones
*   **completely reworked** [**order form**](https://etke.cc/order/) - easier for you and for us - a quicker turnaround time when deploying servers
*   began offering **more powerful servers** (using [Hetzner Cloud](https://hetzner.cloud/?ref=1yg3ZZmtrg5A)'s CPX line) and **in more geographic locations** - **including the U.S.**

We encourage you to read the [our original news announcement](https://etke.cc/news/lnyorh4ggeh9xadrob_rtuuqu_jj-s-ojfvvfzlmxpm/) for more details about these changes.

### New component additions

In the past year, we have launched many new Matrix-related and non-Matrix components.

If you'd like to use one or more of the components seen below, [contact us](https://etke.cc/contacts/) and we'll install it to your existing server.

### New Matrix-related components

All of the Matrix components described below are **new additions** to our stack. **Some are installed by default** to all servers managed by us, as part of our [base Matrix components stack](https://etke.cc/help/faq#what-are-the-base-matrix-components-installed-on-the-server).

*   (free) [Sliding-sync](https://github.com/matrix-org/sliding-sync) - assists next-generation clients like [Element X](https://element.io/labs/element-x) in talking to the homeserver in an optimized manner. This component is installed by default to all servers as part of our [base Matrix components stack](https://etke.cc/help/faq#what-are-the-base-matrix-components-installed-on-the-server).
*   (free) Encrypted Bridges - we now offer a checkbox on our order form that globally enables encryption for all bridges that support it. This is experimental and may not work well for some bridges, so we don't recommend it to everyone.
*   (free) The [synapse\_auto\_compressor](https://github.com/matrix-org/rust-synapse-compress-state/#automated-tool-synapse_auto_compressor) tool, which runs in the background and periodically compresses the (Postgres) database for the Synapse homeserver, so that it runs optimally. This component is installed by default to all servers as part of our [base Matrix components stack](https://etke.cc/help/faq#what-are-the-base-matrix-components-installed-on-the-server).
*   (free) We've switched to [exim-relay](https://github.com/devture/exim-relay) for delivering emails (optionally via an [SMTP relay](https://etke.cc/help/extras/smtp-relay)) and wired all email-sending components (the Synapse Matrix homeserver and other addons) to it.
*   (free addition to our $5/mo bridge pack) a [Google Messages bridge](https://etke.cc/help/bridges/mautrix-gmessages) (powered by `mautrix-gmessages`). The old [Google Chat bridge](/help/bridges/mautrix-googlechat) still continues to work.
*   (+$1/mo) the [SchildiChat](https://schildi.chat/) web-based Matrix client - an Element-web fork offering more customization.
*   (+$3/mo) [ChatGPT](https://etke.cc/help/bots/chatgpt/) - a Large-Language-Model (LLM) bot that you can talk to via Matrix, based on [OpenAI](https://openai.com/)'s [ChatGPT](https://openai.com/chatgpt) (Requires a separately-obtained [OpenAI API key](https://help.openai.com/en/articles/4936850-where-do-i-find-my-api-key))
*   (+$2/mo) [S3 storage support for the Synapse homeserver](https://etke.cc/help/extras/synapse-s3-storage) - for infinite Matrix media storage (Requires a separately obtained AWS S3-compatible object store of your choice)
*   (+$5/mo) [Synapse workers](https://github.com/element-hq/synapse/blob/master/docs/workers.md) - a multi-process homeserver setup to more efficiently handle many users and large rooms (Requires a powerful server - 8+ GB of RAM)

We've also replaced our [Signal bridge](https://etke.cc/help/bridges/mautrix-signal) (`mautrix-signal`) offering with the new bridge implementation (complete rewrite from [Python](https://www.python.org/) to [Golang](https://go.dev/)). 

### New non-Matrix-related components

*   (+$1/mo) \[previously-offered and making a comeback\] the [Miniflux](https://etke.cc/help/extras/miniflux) RSS reader
*   (+$1/mo) \[previously-offered and making a comeback\] the [Radicale](https://radicale.org/) CalDAV/CardDAV server
*   (+$1/mo) \[previously-offered and making a comeback\] the [Uptime Kuma](https://etke.cc/help/extras/uptime-kuma) monitoring system
*   (+$1/mo) \[brand new\] the [Linkding](https://github.com/sissbruecker/linkding) bookmark manager
*   (+$2/mo) \[brand new\] the [Vaultwarden](https://etke.cc/help/extras/vaultwarden) password manager (a lightweight [Bitwarden](https://bitwarden.com/)\-compatible server which can be used with the Bitwarden client apps)
*   (+$2/mo) \[brand new\] the [Firezone](https://www.firezone.dev/) - a VPN server based on [WireGuard](https://www.wireguard.com/) with a Web UI
*   (+$3/mo) \[brand new\] the [GoToSocial](https://gotosocial.org/) ActivityPub server (a lightweight [Mastodon](https://joinmastodon.org/) alternative)

## New or discontinued etke.cc services

### Dedicated support

On the services front, we're now offering a [Dedicated support](https://etke.cc/services/support) tier for $100/mo. This is an addition to the free Basic support tier that we offer to all etke.cc customers.

The dedicated support tier is useful when your product relies on Matrix and you may require quicker help in case of outages. The main differences with the Basic tier are the **dedicated Matrix chat room** with etke.cc developers and the **higher priority of your requests** relative to others.

### Matrix Rooms Search

We've developed the [Matrix Rooms Search tool](https://gitlab.com/etke.cc/mrs/api) (naturally, as AGPLv3-licensed free-software). Anyone can run their instance and index the global Matrix Federation.

MRS provides a search across room names, topics, etc., allowing for better discoverability of rooms across all of Matrix.

We're making Matrix Rooms Search available to etke.cc customers in 2 ways:

1.  The [MatrixRooms.info](https://matrixrooms.info/) website
2.  Pre-configured as an alternative Rooms Directory in Element

### More domain choices

To those who don't wish to use their own custom domain for their server, we've been offering hosting on etke.cc-owned domains like `my-business.etke.host`.

At the expense of some independence (afforded by having your own custom domain), using etke.cc-owned domains allows people to avoid dealing with domains, DNS, and the cost that comes with that.

We now offer hosting on these additional domains:

*   `onmatrix.chat`
*   `kupo.email`
*   `matrix.town`
*   `matrix.fan`
*   `ma3x.chat`

When making a [new order](https://etke.cc/order/), you can claim your subdomain on any of these domains and get your Matrix server deployed more quickly and easily (no DNS configuration required on your side).

### Discontinuing our Custom Consulting/Development service

We used to offer a Custom Consulting/Development service - helping people with custom deployments or integrations on top of Matrix.

We have **discontinued this service in order to free up time and let us focus on improving etke.cc for everyone**. This is part of the reason why we have so many major improvements to announce in this birthday post.

## Improved internals

Besides the many new components, services and tools, we've also done a lot of internal work - improving reliability and performance.

Below are some of the important internal changes we've done:

*   [The Scheduler](https://etke.cc/scheduler):
    *   Automatic HTTP, DNS, and port checks, including federation tests, on `run ping` and `run maintenance`
    *   Cumulative alerts - we combine multiple failure alerts into a single message that we deliver via Matrix and email
    *   Improved deliverability of emails, thanks to sending them via [Postmark](https://postmarkapp.com/)
    *   Full overhaul of the agent/worker (injector) part making it blazing-fast and decreasing the delay (to milliseconds) between receiving a command from the user and executing it
    *   Huge refactoring of the codebase
    *   Many automation improvements, allowing us to more quickly and easily install new servers
*   Server Maintenance:
    *   Automatic Postgres database vacuuming on each maintenance run
    *   Automatic Postgres database tuning (based on [PGTune](https://pgtune.leopard.in.ua/)'s calculaton logic) which takes into account your server's configuration and installed components
*   Networking: reworked Matrix stack networking for improved security, performance and ease of plugging-in new services (we've migrated from [nginx](https://nginx.org/) to [Traefik](https://traefik.io/))
*   Emails: we've also configured [DKIM](https://en.wikipedia.org/wiki/DomainKeys_Identified_Mail), [SPF](https://en.wikipedia.org/wiki/Sender_Policy_Framework) and [rDNS](https://en.wikipedia.org/wiki/List_of_DNS_record_types#PTR) for customers hosted on our ([Hetzner Cloud](https://hetzner.cloud/?ref=1yg3ZZmtrg5A)) servers - this improves outgoing email deliverability for big providers like Google (see [New Gmail protections for a safer, less spammy inbox](https://blog.google/products/gmail/gmail-security-authentication-spam-protection/))

## Free-software work

We've also done a lot of work on a new free-software (AGPLv3) Ansible mega-playbook - [mash-playbook](https://github.com/mother-of-all-self-hosting/mash-playbook), which currently includes 80+ components. Many of these began their life at etke.cc, while others were developed by us (or by the community) later on. Some of these components will be offered as addon components to etke.cc customers in the future. There are no technical difficulties to offering all of them immediately, but supporting a new component is not so simple - it requires documentation, commitment, support-staff training, etc.

Components from mash-playbook and the [matrix-docker-ansible-deploy](https://github.com/spantaleev/matrix-docker-ansible-deploy) playbook are at the core of our automation ([etke/ansible](https://gitlab.com/etke.cc/ansible)), powering all servers managed by us.

As part of our work over the past year, we have also developed and released as free-software (AGPLv3) the following new tools:

*   [MRS](https://gitlab.com/etke.cc/mrs/api) - Matrix Rooms Search, the backend that powers our [MatrixRooms.info](https://matrixrooms.info/) service (discussed above)
*   [A.G.R.U](https://gitlab.com/etke.cc/tools/agru) - a command-line tool like [ansible-galaxy](https://docs.ansible.com/ansible/latest/cli/ansible-galaxy.html), but faster
*   [ansible-ssh](https://gitlab.com/etke.cc/tools/ansible-ssh) - a command-line tool (wrapper around `ssh`) that makes it easy to SSH into hosts found in an [Ansible inventory](https://docs.ansible.com/ansible/latest/inventory_guide/index.html) file
*   [radicale-auth-matrix](https://gitlab.com/etke.cc/radicale-auth-matrix) - an adapter that connects [Radicale](https://radicale.org/)'s user authentication system to Matrix

## Numbers!

*   We've **installed 243 new Matrix servers**
*   **Pushed 461 updates and enhancements** to the automation framework used as the service core
*   **Posted 60 updates** in the announcements room, so you're always up-to-date with what we're working on

If you're curious to travel back in time, here are posts from previous etke.cc birthdays:

*   [_2 years celebration post_](https://etke.cc/news/upsyw4ykbtgmwhz8k7ukldx0zbbfq-fh0iqi3llixi0/)
*   [_1 year celebration post_](https://etke.cc/news/j-48qcbzgshru3ap7hictalntpwqtotgltct01mhrmw/)


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$KyN2O2HcCqNB0YFGv2zMmm5wdv-s2j8XWTZE6U5WF0c?via=etke.cc&via=matrix.org)
