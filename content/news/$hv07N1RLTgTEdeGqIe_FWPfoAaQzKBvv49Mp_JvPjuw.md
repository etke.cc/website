---
id: '$hv07N1RLTgTEdeGqIe_FWPfoAaQzKBvv49Mp_JvPjuw'
date: '2021-06-01 16:38:19.054 +0000 UTC'
title: '2021-06-01 16:38 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-06-01 16:38 UTC'
---

**servers updates**
- IRC (appservice) 0.25.0 -> 0.26.0
- Element Web 1.7.28 -> 1.7.29
- Hydrogen (**very** experimental) 0.1.53
- Grafana 7.5.6 -> 7.5.7
- Nginx 1.20.0 -> 1.21.0
- Prometheus 2.27.0 -> 2.27.1
- Synapse Admin latest -> 0.8.1 _not a real upgrade, just version pinning_
- Synapse 1.34.0 -> 1.35.0

if you want to discuss that update - #discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$hv07N1RLTgTEdeGqIe_FWPfoAaQzKBvv49Mp_JvPjuw?via=etke.cc&via=matrix.org)
