---
id: '$NqcLrvjF4x2FMz15ra9ope1dsEn_sKNjwbCFBmah8AU'
date: '2022-02-17 07:11:33.902 +0000 UTC'
title: '2022-02-17 07:11 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-02-17 07:11 UTC'
---

**servers updates in progress**

_due to whatsapp server changes, whatsapp bridge was broken_

* Whatsapp bridge: 0.2.3 -> 0.2.4

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$NqcLrvjF4x2FMz15ra9ope1dsEn_sKNjwbCFBmah8AU?via=etke.cc&via=matrix.org)
