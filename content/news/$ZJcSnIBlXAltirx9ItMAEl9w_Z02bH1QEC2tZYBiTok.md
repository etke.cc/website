---
id: '$ZJcSnIBlXAltirx9ItMAEl9w_Z02bH1QEC2tZYBiTok'
date: '2022-04-19 16:59:10.554 +0000 UTC'
title: '2022-04-19 16:59 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-04-19 16:59 UTC'
---

**servers update in progress**

* borgbackup latest
* element 1.10.9 -> 1.10.10
* #honoroit:etke.cc 0.9.6 -> 0.9.7
* uptime kuma 1.14.0 -> 1.14.1
* synapse 1.56.0 -> 1.57.0
* telegram bridge 0.11.2 -> 0.11.3
* whatsapp bridge 0.3.0 -> 0.3.1

___

**service updates**

* borg backups now offered officially as component for etke.cc subscribers _we work with it unofficially for a while and several customers got it with the first rough versions, but now it's ready to use and can backup both your matrix server configs, files and databases (sql dumps)_

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$ZJcSnIBlXAltirx9ItMAEl9w_Z02bH1QEC2tZYBiTok?via=etke.cc&via=matrix.org)
