---
id: '$uPV9Te95bn-2Xs1j2imlhWVVY_1u0xxkCUn49HmVbIE'
date: '2024-07-01 11:03:57.735 +0000 UTC'
title: '2024-07-01 11:03 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-07-01 11:03 UTC'
---

**Service Updates**

As it was announced a long time ago, 2 major changes hit the transition's deadline and are enforced starting from 1st July 2024:

- Any customer with a deprecated subscription model (Maintenances and Turnkeys) will be asked to switch to the By Complexity model on any support request (if you got your matrix server installed after the [announcement on December 2023](https://etke.cc/news/lnyorh4ggeh9xadrob_rtuuqu_jj-s-ojfvvfzlmxpm/) - don't worry, you already use By Complexity price model)
- All [deprecated components announced previously](https://etke.cc/help/faq#what-about-deprecated-components) are disabled

**Deprecated Components Alternatives and Migration Notes**

Almost every component has an alternative, usually with more features and options than the original one. We understand there could be confusion, so here are migration notes and suggestions for each of them:

- Dimension - unfortunately, the only alternative is the built-in integration manager. For customers who had Dimension with Etherpad, etherpad is still available on dimension.your-server.com/etherpad (i.e., nothing changed)
- Wireguard (and dnsmasq) - alternative is [Firezone](https://etke.cc/help/extras/firezone) (+$2/month) that provides UI for devices management and VPN server configuration. You may request adding that component by [contacting us](https://etke.cc/contacts/)
- Discord bridge (mx-puppet-discord) - alternative is [Discord bridge (mautrix-discord)](https://etke.cc/help/bridges/mautrix-discord). You may request adding that component by [contacting us](https://etke.cc/contacts/)
- Facebook bridge (mautrix-facebook) - alternative is [Facebook bridge (mautrix-meta)](https://etke.cc/help/bridges/mautrix-meta-messenger/). You may request adding that component by [contacting us](https://etke.cc/contacts/)
- GroupMe bridge (mx-puppet-groupme) - unfortunately, there is no alternative
- Instagram bridge (mautrix-instagram) - alternative is [Instagram bridge (mautrix-meta)](https://etke.cc/help/bridges/mautrix-meta-instagram/). You may request adding that component by [contacting us](https://etke.cc/contacts/)
- Slack bridge (mx-puppet-slack) - alternative is [Slack bridge (mautrix-slack)](https://etke.cc/help/bridges/mautrix-slack/). You may request adding that component by [contacting us](https://etke.cc/contacts/)
- Steam bridge (mx-puppet-steam) unfortunately, there is no alternative
- Webhooks bridge (appservice-webhooks) - alternative is [Webhooks bridge (hookshot)](https://etke.cc/help/bridges/hookshot/). You may request adding that component by [contacting us](https://etke.cc/contacts/)

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$uPV9Te95bn-2Xs1j2imlhWVVY_1u0xxkCUn49HmVbIE?via=etke.cc&via=matrix.org)
