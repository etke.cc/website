---
id: '$Cz0NmFieCjy1XpAEh1GWG2n8os9_h7QlxjoeuWTE2Jo'
date: '2021-06-18 18:42:44.684 +0000 UTC'
title: '2021-06-18 18:42 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-06-18 18:42 UTC'
---

https://matrix.org/blog/2021/06/18/this-week-in-matrix-2021-06-18

This Week in Matrix (huge one)


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$Cz0NmFieCjy1XpAEh1GWG2n8os9_h7QlxjoeuWTE2Jo?via=etke.cc&via=matrix.org)
