---
id: '$U4mOeh5jsMh6yGZebI9mOx5xm7rofluU_s5v_i1JfwY'
date: '2024-02-01 21:33:56.139 +0000 UTC'
title: '2024-02-01 21:33 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-02-01 21:33 UTC'
---

**Stable Updates Published**

- Element / [app.etke.cc](https://app.etke.cc/) 1.11.55 -> 1.11.57 _there was no 1.11.56_
- Jitsi 9111 -> 9220
- Synapse 1.99.0 -> 1.100.0
- Vaultwarden 1.30.1 -> 1.30.2

_this update contains fixes for S3 and coturn issues_

**Service Updates: Synapse Workers are available for order**

Larger matrix servers may suffer from Synapse slowness. To increase the throughput and performance of your matrix server, you could now order Synapse Workers (charged extra) to be enabled on your matrix server to offload Sync, Client, Federation, Encryption, Authorization, Events, Account Data, Receipts, Presence, and Directory APIs to worker processes.

Existing customers may request Synapse Workers to be enabled by [contacting us](https://etke.cc/contacts/)

**Service Updates: Better Email Deliverability for the New Orders**

As an answer to [Google's](https://blog.google/products/gmail/gmail-security-authentication-spam-protection/) and [Yahoo's](https://blog.postmaster.yahooinc.com/post/730172167494483968/more-secure-less-spam) tightened requirements for emails, prerequisites for all new orders were updated to include DMARC and SPF DNS records. For hosting orders, we additionally configure rDNS record to resolve to `matrix.your-server.com` by default.

Existing customers are unaffected, but may update their DNS records to accommodate the changes:

**For custom domains**:

- Add new `TXT` record for `matrix` subdomain with the following value: `v=spf1 ip4:REPLACE_WITH_SERVER_IPv4 -all` (`REPLACE_WITH_SERVER_IPv4` should be replaced with your matrix server's IPv4)
- Add new `TXT` record for `_dmarc.matrix` subdomain with the following value: `v=DMARC1; p=quarantine;`
- Reverse DNS (rDNS) records configuration depends on your VPS provider, you may wish to contact their support to help with changing it

**For etke.cc-owned domains** (`etke.host`, `kupo.email`, `ma3x.chat`, `matrix.fan`, `matrix.town`, `onmatrix.chat`)

Contact us: [etke.cc/contacts](https://etke.cc/contacts)

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$U4mOeh5jsMh6yGZebI9mOx5xm7rofluU_s5v_i1JfwY?via=etke.cc&via=matrix.org)
