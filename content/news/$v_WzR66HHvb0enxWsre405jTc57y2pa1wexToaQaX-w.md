---
id: '$v_WzR66HHvb0enxWsre405jTc57y2pa1wexToaQaX-w'
date: '2024-06-20 20:03:25.371 +0000 UTC'
title: '2024-06-20 20:03 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-06-20 20:03 UTC'
---

**Stable Updates Published**

- [Element](https://github.com/element-hq/element-web): [v1.11.68](https://github.com/element-hq/element-web/releases/tag/v1.11.68) -> [v1.11.69](https://github.com/element-hq/element-web/releases/tag/v1.11.69)
- [Gmessages](https://github.com/mautrix/gmessages): [v0.4.1](https://github.com/mautrix/gmessages/releases/tag/v0.4.1) -> [v0.4.2](https://github.com/mautrix/gmessages/releases/tag/v0.4.2)
- [Prometheus](https://github.com/prometheus/prometheus): [v2.52.0](https://github.com/prometheus/prometheus/releases/tag/v2.52.0) -> [v2.53.0](https://github.com/prometheus/prometheus/releases/tag/v2.53.0)
- [Radicale](https://github.com/tomsquest/docker-radicale): [3.2.1.0](https://github.com/tomsquest/docker-radicale/releases/tag/3.2.1.0) -> [3.2.2.0](https://github.com/tomsquest/docker-radicale/releases/tag/3.2.2.0)
- [Signal](https://github.com/mautrix/signal): [v0.6.1](https://github.com/mautrix/signal/releases/tag/v0.6.1) -> [v0.6.2](https://github.com/mautrix/signal/releases/tag/v0.6.2)
- [Synapse](https://github.com/element-hq/synapse): [v1.108.0](https://github.com/element-hq/synapse/releases/tag/v1.108.0) -> [v1.109.0](https://github.com/element-hq/synapse/releases/tag/v1.109.0)
- [Whatsapp](https://github.com/mautrix/whatsapp): [v0.10.7](https://github.com/mautrix/whatsapp/releases/tag/v0.10.7) -> [v0.10.8](https://github.com/mautrix/whatsapp/releases/tag/v0.10.8)

**Reminder**

- [In relation to our new pricing model: When will old subscriptions (Maintenance and Turnkeys) be deprecated?](https://etke.cc/help/faq#when-will-old-subscriptions-maintenance-and-turnkeys-be-deprecated)
- [Upcoming removal of long-deprecated components](https://etke.cc/help/faq#what-about-deprecated-components)

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$v_WzR66HHvb0enxWsre405jTc57y2pa1wexToaQaX-w?via=etke.cc&via=matrix.org)
