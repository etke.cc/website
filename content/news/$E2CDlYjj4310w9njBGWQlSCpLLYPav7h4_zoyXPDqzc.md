---
id: '$E2CDlYjj4310w9njBGWQlSCpLLYPav7h4_zoyXPDqzc'
date: '2021-12-17 17:12:21.046 +0000 UTC'
title: '2021-12-17 17:12 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-12-17 17:12 UTC'
---

> <@_neb_rssbot_=40aine=3aetke.cc:matrix.org> matrix.org: Synapse 1.49.0 released ( https://matrix.org/blog/2021/12/14/synapse-1-49-0-released )

That version was already installed on all subscribers' servers on Tuesday.

Seems like the announcement on matrix blog was posted with delay


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$E2CDlYjj4310w9njBGWQlSCpLLYPav7h4_zoyXPDqzc?via=etke.cc&via=matrix.org)
