---
id: '$vg4N7b_QFXggOu8gmSTnH2x2Mb7HOrS_Hl_1bcduq6I'
date: '2023-03-24 07:15:12.644 +0000 UTC'
title: '2023-03-24 07:15 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-03-24 07:15 UTC'
---

**Stable Updates Published**

- Borgmatic 1.7.8 -> 1.7.9
- Discord bridge 0.1.1 -> 0.2.0
- Grafana 9.4.3 -> 9.4.7
- Hookshot 2.7.0 -> 3.0.1
- Languagetool 6.0-dockerupdate-3 -> 6.0-dockerupdate-4
- Miniflux 2.0.42 -> 2.0.43
- Ntfy 2.1.2 -> 2.2.0
- Prometheus postgres exporter 0.11.1 -> 0.12.0
- Prometheus 2.42.0 -> 2.43.0
- Uptime Kuma 1.20.2 -> 1.21.0
- WhatsApp bridge 0.8.2 -> 0.8.3

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$vg4N7b_QFXggOu8gmSTnH2x2Mb7HOrS_Hl_1bcduq6I?via=etke.cc&via=matrix.org)
