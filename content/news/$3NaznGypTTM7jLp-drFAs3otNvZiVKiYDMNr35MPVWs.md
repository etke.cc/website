---
id: '$3NaznGypTTM7jLp-drFAs3otNvZiVKiYDMNr35MPVWs'
date: '2021-06-03 17:14:25.411 +0000 UTC'
title: '2021-06-03 17:14 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-06-03 17:14 UTC'
---

**servers updates**
* Synapse 1.35.0 -> 1.35.1 _bugfix_
* Hydrogen 0.1.53 -> 0.1.56

if you want to discuss that update - #discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$3NaznGypTTM7jLp-drFAs3otNvZiVKiYDMNr35MPVWs?via=etke.cc&via=matrix.org)
