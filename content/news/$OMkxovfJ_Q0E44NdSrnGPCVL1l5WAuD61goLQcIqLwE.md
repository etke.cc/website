---
id: '$OMkxovfJ_Q0E44NdSrnGPCVL1l5WAuD61goLQcIqLwE'
date: '2024-02-08 20:58:13.744 +0000 UTC'
title: '2024-02-08 20:58 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-02-08 20:58 UTC'
---

**Stable Updates Published**

- #buscarron:etke.cc 1.3.1 -> 1.4.0
- #honoroit:etke.cc 0.9.19 -> 0.9.20
- Signal bridge _updated pinned commit_
- Synapse Admin 0.8.7 -> 0.9.1
- Vaultwarden 1.30.2 -> 1.30.3

**Reminder**

All announcements published into the #news:etke.cc room are published on [etke.cc/news](https://etke.cc/news) page, [RSS feed](https://etke.cc/news/index.xml), and [Fediverse account](https://mastodon.matrix.org/@etkecc) for your convenience. Moreover, we've added `Attention needed!` block with the most important things on the news page

Please keep an eye on them, to avoid missing important information. We publish announcements only once a week (except security updates - announcements published ASAP), so you could safely enable "All messages" notification preset.

**Service Updates: Deprecating Abandoned Components**

Some of the components we offered previously have been abandoned or deprecated by their developers, but we continue to maintain them on customers' servers (if any of the abandoned components are installed).

Unfortunately, such components misbehave and sometimes can't even work with modern APIs, so we've proclaimed them deprecated many months ago and now defined a removal plan.

Starting from **1st July 2024**, the following components will be **uninstalled** from customers servers:

- **[Discord bridge (mx-puppet-discord)](https://etke.cc/help/bridges/mx-puppet-discord)** which has been unmaintained by its developers since May 2022. It got superseded by [Discord bridge (mautrix-discord)](https://etke.cc/help/bridges/mautrix-discord)
- **[Slack bridge (mx-puppet-slack)](https://etke.cc/help/bridges/mx-puppet-slack)** which has been unmaintained by its developers since May 2022. It got superseded by [Slack bridge (mautrix-slack)](https://etke.cc/help/bridges/mautrix-slack)
- **[Webhooks bridge (appservice-webhooks)](https://etke.cc/help/bridges/appservice-webhooks)** which has been unmaintained by its developers since November 2022. It got superseded by [Webhooks bridge (hookshot)](https://etke.cc/help/bridges/hookshot)
- **Dimension integration manager** which is unmaintained by its developers since November 2022. There is no free-software alternative integration manager
- **Wireguard** (and dnsmasq) which has been superseded by Firezone

Almost every component has maintained and updated successor, so you have time to migrate to the new components until the deprecated ones are removed.

More details are available in the related FAQ entry: [etke.cc/help/faq#what-about-deprecated-components](https://etke.cc/help/faq#what-about-deprecated-components)

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$OMkxovfJ_Q0E44NdSrnGPCVL1l5WAuD61goLQcIqLwE?via=etke.cc&via=matrix.org)
