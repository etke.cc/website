---
id: '$meeDrrYKEcCbbrTM_nuC4nN2Ktrgx66ZEBKxW_RH44M'
date: '2023-04-13 20:08:14.914 +0000 UTC'
title: '2023-04-13 20:08 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-04-13 20:08 UTC'
---

**Stable Updates Published**

- Element / [app.etke.cc](https://app.etke.cc/) 1.11.28 -> 1.11.29
- Synapse 1.80.0 -> 1.81.0

Note to BorgBackup users: the Borgmatic v1.7.11 contains [a regression issue](https://projects.torsion.org/borgmatic-collective/borgmatic/issues/668), but it doesn't affect ability to actually backup your server, so there won't be version downgrade

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

**Service Updates**

- [etke.cc](https://etke.cc/)'s ssh keys have been rotated, new keys (available at [etke.cc/ssh.key](https://etke.cc/ssh.key)) were installed on customers' servers. That's an automatic process, you don't need to do anything
- [The Scheduler](https://etke.cc/scheduler) will automatically check your server's HTTP endpoints in case of `run ping` (all known endpoints) and `run maintenance` (only failed ones) commands from now on. You can try it yourself or see the example on the [docs page](https://etke.cc/scheduler/#ping)

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$meeDrrYKEcCbbrTM_nuC4nN2Ktrgx66ZEBKxW_RH44M?via=etke.cc&via=matrix.org)
