---
id: '$pE2a5_UuaGW5hDqtk1DGqyLEWDSNz69hX9ls780k1Xo'
date: '2023-12-28 21:02:03.909 +0000 UTC'
title: '2023-12-28 21:02 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-12-28 21:02 UTC'
---

**Stable Updates Published**

- Hookshot 4.7.0 -> 5.0.0
- Telegram bridge 0.15.0 -> 0.15.1

**Service Updates: New Domains**

The following new domains are offered for subdomain orders with no extra fee: `matrix.town`, `matrix.fan`, `ma3x.chat`.
You can get your `@own:server.matrix.town` or `@be:a.matrix.fan`, or `@unusual:name.ma3x.chat`

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$pE2a5_UuaGW5hDqtk1DGqyLEWDSNz69hX9ls780k1Xo?via=etke.cc&via=matrix.org)
