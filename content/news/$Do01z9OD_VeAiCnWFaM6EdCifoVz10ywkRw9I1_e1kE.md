---
id: '$Do01z9OD_VeAiCnWFaM6EdCifoVz10ywkRw9I1_e1kE'
date: '2021-08-31 18:38:01.067 +0000 UTC'
title: '2021-08-31 18:38 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-08-31 18:38 UTC'
---

**servers update in progress**

_this update contains various configuration changes to decrease the load created by bridges with rarely needed things. If you don't like any of the following changes, just send me a PM and it will be reverted for your server_

new versions:
- Mjolnir 0.1.18 -> 0.1.19
- Hydrogen 0.2.5 -> 0.2.7
- Coturn 4.5.2-r3 -> 4.5.2-r4
- Grafana 8.0.6 -> 8.1.2
- Jitsi 5963 -> 6173
- Mailer (exim) 4.94.2-r0-3 -> 4.94.2-r0-4
- Prometheus 2.29.1 -> 2.29.2
- Synapse 1.41.0 -> 1.41.1 _security fix_

Turnkey subscription plan
- enabled external firewall

WireGuard
- added full IPv6 support, now it works in dual-stack mode
- regenerated devices' configs (keys didn't change, so you can work with old config or copy new configs to enable ipv6)

Telegram

_it's the most resource-heavy bridge because people tend to bridge large chats, so with the following changes your server's life will be easier and require less CPU/RAM/disk_
- enabled sync of direct chats on startup
- disabled delivery receipts
- (only for new rooms) set initial sync size of telegram users in the bridged room to 10, other members will be synced when they send a message to that room
- (only for new rooms) disabled sync of members of bridged telegram channels (you can't send any message to telegram channel, only read it, so no need to waste resources on puppets for readers)
- (only for new rooms) explicitly set room name and avatar on creation

LinkedIn, Instagram, Facebook, Discord, GroupME, Skype, Slack, Steam, Twitter
- disabled bridge presence

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$Do01z9OD_VeAiCnWFaM6EdCifoVz10ywkRw9I1_e1kE?via=etke.cc&via=matrix.org)
