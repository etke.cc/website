---
id: '$xnornTmKXheuYFyIyE788ncJgXBoanEhMPZ5i3wCu50'
date: '2023-11-02 21:04:42.887 +0000 UTC'
title: '2023-11-02 21:04 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-11-02 21:04 UTC'
---

**Stable Updates Published**

- Borgmatic 1.8.3 -> 1.8.4
- Cinny 3.0.0 -> 3.2.0
- IRC bridge 1.14.5 -> 1.14.6
- Synapse 1.95.0 -> 1.95.1

**Service Updates: DNS and TCP/UDP ports checks in [Scheduler](https://etke.cc/help/extras/scheduler/)**

`run ping` and `run maintenance` of [etke.cc/scheduler](https://etke.cc/help/extras/scheduler/) will check for expected DNS records and ports from now on. That change provides better observability and allows fixing issues before they become problems.

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$xnornTmKXheuYFyIyE788ncJgXBoanEhMPZ5i3wCu50?via=etke.cc&via=matrix.org)
