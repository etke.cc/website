---
id: '$yZly-TcJicrZ-piYhNsSUFj1OPRDXmq-PT3fEsV-oYA'
date: '2024-06-06 20:37:49.542 +0000 UTC'
title: '2024-06-06 20:37 UTC'
author: '@slavi:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-06-06 20:37 UTC'
---

**Stable Updates Published**

* Borgmatic: 1.8.9 -> 1.8.11
* [Element](https://github.com/element-hq/element-web): [v1.11.67](https://github.com/element-hq/element-web/releases/tag/v1.11.67) -> [v1.11.68](https://github.com/element-hq/element-web/releases/tag/v1.11.68)
* [Etherpad](https://github.com/ether/etherpad-lite): [2.0.3](https://github.com/ether/etherpad-lite/releases/tag/2.0.3) -> [2.1.0](https://github.com/ether/etherpad-lite/releases/tag/2.1.0)
* [Sygnal](https://github.com/matrix-org/sygnal): [v0.14.2](https://github.com/matrix-org/sygnal/releases/tag/v0.14.2) -> [v0.14.3](https://github.com/matrix-org/sygnal/releases/tag/v0.14.3)
* [Synapse Reverse Proxy Companion](https://github.com/nginx/nginx): [1.25.5-alpine](https://github.com/nginx/nginx/releases/tag/release-1.25.5) -> [1.27.0-alpine](https://github.com/nginx/nginx/releases/tag/release-1.27.0)
* [Wechat](https://github.com/duo/matrix-wechat): [0.2.4](https://github.com/duo/matrix-wechat/releases/tag/0.2.4) _new_
* Wechat Agent: 0.0.1 _new_


**Service Update: New Component - [WeChat bridge](https://etke.cc/help/bridges/wechat/)**

[WeChat](https://www.wechat.com/) bridge is available for order. Existing customers can request it by [contacting us](https://etke.cc/contacts/)

**Service Update: [Monitoring](https://etke.cc/services/monitoring/) Alerts changes**

Starting this week, we are gradually implementing an exponential backoff mechanism for monitoring alerts. If an alert doesn't change, we'll email and Matrix-message you less often about the same issue.

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$yZly-TcJicrZ-piYhNsSUFj1OPRDXmq-PT3fEsV-oYA?via=etke.cc&via=matrix.org)
