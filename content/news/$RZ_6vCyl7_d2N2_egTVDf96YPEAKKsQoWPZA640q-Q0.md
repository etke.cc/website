---
id: '$RZ_6vCyl7_d2N2_egTVDf96YPEAKKsQoWPZA640q-Q0'
date: '2023-10-31 15:38:59.077 +0000 UTC'
title: '2023-10-31 15:38 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-10-31 15:38 UTC'
---

**Servers Update In Progress (Security Fix)**

- Synapse 1.95.0 -> 1.95.1

> Cached device information of remote users can be queried from Synapse. This can be used to enumerate the remote users known to a homeserver.

[Details](https://github.com/matrix-org/synapse/releases/tag/v1.95.1)

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$RZ_6vCyl7_d2N2_egTVDf96YPEAKKsQoWPZA640q-Q0?via=etke.cc&via=matrix.org)
