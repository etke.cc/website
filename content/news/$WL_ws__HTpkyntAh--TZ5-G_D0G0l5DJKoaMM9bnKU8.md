---
id: '$WL_ws__HTpkyntAh--TZ5-G_D0G0l5DJKoaMM9bnKU8'
date: '2021-07-18 19:41:56.782 +0000 UTC'
title: '2021-07-18 19:41 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-07-18 19:41 UTC'
---

**service updates**

* Twitter bridge not offered anymore - it's unstable, hard to configure from both sides and fails too often (nothing changed to existing installations, we just stop offering it to new customers)
* Added [case studies](https://etke.cc/#cases) section on website, now contains fresh case of the Reidel Law Firm
* Mother Miounne now supports encryption (alpha, very experimental) and persistent crypto store, room: #miounne:etke.cc 

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$WL_ws__HTpkyntAh--TZ5-G_D0G0l5DJKoaMM9bnKU8?via=etke.cc&via=matrix.org)
