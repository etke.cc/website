---
id: '$UPSyw4YkbTgMwHZ8K7UkLdx0zbBfq-Fh0iqI3lLiXi0'
date: '2023-02-12 11:40:42.816 +0000 UTC'
title: '2023-02-12 11:40 UTC'
author: '@slavi:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-02-12 11:40 UTC'
---

# 🎉 Today marks 2 years of etke.cc! 🎉

The etke.cc managed Matrix hosting service was launched on the 12th of February 2021 and has turned 2 years old today!

Below, we'd like to show you what improvements have happened over the last year and what's coming ahead for us.

## Robotic assistance for increased human-friendliness

[The Scheduler](https://etke.cc/scheduler) is a major service developed by etke.cc, which we introduced in the past year - it **gives customers more control over managing their Matrix server**.

It started as a solution to the "I'd like server maintenance to happen during a specific time window" request we were getting often.
Over time, it grew into **a toolkit for managing your Matrix homeserver via chat**.

Using Scheduler, you can **send a chat message** to the [`@scheduler:etke.cc` bot](https://matrix.to/#/@scheduler:etke.cc) to:

- **configure the automated maintenance time window** (e.g., `recurring maintenance Monday 15:04`)
- **run maintenance** (server update) any time you wish (e.g., `run maintenance`)
- **restart a specific service** you're observing trouble with (e.g., `run restart facebook`)
- **check disk usage** (e.g., `run disk`)

If you haven't used Scheduler already, we urge you to give it a try! It really is a powerful tool.

## Website improvements

* You need to have been in the dark, to be able to appreciate the light. We've **added a Light theme** to the etke.cc website, making it more approachable.
* We've **improved our [order form](https://etke.cc/order)'s usability and order processing performance**. Furthermore, we now process orders more quickly and are continuing to improve in this area. Stay tuned for an all-new order form coming soon, which plans to eliminate back-and-forth email exchanges between you and us. This will lead to you getting a fully-functioning Matrix server hosted much sooner after ordering.
* We've **improved our website's [Help](https://etke.cc/help/) section**. Each bridge got a dedicated documentation page to help you find things more easily and not get overwhelmed with unrelated information. We still have more work to do in this area to document some remaining components and services that we offer.
* **Multilingual** website, order processing, and support were implemented. Check out the [German version of our website](https://etke.cc/de/).
* We've **added [more communication channels](https://etke.cc/contacts)** to simplify support requests coming from different platforms.

## Stability is key

* The internal etke.cc **container registry mirror** (powered by [Docker Registry](https://docs.docker.com/registry/)) was added to our stack, providing a more stable and quicker maintenance process. We previously used to pull components from their original registry (Docker Hub, Gitlab Registry, etc.), but these registries (especially Gitlab Registry) often experienced downtime and caused our maintenance process to break. No more! All container images are now mirrored on etke.cc infrastructure.
* The internal etke.cc **monitoring system** (powered by [Prometheus](https://prometheus.io/) and [Grafana](https://grafana.com/)) was implemented to monitor common issues and fatal outages of our customers' servers.
* [Alerting](https://etke.cc/scheduler#alert) functionality was added later on, based on integrating our monitoring system with Scheduler. Problems like "high disk usage" and others, now trigger emails and Matrix notifications which are sent both to our support staff and server owners. This feature has **prevented lots of downtime occurrences already**, by notifying customers before actual outages happen (knowing your disk usage is at 80% can make you act before it reaches 100% and causes downtime)

## Something new to play with

We've integrated 8 additional components into the Matrix stack. Some of them replaced existing ones - like `mautrix` bridges superseding unmaintained `mx-puppet` ones. Others are brand-new tools and services:

* (new) [`ntfy`](https://ntfy.sh) ([UnifiedPush](https://unifiedpush.org) provider)
* (new) `buscarron` (web forms to matrix bot)
* (new) `postmoogle` (email bridge)
* (new) [`borgbackup`](https://www.borgbackup.org/) (the "Holy Grail" of backups)
* `mautrix-slack` replaced `mx-puppet-slack`
* `mautrix-discord` replaced `mx-puppet-discord`
* `go-skype-bridge` replaced `mx-puppet-skype`
* `hookshot` replaced `appservice-webhooks` (webhooks/rss bridge)

If you already have old components installed (like mx-puppet-slack), you can use them as-is, until they break completely. If you wish to switch from a deprecated component to a new one - just [contact us](https://etke.cc/contacts/).

We now offer [Ntfy](https://ntfy.sh) and enable it by default for new installations. With Ntfy hosted on your server, you can use your personal UnifiedPush provider for delivering push notifications to your mobile phone. Your push notifications (even though they're normally E2E-encrypted) will no longer go through Google or Apple servers. [Full list of apps that support Ntfy](https://unifiedpush.org/users/apps/)

## The team is growing

etke.cc started as a one-man project 2 years ago, providing a service on top of the [matrix-docker-ansible-deploy](https://github.com/spantaleev/matrix-docker-ansible-deploy) Ansible playbook. The playbook is built in the open as [free software](https://en.wikipedia.org/wiki/Free_software) by [hundreds of contributors](https://github.com/spantaleev/matrix-docker-ansible-deploy/graphs/contributors).

etke.cc has provided a way for people to **host the open Matrix platform in an open, no-vendor-lock-in way**. We believe that this is a selling point that is **unique to etke.cc** among all of its competitors.

In the past year, [Slavi Pantaleev](https://github.com/spantaleev), the original playbook developer, has joined the etke.cc team.
This helps things in multiple ways:

- provides increased manpower to etke.cc, so that things will run more smoothly and more and better services can be added in the future
- guarantees etke.cc's stability, because all upstream Ansible playbook changes now consider etke.cc customers and usage-patterns
- etke.cc's profits trickle back into sponsoring playbook (and other Matrix-related) development

Besides him, a few other members have joined the team part-time. They help with website improvements, support request handling, etc.

etke.cc follows in the spirit of the playbook and companies who do their work in the open, as [Gitlab does](https://www.cnbc.com/2020/07/18/gitlab-worked-remotely-with-over-1000-employees-before-coronavirus.html). All the work we do is released as [free software](https://en.wikipedia.org/wiki/Free_software) in [our Gitlab repositories](https://gitlab.com/etke.cc)

## Open Source

* [Buscarron](https://gitlab.com/etke.cc/buscarron), a "web form to matrix bot", was created and evolved during that year. It's [etke.cc](https://etke.cc)'s back office for the order processing
* [Postmoogle](https://gitlab.com/etke.cc/postmoogle), an SMTP server with a Matrix interface instead of IMAP (read: email bridge), was created and deeply integrated into [etke.cc](https://etke.cc)'s processes. It handles 99% of etke.cc email communications
* [Scheduler](https://gitlab.com/etke.cc/int/scheduler), [Injector](https://gitlab.com/etke.cc/int/ansible-injector), [Bunny Uploader](https://gitlab.com/etke.cc/int/bunny-upload), and many more were developed as well

## Numbers!

* We've **installed 124 new Matrix servers**
* **Pushed 453 updates and enhancements** to the automation framework used as the service core
* **Posted 72 updates** in the announcements room, so you're always up-to-date with what we're working on

_Do you want to see what's changed? Here is [the 1 year celebration post](https://etke.cc/news/j-48qcbzgshru3ap7hictalntpwqtotgltct01mhrmw/)_


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$UPSyw4YkbTgMwHZ8K7UkLdx0zbBfq-Fh0iqI3lLiXi0?via=etke.cc&via=matrix.org)
