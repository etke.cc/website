---
id: '$M312ESIiL4hCT0-A2wEZsTsv-wknOf1NprajSmEa8Nk'
date: '2021-06-08 15:05:24.805 +0000 UTC'
title: '2021-06-08 15:05 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-06-08 15:05 UTC'
---

**severs updates**

* IRC (appservice) 0.26.0 -> 0.26.1
* Element 1.7.29 -> 1.7.30
* Miniflux 2.0.30 -> 2.0.31

_no synapse update, because only RC version has been released, waiting for stable release_

PS: If you want to discuss this update - #discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$M312ESIiL4hCT0-A2wEZsTsv-wknOf1NprajSmEa8Nk?via=etke.cc&via=matrix.org)
