---
id: '$tkb-xzPx0VO_90Ilc46udRUy_ZNW6OSbP3aiIQLxRFU'
date: '2023-04-03 07:20:25.135 +0000 UTC'
title: '2023-04-03 07:20 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-04-03 07:20 UTC'
---

**Outage is in progress**

- **What**: etke.host domain
- **Impact**: customers using etke.host as their matrix server's domain, etke.cc email communications
- **ETA**: unknown

Domain etke.host lost DNS records with type NS due to an unknown reason. The domain registrar has already been notified about the problem.
No more information is available at this moment.


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$tkb-xzPx0VO_90Ilc46udRUy_ZNW6OSbP3aiIQLxRFU?via=etke.cc&via=matrix.org)
