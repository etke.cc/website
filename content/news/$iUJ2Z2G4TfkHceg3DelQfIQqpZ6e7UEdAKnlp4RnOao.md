---
id: '$iUJ2Z2G4TfkHceg3DelQfIQqpZ6e7UEdAKnlp4RnOao'
date: '2022-12-25 13:56:07.72 +0000 UTC'
title: '2022-12-25 13:56 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-12-25 13:56 UTC'
---

**(hotfix/off cycle) stable updates published**

* Linkedin bridge 0.5.3 -> 0.5.4

_due to the nature of that update (hotfix), new features of v0.5.4 are not enabled in the stable release. If you want to get spaces support in Linkedin before the next stable update (next Friday) - [switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$iUJ2Z2G4TfkHceg3DelQfIQqpZ6e7UEdAKnlp4RnOao?via=etke.cc&via=matrix.org)
