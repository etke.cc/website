---
id: '$pHWZxqY-co_2bljtFeSyLlthmS4EPRwiruMJAq6xRoc'
date: '2023-09-19 19:42:44.633 +0000 UTC'
title: '2023-09-19 19:42 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-09-19 19:42 UTC'
---

**Servers Update In Progress (More Security Fixes)**

- Facebook bridge 0.5.0 -> 0.5.1
- Linkedin 0.5.4 -> latest
- Instagram bridge 0.3.0 -> 0.3.1
- Telegram bridge 0.14.1 -> 0.14.2
- Twitter bridge 0.1.6 -> 0.1.7
- WhatsApp bridge 0.10.1 -> 0.10.2

Those updates related to the same security issue with webp

[Details](https://github.com/advisories/GHSA-j7hp-h8jx-5ppr)

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$pHWZxqY-co_2bljtFeSyLlthmS4EPRwiruMJAq6xRoc?via=etke.cc&via=matrix.org)
