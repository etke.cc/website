---
id: '$XJltI8Bl2_NZW3azYu3HMNfH0r49X-MalzljEyB-2t4'
date: '2021-07-20 18:01:08.049 +0000 UTC'
title: '2021-07-20 18:01 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-07-20 18:01 UTC'
---

**servers updates**
- Reminder bot 0.2.0 -> 0.2.1 _server owners who had issues with reminder bot before that release - try to use it tomorrow (update will take some time)_
- Element 1.7.32 -> 1.7.33
- Hydrogen 0.2.0 -> 0.2.3
- Grafana 8.0.5 -> 8.0.6
- Jitsi 5765-1 -> 5963

Discuss the update in #discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$XJltI8Bl2_NZW3azYu3HMNfH0r49X-MalzljEyB-2t4?via=etke.cc&via=matrix.org)
