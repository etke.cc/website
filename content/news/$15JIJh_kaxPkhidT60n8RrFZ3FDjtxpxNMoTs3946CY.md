---
id: '$15JIJh_kaxPkhidT60n8RrFZ3FDjtxpxNMoTs3946CY'
date: '2022-05-03 17:09:57.07 +0000 UTC'
title: '2022-05-03 17:09 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-05-03 17:09 UTC'
---

**servers update in progress**

- Grafana 8.4.1 -> 8.5.1
- Synapse 1.57.1 -> 1.58.0
- WhatsApp bridge _latest_

---

**service updates**

- (for new orders only) Turnkey prices have been increased to reflect the setup cost
- etke.cc got localization support (only English and Russian are supported for now, Bulgarian will be added later)


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$15JIJh_kaxPkhidT60n8RrFZ3FDjtxpxNMoTs3946CY?via=etke.cc&via=matrix.org)
