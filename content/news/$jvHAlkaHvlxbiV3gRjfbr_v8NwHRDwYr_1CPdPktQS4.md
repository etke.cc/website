---
id: '$jvHAlkaHvlxbiV3gRjfbr_v8NwHRDwYr_1CPdPktQS4'
date: '2022-02-22 17:08:30.599 +0000 UTC'
title: '2022-02-22 17:08 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-02-22 17:08 UTC'
---

**servers update in progress**
- Element 1.10.3 -> 1.10.4
- Grafana 8.3.4 -> 8.4.1
- Signal (bridge) 0.2.2 -> 0.2.3
- Signal (daemon) 0.16.1 -> 0.17.0
- Telegram 0.11.1 -> 0.11.2
- Prometheus 2.33.1 -> 2.33.3
- Synapse-admin 0.8.4 -> 0.8.5
- Synapse 1.52.0 -> 1.53.0

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$jvHAlkaHvlxbiV3gRjfbr_v8NwHRDwYr_1CPdPktQS4?via=etke.cc&via=matrix.org)
