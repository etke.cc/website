---
id: '$NWYAqOHUDk6JX-C1nKs7uUgBAFIeZHosEngbuPXW2Ew'
date: '2021-11-02 17:43:02.066 +0000 UTC'
title: '2021-11-02 17:43 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-11-02 17:43 UTC'
---

**servers update in progress**

new versions:
- IRC (appservice) 0.31.0 -> 0.32.1
- IRC (heisenbridge) 1.3.0 -> 1.4.1
- Hydrogen 0.2.7 -> 0.2.19
- Grafana 8.1.4 -> 8.2.2
- #miounne:etke.cc 2.0.0 -> 2.1.0
- Prometheus 2.29.2 -> 2.30.3
- Synapse 1.45.1 -> 1.46.0

configuration changes:

- removed custom element web themes

_following changes applying only to federated matrix servers without explicit value(-s) set before (eg: for legal purposes) to increase users privacy a bit. If you don't want any of the following changes - send me a PM_
- redactions retention explicitly set to 5 minutes
- users IP log retention explicitly set to 5 minutes

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$NWYAqOHUDk6JX-C1nKs7uUgBAFIeZHosEngbuPXW2Ew?via=etke.cc&via=matrix.org)
