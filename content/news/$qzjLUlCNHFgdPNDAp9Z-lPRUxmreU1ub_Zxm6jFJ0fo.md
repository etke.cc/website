---
id: '$qzjLUlCNHFgdPNDAp9Z-lPRUxmreU1ub_Zxm6jFJ0fo'
date: '2023-10-26 19:48:05.304 +0000 UTC'
title: '2023-10-26 19:48 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-10-26 19:48 UTC'
---

**Stable Updates Published**

- Cinny 2.2.6 -> 3.0.0
- Element / [app.etke.cc](https://app.etke.cc/) 1.11.46 -> 1.11.47
- Grafana 10.1.5 -> 10.2.0
- Nginx 1.25.2 -> 1.25.3
- Synapse 1.94.0 -> 1.95.0

This update fixes the recent Jitsi auth issues, but the fix may require reset of the existing internal jitsi accounts if you use internal jitsi auth.

**Service Updates: Embrace the Matrix Federation directly from Element Web**

Starting with this update, the Element Web app we offer on [app.etke.cc](https://app.etke.cc/) and install on customers' servers includes native integration with the #mrs:etke.cc project - a fully-featured Matrix rooms search engine working across federation - using the [MatrixRooms.info](https://matrixrooms.info/) demo instance. That means, you can find brilliant Matrix communities from other matrix servers without even leaving your client app.

> Note: #mrs:etke.cc implementation of Matrix Federation API is in testing phase

**Service Updates: [MSC1929](https://github.com/matrix-org/matrix-spec-proposals/pull/1929) enabled by default for new customers**

Matrix admin contacts MSC provides a way to contact server owners in case of issues. From now on, we enable MSC1929 integration by default for all new customers, leaving existing customers unaffected (you can request enabling MSC1929 for your server using [etke.cc/contacts](https://etke.cc/contacts/)):

- If base domain is served from your matrix server, then you only need to ask us to enable integration.
- If you're serving base domain from any other server, you will need to set up one more redirect, for the new `/.well-known/matrix/support` file (`https://your-server.com/.well-known/matrix/support` -> `https://matrix.your-server.com/.well-known/matrix/support`). This redirect should be configured the same way you've already configured redirects for the `client` and `server` files.

In any case, if you'd like to adjust your contact information (Matrix ID or email address) which will be published in the new /.well-known/matrix/support file, please let us know.

**Service Updates: [Scheduler](https://etke.cc/help/extras/scheduler)'s `ping` [MSC1929](https://github.com/matrix-org/matrix-spec-proposals/pull/1929) integration**

`run ping` of [etke.cc/scheduler](https://etke.cc/help/extras/scheduler) got MSC1929 integration as well, so it will check if the support file is valid and not empty, but check's status won't be considered as failed because MSC1929 is completely optional.

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$qzjLUlCNHFgdPNDAp9Z-lPRUxmreU1ub_Zxm6jFJ0fo?via=etke.cc&via=matrix.org)
