---
id: '$5ZRiNOroskDIh9Qh8zlZjTI3bveQlyhV6C-aNwxS0tY'
date: '2021-08-03 17:04:18.435 +0000 UTC'
title: '2021-08-03 17:04 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-08-03 17:04 UTC'
---

**servers updates**
- ma1sd 2.4.0 -> 2.5.0
- Synapse 1.38.1 -> 1.39.0
- LanguageTool 5.3 -> 5.4
- #miounne:etke.cc got matrix-registration integration (invite tokens management)

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$5ZRiNOroskDIh9Qh8zlZjTI3bveQlyhV6C-aNwxS0tY?via=etke.cc&via=matrix.org)
