---
id: '$7qvwRG94s0Wvj7cGgAut1F_BNd6Jo8CcvhHadfTlsVQ'
date: '2021-07-01 09:51:35.614 +0000 UTC'
title: '2021-07-01 09:51 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-07-01 09:51 UTC'
---

There is announcement about yesterday update on Matrix blog: https://matrix.org/blog/2021/06/30/security-update-synapse-1-37-1-released

Homeserver owners with open registration, review your users in synapse-admin, please

PS: I added RSS feed of matrix.org blog to this room, so you will see such updates as soon as they posted


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$7qvwRG94s0Wvj7cGgAut1F_BNd6Jo8CcvhHadfTlsVQ?via=etke.cc&via=matrix.org)
