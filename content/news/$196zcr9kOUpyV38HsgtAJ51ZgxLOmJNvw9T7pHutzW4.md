---
id: '$196zcr9kOUpyV38HsgtAJ51ZgxLOmJNvw9T7pHutzW4'
date: '2021-09-13 16:12:42.87 +0000 UTC'
title: '2021-09-13 16:12 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-09-13 16:12 UTC'
---

Note to F-droid users: there is unofficial repo with automated builds of element Android development branch: [fdroid.krombel.de](https://fdroid.krombel.de/element-dev-fdroid/fdroid/repo?fingerprint=FD146EF30FA9F8F075BDCD9F02F069D22061B1DF7CC90E90821750A7184BF53D) (maintained by @krombel:msg-net.de)

That repo contains fixed version of the app.


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$196zcr9kOUpyV38HsgtAJ51ZgxLOmJNvw9T7pHutzW4?via=etke.cc&via=matrix.org)
