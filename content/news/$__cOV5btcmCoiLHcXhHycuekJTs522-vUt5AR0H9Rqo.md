---
id: '$__cOV5btcmCoiLHcXhHycuekJTs522-vUt5AR0H9Rqo'
date: '2022-09-28 15:48:30.459 +0000 UTC'
title: '2022-09-28 15:48 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-09-28 15:48 UTC'
---


**servers update in progress (security fix)**

- Element 1.11.5 -> 1.11.8
- Cinny 2.2.0 -> 2.2.1

> off cycle deployment on all customers' servers with the fixes for CVE-2022-39249, CVE-2022-39250, CVE-2022-39251, CVE-2022-39236

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$__cOV5btcmCoiLHcXhHycuekJTs522-vUt5AR0H9Rqo?via=etke.cc&via=matrix.org)
