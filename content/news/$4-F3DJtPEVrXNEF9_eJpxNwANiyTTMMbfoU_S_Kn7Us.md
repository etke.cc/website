---
id: '$4-F3DJtPEVrXNEF9_eJpxNwANiyTTMMbfoU_S_Kn7Us'
date: '2023-06-08 20:18:09.601 +0000 UTC'
title: '2023-06-08 20:18 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-06-08 20:18 UTC'
---

**Stable Updates Published**

- Borgmatic 1.7.13 -> 1.7.14
- Element / [app.etke.cc](https://app.etke.cc/) 1.11.31 -> 1.11.32
- Grafana 9.5.2 -> 9.5.3
- Hookshot 4.1.0 -> 4.2.0
- Synapse 1.84.1 -> 1.85.2

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$4-F3DJtPEVrXNEF9_eJpxNwANiyTTMMbfoU_S_Kn7Us?via=etke.cc&via=matrix.org)
