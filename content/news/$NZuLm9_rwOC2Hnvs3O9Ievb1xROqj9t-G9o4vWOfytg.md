---
id: '$NZuLm9_rwOC2Hnvs3O9Ievb1xROqj9t-G9o4vWOfytg'
date: '2022-12-23 08:22:29.462 +0000 UTC'
title: '2022-12-23 08:22 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-12-23 08:22 UTC'
---

**stable updates published**

- Element / [app.etke.cc](https://app.etke.cc/) 1.11.16 -> 1.11.17
- Grafana 9.3.1 -> 9.3.2
- Hydrogen 0.3.5 -> 0.3.6
- Nginx 1.23.2 -> 1.23.3
- Prometheus 2.40.7 -> 2.41.0
- Redis 7.0.6 -> 7.0.7
- Synapse 1.73.0 -> 1.74.0
- WhatsApp bridge 0.7.2 -> 0.8.0

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$NZuLm9_rwOC2Hnvs3O9Ievb1xROqj9t-G9o4vWOfytg?via=etke.cc&via=matrix.org)
