---
id: '$MDCWYtyM1Y-cKTZ-r08gjwZvYMxtIFUUd5OQP0LJPfE'
date: '2022-01-18 16:21:29.379 +0000 UTC'
title: '2022-01-18 16:21 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-01-18 16:21 UTC'
---

Note on Synapse 1.50.1 release

that release contains only one fix for an issue that doesn't affect any etke.cc subscribers, so it will not be installed during current maintenance


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$MDCWYtyM1Y-cKTZ-r08gjwZvYMxtIFUUd5OQP0LJPfE?via=etke.cc&via=matrix.org)
