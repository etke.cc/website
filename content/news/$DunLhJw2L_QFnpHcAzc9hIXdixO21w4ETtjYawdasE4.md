---
id: '$DunLhJw2L_QFnpHcAzc9hIXdixO21w4ETtjYawdasE4'
date: '2023-08-31 20:02:07.513 +0000 UTC'
title: '2023-08-31 20:02 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-08-31 20:02 UTC'
---

**Stable Updates Published**

- Element / [app.etke.cc](https://app.etke.cc/) 1.11.39 -> 1.11.40
- SchildiChat 1.11.30-sc.2 _new_
- Grafana 10.0.3 -> 10.1.0
- Sliding Sync 0.99.5 -> 0.99.7
- Synapse 1.90.0 -> 1.91.0
- Uptime Kuma 1.23.0 -> 1.23.1

_That update package contains fixes for several synapse bugs related to rapidly increasing disk utilization. We recommend that you take advantage of the 'run maintenance' of [etke.cc/scheduler](https://etke.cc/scheduler) as soon as possible._

---

**Service Updates: New Component**

The new [SchildiChat](https://schildi.chat/) web app is available for order for new customers, existing customers can request it using [etke.cc/contacts](https://etke.cc/contacts).

SchildiChat Web is a (catching up) Element Web fork with additional neat features. While it is 10 versions behind the upstream, it still has way more customization options.

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$DunLhJw2L_QFnpHcAzc9hIXdixO21w4ETtjYawdasE4?via=etke.cc&via=matrix.org)
