---
id: '$AW9XwldEBMXT-ZeHe20nJ7oGvJvqzctKv9FJ3CRLNug'
date: '2022-03-15 18:34:25.603 +0000 UTC'
title: '2022-03-15 18:34 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-03-15 18:34 UTC'
---

**servers update in progress**

- Element 1.10.6 -> 1.10.7
- Cinny 1.8.0 -> 1.8.1
- Miniflux 2.0.35 -> 2.0.36

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$AW9XwldEBMXT-ZeHe20nJ7oGvJvqzctKv9FJ3CRLNug?via=etke.cc&via=matrix.org)
