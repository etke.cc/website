---
id: '$iFg6Z7I7Znln9Z1XV6NTdyynRbVW4fLuQgfP9YfIRME'
date: '2024-01-05 09:40:46.135 +0000 UTC'
title: '2024-01-05 09:40 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-01-05 09:40 UTC'
---

**Stable Updates Published**

- Element / [app.etke.cc](https://app.etke.cc/) 1.11.52 -> 1.11.53
- Etherpad 1.9.5 -> 1.9.6
- Hookshot 5.0.0 -> 5.1.2
- Signal bridge _new_
- Uptime Kuma 1.23.10 -> 1.23.11

**Service Updates: Signal bridge was replaced**

Signal bridge has been replaced with the new golang implementation. While the new bridge implementation works as drop-in replacement, you will need to relink your devices after installation. The new bridge implementation doesn't have an option to link matrix account as primary device (i.e. register with it in Signal)

**Service Updates: Signal contact option was removed**

Due to the bridge updates mentioned above, we no longer support Signal contact method, all other [contacts](https://etke.cc/contacts/) remains unchanged

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$iFg6Z7I7Znln9Z1XV6NTdyynRbVW4fLuQgfP9YfIRME?via=etke.cc&via=matrix.org)
