---
id: '$DFdfmIv5jtzzjr28FHU3LOynaIe7xlXab319o1H89jg'
date: '2022-02-04 05:32:34.479 +0000 UTC'
title: '2022-02-04 05:32 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-02-04 05:32 UTC'
---

FOSDEM'22 will start on February 5 (tomorrow).


FOSDEM is a free event for software developers to meet, share ideas and collaborate.

Every year, thousands of developers of free and open source software from all over the world gather at the event in Brussels.

Check the schedule on fosdem.org and join the #fosdem2022:fosdem.org space.

Space structure may be a bit confusing, but keep in mind that each talk will have own matrix room. 

For example, matrix stand is #matrix-stand:fosdem.org and dev room is #matrix-devroom:fosdem.org 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$DFdfmIv5jtzzjr28FHU3LOynaIe7xlXab319o1H89jg?via=etke.cc&via=matrix.org)
