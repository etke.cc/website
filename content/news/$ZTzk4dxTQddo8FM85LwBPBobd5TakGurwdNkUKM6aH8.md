---
id: '$ZTzk4dxTQddo8FM85LwBPBobd5TakGurwdNkUKM6aH8'
date: '2022-05-10 16:18:39.783 +0000 UTC'
title: '2022-05-10 16:18 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-05-10 16:18 UTC'
---

**servers update in progress**

- #buscarron:etke.cc 1.0.0 -> 1.1.0
- Cinny 1.8.2 -> 2.0.0
- Discord bridge latest -> 0.1.1 _pin_
- Element 1.10.11 -> 1.10.12
- Synapse 1.58.0 -> 1.58.1

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$ZTzk4dxTQddo8FM85LwBPBobd5TakGurwdNkUKM6aH8?via=etke.cc&via=matrix.org)
