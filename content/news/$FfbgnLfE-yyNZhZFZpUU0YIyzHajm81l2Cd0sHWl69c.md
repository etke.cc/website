---
id: '$FfbgnLfE-yyNZhZFZpUU0YIyzHajm81l2Cd0sHWl69c'
date: '2022-06-24 11:10:41.351 +0000 UTC'
title: '2022-06-24 11:10 UTC'
author: '@slavi:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-06-24 11:10 UTC'
---

**servers update completed**

- Buscarron 1.1.0 -> 1.2.0
- Uptime-Kuma 1.16.1 -> 1.17.1

---

Our **new maintenance system** ([Scheduler](https://etke.cc/scheduler)), which has been in development and testing for a while, is now **out of closed beta**. Thanks to everyone who helped during the testing period! 🎉

The new automated system **maintains the servers** of all etke.cc customers, according to their **preferred maintenance schedule** and **server-stability setting**.
All existing customers have been assigned a random server maintenance schedule, but are encouraged to talk to the Scheduler Bot and pick their own convenient maintenance schedule (day of the week + time).

Besides doing automated maintenance, the Scheduler Bot lets you run maintenance manually (whenever you'd like), as well as perform various tasks on your server (restart bridges, etc).

Check the [Scheduler documentation](https://etke.cc/scheduler) for more details!

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$FfbgnLfE-yyNZhZFZpUU0YIyzHajm81l2Cd0sHWl69c?via=etke.cc&via=matrix.org)
