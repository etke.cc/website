---
id: '$YdMLeD5qF8Hl8D3uQj_UskA1aahNWev58q2xJSe3Udc'
date: '2023-10-10 14:04:22.46 +0000 UTC'
title: '2023-10-10 14:04 UTC'
author: '@slavi:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-10-10 14:04 UTC'
---

**Servers Update In Progress (Security Fix)**

- Synapse 1.93.0 -> 1.94.0

> Fix for a moderate severity security issue for the Synapse homeserver component.

[Details](https://github.com/matrix-org/synapse/security/advisories/GHSA-5chr-wjw5-3gq4)

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$YdMLeD5qF8Hl8D3uQj_UskA1aahNWev58q2xJSe3Udc?via=etke.cc&via=matrix.org)
