---
id: '$XWfk1BgfOpH-rjxc9VOmJl6bHaMt2YzLQDUYGHfokCE'
date: '2021-09-13 15:53:17.902 +0000 UTC'
title: '2021-09-13 15:53 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-09-13 15:53 UTC'
---

Due to security fix, etke.cc subscribers' servers will be updated today instead of tomorrow, an announcement with updated components will be posted later today.


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$XWfk1BgfOpH-rjxc9VOmJl6bHaMt2YzLQDUYGHfokCE?via=etke.cc&via=matrix.org)
