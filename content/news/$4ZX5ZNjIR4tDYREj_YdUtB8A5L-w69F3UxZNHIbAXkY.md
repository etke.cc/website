---
id: '$4ZX5ZNjIR4tDYREj_YdUtB8A5L-w69F3UxZNHIbAXkY'
date: '2022-10-21 05:11:14.16 +0000 UTC'
title: '2022-10-21 05:11 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-10-21 05:11 UTC'
---

**stable updates published**

- Grafana 9.2.0 -> 9.2.1
- Miniflux 2.0.38 -> 2.0.39
- Synapse 1.68.0 -> 1.69.0
- WhatsApp bridge 0.7.0 -> 0.7.1

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_


#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$4ZX5ZNjIR4tDYREj_YdUtB8A5L-w69F3UxZNHIbAXkY?via=etke.cc&via=matrix.org)
