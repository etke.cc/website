---
id: '$92ng3pe2b_cwsCx6DOH6GT_Tx-aDmYIqD2eRTCTG7Z8'
date: '2021-06-11 18:30:58.621 +0000 UTC'
title: '2021-06-11 18:30 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-06-11 18:30 UTC'
---

https://matrix.org/blog/2021/06/11/this-week-in-matrix-2021-06-11


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$92ng3pe2b_cwsCx6DOH6GT_Tx-aDmYIqD2eRTCTG7Z8?via=etke.cc&via=matrix.org)
