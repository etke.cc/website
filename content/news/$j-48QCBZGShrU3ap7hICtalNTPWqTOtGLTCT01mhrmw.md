---
id: '$j-48QCBZGShrU3ap7hICtalNTPWqTOtGLTCT01mhrmw'
date: '2022-02-12 09:12:16.032 +0000 UTC'
title: '2022-02-12 09:12 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-02-12 09:12 UTC'
---

🎉 **Today marks 1 year of the etke.cc!** 🎉

During that year, we did:
- Pushed 234 updates and enhancements to the automation framework used as the service core
- Integrated 17 additional components to the matrix stack
- Developed 5 bots and tools to extend matrix capabilities
- Installed 92 new matrix servers
- Helped 172 people and organizations to achieve their goals in the matrix
- Posted 74 updates in the announcements room

Some history:
- the project (not service yet) started on February 12, 2021
- the first installed server was etke.cc itself (yes, etke.cc homeserver is a customer of etke.cc service from the day 1)
- the second installed server was a chatbot, that uses matrix as a platform to interact with users across different chat networks (Telegram, WhatsApp, Signal, etc.) - one API to rule them all.
- In May 2021 the service was published globally

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$j-48QCBZGShrU3ap7hICtalNTPWqTOtGLTCT01mhrmw?via=etke.cc&via=matrix.org)
