---
id: '$nD17EkFZrvBeXIBTsjFkJoe0GOGa4IPIo59thlkkGJw'
date: '2023-02-02 21:05:31.27 +0000 UTC'
title: '2023-02-02 21:05 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-02-02 21:05 UTC'
---

**Stable Updates Published**

- Borgmatic 1.7.5 -> 1.7.6
- Cinny 2.2.3 -> 2.2.4
- Coturn 4.6.1-r0 -> 4.6.1-r1
- Discord bridge latest -> 0.1.0 _pin_
- Element 1.11.20 -> 1.11.22
- Jitsi stable-8218 -> stable-8252
- Miniflux 2.0.41 -> 2.0.42
- Prometheus 2.41.0 -> 2.42.0
- Soft-Serve 0.4.4 -> 0.4.5
- Synapse Admin 0.8.5 -> 0.8.6
- Synapse 1.75.0 -> 1.76.0

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$nD17EkFZrvBeXIBTsjFkJoe0GOGa4IPIo59thlkkGJw?via=etke.cc&via=matrix.org)
