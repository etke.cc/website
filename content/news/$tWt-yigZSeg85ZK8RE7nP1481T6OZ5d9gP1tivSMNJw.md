---
id: '$tWt-yigZSeg85ZK8RE7nP1481T6OZ5d9gP1tivSMNJw'
date: '2021-12-13 16:10:05.319 +0000 UTC'
title: '2021-12-13 16:10 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-12-13 16:10 UTC'
---

**off cycle servers update in progress**

due to security fix in matrix-js-sdk (part of Element web)

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$tWt-yigZSeg85ZK8RE7nP1481T6OZ5d9gP1tivSMNJw?via=etke.cc&via=matrix.org)
