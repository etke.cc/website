---
id: '$K9NZGHPy9UA-TmtZKYfcKVZS5XXj-OEZOKfB2Qlrtls'
date: '2022-02-01 18:43:41.55 +0000 UTC'
title: '2022-02-01 18:43 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-02-01 18:43 UTC'
---

**servers update in progress**
- #honoroit:etke.cc 0.9.3 -> 0.9.4
- Mjolnir 1.2.1 -> 1.3.1
- Cinny 1.6.1 -> 1.7.0
- Element 1.9.9 -> 1.10.1
- Hydrogen 0.2.23 -> 0.2.25
- Corporal 2.2.2 -> 2.2.3
- Exim 4.95-r0 -> 4.95-r0-2
- Facebook 0.3.2 -> 0.3.3
- Radicale 3.1.2.0 -> 3.1.3.0

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$K9NZGHPy9UA-TmtZKYfcKVZS5XXj-OEZOKfB2Qlrtls?via=etke.cc&via=matrix.org)
