---
id: '$xCecYmYAgXnXOYKTJGiJgcVNQ9rQbMic38BFeuoFATs'
date: '2023-02-16 21:11:47.73 +0000 UTC'
title: '2023-02-16 21:11 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-02-16 21:11 UTC'
---

**Stable Updates Published**

- Coturn 4.6.1-r1 -> 4.6.1-r2
- Discord bridge 0.1.0 -> 0.1.1
- Element 1.11.22 -> 1.11.23
- Hydrogen 0.3.6 -> 0.3.8
- Uptime Kuma 1.19.6 -> 1.20.1
- Languagetool 6.0-dockerupdate-2 -> 6.0-dockerupdate-3
- Ntfy 1.30.1 -> 1.31.0
- #postmoogle:etke.cc 0.9.12 -> 0.9.14
- Synapse 1.76.0 -> 1.77.0
- WhatsApp bridge 0.8.1 -> 0.8.2

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

**Service Updates**

- [The Scheduler](https://etke.cc/) log output in case of maintenance issues is more readable now

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$xCecYmYAgXnXOYKTJGiJgcVNQ9rQbMic38BFeuoFATs?via=etke.cc&via=matrix.org)
