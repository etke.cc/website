---
id: '$K8IuHH3X5uF7n0qXMgbCaiVHBl6yKfiOh1ScnLjrbao'
date: '2022-12-16 08:02:59.332 +0000 UTC'
title: '2022-12-16 08:02 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-12-16 08:02 UTC'
---

**stable updates published**

- Instagram bridge 0.2.2 -> 0.2.3
- Jitsi stable-8044 -> stable-8138-1
- Exim 4.95-r0-4 -> 4.96-r1-0
- Miniflux 2.0.40 -> 2.0.41
- Prometheus 2.40.5 -> 2.40.7
- Redis 7.0.4 -> 7.0.6

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

**service updates**

- The **[etke.cc](https://etke.cc) website got redesigned**. It now has 2 themes (light and dark) and the ability to pick the correct one based on system preferences. The [Order Form](https://etke.cc/order/) has been reworked - it's now more beautiful and simpler. Feedback is welcome!
- We now **support hosting Bring-Your-Own-Server orders on our own domain** (Domain Type = Subdomain). This means that if you have a spare computer to run Matrix on, you can do it without buying a domain name. Claim your `CUSTOM.etke.host` domain now!
---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$K8IuHH3X5uF7n0qXMgbCaiVHBl6yKfiOh1ScnLjrbao?via=etke.cc&via=matrix.org)
