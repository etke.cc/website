---
id: '$h3INxWjVLkt0yN7H1mcXDthV2SEr4tsVoNg9cfRYbPA'
date: '2022-09-30 06:04:11.952 +0000 UTC'
title: '2022-09-30 06:04 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-09-30 06:04 UTC'
---

**stable updates published**

- Cinny 2.2.0 -> 2.2.2
- Element 1.11.5 -> 1.11.8
- Jitsi stable-7648-4 -> stable-7830
- Languagetool 5.8 -> 5.9
- Ntfy 1.27.2 -> 1.28.0
- #postmoogle:etke.cc 0.9.3 -> 0.9.4
- Prometheus node exporter 1.3.1 -> 1.4.0
- Synapse 1.67.0 -> 1.68.0
- Telegram bridge 0.12.0 -> 0.12.1

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/scheduler#fresh-testing)_

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$h3INxWjVLkt0yN7H1mcXDthV2SEr4tsVoNg9cfRYbPA?via=etke.cc&via=matrix.org)
