---
id: '$xgDRRCQ37cRvZmWoyDFTyYVciYoWEiLh3e92qZ7XiVo'
date: '2023-06-30 07:45:29.265 +0000 UTC'
title: '2023-06-30 07:45 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-06-30 07:45 UTC'
---

**Stable Updates Published**

- Borgmatic 1.7.14 -> 1.7.15
- Grafana 9.5.3 -> 10.0.1
- Miniflux 2.0.44 -> 2.0.45
- Ntfy 2.5.0 -> 2.6.1
- Prometheus Postgres exporter 0.12.0 -> 0.13.1
- Prometheus 2.44.0 -> 2.45.0
- Telegram bridge 0.14.0 -> 0.14.1
- Uptime Kuma 1.21.3 -> 1.22.0

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$xgDRRCQ37cRvZmWoyDFTyYVciYoWEiLh3e92qZ7XiVo?via=etke.cc&via=matrix.org)
