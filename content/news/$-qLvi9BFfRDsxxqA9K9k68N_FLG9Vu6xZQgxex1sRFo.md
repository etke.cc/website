---
id: '$-qLvi9BFfRDsxxqA9K9k68N_FLG9Vu6xZQgxex1sRFo'
date: '2021-12-13 15:24:21.7 +0000 UTC'
title: '2021-12-13 15:24 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-12-13 15:24 UTC'
---

> <@_neb_rssbot_=40aine=3aetke.cc:matrix.org> matrix.org: Disclosure: buffer overflow in libolm and matrix-js-sdk ( https://matrix.org/blog/2021/12/13/disclosure-buffer-overflow-in-libolm-and-matrix-js-sdk )

Element web 1.9.7 is **not released** at this moment, waiting for the release...


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$-qLvi9BFfRDsxxqA9K9k68N_FLG9Vu6xZQgxex1sRFo?via=etke.cc&via=matrix.org)
