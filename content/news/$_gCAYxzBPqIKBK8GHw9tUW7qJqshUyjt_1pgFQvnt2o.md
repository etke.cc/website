---
id: '$_gCAYxzBPqIKBK8GHw9tUW7qJqshUyjt_1pgFQvnt2o'
date: '2023-08-10 20:00:21.064 +0000 UTC'
title: '2023-08-10 20:00 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-08-10 20:00 UTC'
---

**Stable Updates Published**

- Element 1.11.37 -> 1.11.38
- Sliding Sync proxy 0.99.5  _new_

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

**Service Updates: New component**

As [Element X](https://element.io/labs/element-x) is becoming more usable, at [etke.cc](https://etke.cc/) we'd like to help push the future of Matrix forward.
For this reason, we've decided to add Sliding Sync support so that customers who wish to experiment with new technology can give it a try.

For iOS, there's a [TestFlight](https://developer.apple.com/testflight/) ([enroll here](https://testflight.apple.com/join/uZbeZCOi)). The iOS version is mostly usable and has basic functionality like sending messages, photos, videos, locations, and reactions, but it does not support threads or many other features. [Nightly APK builds](https://github.com/vector-im/element-x-android/blob/develop/docs/nightly_build.md) are available for the Android version, which is even less feature-rich.

Element X is still under heavy development, so it is expected that there will be bugs and missing features. It shouldn't cause data loss or break your conversations/rooms, as they appear in other clients. However, problems are still expected.

**Service Updates: New Domain**

The new `onmatrix.chat` domain is available for order for new customers. Now you can get a matrix server with MXIDs like `@john:doe.onmatrix.chat` with no extra fee

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$_gCAYxzBPqIKBK8GHw9tUW7qJqshUyjt_1pgFQvnt2o?via=etke.cc&via=matrix.org)
