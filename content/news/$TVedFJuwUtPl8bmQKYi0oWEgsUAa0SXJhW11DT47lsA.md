---
id: '$TVedFJuwUtPl8bmQKYi0oWEgsUAa0SXJhW11DT47lsA'
date: '2023-04-20 20:05:18.736 +0000 UTC'
title: '2023-04-20 20:05 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-04-20 20:05 UTC'
---

**Stable Updates Published**

- Borgmatic 1.7.11 -> 1.7.12
- Discord bridge 0.2.0 -> 0.3.0
- WhatsApp bridge 0.8.3 -> 0.8.4

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

**Service Updates**

- customers on [`fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing) migrated from nginx reverse proxy to traefik

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$TVedFJuwUtPl8bmQKYi0oWEgsUAa0SXJhW11DT47lsA?via=etke.cc&via=matrix.org)
