---
id: '$hJIMFutDG5WO4xyYBI9Bo2AEe-f3j8n5srbw8WFvZEY'
date: '2021-10-01 07:00:37.915 +0000 UTC'
title: '2021-10-01 07:00 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-10-01 07:00 UTC'
---

**servers notice**

etke.cc's ssh key has been rotated, new key (already added to subscribers' servers):

```
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJCknXeG3Dg6pnnNagtwuXqnjJpDuDaqxKkU/N9M65JC @aine:etke.cc
```

Website has been updated, new key will be available on https://etke.cc/ssh.key after CDN cache propagation

PS: Just to clarify - you don't need to do anything, etke.cc's ssh keys rotated every 3 months, that's an automated process. 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$hJIMFutDG5WO4xyYBI9Bo2AEe-f3j8n5srbw8WFvZEY?via=etke.cc&via=matrix.org)
