---
id: '$MqoRHhL6ImaAit2u7CD-xlEvzs2KGC2yuYqjMDH6I7k'
date: '2023-03-28 17:17:36.47 +0000 UTC'
title: '2023-03-28 17:17 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-03-28 17:17 UTC'
---

**Servers Update In Progress (Security Fix)**

- Element 1.11.25 -> Element 1.11.26

> The issues involve prototype pollution via events containing special strings in key locations, which can temporarily disrupt normal functioning of matrix-js-sdk and matrix-react-sdk, potentially impacting the consumer's ability to process data safely.

> Although we have only demonstrated a denial-of-service-style impact, we cannot completely rule out the possibility of a more severe impact due to the relatively extensive attack surface. We have therefore classified this as High severity and strongly recommend upgrading as a precautionary measure.

from: [matrix.org security announcement](https://matrix.org/blog/2023/03/28/security-releases-matrix-js-sdk-24-0-0-and-matrix-react-sdk-3-69-0)

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$MqoRHhL6ImaAit2u7CD-xlEvzs2KGC2yuYqjMDH6I7k?via=etke.cc&via=matrix.org)
