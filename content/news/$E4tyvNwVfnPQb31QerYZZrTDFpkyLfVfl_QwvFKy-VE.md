---
id: '$E4tyvNwVfnPQb31QerYZZrTDFpkyLfVfl_QwvFKy-VE'
date: '2023-09-21 20:04:41.72 +0000 UTC'
title: '2023-09-21 20:04 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-09-21 20:04 UTC'
---

**Stable Updates Published**

- Discord bridge 0.6.1 -> 0.6.2
- Facebook bridge 0.5.0 -> 0.5.1
- Google Messages bridge 0.1.0 -> 0.2.0
- Grafana 10.1.1 -> 10.1.2
- Instagram bridge 0.3.0 -> 0.3.1
- Jitsi 8922-1 -> stable-8960
- Linkedin 0.5.4 -> latest
- Miniflux 2.0.47 -> 2.0.48
- Prometheus Postgres Exporter 0.13.2 -> 0.14.0
- Synapse 1.92.1 -> 1.92.3
- Twitter bridge 0.1.6 -> 0.1.7
- Uptime Kuma 1.23.1 -> 1.23.2
- WhatsApp bridge 0.10.0 -> 0.10.2

_synapse and the vast majority of the bridges updates were already installed previously as part of off cycle security fixes_

---

**Service Updates: Customer Support is available now**

In the past, our subscription plans didn't include customer support. The [etke.cc/contacts](https://etke.cc/contacts) page on our website was mainly for getting in touch in cases of service issues.

But here's the deal: we've been spending a lot of developer time each month helping you with all sorts of questions, even some things we can't control. This has sometimes taken away from our core work of improving and maintaining our services.

To keep our Matrix hosting services affordable and sustainable, we've decided to be clear about what we can help you with. We've created the [etke.cc/services/support](https://etke.cc/services/support) page, where you can find all the details about customer support and what you can expect from us.

In a nutshell, all our subscription plans now include [the "Basic" support level](https://etke.cc/services/support/#basic), which gives you some reasonable customer assistance (up to 4 support requests per month). If you're a business customer, we're offering an even better ["Dedicated" support option](https://etke.cc/services/support/#dedicated), with faster response times and more support requests per month. Soon, when you visit our new order form on the website, you'll be able to choose your support plan instead of being stuck with basic support.

We appreciate your understanding, and we're committed to providing you with top-notch service.

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$E4tyvNwVfnPQb31QerYZZrTDFpkyLfVfl_QwvFKy-VE?via=etke.cc&via=matrix.org)
