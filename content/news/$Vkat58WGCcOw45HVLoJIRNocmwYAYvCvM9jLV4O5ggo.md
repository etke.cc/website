---
id: '$Vkat58WGCcOw45HVLoJIRNocmwYAYvCvM9jLV4O5ggo'
date: '2021-12-11 18:16:58.352 +0000 UTC'
title: '2021-12-11 18:16 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-12-11 18:16 UTC'
---

**off cycle servers update in progress**

Due to security fixes in signal daemon (not the bridge itself, but component of; uses log4j) and grafana (`CVE-2021-43798`)

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$Vkat58WGCcOw45HVLoJIRNocmwYAYvCvM9jLV4O5ggo?via=etke.cc&via=matrix.org)
