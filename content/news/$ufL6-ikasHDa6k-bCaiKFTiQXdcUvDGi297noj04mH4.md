---
id: '$ufL6-ikasHDa6k-bCaiKFTiQXdcUvDGi297noj04mH4'
date: '2022-07-29 07:51:56.797 +0000 UTC'
title: '2022-07-29 07:51 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-07-29 07:51 UTC'
---

**stable updates published**

- Kakotalk _new_
- Discord (mautrix) _new_
- Element 1.11.0 -> 1.11.1
- Grafana 9.0.4 -> 9.0.5
- Prometheus postgres exporter 0.10.1 -> 0.11.0

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/scheduler#fresh-testing)_

---

**service updates**

- [LanguageTool](https://languagetool.org/) component now supports multi-language n-grams (previously only `en` language supported n-grams). Supported languages: de, en, es, fr, he, it, nl, ru, zh
- Kakaotalk bridge avaialble for order on etke.cc
- Discord (mautrix) bridge replaces the old, unmaintained for a while, mx-puppet bridge for new customers. Existing servers are **not** affected, but you may request to replace the existing bridge on your server to the new one using the @support:etke.cc

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$ufL6-ikasHDa6k-bCaiKFTiQXdcUvDGi297noj04mH4?via=etke.cc&via=matrix.org)
