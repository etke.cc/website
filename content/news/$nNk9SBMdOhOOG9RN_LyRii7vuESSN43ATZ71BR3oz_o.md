---
id: '$nNk9SBMdOhOOG9RN_LyRii7vuESSN43ATZ71BR3oz_o'
date: '2023-04-04 11:18:09.256 +0000 UTC'
title: '2023-04-04 11:18 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-04-04 11:18 UTC'
---

**The outage has been resolved**

- **What**: etke.host domain
- **Impact**: customers using etke.host as their matrix server's domain, etke.cc email communications

Domain registry operator unsuspended the etke.host domain, all services using that domain name have been resolved


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$nNk9SBMdOhOOG9RN_LyRii7vuESSN43ATZ71BR3oz_o?via=etke.cc&via=matrix.org)
