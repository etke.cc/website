---
id: '$PW5915OV-yfnT10e1zxyIWmP3vtjY1xwTOn2ZQaVFN0'
date: '2023-10-05 20:00:48.499 +0000 UTC'
title: '2023-10-05 20:00 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-10-05 20:00 UTC'
---

**Stable Updates Published**

- Borg 1.2.4 -> 1.2.6
- Element / [app.etke.cc](https://app.etke.cc/) 1.11.44 -> 1.11.45
- Google Chat bridge 0.5.0 -> 0.5.1
- Grafana 10.1.2 -> 10.1.4
- #postmoogle:etke.cc 0.9.15 -> 0.9.16
- Prometheus 2.47.0 -> 2.47.1

---

**Service Updates: New Domain Name**

The new `kupo.email` joins the `etke.host` and `onmatrix.chat` domains available for order for new customers. Now you can get a matrix server with MXIDs like `@postmoogle:fan.kupo.email` with no extra fee

**Service Updates: [MatrixRooms.info](https://matrixrooms.info/) Got [MSC1929](https://github.com/matrix-org/matrix-spec-proposals/pull/1929) Integration**

This is the first matrix project that integrates the admin contacts proposal at scale. Currently, it works with email addresses only, so when a room is reported on [MatrixRooms.info](https://matrixrooms.info/), the report details will be sent to the homeserver owner of the room's server if that server provides email address(-es) in the support  file

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$PW5915OV-yfnT10e1zxyIWmP3vtjY1xwTOn2ZQaVFN0?via=etke.cc&via=matrix.org)
