---
id: '$EaCKIwz3QVdAA_u0G-KGavsmhLq9mjDlZm9fpqEdfjI'
date: '2024-02-22 21:32:48.975 +0000 UTC'
title: '2024-02-22 21:32 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-02-22 21:32 UTC'
---

**Stable Updates Published**

- GoToSocial 0.13.0 -> 0.13.3
- Hookshot 5.1.2 -> 5.2.1
- Miniflux 2.0.51 -> 2.1.0
- Signal bridge 0.5.0 _first actual realease of the go-based bridge_

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$EaCKIwz3QVdAA_u0G-KGavsmhLq9mjDlZm9fpqEdfjI?via=etke.cc&via=matrix.org)
