---
id: '$H6T0c8EqCptIzeYSbKJaFvGBz6yOOUfSc7QJJSoPQl4'
date: '2021-06-25 18:07:09.514 +0000 UTC'
title: '2021-06-25 18:07 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-06-25 18:07 UTC'
---

This Week In Matrix digest: https://matrix.org/blog/2021/06/25/this-week-in-matrix-2021-06-25/

The news & discussions of TWIM items take place in #thisweekinmatrix:matrix.org room


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$H6T0c8EqCptIzeYSbKJaFvGBz6yOOUfSc7QJJSoPQl4?via=etke.cc&via=matrix.org)
