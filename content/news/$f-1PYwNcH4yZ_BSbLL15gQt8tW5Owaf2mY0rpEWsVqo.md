---
id: '$f-1PYwNcH4yZ_BSbLL15gQt8tW5Owaf2mY0rpEWsVqo'
date: '2023-11-16 21:12:54.873 +0000 UTC'
title: '2023-11-16 21:12 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-11-16 21:12 UTC'
---

**Stable Updates Published**

- Element / [app.etke.cc](https://app.etke.cc/) 1.11.48 -> 1.11.49
- Grafana 10.2.0 -> 10.2.1
- Jitsi stable-8960-1 -> stable-9078
- Miniflux 2.0.49 -> 2.0.50
- Prometheus node exporter 1.6.1 -> 1.7.0
- Prometheus 2.47.2 -> 2.48.0
- Uptime Kuma 1.23.3 -> 1.23.4

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$f-1PYwNcH4yZ_BSbLL15gQt8tW5Owaf2mY0rpEWsVqo?via=etke.cc&via=matrix.org)
