---
id: '$iwCSug9ihwtbk-LPR2P4tqDanDVXerMJr1e_C4qObds'
date: '2024-07-04 20:00:09.461 +0000 UTC'
title: '2024-07-04 20:00 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-07-04 20:00 UTC'
---

**Stable Updates Published**

- [Jitsi](https://github.com/jitsi/docker-jitsi-meet): [stable-9457-2](https://github.com/jitsi/docker-jitsi-meet/releases/tag/stable-9457-2) -> [stable-9584](https://github.com/jitsi/docker-jitsi-meet/releases/tag/stable-9584)
- [Sliding Sync](https://github.com/matrix-org/sliding-sync): [v0.99.18](https://github.com/matrix-org/sliding-sync/releases/tag/v0.99.18) -> [v0.99.19](https://github.com/matrix-org/sliding-sync/releases/tag/v0.99.19)
- [Synapse](https://github.com/element-hq/synapse): [v1.109.0](https://github.com/element-hq/synapse/releases/tag/v1.109.0) -> [v1.110.0](https://github.com/element-hq/synapse/releases/tag/v1.110.0)

**Service Updates: [GoToSocial](https://etke.cc/help/extras/gotosocial) S3 Support**

GoToSocial supports S3 storage, and starting this week, we added optional s3 configuration to the GoToSocial component [on the order form](https://etke.cc/order/) for new customers. Existing customers may request s3 storage by [contacting us](https://etke.cc/contacts/)

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$iwCSug9ihwtbk-LPR2P4tqDanDVXerMJr1e_C4qObds?via=etke.cc&via=matrix.org)
