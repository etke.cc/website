---
id: '$POLYYsFzdJzLvdx_wDU_5l-8FHqqyvwjk4gsBlZGo3c'
date: '2021-12-18 23:46:12.946 +0000 UTC'
title: '2021-12-18 23:46 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-12-18 23:46 UTC'
---

we're starting to use #honoroit:etke.cc   for support and announcements automation. Nothing really changed, so consider that as "field test" of a new technology (of course it will be available to order when it will be stable enough)


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$POLYYsFzdJzLvdx_wDU_5l-8FHqqyvwjk4gsBlZGo3c?via=etke.cc&via=matrix.org)
