---
id: '$iYIYNmFQz1J5NSfCoZy-Ox0AULCkhIazlkiNGCnCLpg'
date: '2021-09-21 16:47:26.204 +0000 UTC'
title: '2021-09-21 16:47 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-09-21 16:47 UTC'
---

**servers update in progress**

new versions:
- IRC (appservice) 0.30.0 -> 0.31.0
- IRC (heisenbridge) 1.0.1 -> 1.1.1
- Mjolnir 0.1.19 -> 1.1.20 _+changed versioning scheme_
- Element 1.8.4 -> 1.8.5
- Grafana 8.1.3 -> 8.1.4
- Nginx 1.21.1 -> 1.21.3
- Synapse 1.42.0 -> 1.43.0

**service updates**

We start offering hosting options for people and organizations who don't want to setup their own VPS, based on Hetzner with following locations: Germany and Finland. Right now, 3 plans available - small ( $15 ), medium ( $18 ) and big ( $23 ), all plans include following features:
- VPS itself
- Maintenance
- Email services for domain
- Provider-level firewall (rules are the same as on-server firewall + strict IP whitelist for ssh)
- Daily backups (7 days retention)

We still don't want to be some kind of SaaS provider, so even now we offer hosting that's still considered as your own server, the only difference is you pay for VPS not to AWS or other cloud provider directly, but through us. So, you have root access to your server and control how it works inside.

If you have any questions - ask them in #discussion:etke.cc  room

New order system on website is up and running (our website is heavily cached, so in case you don't see new order form with different options try to clear cache in your browser)

**call to action (organizations)**

As you saw on website, we added Case Studies section some time ago and the first organization who published their story is Reidel Law Firm. We want to add more cases to that section to help more organizations choose Matrix for internal communications (and, I hope, external, too).

So if you use Matrix in your organization as messenger or platform for your product or in any other way - please, send me PM, etke.cc service will be honored to publish your story


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$iYIYNmFQz1J5NSfCoZy-Ox0AULCkhIazlkiNGCnCLpg?via=etke.cc&via=matrix.org)
