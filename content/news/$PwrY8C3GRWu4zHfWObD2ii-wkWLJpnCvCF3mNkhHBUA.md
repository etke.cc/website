---
id: '$PwrY8C3GRWu4zHfWObD2ii-wkWLJpnCvCF3mNkhHBUA'
date: '2022-06-28 14:18:50.091 +0000 UTC'
title: '2022-06-28 14:18 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-06-28 14:18 UTC'
---

**servers update in progress (security fix)**

* Synapse 1.61.0 -> Synapse 1.61.1

> This patch release fixes a security issue regarding URL previews, affecting all prior versions of Synapse. Server administrators are encouraged to update Synapse as soon as possible. We are not aware of these vulnerabilities being exploited in the wild.

[Details](https://github.com/matrix-org/synapse/releases/tag/v1.61.1)

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$PwrY8C3GRWu4zHfWObD2ii-wkWLJpnCvCF3mNkhHBUA?via=etke.cc&via=matrix.org)
