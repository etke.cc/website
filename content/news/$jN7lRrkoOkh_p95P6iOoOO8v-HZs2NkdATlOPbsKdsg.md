---
id: '$jN7lRrkoOkh_p95P6iOoOO8v-HZs2NkdATlOPbsKdsg'
date: '2022-05-17 15:47:45.725 +0000 UTC'
title: '2022-05-17 15:47 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-05-17 15:47 UTC'
---

**servers update in progress**

* Cinny 2.0.0 -> 2.0.3
* Slack bridge latest -> 0.1.2 _fork change, version pin_
* Synapse 1.58.1 -> 1.59.0

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$jN7lRrkoOkh_p95P6iOoOO8v-HZs2NkdATlOPbsKdsg?via=etke.cc&via=matrix.org)
