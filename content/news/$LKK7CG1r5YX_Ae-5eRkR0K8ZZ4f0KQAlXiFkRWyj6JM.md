---
id: '$LKK7CG1r5YX_Ae-5eRkR0K8ZZ4f0KQAlXiFkRWyj6JM'
date: '2021-12-01 16:55:27.348 +0000 UTC'
title: '2021-12-01 16:55 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-12-01 16:55 UTC'
---

**service updates**

Due to [discounting PayPal on buymeacoffee side](https://help.buymeacoffee.com/en/articles/5777776-discontinuing-paypal-faq), any new payments (both recurring, like new maintenance subscriptions, and one time, like setup or support) will be processed through stripe and payoneer.

from BMC FAQ (link above):
> Will my membership renewals be affected?
> First off, please note that this applies only to creators who have active members (monthly recurring payments via PayPal). Membership payment renewals won't be affected as per our knowledge. However, we recommend suggesting your members to resubscribe whenever you're ready.

so, no need to worry for existing subscribers.

I don't like the situation and forced changes to payment processing (we already had issues with recurring payments with stripe+payoneer before), but that's the only way we have.

Due to unexpected and urgency of the situation, we don't have any other info to share. There are some concerns regarding fees, but we will review them soon and if stripe+payoneer fees are higher than paypal, prices may be increased to cover that (we do **NOT** increasing them, just need some time to review the terms we were forced to accept), fun fact: your $5 payment is $4 for us (and $1 is a fee)

If you have any questions - ask them in #discussion:etke.cc so other customers may read them, too (keep in mind, that we don't have answers right now and have almost the same info that you have).


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$LKK7CG1r5YX_Ae-5eRkR0K8ZZ4f0KQAlXiFkRWyj6JM?via=etke.cc&via=matrix.org)
