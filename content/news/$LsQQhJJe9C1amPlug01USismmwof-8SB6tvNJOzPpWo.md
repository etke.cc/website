---
id: '$LsQQhJJe9C1amPlug01USismmwof-8SB6tvNJOzPpWo'
date: '2023-03-30 20:01:44.118 +0000 UTC'
title: '2023-03-30 20:01 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-03-30 20:01 UTC'
---

**Stable Updates Published**

- BorgBackup 1.2.3 → 1.2.4
- Borgmatic 1.7.9 → 1.7.10
- Cinny 2.2.4 → 2.2.6
- Element 1.11.25 → 1.11.26
- Hookshot 3.0.1 → 3.1.1
- Ntfy 2.2.0 → 2.3.0
- Soft Serve 0.4.6 → 0.4.7
- Synapse 1.79.0 → 1.80.0
- Uptime Kuma 1.21.0 → 1.21.1

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

**Service Updates**

- Automatic redirect from base domain to an installed web app (e.g. `your-server.com` → `element.your-server.com`) is now configured automatically for etke.cc customers who serve base domain from their matrix server
- [Frequently Asked Questions](https://etke.cc/help/faq) section has been added to the website. If you think something is missing - please, send your ideas in #discussion:etke.cc

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$LsQQhJJe9C1amPlug01USismmwof-8SB6tvNJOzPpWo?via=etke.cc&via=matrix.org)
