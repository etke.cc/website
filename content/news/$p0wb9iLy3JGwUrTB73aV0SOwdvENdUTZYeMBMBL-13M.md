---
id: '$p0wb9iLy3JGwUrTB73aV0SOwdvENdUTZYeMBMBL-13M'
date: '2023-07-07 05:10:13.518 +0000 UTC'
title: '2023-07-07 05:10 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-07-07 05:10 UTC'
---

**Stable Updates Published**

- Coturn 4.6.2-r3 -> 4.6.2-r4
- Element 1.11.34 -> 1.11.35
- Hookshot 4.3.0 -> 4.4.0
- Ntfy 2.6.1 -> 2.6.2
- Synapse 1.86.0 -> 1.87.0
- Uptime Kuma 1.22.0 -> 1.22.1

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$p0wb9iLy3JGwUrTB73aV0SOwdvENdUTZYeMBMBL-13M?via=etke.cc&via=matrix.org)
