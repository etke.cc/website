---
id: '$_qXPlRvH50R56PQo-9Bf0h97EBIw9exypNKTcj3T800'
date: '2021-08-16 16:45:45.063 +0000 UTC'
title: '2021-08-16 16:45 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-08-16 16:45 UTC'
---

New post on element blog regarding new features included in latest releases:

https://element.io/blog/introducing-voice-messages-and-so-much-more/​

I wish threads among them...


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$_qXPlRvH50R56PQo-9Bf0h97EBIw9exypNKTcj3T800?via=etke.cc&via=matrix.org)
