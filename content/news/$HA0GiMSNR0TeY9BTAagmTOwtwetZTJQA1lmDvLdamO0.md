---
id: '$HA0GiMSNR0TeY9BTAagmTOwtwetZTJQA1lmDvLdamO0'
date: '2023-09-07 20:05:23.721 +0000 UTC'
title: '2023-09-07 20:05 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-09-07 20:05 UTC'
---

**Stable Updates Published**

- Grafana 10.1.0 -> 10.1.1
- #honoroit:etke.cc 0.9.18 -> 0.9.19
- Hydrogen 0.4.0 -> 0.4.1
- Jitsi stable-8615 -> stable-8922
- Prometheus 2.45.0 -> 2.47.0
- Sliding Sync 0.99.7 -> 0.99.10
- Synapse 1.91.0 -> 1.91.2

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$HA0GiMSNR0TeY9BTAagmTOwtwetZTJQA1lmDvLdamO0?via=etke.cc&via=matrix.org)
