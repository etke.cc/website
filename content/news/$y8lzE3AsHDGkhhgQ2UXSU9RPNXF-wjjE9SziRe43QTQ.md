---
id: '$y8lzE3AsHDGkhhgQ2UXSU9RPNXF-wjjE9SziRe43QTQ'
date: '2022-10-14 06:47:02.648 +0000 UTC'
title: '2022-10-14 06:47 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-10-14 06:47 UTC'
---

**stable updates published**

- Element 1.11.8 -> 1.11.10
- Grafana 9.1.7 -> 9.2.0
- Instagram bridge 0.2.1 -> latest _critical fix is not part of any pinned release yet, thus we temporary switch to the latest version_
- Jitsi stable-7830 -> 7882
- Uptime Kuma 1.18.3 -> 1.18.5
- #postmoogle:etke.cc 0.9.4 -> 0.9.7
- Prometheus 2.39.0 -> 2.39.1

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

**service updates**

- [etke.cc](https://etke.cc/)'s ssh keys have been rotated. The new keys are available on the website. This is an automated process which aims to increase security - you don't need to do anything.
- [etke.cc](https://etke.cc/)'s website got a redesign. A lot of new information (about bridges, bots, and extra services we offer) has been added to the [etke.cc/help](https://etke.cc/help) section ([German](https://etke.cc/de/help), [Russian](https://etke.cc/ru/help)). More improvements are coming in the near future!
- #postmoogle:etke.cc got new security features to prevent incoming spam

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$y8lzE3AsHDGkhhgQ2UXSU9RPNXF-wjjE9SziRe43QTQ?via=etke.cc&via=matrix.org)
