---
id: '$N8F2mlVGVwMd6RIa7_eGc5yb7On8bROIZOwLPvv9oSw'
date: '2021-05-19 14:53:09.194 +0000 UTC'
title: '2021-05-19 14:53 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-05-19 14:53 UTC'
---

... and one more thing, came literally 5 minutes after I posted the service update

etke.cc added to the https://matrix.org/hosting page 👍️


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$N8F2mlVGVwMd6RIa7_eGc5yb7On8bROIZOwLPvv9oSw?via=etke.cc&via=matrix.org)
