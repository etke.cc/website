---
id: '$jlmMkQuNW5zwUs3L2XEKLUdO7C_bvjD1Zqgj2IMYVVM'
date: '2021-08-10 17:49:21.107 +0000 UTC'
title: '2021-08-10 17:49 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-08-10 17:49 UTC'
---

**note to subscribers**: please, reboot your servers tomorrow (or at least in 3-5 hours, not earlier) to load new kernel versions and cleanup iptables rules.

There was an issue with firewall configuration and because of that downtime (< 1 hour) occurred, sorry! To avoid other issues, server reboot is encouraged.

If you face that issue again, please, run following command: `sudo iptables -I DOCKER-USER 1 -j RETURN`


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$jlmMkQuNW5zwUs3L2XEKLUdO7C_bvjD1Zqgj2IMYVVM?via=etke.cc&via=matrix.org)
