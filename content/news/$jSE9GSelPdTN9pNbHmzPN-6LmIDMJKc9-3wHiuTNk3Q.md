---
id: '$jSE9GSelPdTN9pNbHmzPN-6LmIDMJKc9-3wHiuTNk3Q'
date: '2021-12-07 18:04:19.233 +0000 UTC'
title: '2021-12-07 18:04 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-12-07 18:04 UTC'
---

**servers update in progress**
- Corporal 2.2.1 -> 2.2.2
- Element 1.9.5 -> 1.9.6
- Facebook bridge 0.3.2 -> latest _bugfix released for the broken login, but it was not included in a pinned release yet, so installing latest version to fix the login flow for new users (existing/already logged in users are not affected by that bug)_
- Grafana 8.2.2 -> 8.3.0
- Prometheus 2.30.2 -> 2.31.1
- Redis 6.2.4 -> 6.2.6

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$jSE9GSelPdTN9pNbHmzPN-6LmIDMJKc9-3wHiuTNk3Q?via=etke.cc&via=matrix.org)
