---
id: '$qxdRxtZLd5SmvvFevsKCFHqDJ9HJNhDtD_jE5qv1MmY'
date: '2023-01-26 21:21:37.61 +0000 UTC'
title: '2023-01-26 21:21 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-01-26 21:21 UTC'
---

**Stable Updates Published**

- Element 1.11.19 -> 1.11.20
- Grafana 9.3.2 -> 9.3.6
- Hookshot 2.6.1 -> 2.7.0

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/help/extras/scheduler/#fresh-testing)_

---

**Service Updates**

- we've improved coturn (audio/video calls) security for all customers

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$qxdRxtZLd5SmvvFevsKCFHqDJ9HJNhDtD_jE5qv1MmY?via=etke.cc&via=matrix.org)
