---
id: '$cqAn_ORuidBPwdvGQZp9ecbIZEReBctzos4GwDJIr1s'
date: '2023-04-03 07:49:04.379 +0000 UTC'
title: '2023-04-03 07:49 UTC (updated)'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-04-03 07:49 UTC'
---

**Outage is in progress** (update)

- **What**: etke.host domain
- **Impact**: customers using etke.host as their matrix server's domain, etke.cc email communications
- **ETA**: unknown

The etke.host domain name was put on hold by the domain registry operator due to a domain name listing in Google Safe Browsing.

We already sent requests to unlist the domain name from Google Safe Browsing and unsuspend it from the domain registry operator side because the reasons listed on the Google Safe Browsing side are false accusations and that domain name was never used for such actions from the moment it was registered by us.

Unfortunately, such requests may take a while before they will be fulfilled.


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$cqAn_ORuidBPwdvGQZp9ecbIZEReBctzos4GwDJIr1s?via=etke.cc&via=matrix.org)
