---
id: '$p5AkdZP6ITWz2LQlK9osYmlycf8uDYyguf5BCd1QF3k'
date: '2024-07-11 20:00:24.475 +0000 UTC'
title: '2024-07-11 20:00 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-07-11 20:00 UTC'
---

**Stable Updates Published**

- [Container Socket Proxy](https://github.com/Tecnativa/docker-socket-proxy): [0.1.2](https://github.com/Tecnativa/docker-socket-proxy/releases/tag/v0.1.2) -> [0.2.0](https://github.com/Tecnativa/docker-socket-proxy/releases/tag/v0.2.0)
- [Element](https://github.com/element-hq/element-web): [v1.11.69](https://github.com/element-hq/element-web/releases/tag/v1.11.69) -> [v1.11.70](https://github.com/element-hq/element-web/releases/tag/v1.11.70)
- [Etherpad](https://github.com/ether/etherpad-lite): [2.1.0](https://github.com/ether/etherpad-lite/releases/tag/2.1.0) -> [2.1.1](https://github.com/ether/etherpad-lite/releases/tag/2.1.1)
- [Exim Relay](https://github.com/devture/exim-relay): [4.97.1-r0-0](https://github.com/devture/exim-relay/releases/tag/4.97.1-r0-0) -> [4.97.1-r0-1](https://github.com/devture/exim-relay/releases/tag/4.97.1-r0-1)
- [Honoroit](https://gitlab.com/etke.cc/honoroit): [v0.9.21](https://gitlab.com/etke.cc/honoroit/-/tags/v0.9.21) -> [v0.9.22](https://gitlab.com/etke.cc/honoroit/-/tags/v0.9.22)
- [Jitsi](https://github.com/jitsi/docker-jitsi-meet): [stable-9584](https://github.com/jitsi/docker-jitsi-meet/releases/tag/stable-9584) -> [stable-9584-1](https://github.com/jitsi/docker-jitsi-meet/releases/tag/stable-9584-1)
- [Miniflux](https://github.com/miniflux/v2): [2.1.3](https://github.com/miniflux/v2/releases/tag/2.1.3) -> [2.1.4](https://github.com/miniflux/v2/releases/tag/2.1.4)
- [Prometheus](https://github.com/prometheus/prometheus): [v2.53.0](https://github.com/prometheus/prometheus/releases/tag/v2.53.0) -> [v2.53.1](https://github.com/prometheus/prometheus/releases/tag/v2.53.1)
- [Synapse Admin](https://github.com/Awesome-Technologies/synapse-admin): [0.10.1](https://github.com/Awesome-Technologies/synapse-admin/releases/tag/0.10.1) -> [0.10.2](https://github.com/Awesome-Technologies/synapse-admin/releases/tag/0.10.2)
- [Traefik](https://github.com/traefik/traefik): [v2.11.4](https://github.com/traefik/traefik/releases/tag/v2.11.4) -> [v3.0.4](https://github.com/traefik/traefik/releases/tag/v3.0.4)
- [Vaultwarden](https://github.com/dani-garcia/vaultwarden): [1.30.5](https://github.com/dani-garcia/vaultwarden/releases/tag/1.30.5) -> [1.31.0](https://github.com/dani-garcia/vaultwarden/releases/tag/1.31.0)

**Service Updates: [HTTP/3](https://en.wikipedia.org/wiki/HTTP/3) Support**

[HTTP/3](https://en.wikipedia.org/wiki/HTTP/3) is now supported and enabled by default for all customers, making access to services even faster.

On-Premises customers, which have an additional firewall in front of their server, will need to open UDP ports `443` and `8448` in their provider-level firewall (if enabled) for HTTP/3 to work.

**Service Updates: [The Scheduler](https://etke.cc/help/extras/scheduler) Gets `price` Command**

[The price command](https://etke.cc/help/extras/scheduler/#price) provides an overview of your server’s cost with a detailed breakdown by components, it's especially handy when you want to adjust your server's components and their cost.

**Notice About Subscriptions Migration**

> [Context](https://etke.cc/help/faq#when-will-old-subscriptions-maintenance-and-turnkeys-be-deprecated). Impacts only customers on the deprecated price model (Maintenance and Turnkey subscriptions). If you got your server after December 2023 or already switched to the new By Complexity model, you are not affected and thus you don't need to do anything.

If you intend to switch from a deprecated subscription plan, you don't need to submit a new order. Just obtain the price for your specific server by using [`run price` of etke.cc/scheduler](https://etke.cc/help/extras/scheduler/#price) and follow the [step-by-step guide](https://etke.cc/help/payments#how-to-become-a-subscriber) to update your payment details

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$p5AkdZP6ITWz2LQlK9osYmlycf8uDYyguf5BCd1QF3k?via=etke.cc&via=matrix.org)
