---
id: '$labtsksU53RPmSo5V54dwXC5h9bX6ywbMYWQoMdUCKU'
date: '2023-11-30 20:54:06.382 +0000 UTC'
title: '2023-11-30 20:54 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-11-30 20:54 UTC'
---

**Stable Updates Published**

- Synapse 1.96.1 -> 1.97.0
- Telegram bridge 0.14.2 -> 0.15.0
- Traefik 2.10.5 -> 2.10.6
- Uptime Kuma 1.23.6 -> 1.23.7

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$labtsksU53RPmSo5V54dwXC5h9bX6ywbMYWQoMdUCKU?via=etke.cc&via=matrix.org)
