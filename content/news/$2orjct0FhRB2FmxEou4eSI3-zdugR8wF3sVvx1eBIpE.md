---
id: '$2orjct0FhRB2FmxEou4eSI3-zdugR8wF3sVvx1eBIpE'
date: '2023-11-23 21:10:48.571 +0000 UTC'
title: '2023-11-23 21:10 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2023-11-23 21:10 UTC'
---

**Stable Updates Published**

- Borgmatic 1.8.4 -> 1.8.5
- Discord bridge 0.6.3 -> 0.6.4
- Element / [app.etke.cc](https://app.etke.cc/) 1.11.49 -> 1.11.50
- Google Messages bridge 0.2.1 -> 0.2.2
- Grafana 10.2.1 -> 10.2.2
- Hookshot 4.5.1 -> 4.6.0
- Jitsi 9078 -> 9111
- Ntfy 2.7.0 -> 2.8.0
- Sygnal 0.12.0 -> 0.13.0
- Synapse 1.95.1 -> 1.96.1
- Uptime Kuma 1.23.4 -> 1.23.6
- Vaultwarden 1.30.0 -> 1.30.1
- WhatsApp bridge 0.10.3 -> 0.10.4

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$2orjct0FhRB2FmxEou4eSI3-zdugR8wF3sVvx1eBIpE?via=etke.cc&via=matrix.org)
