---
id: '$uVytbaAUoaFF7OO1hfDYFGbQbeAeUVxAgE4qha9UgB0'
date: '2021-06-30 17:12:34.085 +0000 UTC'
title: '2021-06-30 17:12 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-06-30 17:12 UTC'
---

**servers update**
- Synapse 1.37.0 -> 1.37.1
- Postgres 13.2 -> 13.3
- etke.cc ssh key rotation, form now on actual public key is available on [etke.cc/ssh.key](https://etke.cc/ssh.key), old keys automatically revoked/removed

_that's bugfix update_


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$uVytbaAUoaFF7OO1hfDYFGbQbeAeUVxAgE4qha9UgB0?via=etke.cc&via=matrix.org)
