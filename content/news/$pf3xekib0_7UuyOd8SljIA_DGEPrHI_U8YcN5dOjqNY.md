---
id: '$pf3xekib0_7UuyOd8SljIA_DGEPrHI_U8YcN5dOjqNY'
date: '2021-06-22 18:30:55.95 +0000 UTC'
title: '2021-06-22 18:30 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-06-22 18:30 UTC'
---

**servers updates**
- Element Web 1.7.30 -> 1.7.31
- Grafana 8.0.2 -> 8.0.3
- Exim 4.94.2-r0-1 -> 4.94.2-r0-2

NOTE: database cleanups will be performed today (rooms' state compress, postgres' full vacuum), so update will take much more time


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$pf3xekib0_7UuyOd8SljIA_DGEPrHI_U8YcN5dOjqNY?via=etke.cc&via=matrix.org)
