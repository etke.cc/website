---
id: '$CmYV6gDJAPZf9dPR4uypkKKtHcrI8L9c-dnjgBIMIfQ'
date: '2024-05-23 19:58:01.91 +0000 UTC'
title: '2024-05-23 19:58 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2024-05-23 19:58 UTC'
---

**Stable Updates Published**

- [Element](https://github.com/element-hq/element-web): [v1.11.66](https://github.com/element-hq/element-web/releases/tag/v1.11.66) -> [v1.11.67](https://github.com/element-hq/element-web/releases/tag/v1.11.67)
- [Gmessages](https://github.com/mautrix/gmessages): [v0.4.0](https://github.com/mautrix/gmessages/releases/tag/v0.4.0) -> [v0.4.1](https://github.com/mautrix/gmessages/releases/tag/v0.4.1)
- [Meta Instagram](https://github.com/mautrix/meta): [v0.3.0](https://github.com/mautrix/meta/releases/tag/v0.3.0) -> [v0.3.1](https://github.com/mautrix/meta/releases/tag/v0.3.1)
- [Meta Messenger](https://github.com/mautrix/meta): [v0.3.0](https://github.com/mautrix/meta/releases/tag/v0.3.0) -> [v0.3.1](https://github.com/mautrix/meta/releases/tag/v0.3.1)
- [Prometheus Node Exporter](https://github.com/prometheus/node_exporter): [v1.8.0](https://github.com/prometheus/node_exporter/releases/tag/v1.8.0) -> [v1.8.1](https://github.com/prometheus/node_exporter/releases/tag/v1.8.1)
- [Radicale](https://github.com/tomsquest/docker-radicale): [3.1.9.1](https://github.com/tomsquest/docker-radicale/releases/tag/3.1.9.1) -> [3.2.0.0](https://github.com/tomsquest/docker-radicale/releases/tag/3.2.0.0)
- [Signal](https://github.com/mautrix/signal): [v0.6.0](https://github.com/mautrix/signal/releases/tag/v0.6.0) -> [v0.6.1](https://github.com/mautrix/signal/releases/tag/v0.6.1)
- [Sliding Sync](https://github.com/matrix-org/sliding-sync): [v0.99.17](https://github.com/matrix-org/sliding-sync/releases/tag/v0.99.17) -> [v0.99.18](https://github.com/matrix-org/sliding-sync/releases/tag/v0.99.18) [Sygnal](https://github.com/matrix-org/sygnal): [v0.14.1](https://github.com/matrix-org/sygnal/releases/tag/v0.14.1) -> [v0.14.2](https://github.com/matrix-org/sygnal/releases/tag/v0.14.2)

**Reminder**

The following items may need your attention:

- [In relation to our new pricing model: When will old subscriptions (Maintenance and Turnkeys) be deprecated?](https://etke.cc/help/faq#when-will-old-subscriptions-maintenance-and-turnkeys-be-deprecated)
- [Upcoming removal of long-deprecated components](https://etke.cc/help/faq#what-about-deprecated-components)

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$CmYV6gDJAPZf9dPR4uypkKKtHcrI8L9c-dnjgBIMIfQ?via=etke.cc&via=matrix.org)
