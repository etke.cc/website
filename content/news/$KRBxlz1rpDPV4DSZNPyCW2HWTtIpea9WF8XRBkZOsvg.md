---
id: '$KRBxlz1rpDPV4DSZNPyCW2HWTtIpea9WF8XRBkZOsvg'
date: '2022-08-19 07:20:18.837 +0000 UTC'
title: '2022-08-19 07:20 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-08-19 07:20 UTC'
---

**stable updates published**

- Element 1.11.2 -> 1.11.3
- Grafana 9.0.7 -> 9.1.0
- Jitsi stable-7577-2 -> stable-7648-2
- Miniflux 2.0.37 -> 2.0.38
- Nginx 1.23.0 -> 1.23.1
- Prometheus 2.37.0 -> 2.38.0
- Signal (daemon) 0.20.0 -> 0.21.0
- Synapse 1.64.0 -> 1.65.0
- WhatsApp 0.6.0 -> 0.6.1

_Do you want to receive updates up to 1 week before the stable release? [Switch to the `fresh` stability branch](https://etke.cc/scheduler#fresh-testing)_

---

**etherpad update**

From now on the Etherpad component provided with integrated DOC/PDF/ODT support and number of different plugins, it will be updated automatically if your server already has Etherpad installed

---

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$KRBxlz1rpDPV4DSZNPyCW2HWTtIpea9WF8XRBkZOsvg?via=etke.cc&via=matrix.org)
