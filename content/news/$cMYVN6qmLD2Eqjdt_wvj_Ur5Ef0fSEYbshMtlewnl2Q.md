---
id: '$cMYVN6qmLD2Eqjdt_wvj_Ur5Ef0fSEYbshMtlewnl2Q'
date: '2022-01-25 20:00:53.998 +0000 UTC'
title: '2022-01-25 20:00 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-01-25 20:00 UTC'
---

**servers update in progress**

- #honoroit:etke.cc 0.9.2 -> 0.9.3
- Miniflux 2.0.34 -> 2.0.35
- Prometheus postgres exporter 0.10.0 -> 0.10.1
- Radicale 3.1.0.0 -> 3.1.2.0
- Synapse 1.50.0 -> 1.51.1

#discussion:etke.cc


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$cMYVN6qmLD2Eqjdt_wvj_Ur5Ef0fSEYbshMtlewnl2Q?via=etke.cc&via=matrix.org)
