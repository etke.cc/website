---
id: '$RbFcuZJVlIayOj4KwDxjb88n5DbBOv2Z8ax8jsXhFQo'
date: '2022-04-05 18:15:44.544 +0000 UTC'
title: '2022-04-05 18:15 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2022-04-05 18:15 UTC'
---

**servers update in progress**

* Languagetool 5.6 -> 5.7
* Synapse 1.55.2 -> 1.56.0

___

**service updates**

* etke.cc ssh key has been rotated. The new key will be available on website after the maintenance ends. That's automated process, you don't need to do anything.
* a new component available for existing etke.cc subscribers - borg backups to any borg provider through ssh. It's not available on website yet because it's a bit rough around the edges and there are some things to enhance, but it's fully usable and can be configured per your request. Keep in mind that you should have borg provider with ssh connection before requesting the integration.
* initial setup price increased to curb demand

___

#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$RbFcuZJVlIayOj4KwDxjb88n5DbBOv2Z8ax8jsXhFQo?via=etke.cc&via=matrix.org)
