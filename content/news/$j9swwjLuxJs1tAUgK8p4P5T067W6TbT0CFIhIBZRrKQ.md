---
id: '$j9swwjLuxJs1tAUgK8p4P5T067W6TbT0CFIhIBZRrKQ'
date: '2021-10-26 17:05:39.698 +0000 UTC'
title: '2021-10-26 17:05 UTC'
author: '@aine:etke.cc'
draft: false
layout: singlenews
description: 'New message published on 2021-10-26 17:05 UTC'
---

**servers update in progress**
- Element 1.9.2 -> 1.9.3
- IRC (heisenbridge) 1.2.1 -> 1.3.0
- #miounne:etke.cc 1.0.2 -> 2.0.0
- Synapse 1.45.0 -> 1.45.1

**service updates**
- removed [panelbear analytics](https://panelbear.com/) from website - it's privacy-friendly, but we decided that `no log` policy is more valuable than knowledge about how many visitors are on etke.cc


#discussion:etke.cc 


[Source](https://matrix.to/#/!gqlCuoCdhufltluRXk:etke.cc/$j9swwjLuxJs1tAUgK8p4P5T067W6TbT0CFIhIBZRrKQ?via=etke.cc&via=matrix.org)
