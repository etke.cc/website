---
title: Services
draft: false
layout: singletoc
description: Explore the range of services provided by etke.cc
---

Welcome to our Services page, where you can discover the array of offerings from etke.cc.

## Initial Setup

**Prerequisites**

Before we dive into our services, here are a couple of things you'll need:

* An x86/amd64 Linux server with a Debian-based distribution (we recommend the latest Ubuntu LTS).
* A domain name to use with your Matrix server. Alternatively, you can obtain a subdomain from us at no extra cost.

**What We Do**

Once you provide us with SSH access to your Debian-based VPS, we take care of the following:

* Setting up Docker
* Configuring Matrix homeserver components (e.g., Synapse, Coturn, Synapse-Admin, Element app, etc.) based on your order
* Handling database backups (gzip SQL dump for on-premises installations or server snapshots for hosted options)
* Configuring swap based on your VPS's RAM
* Configuring fail2ban for intrusion prevention
* Setting up the firewall (ufw)
* Conducting SSH security hardening

As part of the setup process, we also ensure:

* System package updates
* Cleanup of unused system and Docker resources
* Logs retention for 7 days

## Maintenance

**When Is Maintenance Required?**

Maintenance services become available only after the initial setup. We exclusively offer maintenance for homeservers installed by us, owing to variations in configuration and deployment methods. If you have an existing Matrix server installed through other means, you can [get in touch](/contacts/) with us for integration.

**Automatic Weekly Maintenance**

Our automated maintenance runs once a week, in accordance with your chosen [recurring schedule](/help/extras/scheduler/#recurring) managed by our [scheduler](/help/extras/scheduler/). 
If you haven't set a schedule, a default one will be assigned for your server's upkeep.

We release stable updates every Friday, ensuring your server stays current. 
In addition to automatic weekly maintenance, you have the option to manually trigger maintenance via our Scheduler bot at any time, although it's usually unnecessary. 
Sit back, relax, and let us handle the weekly maintenance for you.

As part of our maintenance, we perform the following tasks every week:

* System package updates
* Matrix component updates
* Cleanup of unused system, Docker, and Matrix resources (e.g., cache, logs, thumbnails)
* Logs retention for 7 days
* Automatic tuning and vacuuming of your database, tailored to your server's configuration

Note that maintenance does not cover distribution upgrades (e.g., from Ubuntu 20.04 to 22.04), which are considered major upgrades and require a manual intervention. You can either do them yourself with our guidance (no extra cost) or request our assistance for a fee.
[More details](/help/distro-upgrade)

## Additional benefits

With any server subscription plan, you can enjoy the following included benefits:

* Access to the [Scheduler bot](/help/extras/scheduler/)
* [Basic support](/services/support/#basic)
* [Monitoring](/services/monitoring/) with email and Matrix alerts
* Access to a private Docker registry containing all Matrix components, ensuring stability by avoiding issues with public registries

## Self-Service Bot

**Manage Your Server with Ease**

Our [Scheduler bot](/help/extras/scheduler/) is a self-service tool that allows you to manage your server with ease.
You can schedule maintenance, monitor your server's health, and more, all from the comfort of your Matrix client.

Learn more about the Scheduler bot [here](/help/extras/scheduler/).

## Hosting

**Get a server without dealing with infrastructure yourself**

Our [Hosting service](/services/hosting/) is for those of you who'd rather avoid dealing with server infrastructure.

If you're not a fan of running servers **On-Premises** (on your own infrastructure or on a cloud provider of your choice), we can rent a server and manage it on your behalf from [Hetzner Cloud](https://hetzner.cloud/?ref=1yg3ZZmtrg5A).

Learn more about our Hosting service [here](/services/hosting/).

## Monitoring

**Stay Informed**

Our [monitoring service](/services/monitoring/) keeps you informed about your server's health and performance.
We offer monitoring reports via email and Matrix alerts, ensuring you're always up-to-date on your server's status.

Learn more about our monitoring service [here](/services/monitoring/).

## Private Docker Registry

**Avoid Public Registries Issues**

Our [private Docker registry](/services/registry/) acts as a mirror for public registries, eliminating common issues such as rate limits, unavailability, and restricted access.

Learn more about our private Docker registry [here](/services/registry/).

## Customer Support

**Choose the Level of Support**

We offer various levels of customer support. The "Basic" level is included with [Maintenance](#maintenance) and [Turnkey](#turnkey) subscriptions by default. For more extensive support, our "Dedicated" level is available upon request.

For additional details, please visit our [support page](/services/support/).

## [MatrixRooms.info](https://matrixrooms.info)

**Enhance Your Matrix Experience**

Discover vibrant communities and enrich your Matrix experience with [MatrixRooms.info](https://matrixrooms.info), our in-house Matrix room search engine. It's a valuable resource for finding and joining interesting Matrix communities.

Learn more about MatrixRooms.info [here](https://matrixrooms.info/about).

## Custom Development

**Tailored Solutions**

If you require custom features within our open-source projects, we're here to help at a fair price. Get more details about our custom development services [here](/services/customdev).
