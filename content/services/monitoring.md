---
title: Monitoring
draft: false
layout: singletoc
description: on that page, you can read about the Monitoring service we offer.
---

At [etke.cc](https://etke.cc), we provide monitoring services for your Matrix server.
This service is included with all server subscriptions we provide.

## What is monitored?

We monitor the following aspects of your server:

### Ports

We monitor the availability of the expected ports on your server based on the services you have enabled.
On a basic Matrix server without any add-ons, the following ports will be monitored:

* `22/tcp` - SSH port
* `80/tcp`, `443/tcp`, `443/udp` - HTTP, HTTPS port. UDP is necessary for [HTTP/3](https://en.wikipedia.org/wiki/HTTP/3).
* `8448/tcp` and `8448/udp` - Matrix federation port (if federation is enabled). UDP is necessary for [HTTP/3](https://en.wikipedia.org/wiki/HTTP/3).
* `3478/tcp+udp`, `5349/tcp+udp` - TURN ports (VoIP calls)
* `49152-49573/udp` - TURN ports (VoIP calls)

But if you have additional components, e.g., [Postmoogle email bridge](/help/bridges/postmoogle/), additional ports will be monitored:
`25/tcp` and `587/tcp` (SMTP ports).

A full list of the required ports per component can be found in the [What ports should be open](/order/status/#ports-and-firewalls) FAQ section.

### DNS records

Similar to ports, we monitor the existence and correctness of the DNS records needed for your server to function properly.
For a basic Matrix server without any add-ons, the following DNS records will be monitored:

* `@` base/apex domain record - should be present and point to your server's IP address (only if you serve the base domain from the matrix server itself)
* `matrix` subdomain record, used for the Matrix server itself - should be present and point to your server's IP address

But if you have additional components, e.g., [Linkding](/help/extras/linkding/), additional DNS records will be monitored:
the `linkding` subdomain record - as with previous records, it should be present and point to your server's IP address (via CNAME or A record).

### HTTP endpoints

We monitor the availability of the HTTP endpoints of the services you have enabled.
On a basic Matrix server without any add-ons, the following HTTP endpoints will be monitored:

* `https://your-server.com/.well-known/matrix/server` - Matrix server discovery endpoint (Matrix Federation API, if federation is enabled)
* `https://your-server.com/.well-known/matrix/client` - Matrix server discovery endpoint (Matrix Client-Server API)
* `https://your-server.com/.well-known/matrix/support` - Matrix server contacts endpoint ([MSC1929](https://github.com/matrix-org/matrix-spec-proposals/blob/main/proposals/1929-admin-contact.md), part of Matrix protocol v1.10)
* `https://matrix.your-server.com:8448/_matrix/federation/v1/version` - Matrix Federation version endpoint (Matrix Federation API, if federation is enabled)
* `https://matrix.your-server.com/_matrix/client/versions` - Matrix client version endpoint (Matrix Client-Server API)
* `https://element.your-server.com` - Element web client (enabled by default on all servers)

But if you have additional components, e.g., [Funkwhale](/help/extras/funkwhale/), additional HTTP endpoints will be monitored:
`https://funkwhale.your-server.com/api/v1/instance/nodeinfo/2.0` - Funkwhale NodeInfo endpoint.

### VPS metrics

We monitor the basic metrics of your Matrix server's host:

* `CPU` - CPU usage; we recommend keeping it below 80% for optimal performance
* `RAM` - RAM usage; we recommend keeping it below 80% for optimal performance
* `Disk` disk usage; we recommend keeping it below 80% for optimal performance

If any of the metrics exceed the recommended values, we will notify you about it.

Additionally, we monitor the state of the Docker containers running on your server to ensure that all services are running as expected, but that information is not yet available in the monitoring report you receive (either using the [ping command](/help/extras/scheduler/#ping) of the Scheduler or by automatic monitoring alerts).

## How long is the monitoring data stored?

We store the monitoring data for the last 7 days on our side.

## How to access the monitoring data?

At this moment, you can receive the monitoring report by using the [ping command](/help/extras/scheduler/#ping) of the Scheduler, or by automatic monitoring alerts.

## How the report is looks like?

### All services are up and running

> The following report shows that all services are up and running, and there are no issues with the server.

* 🟩 CPU utilization is ~**7%**
* 🟩 Disk utilization is ~**35%**
* 🟩 RAM utilization is ~**77%**
* 🟩 [etke.cc federation](https://federationtester.matrix.org/#etke.cc)
* 🟩 https://etke.cc/.well-known/matrix/server
* 🟩 https://matrix.etke.cc/_matrix/client/versions
* 🟩 https://matrix.etke.cc:8448/_matrix/federation/v1/version
* 🟩 https://social.etke.cc

### There are issues with the server

> The following report shows that there are issues with the server that require your attention, namely:
> * High disk utilization
> * Timeout on the 25/tcp and 587/tcp ports
> * Invalid content in the delegation endpoint (`/.well-known/matrix/server`)
> * Invalid DNS records for the `social.etke.cc` subdomain

* 🟩 CPU utilization is ~**7%**
* 🟥 Disk utilization is ~85%: recommended is below 80% ([help](/services/monitoring/#high-disk-usage))
* 🟩 RAM utilization is ~**77%**
* 🟥 25 (tcp port): timeout ([help](/services/monitoring/#ports-failures))
* 🟥 587 (tcp port): timeout ([help](/services/monitoring/#ports-failures))
* 🟥 [etke.cc federation]: invalid character '<' looking for beginning of value ([help](/services/monitoring/#well-knownmatrixserver-failures))
* 🟩 https://matrix.etke.cc/_matrix/client/versions
* 🟩 https://matrix.etke.cc:8448/_matrix/federation/v1/version
* 🟥 social.etke.cc (DNS record): no or invalid A record; invalid CNAME record (expected: 1.2.3.4, actual: 4.3.2.1, 5.6.7.8) ([help](/services/monitoring/#dns-records-failures))

## How do I get the monitoring report?

### On-demand

You can receive the monitoring report by using the [ping command](/help/extras/scheduler/#ping) of the Scheduler

### Automatic

We will notify you about any issues with your server through automatic monitoring alerts sent to your matrix account and email address daily. The report is generated between 3 a.m. and 5 a.m. UTC every day (fixed schedule).

To avoid spamming you with the same alerts, an exponential backoff mechanism is used, meaning that if the same issue persists for more than one day, you will receive the alert less frequently.


## What to do if the monitoring report shows issues?

There are different kinds of issues that can be shown in the monitoring report, and each of them requires different actions to be taken.
Below are the most common issues and the suggested actions to resolve them.

### DNS records failures

If the monitoring report contains DNS record failures, you should revisit your DNS configuration.
* All matrix-related subdomains should point to your server's IP address
* If you serve the base/apex domain from the matrix server itself, it should point to your server's IP address as well
* If you use any DNS proxy (like Cloudflare Proxy), it should be disabled for any of the matrix server's records

If that didn't help, please [contact us](/contacts/) to resolve the issue.

### High CPU usage

If the CPU usage of your server is constantly above 80%, you should consider upgrading your server to a more powerful one or removing additional components.

The minimum recommended configuration for a basic Matrix sever **without any add-ons** is 2 vCPUs:
* If your server has less than 2 vCPUs, consider upgrading it as soon as possible
* If your server has 2 vCPUs, and you have any additional components enabled, consider removing them as soon as possible
* If your server has 2 vCPUs, and you don't have any additional components enabled, one of the possible reasons for high CPU usage is that a user of your server joined a large room over federation (1000+ members). You could identify such rooms and remove them using [synapse-admin](/help/extras/synapse-admin/)

**On-premises servers**: please resize your server to a more powerful one using your VPS provider dashboard.

**Hosting by etke.cc**: please [contact us](/contacts/) to discuss the upgrade options.

### High RAM usage

If the RAM usage of your server is constantly above 80%, you should consider upgrading your server to a more powerful one or removing additional components.

Minimum recommended configuration for a basic Matrix sever **without any add-ons** is 2 GB of RAM:
* If your server has less than 2 GB of RAM, consider upgrading it as soon as possible
* If your server has 2 GB of RAM and you have any additional components enabled, consider removing them as soon as possible
* If your server has 2 GB of RAM and you don't have any additional components enabled, one of the possible reasons for high RAM usage is that a user of your server joined a large room over federation (1000+ members). You could identify such rooms and remove them using [synapse-admin](/help/extras/synapse-admin/)

**On-premises servers**: please resize your server to a more powerful one using your VPS provider dashboard.

**Hosting by etke.cc**: please [contact us](/contacts/) to discuss the upgrade options.

### High Disk usage

If the Disk usage of your server is constantly above 80%, you should consider upgrading your server or [removing the old media using synapse-admin](/help/extras/synapse-admin/#clearing-old-media).

The minimal recommended configuration for a basic Matrix sever **without any add-ons** is 40 GB of disk space:
* If your server has less than 40 GB of disk space, consider upgrading it as soon as possible
* If your server has 40 GB of disk space, consider removing the old media using [synapse-admin](/help/extras/synapse-admin/#clearing-old-media) as soon as possible
* If your server has 40 GB of disk space, and you don't have any old media or don't want to remove it, consider upgrading your server as soon as possible

**On-premises servers**: please resize your server's disk to a bigger one using your VPS provider dashboard.

**Hosting by etke.cc**: please [contact us](/contacts/) to discuss the upgrade options.

### Metrics failures

If you get Metrics failed in the monitoring report, that means something is wrong with your VPS or DNS.
That failure is the outcome of other other issues, possible reasons:

* [Misconfigured DNS records](/services/monitoring/#dns-records-failures)
* [High CPU usage](/services/monitoring/#high-cpu-usage)
* [High RAM usage](/services/monitoring/#high-ram-usage)
* [High Disk usage](/services/monitoring/#high-disk-usage)
* [Misconfigured firewall](/services/monitoring/#ports-failures)

**On-premises servers**: please ensure [all mandatory ports are open](/order/status/#ports-and-firewalls) and reachable from the internet, and the DNS records are configured properly, and CPU/RAM/Disk utilization is below recommended 80%. If that doesn't help, please [contact us](/contacts/) to resolve the issue.

**Hosting by etke.cc**: please ensure DNS records are configured properly. If that doesn't help [contact us](/contacts/) to resolve the issue.

### `/.well-known/matrix/server` failures

If the `/.well-known/matrix/server` endpoint is not available (be it a 404 error, invalid content, or any other issue), you should ensure the delegation is configured properly.

* If your base/apex domain is served outside the Matrix server (e.g., when you have a website on that domain), please refer to the [delegation redirects configuration](/order/status#delegation-redirects) FAQ section.
* If your base/apex domain is served by the Matrix server itself, please [contact us](/contacts/) to resolve the issue.

### `/.well-known/matrix/client` failures

If the `/.well-known/matrix/client` endpoint is not available (be it a 404 error, invalid content, or any other issue), you should ensure the delegation is configured properly.

* If your base/apex domain is served outside the Matrix server (e.g., when you have a website on that domain), please refer to the [delegation redirects configuration](/help/faq#delegation-redirects) FAQ section.
* If your base/apex domain is served by the Matrix server itself, please [contact us](/contacts/) to resolve the issue.

### `/.well-known/matrix/support` failures

If the `/.well-known/matrix/support` endpoint is not available (be it a 404 error, invalid content, or any other issue), you should ensure the delegation is configured properly.

* If your base/apex domain is served outside the Matrix server (e.g., when you have a website on that domain), please refer to the [delegation redirects configuration](/help/faq#delegation-redirects) FAQ section.
* If your base/apex domain is served by the Matrix server itself, please [contact us](/contacts/) to resolve the issue.

### `/_matrix/federation/v1/version` failures

If the `/_matrix/federation/v1/version` endpoint is not available (be it a 404 error, invalid content, or any other issue), something is wrong with your matrix server. Such failures are usually the outcome, not the cause of the issue, so you should check if there are any other issues (usually high CPU, RAM, or disk usage).

If your server has the Matrix Federation API disabled, and you still get this error, please [contact us](/contacts/) to resolve the issue.

### `/_matrix/client/versions` failures

If the `/_matrix/client/versions` endpoint is not available (be it a 404 error, invalid content, or any other issue), something is wrong with your matrix server. Such failures are usually the outcome, not the cause of the issue, so you should check if there are any other issues (usually, high CPU, RAM, or disk usage).

### Any other HTTP endpoints failures

Please, ensure:
* The DNS records are configured properly, pointing to the matrix server's IP, and any DNS proxy (like Cloudflare Proxy) is disabled for any of the matrix server's subdomains.
* The ports are open and reachable from the internet (on-premises servers only)
* CPU, RAM, and disk usage are below 80%

If that's not the case, please [contact us](/contacts/) to resolve the issue.

### Ports failures

Port failures mean a port that is expected to be open is unreachable for some reason. The most common reasons are:
* misconfigured firewall (on-premises servers)
* high CPU, RAM, or disk usage

**On-premises servers**: please ensure [all mandatory ports are open](/order/status/#ports-and-firewalls) and reachable from the internet. If that doesn't help, please [contact us](/contacts/) to resolve the issue.

**Hosting by etke.cc**: please [contact us](/contacts/) to resolve the issue.
