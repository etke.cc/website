---
title: Hosting
draft: false
layout: singletoc
description: Information about the etke.cc Hosting service - server sizes, regions, SSH access, etc.
---

When choosing a Hosting Type for [etke.cc](https://etke.cc), our [order form](/order/) provides you with 2 choices:

- **Hosting** (VPS server provided by us)
- **On-Premises** (Your Own Server)

The **Hosting** service means that we'll be hosting the server for you. This is a **fully-managed service with the least amount of hassle for you**.

Our hosted servers are powered by [Hetzner Cloud](https://hetzner.cloud/?ref=1yg3ZZmtrg5A), are available in various [sizes](#server-sizes) and across multiple [regions](#regions).

If you'd like more choice (with regard to server sizes, regional availability, etc.), you can always get an **On-Premises** server and handle the infrastructure yourself.


## Server Sizes

We provide a few **different server sizes** for hosted servers:

- Small: (2 vCPUs, 2 GB of RAM and 40 GB of disk space). Not powerful enough to run addon services.
- Medium: (3 vCPUs, 4 GB of RAM and 80 GB of disk space). A good choice for small deployments.
- Big: (4 vCPUs, 8 GB of RAM and 160 GB of disk space)
- Huge: (8 vCPUs, 16 GB of RAM and 240 GB of disk space)
- Monster: (16 vCPUs, 32 GB of RAM and 360 GB of disk space)

You can start with any server size and upgrade to a larger instance later on.


## Regions

Our hosted servers are currently provided in the following regions:

- Europe
  - Germany
    - Nuremberg
    - Falkenstein
  - Finland
    - Helsinki
- USA
  - Virginia
    - Ashburn
  - Oregon
    - Hillsboro


## Access

When you order a Hosted server from us, we'll be renting a server from [Hetzner Cloud](https://hetzner.cloud/?ref=1yg3ZZmtrg5A) and managing it on your behalf.

Despite this, if you'd like, you can get full `root` [SSH](https://en.wikipedia.org/wiki/Secure_Shell) access and are free to run other services on your server yourself.

For increased security, we default to restricting SSH port (`22`) connectivity to a a list of known IP addresses/networks. If you request SSH access, we'll ask you for:

- your **public** SSH key (of type `ed25519` or `rsa`)
- a comma-separated list of IP addresses from which you will access

If you do not have a static IP address and are okay with the security risks, we may allow connectivity to port `22` from anywhere.

It's important to note that:

- you **modifying Matrix configuration manually on the server is prohibited** because:
  - it often causes breakage and increases our [support](/services/support/) burden
  - will typically be rolled-back by our automation ([scheduler bot](/help/extras/scheduler/))

- you **running other services along our stack is OK**, but:
  - may cause breakage
  - may break at any time in the future due to service upgrades performed by us
  - comes without any support from us

More details are available in [the FAQ entry](/help/faq/#can-i-modify-the-server-configuration-or-run-additional-services)


## CPU architecture

We only support the [amd64](https://en.wikipedia.org/wiki/X86-64#AMD64) (`x86_64`) [CPU](https://en.wikipedia.org/wiki/Central_processing_unit) architecture right now, because it's what most services are packaged & tested for.

The [ARM](https://en.wikipedia.org/wiki/ARM_architecture_family) CPU architecture is currently **not supported**.
