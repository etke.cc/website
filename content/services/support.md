---
title: Customer Support
draft: false
layout: single
description: on that page, you can read about the Customer Support service we offer.
---

We provide the following levels of customer support:

## Basic

* Price: FREE
* Free requests per 30 days: 4
* Additional requests: $5/each
* Priority: standard
* Contact method: [contacts](/contacts)
* Estimated Response Time: up to 48h (Monday-Friday)
* [Covered topics](#topics)

This support level is included with all server subscriptions we provide.
Usually, the vast majority of our customers perform 1-2 support requests per month at most,
so this support level should be adequate.

## Dedicated

* Price: $100/month
* Free requests per 30 days: 10
* Additional requests: $5/each
* Priority: high
* Contact method: a specific dedicated matrix room with [etke.cc](https://etke.cc) developers (not email, slack, etc.)
* Estimated Response Time: up to 12h (Monday-Friday)
* [Covered topics](#topics)

This support level is useful when your product relies on Matrix and you may require quicker help in case of outages.
The main differences with the Basic level are the dedicated Matrix chat room with [etke.cc](https://etke.cc) developers and the higher priority of your requests relative to others.

## Estimated Response Time

Our estimated response times of 12 hours or 48 hours are calculated based on our working hours.
This means our response time calculations are made during our active working periods, excluding weekends and holidays.
If you send a request on Friday evening, you can expect a response on Monday morning, for example.

## How requests are counted

**Note**: Any support request related to a service outage (e.g., the Matrix server being down) is counted but **not charged**,
even if you exceed the number of requests included with your support plan.

We consider any contact from a customer or their representative using any contact method 
(including DMs/emails to specific etke.cc developers) with a specific topic as a support request. 
If within 1 message you request multiple things (e.g., add SchildiChat, disable federation, and enable jitsi auth), 
each separate thing is counted as a request. 
So, considering that example, that message is counted as **three** requests.

We're counting the number of support requests you've made using any available contact method for the **last** 30 days (sliding window).
For example, if it's October 1st now, and you've requested some changes to your server on September 13th and then today (October 1st), the number of used support requests would be **2**.

## Topics

**Covered topics**:
* server outages (e.g., server is down or server returns 404 errors)
* component outages (e.g., postmoogle is down due to system issue)
* system issues (e.g., high disk utilization)
* reconfiguration requests (e.g., add sliding sync to the server)

**NOT covered topics** are anything that is not included within the covered topics above; examples include:
* Guidance about the matrix ecosystem is not covered - you can use the spectacular [Matrix protocol documentation](https://matrix.org/docs)
* How to build a bot/appservice on top of Matrix is not covered - you can use the spectacular [Matrix protocol documentation](https://matrix.org/docs)
* Guidance about some specific bridge usage is not covered - each bridge page within the [Bridges](/help/bridges) section has a `support` button that will lead you to a matrix room with the bridge's developers and deeply involved community
* Why a bridge doesn't work as you expect it to is not covered - each bridge page within the [Bridges](/help/bridges) section has a `support` button that will lead you to a matrix room with the bridge's developers and deeply involved community
* Why a bridge fails to work after code changes is not covered - each bridge page within the [Bridges](/help/bridges) section has a `support` button that will lead you to a matrix room with the bridge's developers and deeply involved community
* How to build a business on top of Matrix is not covered

Rule of thumb here - if your problem is with a component that doesn't work as you expect it to or with a component that you don't know how to make use of - we can't help you with that. Our job is to install and maintain the server as per your request, not provide extensive help with each of the various components of it.
