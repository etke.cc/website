---
title: Custom Development
draft: false
layout: singletoc
description: on that page, you can read about the Custom Development service we offer.
---

We created and maintain [various open-source projects](https://gitlab.com/etke.cc), and we can develop new features you want to see within those projects.

Some highlights:

* [A.G.R.U.](https://gitlab.com/etke.cc/tools/agru) - a fast replacement for the ansible-galaxy command
* [Buscarron](https://gitlab.com/etke.cc/buscarron) - a web forms to matrix bridge
* [EMM](https://gitlab.com/etke.cc/tools/emm) - a tool to export matrix messages using arbitrary templates
* [Honoroit](https://gitlab.com/etke.cc/honoroit) - a helpdesk bot
* [M.A.S.H. playbook](https://github.com/mother-of-all-self-hosting/mash-playbook) - a next iteration of the infrastructure automation
* [Matrix Rooms Search](https://gitlab.com/etke.cc/mrs/api) (and its demo instance [MatrixRooms.info](https://matrixrooms.info)) - a search engine across Matrix Federation
* [Postmoogle](https://gitlab.com/etke.cc/postmoogle) - an email to matrix bridge
* [TTM](https://gitlab.com/etke.cc/tools/ttm) - a `time`-like tool that sends results into matrix rooms
* [ansible-ssh](https://gitlab.com/etke.cc/tools/ansible-ssh) - an SSH client wrapper that uses ansible inventory as configuration source
* [matrix-docker-ansible-deploy playbook](https://github.com/spantaleev/matrix-docker-ansible-deploy) - a core of our automation

Of course, you can just create an issue in a project's repository to request a new feature and get it implemented, if we will find it useful for **our** services or if someone from the open-source community will implement it and send an MR with the requested feature.

But if you want to get that feature for sure and not rely on our and community good will, you can pay the project developers to implement that feature.

## Terms

Our hourly rate is `$150/hour` and we're ready to get the job done with the following terms:

1. The scope of work must be agreed before payment.
2. All the results of commissioned work are released under the GPL/AGPL license (depending on the original project license) as part of the original open-source project.
3. After the scope has been agreed and payment has been made, there is no refund possible.
4. We reserve the right to refuse your request if the commission doesn't fit into our vision of the project.

Please, add the issue URL you us want to implement/fix to the message field

## How to

1. Create a GitLab issue with the clear explanations of the changes you would like to get
2. Send a comment `@etke.cc I want to pay to make that done`
3. One of the etke.cc developers will join, review the issue you created, ask some additional questions
4. Once the scope of work is agreed, we'll ask you to pay for the number of hours we think the implementation will take, if the requested changes fit in our project vision (meaning requests unrelated to the project's scope will be rejected)
5. After the payment, we'll implement the requested changes and release them publicly
