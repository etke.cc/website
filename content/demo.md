---
title: Demo server
noindex: true
draft: false
layout: single
---

To try how your own Matrix server would work before [ordering](/order/), you can **try our demo instance**.

Our demo instance is a fully-featured Matrix server with additional components, and a few limitations.

## Limitations

* Data retention is 1 day - all data is deleted after 1 day, including accounts, rooms, messages, and files.
* No admin access - you can try to use user-level features, but you cannot change server settings.
* Not all features and components are enabled - some features and components are disabled to prevent abuse.
* No guarantees - the server is provided as-is, and we do not guarantee its availability or performance.
* No support - we do not provide support for the demo server.

## Access

### Matrix

You can access the demo server at [https://demo.etke.host](https://demo.etke.host).

Unlike servers we setup, this is an instance with [public registration](/help/faq/#can-i-change-the-registration-sign-up-flow).
You will need to **register a new account** and **confirm your email address** (abuse prevention measure).

The following Matrix-related components are available:

* [Element Web](https://element.demo.etke.host) - recommended web client
* [Cinny](https://cinny.demo.etke.host) - alternative web client
* [Hydrogen](https://hydrogen.demo.etke.host) - catching-up web client
* [SchildiChat](https://schildichat.demo.etke.host) - catching-up web client
* [Ntfy](https://ntfy.demo.etke.host) - UnifiedPush provider ([details](/help/extras/ntfy/))
* [Radicale](https://radicale.demo.etke.host) - CalDAV/CardDAV server, use your matrix login (`user`, not `@user:demo.etke.host`) and password to acccess it

The following [Matrix bots](/help/bots/) and [bridges](/help/bridges/) are also available:

* Reminder-Bot: [@reminder:demo.etke.host](https://matrix.to/#/@reminder:demo.etke.host) ([details](/help/bots/reminder/))
* Discord: [@discordbot:demo.etke.host](https://matrix.to/#/@discordbot:demo.etke.host) ([details](/help/bridges/mautrix-discord/))
* Facebook/Messenger: [@messengerbot:demo.etke.host](https://matrix.to/#/@messengerbot:demo.etke.host) ([details](/help/bridges/mautrix-meta-messenger/))
* Gmessages: [@gmessagesbot:demo.etke.host](https://matrix.to/#/@gmessagesbot:demo.etke.host) ([details](/help/bridges/mautrix-gmessages/))
* Googlechat: [@googlechatbot:demo.etke.host](https://matrix.to/#/@googlechatbot:demo.etke.host) ([details](/help/bridges/mautrix-googlechat/))
* Instagram: [@instagrambot:demo.etke.host](https://matrix.to/#/@instagrambot:demo.etke.host) ([details](/help/bridges/mautrix-meta-instagram/))
* Irc: [@heisenbridge:demo.etke.host](https://matrix.to/#/@heisenbridge:demo.etke.host) ([details](/help/bridges/heisenbridge/))
* Linkedin: [@linkedinbot:demo.etke.host](https://matrix.to/#/@linkedinbot:demo.etke.host) ([details](/help/bridges/beeper-linkedin/))
* Postmoogle: [@emailbot:demo.etke.host](https://matrix.to/#/@emailbot:demo.etke.host) ([details](/help/bridges/postmoogle/))
* Signal: [@signalbot:demo.etke.host](https://matrix.to/#/@signalbot:demo.etke.host) ([details](/help/bridges/mautrix-signal/))
* Skype: [@skypebridgebot:demo.etke.host](https://matrix.to/#/@skypebridgebot:demo.etke.host) ([details](/help/bridges/go-skype-bridge/))
* Slack: [@slackbot:demo.etke.host](https://matrix.to/#/@slackbot:demo.etke.host) ([details](/help/bridges/mautrix-slack/))
* Twitter: [@twitterbot:demo.etke.host](https://matrix.to/#/@twitterbot:demo.etke.host) ([details](/help/bridges/mautrix-twitter/))
* Webhooks: [@hookshot:demo.etke.host](https://matrix.to/#/@hookshot:demo.etke.host) ([details](/help/bridges/hookshot/))
* WeChat: [@wechatbot:demo.etke.host](https://matrix.to/#/@wechatbot:demo.etke.host) ([details](/help/bridges/wechat/))
* Whatsapp: [@whatsappbot:demo.etke.host](https://matrix.to/#/@whatsappbot:demo.etke.host) ([details](/help/bridges/mautrix-whatsapp/))
<!-- * Telegram: [@telegrambot:demo.etke.host](https://matrix.to/#/@telegrambot:demo.etke.host) ([details](/help/bridges/mautrix-telegram)) -->

### Non-Matrix

You can access the following components using `demo` login:

* [Jitsi](https://jitsi.demo.etke.host) - video conferencing, password: `demo` ([details](/help/extras/jitsi/))
* [Miniflux](https://miniflux.demo.etke.host) - RSS reader, password: `demo123` ([details](/help/extras/miniflux/))
* [Grafana](https://stats.demo.etke.host) - server monitoring, password: `demo`
