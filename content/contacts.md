---
title: Contacts
draft: false
description: Contact methods
layout: singletoc
---

## Contacting Us

The contact methods listed below are available for users with a [Basic support level](/services/support/#basic) and for general inquiries about our services, catering to individuals, companies, or communities interested in etke.cc.

If you require assistance with any of our open-source projects, please utilize the respective project's community support room.

Before reaching out, we recommend consulting [the FAQ](/help/faq/) and [Help pages](/help/) to see if your question has already been addressed. If not, anticipate a response within 48 hours.

{{< partial "contacts/primary.html" >}}

Upon sending your request, you will immediately receive an acknowledgment message/email - an automatic response from our Matrix bot. If not, refer to the [Troubleshooting](#troubleshooting) section.

## Troubleshooting

### Chat Support Issues

If you message us on Matrix, the following failure scenarios may happen:

- **Matrix bot didn't join your room**: Ensure your Matrix server is reachable over federation using the [Federation Tester](https://federationtester.matrix.org/).
- **Matrix bot didn't auto-respond to your first message**: Encryption may be breaking communication. Consider using an unencrypted room.

If chat support fails for some reason, please contact us via Email!

### Email Support Issues

If you sent us an email, but did not receive an acknowledgment email:

- Check your spam folder.
- Ensure the email address listed above is marked as "safe" in your email client.
- Verify your email server configuration, especially TLS certificates.

## Other Contact Options

> **NOTE**: These contact methods are provided by third-party services and may not be guaranteed to work.

{{< partial "contacts/secondary.html" >}}

If you have not received a response even after attempting the secondary methods, you may use [the public discussion room](https://matrix.to/#/#discussion:etke.cc) to ask your question.
