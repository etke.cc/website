---
title: warrant canary
draft: false
description: we strive to promote transparency by publicly displaying this message regarding the receipt of secret subpoenas, warrants, or other legal compulsory actions
---

We strive to promote transparency by publicly displaying this message 
regarding the receipt of secret subpoenas, warrants, or other legal compulsory actions. 

We are informing users that we have:

* not been issued any National Security letters
* not been issued any gag orders
* not been served with secret government subpoenas or warrants
* not been issued any Foreign Intelligence Surveillance Court orders
* no knowledge of backdoors in our services, infrastructure or software
* not had any seizures of assets

In the event that one of these items becomes no longer true,
it is our intention to edit or remove this message.

[The source of this page](https://gitlab.com/etke.cc/website/-/blob/master/content/canary.md),
and [the history of this page](https://gitlab.com/etke.cc/website/-/commits/master/content/canary.md) - keep an eye on it.
