---
title: Radicale
draft: false
no_toc: true
layout: extra-service
official_website: https://radicale.org
code: https://github.com/Kozea/Radicale
support: https://github.com/Kozea/Radicale/discussions
docs: https://radicale.org/master.html
description: A lightweight, open-source CalDAV and CardDAV server for syncing calendars and contacts across devices. Simple setup, compatible with various clients. Take control of your data privacy.
---

[Radicale](https://radicale.org) is a versatile and lightweight CalDAV and CardDAV server designed to simplify the synchronization of calendars and contacts across multiple devices and platforms.
Built with a focus on simplicity, reliability, and data privacy, Radicale offers individuals and small organizations an easy-to-use solution for hosting their own personal or internal calendar and contact synchronization service.
With support for standard protocols and compatibility with a wide range of CalDAV and CardDAV clients, Radicale empowers users to take control of their data while maintaining flexibility and accessibility across devices.
Whether for personal use or small-scale organizational needs, Radicale provides a reliable and privacy-conscious alternative to cloud-based synchronization services.

Features:
* **CalDAV Support**: Radicale supports the CalDAV protocol, allowing users to synchronize calendars across different devices and platforms.
* **CardDAV Support**: Radicale also supports the CardDAV protocol, enabling users to synchronize contact information across devices.
* **Matrix Authentication**: We've developed radicale auth over Matrix, providing secure access control for users and ensuring data privacy.
* **Lightweight**: Radicale is designed to be lightweight, making it easy to install and run on various systems, including small servers.
* **Simple Setup**: Radicale offers a straightforward setup process, requiring minimal configuration to get started with calendar and contact synchronization.
* **Flexible Configuration**: While simple to set up, Radicale provides options for advanced configuration and customization to meet specific requirements.
* **Cross-Platform Compatibility**: Radicale is compatible with a wide range of CalDAV and CardDAV clients, including desktop applications, mobile devices, and web-based interfaces.
* **Privacy and Security**: Radicale prioritizes user privacy and data security, allowing users to host their own synchronization service and retain control over their data.
* **Offline Access**: Users can access and modify their calendars and contacts offline, with changes automatically synchronized when a connection is available.
* **Sharing and Collaboration**: Radicale supports sharing calendars and contacts with other users, facilitating collaboration and coordination within teams and organizations.
* **Event and Contact Management**: Radicale allows users to create, edit, and delete calendar events and contact information, providing essential management features for personal and organizational use.
* **Extensibility**: Radicale is extensible through plugins, allowing developers to add additional functionality and integrate with other systems and services.
* **Active Development and Community**: Radicale is actively maintained by a community of developers, ensuring ongoing updates, improvements, and support for users.
* **Open Source**: Radicale is open-source software, licensed under the GNU Affero General Public License (AGPL), allowing users to access, modify, and distribute the code freely.

## Usage

We host your Radicale instance at a URL like: `https://radicale.your-server.com` - just replace `your-server.com` with your actual server domain, and open the URL in your browser. Welcome to your radicale instance!

Most likely, you'll want to configure your CalDAV and CardDAV clients to connect to your Radicale instance. Here are some proven to be working clients:
* [DAVx⁵](https://www.davx5.com/) - Android
* [GNOME Calendar](https://wiki.gnome.org/Apps/Calendar), [Contacts](https://wiki.gnome.org/Apps/Contacts), and [Evolution](https://wiki.gnome.org/Apps/Evolution) - Linux
* [Thunderbird](https://thunderbird.net) - Windows, macOS, Linux

The list is not exclusive, and you can use any CalDAV and CardDAV client that supports the protocols.
Just enter your Radicale URL and your credentials, and you're good to go!
