---
title: Synapse Single Sign-On
draft: false
no_toc: true
layout: extra-service
code: https://github.com/element-hq/synapse
support: https://github.com/element-hq/synapse/issues
docs: https://element-hq.github.io/synapse/latest/openid.html
description: Synapse can be configured to use an OpenID Connect Provider (OP) for authentication, instead of its own local password database. It's offered as an add-on service.
---

The [Synapse](https://github.com/element-hq/synapse) Matrix homeserver can be configured to use an [OpenID](https://en.wikipedia.org/wiki/OpenID) Connect Provider (OP) for authentication, in addition to (or instead of) its own local password database.

When Single Sign-On is enabled, Matrix client applications (like Element) will provide a **Continue with SSO** button as shown in the screenshot below:

![SSO example for the Element client app when configured to use SSO](/img/extra-services/synapse-sso/element-continue-with-sso.webp)

## Getting started

Any OpenID Connect Provider should work with Synapse, as long as it supports the [authorization code flow](https://auth0.com/docs/get-started/authentication-and-authorization-flow/authorization-code-flow). **Frequent** choices are 
[Keycloak](https://element-hq.github.io/synapse/latest/openid.html#keycloak), 
[Google](https://element-hq.github.io/synapse/latest/openid.html#google) and 
[Github](https://element-hq.github.io/synapse/latest/openid.html#github), but **at least 15** 
[other providers](https://element-hq.github.io/synapse/latest/openid.html#sample-configs) are known to work well.

To figure out how to prepare your OpenID Connect Provider and what configuration you would need, see the [Sample Configs](https://element-hq.github.io/synapse/latest/openid.html#sample-configs) section of the Synapse documentation.

**For new orders**, you can provide the Synapse Single Sign-On details using dedicated fields in our [order form](/order/) - no [YAML](https://en.wikipedia.org/wiki/YAML) formatting required.

**For adding SSO to existing servers**, [contact us](/contacts/) and ideally send over a YAML configuration based on the [sample config](https://element-hq.github.io/synapse/latest/openid.html#sample-configs).

## Operation

When at least one Single Sign-On provider is configured, Matrix client apps will start showing additional sign-in buttons (e.g. *Continue with Keycloak*).

By default, Synapse allows authentication **both with its local password database and with the configured Single Sign-On providers**. If you'd like to disable Synapse's local database authentication and only rely on SSO, let us know and we'll adjust the configuration for your server!

**Multiple Sigle Sign-On providers** can be enabled at the same time. Our [order form](/order/) only supports one, but feel free to contact us and we can enable any number of them!
