---
title: Funkwhale
draft: false
no_toc: true
layout: extra-service
official_website: https://funkwhale.audio/
code: https://dev.funkwhale.audio/funkwhale/funkwhale
support: https://matrix.to/#/#funkwhale-support:matrix.org
docs: https://docs.funkwhale.audio/
description: Funkwhale is a self-hosted audio player and publication platform. It enables users to build libraries of existing content and publish their own. Funkwhale uses the ActivityPub protocol to talk to other apps across the Fediverse. Users can share content between Funkwhale pods or with other Fediverse software.
---

Funkwhale is a self-hosted music server and decentralized audio platform designed to provide users with a way to stream, organize, and share music. Here are some key features of Funkwhale:

* **Audio Streaming**: Funkwhale allows users to stream music directly from their server. Users can upload their own music files or import their existing libraries into the platform.
* **Music Library Organization**: Users can organize their music libraries with tags, playlists, and favorites. This makes it easier to navigate and discover music within their collection.
* **Podcasts and Radio**: Funkwhale supports podcasts and internet radio, allowing users to subscribe to podcasts or create their own. Users can also listen to streaming radio stations from within the platform.
* **User Profiles**: Users can create profiles where they can showcase their favorite music, playlists, and podcasts. This adds a social element to the platform, allowing users to discover new music based on the preferences of others.
* **Federation**: Funkwhale is built on a federated architecture, which means that multiple Funkwhale instances can interconnect and share content. This decentralized approach promotes community building and ensures that content remains accessible even if one instance goes offline.
* **Customization and Theming**: Administrators can customize the appearance of their Funkwhale instance with different themes and branding options. This allows for a personalized user experience tailored to the preferences of the community.
* **Accessibility**: Funkwhale is designed with accessibility in mind, with features such as keyboard navigation and screen reader support to ensure that all users can access and enjoy the platform.

## Usage

We host your Funkwhale instance at a URL like: `https://funkwhale.your-server.com` - just replace `your-server.com` with your actual server domain, and open the URL in your browser. Welcome to your Funkwhale instance!

## Optional S3 storage

Funkwhale supports S3-compatible providers, which allows it to store music in an object storage system like [Amazon S3](https://aws.amazon.com/s3/) or any other S3-compatible alternative ([Wasabi](https://wasabi.com/), [Backblaze B2](https://www.backblaze.com/cloud-storage), [Cloudflare R2](https://www.cloudflare.com/developer-platform/r2/), etc).

Moving media files off of the server's local filesystem and into an external storage (the S3 storage system) allows you to **free up disk space on the server** and **effectively gives you infinite storage** (for a relatively-cheap price).

To make your Funkwhale server use an S3 storage provider, you first need to [choose an Object Storage provider](#choosing-an-object-storage-provider), then [create the S3 bucket](#bucket-creation-and-security-configuration) and finally send the details to us.

For new orders, you can provide the S3 Storage details right in our [order form](/order/).


### Choosing an Object Storage provider

You can create [Amazon S3](https://aws.amazon.com/s3/) or another S3-compatible object store like [Backblaze B2](https://www.backblaze.com/b2/cloud-storage.html), [Wasabi](https://wasabi.com), [Digital Ocean Spaces](https://www.digitalocean.com/products/spaces), etc.

Amazon S3 and Backblaze S3 are pay-as-you with no minimum charges for storing too little data.

All these providers have different prices, with Backblaze B2 appearing to be the cheapest.

Wasabi has a minimum charge of 1TB if you're storing less than 1TB, which becomes expensive if you need to store less data than that.

Digital Ocean Spaces has a minimum charge of 250GB ($5/month as of 2022-10), which is also expensive if you're storing less data than that.

Important aspects of choosing the right provider are:

- a provider by a company you like and trust (or dislike less than the others)
- a provider which has a data region close to your Matrix server (if it's farther away, high latency may cause slowdowns)
- a provider which is OK pricewise
- a provider with free or cheap egress (if you need to get the data out often, for some reason) - likely not too important for the common use-case


### Bucket creation and Security Configuration

Now that you've [chosen an Object Storage provider](#choosing-an-object-storage-provider), you need to create a storage bucket.

How you do this varies from provider to provider, with Amazon S3 being the most complicated due to its vast number of services and complicated security policies.

Below, we provider some guides for common providers. If you don't see yours, look at the others for inspiration or read some guides online about how to create a bucket. Feel free to contribute to this documentation with an update!

### Amazon S3

You'll need an Amazon S3 bucket and some IAM user credentials (access key + secret key) with full write access to the bucket. Example IAM security policy:

```json
{
	"Version": "2012-10-17",
	"Statement": [
		{
			"Sid": "Stmt1400105486000",
			"Effect": "Allow",
			"Action": [
				"s3:*"
			],
			"Resource": [
				"arn:aws:s3:::your-bucket-name",
				"arn:aws:s3:::your-bucket-name/*"
			]
		}
	]
}
```

**NOTE**: This policy needs to be attached to an IAM user created from the **Security Credentials** menu. This is not a **Bucket Policy**.


### Backblaze B2

To use [Backblaze B2](https://www.backblaze.com/b2/cloud-storage.html) you first need to sign up.

You [can't easily change which region (US, Europe) your Backblaze account stores files in](https://old.reddit.com/r/backblaze/comments/hi1v90/make_the_choice_for_the_b2_data_center_region/), so make sure to carefully choose the region when signing up (hint: it's a hard to see dropdown below the username/password fields in the signup form).

After logging in to Backblaze:

- create a new **private** bucket through its user interface (you can call it something like `peertube-DOMAIN-media-store`)
- note the **Endpoint** for your bucket (something like `https://s3.us-west-002.backblazeb2.com`).
- adjust its Lifecycle Rules to: Keep only the last version of the file
- go to [App Keys](https://secure.backblaze.com/app_keys.htm) and use the **Add a New Application Key** to create a new one
  - restrict it to the previously created bucket (e.g. `peertube-DOMAIN-media-store`)
  - give it *Read & Write* access

The `keyID` value is your **Access Key** and `applicationKey` is your **Secret Key**.

### Other providers

For other S3-compatible providers, you may not need to configure security policies, etc. (just like for [Backblaze B2](#backblaze-b2)).

You most likely just need to create an S3 bucket and get some credentials (access key and secret key) for accessing the bucket in a read/write manner.
