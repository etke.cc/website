---
title: LanguageTool
draft: false
no_toc: true
layout: extra-service
official_website: https://languagetool.org
docs: https://languagetool.org/
code: https://github.com/languagetool-org/languagetool
support: https://forum.languagetool.org/
description: LanguageTool is an Open Source proofreading software for English, Spanish, French, German, Portuguese, Polish, Dutch, and more than 20 other languages. It finds many errors that a simple spell checker cannot detect.
---

[LanguageTool](https://languagetool.org) is an Open Source proofreading software for English, Spanish, French, German, Portuguese, Polish, Dutch, and [more than 20 other languages](https://dev.languagetool.org/languages). It finds many errors that a simple spell checker cannot detect.

## Usage

**URL**: `https://languagetool.your-server.com`

That is API URL, there is no web interface for this service.
To use LanguageTool with UI, you can install browser extensions, e-mail addons, office plugins, and even apps!

Once a client is installed, you can configure it to use your own LanguageTool instance by setting the URL to `https://languagetool.your-server.com`.

**Browser Extensions**: [Firefox](https://addons.mozilla.org/en-US/firefox/addon/languagetool/), 
[Chrome](https://chrome.google.com/webstore/detail/grammar-and-spell-checker/oldceeleldhonbafppcapldpdifcinji), 
[Safari](https://apps.apple.com/app/id1534275760), 
[Edge](https://microsoftedge.microsoft.com/addons/detail/hfjadhjooeceemgojogkhlppanjkbobc), 
[Opera](https://addons.opera.com/extensions/details/grammar-and-spell-checker-languagetool/)

**E-mail Addons**: [Gmail](https://languagetool.org/google-mail),
[Outlook](https://languagetool.org/windows-desktop),
[Apple Mail](https://languagetool.org/apple-mail),
[Thunderbird](https://languagetool.org/thunderbird)

**Office Plugins**: [Google Docs](https://languagetool.org/google-docs),
[Microsoft Word](https://languagetool.org/word),
[Apple Pages](https://languagetool.org/mac-desktop),
[OpenOffice](https://languagetool.org/open-office),
[LibreOffice](https://languagetool.org/libre-office)

**Apps**: [MacOS](https://languagetool.org/mac-desktop),
[Windows](https://languagetool.org/windows-desktop),
[iOS](https://languagetool.org/ios)

### n-grams

LanguageTool can make use of large n-gram data sets to detect errors with words that are often confused, like _their_ and _there_.
The n-gram data set is **huge** and thus not part of the default configuration ([details](https://dev.languagetool.org/finding-errors-using-n-gram-data)). To make use of it, you need at least 20GB of free disk space per language. If you have that - just [contact us](/contacts) and tell what languages you want to use n-grams for.

**Supported languages**: English, German, Spanish, French, Hebrew, Italian, Dutch, Russian, and Chinese.
