---
title: Vaultwarden
draft: false
layout: extra-service
no_toc: true
docs: https://github.com/dani-garcia/vaultwarden/wiki
code: https://github.com/dani-garcia/vaultwarden
support: https://matrix.to/#/%23vaultwarden:matrix.org
description: Vaultwarden is a password manager server (compatible with Bitwarden) that you can use with the official Bitwarden apps and browser addons. It's offered as an add-on service.
---

[Vaultwarden](https://github.com/dani-garcia/vaultwarden) is an unofficial [Bitwarden](https://bitwarden.com) compatible password manager server that you can use with the official [Bitwarden](https://bitwarden.com/) apps and browser addons.
It's is **much more lightweight than Bitwarden**, which makes it a good choice for hosting.

You can find links to Bitwarden applications in the [download section](https://bitwarden.com/download/) of the Bitwarden website and in some quick links below. Refer to the [Usage](#usage) section for hints on how to connect these applications to your Vaultwarden server.

![Vaultwarden Web UI screenshot](/img/extra-services/vaultwarden/vaultwarden-screenshot.webp)

## Usage

- **Server URL**: `https://vault.your-server.com`
- **Administrative URL**: `https://vault.your-server.com/admin`

The first time you use Vaultwarden, you'd need to create one or more accounts via the Administrative UI, after authenticating with an admin token (password) that we have provided to you.

You then proceed to [use it on the web](#usage-on-the-web), via a [mobile app](#usage-on-mobile) or on the command-line.

Below, we include some quick links and descriptions for how to connect to your Vaultwarden instance, but [any Bitwarden application](https://bitwarden.com/download/) can be made to work with your instance in a similar way.

### Usage on the web

Using a web browser, you have 2 ways to access Vaultwarden.

One is via the **Web UI** available at the *Server URL* (mentioned above). Another is via a **browser addon**:

- [Bitwarden for Firefox](https://addons.mozilla.org/en-US/firefox/addon/bitwarden-password-manager/)
- [Bitwarden for Chrome & Chromium-based browsers](https://chromewebstore.google.com/detail/bitwarden-free-password-m/nngceckbapebfimnlniiiahkandclblb)

When using the browser addons, choose **self-hosted** and point the addon to your *Server URL* (mentioned above).

### Usage on mobile

- [Bitwarden for Android](https://play.google.com/store/apps/details?id=com.x8bit.bitwarden&hl=en_US&gl=US)
- [Bitwarden for iOS](https://apps.apple.com/us/app/bitwarden-password-manager/id1137397744)

When using the mobile apps, choose the **self-hosted** option and point the app to your *Server URL* (mentioned above).
