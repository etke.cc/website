---
title: BorgBackup
draft: false
layout: extra-service
no_toc: true
official_website: https://www.borgbackup.org/
docs: https://borgbackup.readthedocs.io/en/stable/
code: https://github.com/borgbackup/borg
support: https://web.libera.chat/#borgbackup
description: Borg Backup is backup solution (deduplicating archiver with compression and encryption), offered as an add-on service.
---

We can setup [borgbackup](https://www.borgbackup.org/) with [borgmatic](https://torsion.org/borgmatic/) for you.

BorgBackup is a deduplicating backup program with optional compression and encryption.
That means your daily incremental backups can be stored in a fraction of the space and is safe whether you store it at home or on a cloud service.

## Target audience

**If you're getting your server from us**, we automatically [enable server backups](https://docs.hetzner.com/cloud/servers/getting-started/enabling-backups/) with our provider ([Hetzner Cloud](https://hetzner.cloud/?ref=1yg3ZZmtrg5A)), which gives you 7 daily backups of your whole server.

**If you're getting your server from someone else**, similar whole-server snapshot options may be available to you.

Nevertheless, it's **always a good idea to have extra backups**, especially at other locations (off-site, etc.)


## Data being backed up and schedule

BorgBackup will back up the files for all services and also include a database dump of the [PostgreSQL](https://www.postgresql.org/) database we run for you on the server.

The backup will run based a schedule, defaulting to **4am every day**.


## Getting started

To use our BorgBackup service, you will **need a remote server** where backups will be pushed.

There are hosted, borg-compatible solutions available, such as [BorgBase](https://www.borgbase.com) and [rsync.net](https://rsync.net/).


