---
title: The Scheduler
draft: false
layout: extra-service
description: "Discover The Scheduler, an exclusive maintenance system designed for etke.cc customers. Customize your server maintenance with flexibility and a choice of stability branches. Get started with straightforward commands and enjoy four distinct work modes: Run, Schedule, Recurring, and List. Proactively receive alerts about server issues. Select from essential commands like ping, disk, maintenance, and restart. Choose between stability branches 'stable' for reliability and 'fresh' for the latest updates. Stay in control of your server's maintenance and stability with The Scheduler."
aliases:
    - /scheduler
---

**Introduction**

Introducing The Scheduler, a dedicated maintenance system tailored for etke.cc customers. 
This exclusive tool empowers you with the ability to manage essential server maintenance tasks and tackle common challenges like handling disk usage and service restarts.

**Customize Your Experience**

One of the unique features of The Scheduler is its flexibility, 
allowing you to select from different [stability branches](#stability-branches) and maintenance timeframe for your homeserver.
This customization enables you to align your maintenance process with your specific requirements.

**Getting Started**

Initiating and using The Scheduler is straightforward. 
Simply initiate a chat with [@scheduler:etke.cc](https://matrix.to/#/@scheduler:etke.cc) and send the command `help` to access detailed instructions.

> **Note**: The Scheduler bot exclusively serves etke.cc customers, making it an invaluable resource for your server management needs.

**Four Work Modes**

The Scheduler supports four distinct work modes, each designed to cater to specific tasks and needs. 
While you can utilize all modes simultaneously, 
it is recommended to employ recurring maintenance to ensure your server remains up to date and protected.

### Run

The `run` mode immediately executes a [command](#commands), providing rapid results. For instance:

* @customer: run ping
* @scheduler: Command execution initiated. Please be patient, as this may take some time. You'll receive a notification upon completion.
* @scheduler: Execution of `run ping` command **successful**

```
INFO [etke.cc] disk usage: 85%
```

* 🟩 CPU utilization is ~**7%**
* 🟥 Disk utilization is ~85%: recommended is below 80% ([help](/services/monitoring/#high-disk-usage))
* 🟩 RAM utilization is ~**77%**
* 🟥 25 (tcp port): timeout ([help](/services/monitoring/#ports-failures))
* 🟥 587 (tcp port): timeout ([help](/services/monitoring/#ports-failures))
* 🟥 [etke.cc federation]: invalid character '<' looking for beginning of value ([help](/services/monitoring/#well-knownmatrixserver-failures))
* 🟩 https://matrix.etke.cc/_matrix/client/versions
* 🟩 https://matrix.etke.cc:8448/_matrix/federation/v1/version
* 🟥 social.etke.cc (DNS record): no or invalid A record; invalid CNAME record (expected: 1.2.3.4, actual: 4.3.2.1, 5.6.7.8) ([help](/services/monitoring/#dns-records-failures))

Explore the [full list of available commands here](#commands).

### Schedule

The `schedule` mode allows you to plan a command for a specific date and time (UTC). 
For example, scheduling maintenance for July 1st at 5 pm UTC:

* @customer: schedule maintenance 2022-07-01 17:00
* @scheduler: Command scheduled.

On July 1st, around 5:30 pm UTC (accounting for the duration of maintenance), you will receive the following message:

* @scheduler: Execution of `run maintenance` command **successful**
* @scheduler: Identified issues:
* 🟥 [https://etke.cc/.well-known/matrix/server](https://etke.cc/.well-known/matrix/server) ([help](/services/monitoring/#well-knownmatrixserver-failures))

The `schedule` mode essentially schedules the `run` mode for a specified future date and time.

### Recurring

The `recurring` mode configures specific **weekdays** and times for recurring `schedule` mode commands. 
By default, automatic maintenance is scheduled to a random weekday and time. 
This mode simplifies recurring tasks, 
automatically creating corresponding schedules for executing the chosen command at specific times on specified weekdays. 
For example, setting up server maintenance every Friday at 3 pm UTC:

* @customer: recurring maintenance Friday 15:00
* @scheduler: Recurring schedule configured.

Every Friday, around 3:30 pm UTC (considering maintenance duration), you will receive this message:

* @scheduler: Execution of `run maintenance` command **successful**

> **Note**: You can set only one recurring schedule per command (e.g., one `recurring maintenance`). 
> Multiple recurring configurations for the same command will replace one another, 
> leaving you with a single recurring configuration (the last one) — for example, on Monday at 5 pm UTC.

### List

The `list` mode serves as a useful tool for reviewing your executed commands, scheduled tasks, and recurring schedules. 
It is especially handy when you need to check specific information. Here are some examples:

* @customer: list runs
* @scheduler:

```
* 2022-06-12 16:36: run ping
* 2022-05-30 22:01: run maintenance
* 2022-06-19 16:03: run restart slack failed: exit status 1
```

* @customer: list schedules
* @scheduler:

```
* 2022-06-27 16:00: run ping (recurring)
* 2022-07-01 17:00: run ping
```

> **Note**: The `(recurring)` suffix indicates that the task is automatically scheduled because it is part of a (weekly) `recurring` schedule.
> It shows the nearest future recurrence date.

* @customer: list recurring
* @scheduler:



```
* Monday 16:00: run ping
```

### Alert

In addition to its server management capabilities, The Scheduler proactively sends alerts when issues arise with your server. These alerts can include notifications about high disk usage or unreachable API endpoints on your Matrix server.

More details about the monitoring system is available on the [Monitoring](/services/monitoring/) page.

Please note that The Scheduler will automatically initiate a chat with you if you don't already have one set up.

## Commands

**Command List**

The Scheduler provides several essential commands to address various server management needs:

### ping

The `ping` command checks server reachability and disk utilization. It's especially useful when configuring a new firewall or ensuring you have sufficient storage for those important cat photos.

Example:

* @customer: run ping
* @scheduler: Command execution initiated. Please be patient; you will receive a notification upon completion.
* @scheduler: Execution of `run ping` command **successful**

```
INFO [etke.cc] disk usage: 85%
```

* 🟩 CPU utilization is ~**7%**
* 🟥 Disk utilization is ~85%: recommended is below 80% ([help](/services/monitoring/#high-disk-usage))
* 🟩 RAM utilization is ~**77%**
* 🟥 25 (tcp port): timeout ([help](/services/monitoring/#ports-failures))
* 🟥 587 (tcp port): timeout ([help](/services/monitoring/#ports-failures))
* 🟥 [etke.cc federation]: invalid character '<' looking for beginning of value ([help](/services/monitoring/#well-knownmatrixserver-failures))
* 🟩 https://matrix.etke.cc/_matrix/client/versions
* 🟩 https://matrix.etke.cc:8448/_matrix/federation/v1/version
* 🟥 social.etke.cc (DNS record): no or invalid A record; invalid CNAME record (expected: 1.2.3.4, actual: 4.3.2.1, 5.6.7.8) ([help](/services/monitoring/#dns-records-failures))

More details about the monitoring system and checked parameters is available on the [Monitoring](/services/monitoring/) page.

### disk

The `disk` command assesses disk utilization for common server components, providing valuable insights when disk usage is a concern.

Example:

* @customer: run disk
* @scheduler: Command execution initiated. Please be patient; you will receive a notification upon completion.
* @scheduler: Execution of `run disk` command **successful**

```
INFO [etke.cc] disk usage: 65%
INFO [etke.cc] Database: 5.9G
INFO [etke.cc] Database backups: 8.6G
INFO [etke.cc] Media: 3.8G
```

### price

The `price` command provides an overview of your server's cost with detailed breakdown by components.
You may request changes to your server configuration or additional services based on this information, just [contact us](/contacts/)

Example:

* @customer: run price
* @scheduler: Command execution initiated. Please be patient; you will receive a notification upon completion.
* @scheduler: Execution of `run price` command **successful**

The total price for your server is **$65**

Here is the detailed breakdown (free items are skipped):

* **[BorgBackup](https://etke.cc/help/extras/borgbackup)**: `$3` _The \"Holy Grail\" of backups (requires a separate Borg backup provider)_
* **[Matrix](https://etke.cc/help/faq#what-are-the-base-matrix-components-installed-on-the-server)**: `$5` _Base components and services for any server_
* **[Email service](https://etke.cc/help/extras/email-hosting)**: `$5` _A fully-featured multi-user email service for your domain (you@domain.com), powered by Migadu. SMTP Relay is included at no extra cost._
* **[Hosting (Big)](https://etke.cc/services/hosting/)**: `$25` _The VPS where your Matrix server is hosted_
* **[GoToSocial](https://etke.cc/help/extras/gotosocial)**: `$3` _A fast, fun, ActivityPub server_
* **[Buscarron](https://etke.cc/help/bots/buscarron)**: `$3` _A bot that receives web form submissions (HTML/HTTP POST) and sends them to (encrypted) Matrix rooms_
* **[Honoroit](https://etke.cc/help/bots/honoroit)**: `$3` _A helpdesk bot with end-to-end encryption support_
* **[Bridges](https://etke.cc/help/bridges/)**: `$5` _With the help of bridges, you can access different networks right from your own Matrix server_
* **[Synapse Single Sign-On](https://etke.cc/help/extras/synapse-sso)**: `$5` _Log in to Matrix with an OpenID provider as a backend (Auth0, Google, Github, Gitlab, Okta, etc.)_
* **[Synapse Workers](https://etke.cc/help/extras/synapse-workers)**: `$5` _A multi-process homeserver setup to more efficiently handle many users and large rooms. Requires a powerful server (8+ GB of RAM)_
* **[Miniflux](https://etke.cc/help/extras/miniflux)**: `$1` _A minimalist and opinionated feed reader_
* **[Ntfy](https://etke.cc/help/extras/ntfy)**: `$1` _UnifiedPush provider for (private) push notifications_
* **[Radicale](https://etke.cc/help/extras/radicale)**: `$1` _Free and Open-Source CalDAV and CardDAV Server_

### maintenance

This command triggers the [maintenance](/services/#maintenance) process, which typically takes 15-30 minutes to complete. You can find further details on the [services](/services) page.

Example:

* @customer: run maintenance
* @scheduler: Command execution initiated. Please be patient; you will receive a notification upon completion.
* @scheduler: Execution of `run maintenance` command **successful**

### restart

The `restart` command serves to restart a systemd service, particularly helpful when a bridge is unresponsive. It features an internal mapping between "user-friendly" service names and actual service names. For instance, if you wish to restart the Facebook bridge, you can use `run restart facebook` instead of specifying `run restart matrix-mautrix-facebook` (although the latter is equally valid).

Example:

* @customer: run restart slack
* @scheduler: Command execution initiated. Please be patient; you will receive a notification upon completion.
* @scheduler: Execution of `run restart slack` command **successful**

**Note**: In the [`schedule`](#schedule) and [`recurring`](#recurring) modes, the `restart` command must be placed after the date/weekday and time. For example, you can use `schedule restart 2022-07-01 17:00 slack` or `recurring restart Monday 17:00 slack`.

**Stability Branches**

The Scheduler acknowledges that customers have varying preferences when it comes to the stability of updates. To cater to these preferences, we offer different stability branches to choose from. If you wish to switch to an alternative stability branch for your homeserver, kindly [contact us](/contacts) for assistance.

### stable (default)

The `stable` branch, as its name implies, is the go-to choice for stability and reliability. It features well-tested updates and typically lags a few days (up to a week) behind the `fresh` branch.

With `stable` as your default stability branch, we gather component updates throughout the week. We conduct extensive testing on developer servers, open-source community members' servers, and customer servers specifically opted into the `fresh` stability branch. Once we're confident in the stability of these updates, they are published in the `stable` branch, ensuring they won't disrupt your server when installed during maintenance.

`stable` serves as the default stability branch for all customer servers unless customers explicitly request to use `fresh`.

### fresh (testing)

The `fresh` branch is designed for those who crave the latest updates as soon as possible. Updates arrive in this branch without delay, often a week earlier than `stable`. While `fresh` updates might contain breaking changes, we typically prepare workarounds and migrations in advance.

To access updates quickly, even at the risk of encountering occasional breakage, choose the `fresh` stability branch. Note that real-time updates are available in the dedicated room [#fresh:etke.cc](https://matrix.to/#/%23fresh:etke.cc), while the current differences between `fresh` and `stable` are detailed in the [VERSIONS.diff.md in the repository](https://gitlab.com/etke.cc/ansible/-/blob/fresh/VERSIONS.diff.md)

**Stay in Control with The Scheduler**

The Scheduler is here to put you in control of your server's maintenance and stability. Feel free to explore different modes, leverage various commands, and select the stability branch that suits your needs. Should you wish to make changes or have questions, our team is always ready to assist. Just [get in touch](/contacts) to let us know how we can help you maintain and enhance your server.
