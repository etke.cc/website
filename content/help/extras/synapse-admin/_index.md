---
title: Synapse Admin
draft: false
layout: extra-service
description: Discover Synapse Admin, a powerful tool for efficient Matrix Synapse homeserver management. Learn how to log in and address common issues, including handling incorrect Homeserver URLs. Explore bulk registration with CSV import and clearing old media to free up disk space. Get insights into maximizing the potential of Synapse Admin for hassle-free server administration.
aliases:
    - /admin
---

**Synapse Admin: Streamlined Synapse Homeserver Management**

**Synapse Admin** is a valuable tool for efficiently managing your (Matrix) Synapse homeserver. 
Whether you're looking to oversee user accounts, clear media, or handle various administrative tasks, Synapse Admin simplifies the process. 
Explore this guide to discover how to harness its potential.

![Synapse Admin Users Page](/img/synapse-admin/users.webp)

## Getting Started

By default, Synapse Admin is accessible at `https://matrix.your-server.com/synapse-admin`. To log in, you'll find the following fields:

- **Username**: Input your Matrix ID (`@username:your-server.com`), similar to the screenshot below.
- **Password**: Enter your Matrix user's password.
- **Homeserver URL**: This field should auto-populate after you input the correct Matrix ID in the `Username` field. If not, you can manually fill it with the value `https://matrix.your-server.com`, as shown in the screenshot.

A label displaying the homeserver's implementation and version (e.g., `Synapse version 1.62.0`) remains hidden initially and becomes visible when the correct value is set in the `Homeserver URL` field.

![Synapse Admin Login Page](/img/synapse-admin/login.webp)

### Common Issues

#### Incorrect or Empty `Homeserver URL`

Typically, this issue arises when you enter only the local part of your Matrix ID in the `Username` field. Ensure you enter the full Matrix ID (`@username:your-server.com`) instead of just `username` or `@username`. In specific cases, such as when using a CDN without CORS headers, Synapse Admin may not set the correct value automatically, necessitating manual adjustment.

#### No Label with Homeserver Version

If you don't see something like `Synapse version x.xx.x` below the `Homeserver URL` field, you've likely entered an incorrect value in the `Homeserver URL` field, or your homeserver might be offline. In most cases, the problem is due to an incorrectly specified `Homeserver URL` value.

#### Network Error

Network errors may produce different messages, but they typically fall into the following categories:

- **Invalid `Homeserver URL`**: If your `Homeserver URL` field does not contain a value like `https://matrix.your-server.com`, this is often the root of the problem.

- **Username Instead of Matrix ID**: Using a username (the local part of your Matrix ID) in the `Username` field won't work. You must input the full Matrix ID (`@username:your-server.com`) rather than just `username` or `@username`.

- **Insufficient Homeserver Admin Permissions**: Logging into Synapse Admin and utilizing the Synapse Admin API is only possible with Matrix users who possess homeserver admin permissions.

## Bulk Registration

For registering multiple users simultaneously, Synapse Admin provides a convenient CSV Import feature.

First, prepare a `.csv` file with user data for import. The file should have the following content:

```csv
id,displayname,password,is_guest,admin,deactivated
jane.doe,Jane Doe,secretpassword,false,false,false
```

> **NOTE**: The second line is an example.

- **id**: The local part of the Matrix ID (e.g., `username` from `@username:your-server.com`).
- **displayname**: The user's human-readable name.
- **password**: The user's password in plaintext.
- **is_guest**: Should this be a guest account?
- **admin**: Should this be a server admin account?
- **deactivated**: Should this be a deactivated account?

Next, access the CSV Import page (click on the `CSV IMPORT` button in the top right corner, above the users' table).

![Synapse Admin Users Page Highlighted CSV Import](/img/synapse-admin/users-highlight-csvimport.webp)

By default, this page is hosted at `https://matrix.your-server.com/synapse-admin/#/import_users`.

![Synapse Admin Users CSV Import](/img/synapse-admin/users-csvimport.webp)

On this page, configure the following options:

- **Conflict strategy**: Optional, with the default setting as `Stop on conflict`.
- **IDs**: Mandatory, with the option `Update existing records`.
- **Simulate only** (bottom left corner): Mandatory, with the option **unchecked**.

After selecting these options, click on `IMPORT`.

## Clearing Old Media

To free up valuable disk space, occasionally, you'll need to remove old media (uploaded files) from your homeserver. Here's how:

1. Log in to Synapse Admin (see [Login](#login)).
2. Click on the `Users' Media` button in the sidebar.
3. Select the `DELETE MEDIA` button in the top right corner, above the table.

![Synapse Admin Users' Media Page](/img/synapse-admin/users-media.webp)

A new modal window opens with two available filters:

- **Last access before**: A commonly used filter. It removes any media that hasn't been accessed (uploaded, viewed, or downloaded) since the specified date. For instance, if you want to clear media untouched for the last 7 days, select a date 7 days prior to today (as shown in the screenshot).
- **Larger than (in bytes)** (Please note that this is not a typo; it's the actual field name in Synapse Admin v0.8.5). This field is usually not used. However, if specified, it removes media larger than the specified size, including newly uploaded media.

![Synapse Admin Delete Media Modal](/img/synapse-admin/delete-media.webp)
