---
title: Linkding
draft: false
no_toc: true
layout: extra-service
code: https://github.com/sissbruecker/linkding
support: https://github.com/sissbruecker/linkding/issues
docs: https://github.com/sissbruecker/linkding/tree/master/docs
description: Self-hosted bookmark manager that is designed be to be minimal, fast, and easy to use.
---

[Linkding](https://github.com/sissbruecker/linkding) is a lightweight bookmarks manager, designed be to be minimal, fast, and easy to use.

Features:
* Clean UI optimized for readability
* Organize bookmarks with tags
* Bulk editing, Markdown notes, read it later functionality
* Share bookmarks with other users or guests
* Automatically provides titles, descriptions and icons of bookmarked websites
* Automatically archive websites, either as local HTML file or on Internet Archive
* Import and export bookmarks in Netscape HTML format
* Installable as a Progressive Web App (PWA)
* Extensions for [Firefox](https://addons.mozilla.org/firefox/addon/linkding-extension/) and [Chrome](https://chrome.google.com/webstore/detail/linkding-extension/beakmhbijpdhipnjhnclmhgjlddhidpe), as well as a bookmarklet
* REST API for developing 3rd party apps
* Admin panel for user self-service and raw data access

## Usage

We host your Linkding instance at a URL like: `https://linkding.your-server.com` - just replace `your-server.com` with your actual server domain, and open the URL in your browser. Welcome to your Linkding instance!

Linkding comes with an official browser extension that allows to quickly add bookmarks, and search bookmarks through the browser's address bar. You can get the extension here: [Firefox](https://addons.mozilla.org/firefox/addon/linkding-extension/),
[Chrome](https://chrome.google.com/webstore/detail/linkding-extension/beakmhbijpdhipnjhnclmhgjlddhidpe).
