---
title: Email Hosting
draft: false
layout: extra-service
no_toc: true
description: Discover our Email Hosting service for domains, powered by Migadu. Explore our flexible and no lock-in approach, account creation and administration through the user-friendly Migadu admin panel, and SMTP relay configuration for seamless outgoing email sending. Enjoy hassle-free email hosting tailored to your preferences.
---

**Introduction**

Welcome to our Email Hosting service. At [etke.cc](/), we understand that email is a critical aspect of personal and small business communication. Powered by [Migadu](https://www.migadu.com/), our email hosting service is designed to cater to the email needs of individuals and small businesses.

**Flexibility and No Lock-In**

We prioritize your flexibility. If your email hosting needs ever change, you can easily switch another provider of your choice.
At [etke.cc](/), we believe in empowering you to make choices that best suit your evolving requirements.

**Account Creation and Administration**

Upon choosing our email service, we will set up a Migadu admin account exclusively for you.
With this admin account, you'll gain access to [Migadu's admin panel](https://admin.migadu.com).
Through this user-friendly interface, you can take control and manage your domain's mailboxes.
This includes creating new ones, setting up forwarding aliases, configuring catch-all mailboxes, and more.
You will also find information on DNS settings necessary to get your email up and running, such as `MX`, `SPF`, `DMARC`, and `DKIM` records.
Additionally, Migadu provide you with the email client settings required to send and receive emails efficiently.

**SMTP Relay Configuration**

Regardless of whether you choose our email hosting service or opt for another provider,
we offer to configure your Matrix server to send outgoing emails seamlessly.
It's worth noting that SMTP Relay is offered for no extra cost to our email hosting customers.
Explore the details of this service [here](/help/extras/smtp-relay).

We aim to make your email hosting experience hassle-free and tailored to your preferences.
