---
title: Ntfy
draft: false
layout: extra-service
no_toc: true
official_website: https://ntfy.sh
docs: https://ntfy.sh/docs/
code: https://github.com/binwiederhier/ntfy
support: https://matrix.to/#/%23ntfy:matrix.org
description: Explore Ntfy, the effortless HTTP-based pub-sub notification service that fully complies with the UnifiedPush provider specification. Send notifications to mobile or desktop devices from any computer or Matrix server without sign-ups or complex setups. Enhance privacy and autonomy with self-hosted Ntfy instances. Learn how to get started with Ntfy on Android and discover its convenience for hassle-free, privacy-conscious notifications.
---

**Ntfy: Effortless UnifiedPush Notifications**

Welcome to **Ntfy** (pronounced as "notify"), your straightforward HTTP-based pub-sub notification service that fully complies with the [UnifiedPush provider specification](https://unifiedpush.org/).
Ntfy empowers you to effortlessly send notifications to your mobile or desktop devices from any computer and matrix server,
and the best part is, there's no need for sign-ups, costs, or intricate setups.
Additionally, it's open-source, offering the option of running your own instance.

By default, push notifications received on Matrix mobile apps act as wake-up calls for the application and do not transmit actual message payload, such as text message data.
Even without Ntfy, your messages remain private, as push notifications passing through Google servers (on Android) or Apple servers (on iOS) contain only event ids.

However, Ntfy takes Matrix privacy and independence to the next level.
With Ntfy, you can avoid routing these "application wake-up calls" through Google or Apple servers.
Instead, they pass through your self-hosted Ntfy instance on your Matrix server.

Ntfy serves as a valuable tool for enhancing the privacy and autonomy of your Matrix setup.
It ensures that your notifications no longer rely on Google or Apple servers.
For instance, notifications can now be routed through your own Ntfy instance, self-hosted on your Matrix server, offering greater control over your data.

To use Ntfy effectively, you'll need a mobile app that supports the UnifiedPush framework as an alternative for push notifications. Notably, Element mobile apps are well-suited for this purpose.

**Getting Started with Ntfy**

### Android

1. Begin by installing a UnifiedPush distributor app. A recommended example is Ntfy ([Source Code](https://github.com/binwiederhier/ntfy-android), [F-Droid](https://f-droid.org/en/packages/io.heckel.ntfy/), [Google Play](https://play.google.com/store/apps/details?id=io.heckel.ntfy)).
2. Access the app settings and configure your UnifiedPush provider (Ntfy server) as the default server. Typically, it is hosted at `https://ntfy.your-server.com`.
3. Open any compatible Matrix client app (such as SchildiChat or Element) and navigate to the notification settings. Switch the notifications provider to `ntfy`.

![Ntfy Main Screen](/img/ntfy/ntfy-main.webp)
![Ntfy Settings Screen](/img/ntfy/ntfy-settings.webp)
![Matrix Client App Notification Settings](/img/ntfy/matrix-notification-settings.webp)

### iOS

While there is an Ntfy app available for iOS ([App Store](https://apps.apple.com/us/app/ntfy/id1625396347) or [Source Code](https://github.com/binwiederhier/ntfy-ios)), it's important to note that none of the Matrix clients for iOS currently support Ntfy due to [technical limitations of the iOS platform](https://github.com/binwiederhier/ntfy-ios/blob/main/docs/TECHNICAL_LIMITATIONS.md).

If you're developing your own Matrix client app for iOS, be it from scratch or by forking on of the existing [Matrix client applications](https://matrix.org/ecosystem/clients/), you may need to use the [Sygnal](/help/extras/sygnal) push gateway service to deliver push notifications to it.