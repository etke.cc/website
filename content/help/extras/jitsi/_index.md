---
title: Jitsi - video conferencing
draft: false
no_toc: true
layout: extra-service
official_website: https://jitsi.org/
code: https://github.com/jitsi
support: https://community.jitsi.org/
docs: https://jitsi.github.io/handbook/docs/intro/
description: Jitsi is a free video conferencing tool, which is offered as an add-on service.
---

Jitsi is a free video conferencing tool which is often used for group calls **in Matrix**, but **can also be used standalone**.

In the future, Jitsi's usage within Matrix will be replaced with [Element Call](https://github.com/element-hq/element-call). For now, Element Call is not yet supported by us.

![Jitsi](/img/extra-services/jitsi/jitsi-readme-img1.webp)
Jitsi image source: the [Jitsi organization on Github](https://github.com/jitsi).

## Who is it for?

On Matrix:

- **1-on-1** audio/video calls use WebRTC directly (assisted by a TURN server) and **do not use Jitsi**
- **group calls use Jitsi** and sometimes the new [Element Call](https://github.com/element-hq/element-call) service (depending on the client application)

If you don't have your own Jitsi instance, the [public Jitsi instance](https://meet.jit.si/) will be used by default. As such, **you do not necessarily need your own Jitsi instance**.

For additional privacy, you can enable the Jitsi component and your Matrix server will be reconfigured to use it.

Jitsi can also be used standalone, without Matrix. If you wish to have an audio/video conferencing tool installed on your server, that may be another point in favor of installing it.

Keep in mind, that Jitsi is **heavy on server resources** (CPU, RAM, network bandwidth).

We optimize for the common use-case - for us, this is small-scale deployments. Video conferencing tools are heavy and hard to scale. **Our Jitsi setup is not optimized for high availability and scalability**. If you need a solution for hundreds (or even thousands) of people, we may not be the best choice for hosting your Jitsi instance.


## Usage

We host your Jitsi instance at a URL like: `https://jitsi.your-server.com`.

**Within Matrix** (on client apps like Element, etc.) just start an audio/video call in a group room and Jitsi should automatically load inside the Matrix client application.

**Standalone**, you can go to your Jitsi instance URL and start a meeting there.

By default, **your Jitsi instances is open for anyone to use**. This provides the best User Experience (no additional logins, etc.), but allows others to potentially use your instance. If necessary, your Jitsi instance can be protected with additional [Authentication](#authentication).


## Authentication

Beside the default "open to anyone" type of server, Jitsi also supports multiple authentication methods.

We only support one authentication method, called **Jitsi internal authentication**. It uses a list of usernames and password pairs.

When enabled, before joining a meeting (through Matrix or standalone, as described in [usage](#usage)) people will need to provide their (Jitsi internal auth) username and password.

To have Jitsi internal authentication enabled for your Jitsi server, [contact us](/contacts/) and send over the usernames and passwords that you would like to have access to your Jitsi instance.

