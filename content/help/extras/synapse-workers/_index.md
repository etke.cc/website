---
title: Synapse Workers
draft: false
no_toc: true
layout: extra-service
code: https://github.com/element-hq/synapse
support: https://github.com/element-hq/synapse/issues
docs: https://github.com/element-hq/synapse/blob/master/docs/workers.md
description: Synapse Workers is an option for the Synapse homeserver, which allows it to offload CPU-intensive tasks to separate worker processes, improving the performance of the homeserver.
---

[Synapse Workers](https://github.com/element-hq/synapse/blob/master/docs/workers.md) is an option for the Synapse homeserver, which allows it to offload CPU-intensive tasks to separate worker processes, improving the performance of the homeserver.

For small instances (less than 8GB RAM) it is recommended to run Synapse in the default monolith mode.
For larger instances where performance is a concern it can be helpful to split out functionality into multiple separate python processes. These processes are called 'workers', and are (eventually) intended to scale horizontally independently.

We configure the following workers (by default, a single instance of each worker type is enabled, but some types can be scaled up per request):

* **Room Workers** - handle various [Client-Server](https://spec.matrix.org/latest/client-server-api/) & [Federation](https://spec.matrix.org/latest/server-server-api) APIs dedicated to handling specific rooms
* **Sync Workers** - handle various [Client-Server](https://spec.matrix.org/latest/client-server-api/) APIs related to synchronization (most notably [the `/sync` endpoint](https://spec.matrix.org/latest/client-server-api/#get_matrixclientv3sync))
* **Client Readers** - handle various [Client-Server](https://spec.matrix.org/latest/client-server-api/) APIs which are not for specific rooms (handled by **Room Workers**) or for synchronization (handled by **Sync Workers**)
* **Federation Readers** - handle various [Federation](https://spec.matrix.org/latest/server-server-api) APIs which are not for specific rooms (handled by **Room Workers**)
* **Pushers** (_via generic worker_) - handle sending push and email notifications to users
* **Event Persisters** (_via generic worker_) - handle persisting events to the database
* **Federation Senders** (_via generic worker_) - handle sending federation requests to other homeservers
* **Media Repository Workers** - handle media repository requests
* **Appservice Workers** (_via generic worker_) - handle requests from application services, such as bridges
* **User Directory Workers** (_via generic worker_) - handle searches in the user directory
* **Background Workers** - handle background tasks that are run periodically or started via replication
* **Stream Writers: Events** - handle writing events to the event stream
* **Stream Writers: Typing** - handle writing events to the typing stream
* **Stream Writers: To Device** - handle writing events to the to_device stream
* **Stream Writers: Account Data** - handle writing events to the account_data stream
* **Stream Writers: Receipts** - handle writing events to the receipts stream
* **Stream Writers: Presence** - handle writing events to the presence stream
