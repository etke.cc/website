---
title: Help | Extra Services
name: Extra Services
heading: Elevate Your Matrix Server with Extra Services
draft: false
layout: help-section
description: Discover a range of extra services and components to elevate your Matrix server experience. From maintenance control with The Scheduler to efficient Synapse server management with Synapse Admin, email hosting with Migadu, and seamless outgoing email communication with SMTP Relay, explore these services for a more versatile Matrix server setup. Also, free your mobile and desktop notifications from Google's push service with Ntfy. Explore additional services like Prometheus+Grafana, Jitsi, BorgBackup, Buscarron, Honoroit, Single Sign-On, Sygnal, Etherpad, and more to enhance your Matrix server's capabilities.
---

We offer a range of extra services and components (for Matrix and independent ones), which would make your server more useful.

## etke.cc exclusive

The following services are exclusive to etke.cc customers:

* **[The Scheduler](/help/extras/scheduler/)** is a new maintenance system designed exclusively for etke.cc customers. It provides you with full control over server maintenance, ensuring your Matrix server stays in top shape.

## Matrix add-ons

In addition to the various [Bridges](/help/bridges/) and [Bots](/help/bots/), we also offer these additional Matrix services:

* **[Ntfy](/help/extras/ntfy/)** - the go-to choice for mobile app push notifications & script-based notifications without relying on Google's push service
* **[Sygnal](/help/extras/sygnal/)** - a push gateway server allowing Matrix application developers to support push notifications on Android and iOS devices
* **[Synapse Admin](/help/extras/synapse-admin/)** - a web-based tool that lets you manage your Matrix [Synapse](https://github.com/element-hq/synapse) homeserver (user account management, clearing media and chat history, etc)
* **[Synapse Single Sign-On](/help/extras/synapse-sso/)** - log in to your Matrix homeserver using SSO (powered by an OpenID Connect provider like Keycloak, Google, Github and 15+ others)
* **[Synapse S3 Storage](/help/extras/synapse-s3-storage/)** - a module for the Matrix [Synapse](https://github.com/element-hq/synapse) homeserver allowing it to store its media files in an object storage system like [AWS S3](https://aws.amazon.com/s3/) or compatible alternative
* **[Synapse Workers](/help/extras/synapse-workers/)** - an option for the Synapse homeserver, which allows it to offload CPU-intensive tasks to separate worker processes, improving the performance of the homeserver.

## Fediverse components

In addition to Matrix, we also offer Fediverse components to enhance your server's capabilities. Here are some of them:

* **[Funkwhale](/help/extras/funkwhale/)** - a decentralized audio hosting platform
* **[GoToSocial](/help/extras/gotosocial/)** - a decentralized social network platform
* **[Peertube](/help/extras/peertube/)** - a decentralized video hosting platform

## Collaboration components

* **[Email service](/help/extras/email-hosting/)** is powered by [Migadu](https://www.migadu.com/), offering a reliable solution for personal and small business use. Break free from email hosting limitations and enjoy flexibility.
* **[Email (outgoing) SMTP Relay](/help/extras/smtp-relay/)** - increase outgoing email delivery success rate by sending emails via an SMTP server (either via your own SMTP server or our [Email Hosting Service](/help/extras/email-hosting/)).
* **[Etherpad](/help/extras/etherpad/)** - a real-time collaborative document editor
* **[Jitsi](/help/extras/jitsi/)** - a video conferencing tool for use within Matrix and standalone
* **[LanguageTool](/help/extras/languagetool/)** - an Open Source proofreading software for English, Spanish, French, German, Portuguese, Polish, Dutch, and more than 20 other languages. It finds many errors that a simple spell checker cannot detect
* **[Radicale](/help/extras/radicale/)** - a simple CalDAV (calendar) and CardDAV (contact) server

## Reading and bookmarking components

* **[Miniflux](/help/extras/miniflux/)** - a minimalist and opinionated [feed](https://en.wikipedia.org/wiki/Web_feed) reader
* **[Linkding](/help/extras/linkding/)** - a bookmarking service

## Security and privacy components

* **[Firezone](/help/extras/firezone/)** - a VPN service based on WireGuard
* **[Vaultwarden](/help/extras/vaultwarden/)** - a lightweight [Bitwarden](https://bitwarden.com/) compatible password manager server that you can use with the official Bitwarden apps and browser addons

## Monitoring and visualization components

* **[Uptime Kuma](/help/extras/uptime-kuma/)** - a fancy monitoring tool
* **[Prometheus and Grafana](/help/extras/prometheus-grafana/)** - a monitoring and visualization stack for your server

## Additional components

* **[BorgBackup](/help/extras/borgbackup/)** - a backup solution (deduplicating archiver with compression and encryption)
