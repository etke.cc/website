---
title: Etherpad
draft: false
no_toc: true
layout: extra-service
official_website: https://etherpad.org/
code: https://github.com/ether/etherpad-lite
support: https://github.com/ether/etherpad-lite/discussions
docs: https://docs.etherpad.org/
description: Etherpad is a highly customizable open source online editor providing collaborative editing in really real-time.
---

[Etherpad](https://etherpad.org) is a web-based collaborative real-time editor, primarily used for collaborative writing and editing of rich text documents.
It allows multiple users to work on a document simultaneously, seeing each other's changes in real-time.
Etherpad is commonly used in educational settings for collaborative note-taking, in business environments for brainstorming sessions and document drafting, and in open-source communities for collaborative documentation efforts.

Key features of Etherpad include:

* **Real-time Collaboration**: Multiple users can edit the same document simultaneously, with changes being instantly visible to all participants.
* **Version History**: Etherpad maintains a version history of the document, allowing users to track changes over time and revert to previous versions if needed.
* **Chat Functionality**: Etherpad includes a built-in chat function, enabling users to communicate with each other while collaborating on a document.
* **Accessibility**: Etherpad is accessible via web browsers, making it easy for users to collaborate from anywhere with an internet connection.
* **Customization**: Etherpad can be customized and extended with plugins to add additional functionality or integrate with other systems.
* **Open Source**: Etherpad is open-source software, allowing users to access, modify, and distribute the code freely.

## Usage

We host your Etherpad instance at a URL like: `https://etherpad.your-server.com` - just replace `your-server.com` with your actual server domain, and open the URL in your browser. Welcome to your Etherpad instance!
