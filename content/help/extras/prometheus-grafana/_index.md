---
title: Prometheus and Grafana
draft: false
no_toc: true
layout: extra-service
official_website: https://grafana.com/
code: https://github.com/grafana/grafana
support: https://community.grafana.com/
docs: https://grafana.com/docs/grafana/latest/
description: Unlock monitoring and visualization with Prometheus and Grafana. Collect, visualize and optimize metrics effortlessly. Learn more!
---

The [Prometheus](https://prometheus.io/docs/introduction/overview/) and [Grafana](https://grafana.com) stack represents a powerful combination of open-source tools for monitoring, visualization, and alerting in modern IT environments.
At its core, Prometheus acts as a robust monitoring system, capable of collecting and storing metrics from various sources in real-time. 
With its efficient time-series database and flexible querying language, Prometheus provides deep insights into the health and performance of systems, applications, and services. 
Grafana, on the other hand, offers a rich and intuitive platform for visualizing Prometheus metrics through customizable dashboards. 
Its dynamic querying capabilities, extensive visualization options, and support for alerting make Grafana an indispensable companion to Prometheus, empowering users to monitor, troubleshoot, and optimize their infrastructure with ease. 
Together, Prometheus and Grafana form a comprehensive monitoring solution, enabling organizations to achieve unparalleled observability and actionable insights into their environments.

* **Comprehensive Monitoring**: Combined, Prometheus and Grafana offer comprehensive monitoring capabilities for infrastructure, applications, and services. By default, we configure Prometheus to scrape Synapse metrics and install official Synapse dashboard in Grafana.
* **Real-time Metrics**: Prometheus collects and stores metrics in real-time, enabling instant analysis and visualization.
* **Rich Visualization**: Grafana provides rich visualization options, including graphs, charts, and gauges, for displaying Prometheus metrics in meaningful ways.
* **Alerting and Notifications**: Prometheus enables alerting based on predefined rules, while Grafana can be configured to send notifications based on alert conditions.
* **Custom Dashboards**: Grafana allows users to create custom dashboards tailored to specific monitoring needs, incorporating Prometheus metrics along with other data sources.
* **Dynamic Querying**: Grafana's flexible query language and templating support enable dynamic exploration of Prometheus metrics.
* **Historical Analysis**: Both Prometheus and Grafana support historical analysis of metrics data, allowing users to identify trends and patterns over time.
* **Scalability**: The Prometheus + Grafana stack is designed to scale horizontally to accommodate growing monitoring needs.
* **Ease of Integration**: Integrating Prometheus with Grafana is straightforward, enabling quick setup and configuration for monitoring various systems and services.
* **Community Support**: Both Prometheus and Grafana have active communities providing support, resources, and plugins/extensions to enhance functionality.
* **Open Source**: As open-source tools, Prometheus and Grafana offer flexibility, transparency, and cost-effectiveness for monitoring solutions.
* **Extensibility**: The stack can be extended with additional plugins, integrations, and customizations to meet specific monitoring requirements.
* **Centralized Monitoring Hub**: Together, Prometheus and Grafana serve as a centralized monitoring hub, providing visibility into the health and performance of the entire infrastructure.
* **DevOps Integration**: The Prometheus + Grafana stack aligns well with DevOps practices, facilitating continuous monitoring, feedback, and improvement.
* **User-friendly Interface**: Grafana's intuitive interface makes it easy for users to create, customize, and share dashboards, empowering teams to collaborate effectively on monitoring tasks.

## Usage

We host your Grafana (works as frontend for Prometheus) instance at a URL like: `https://stats.your-server.com` - just replace `your-server.com` with your actual server domain, and open the URL in your browser. Welcome to your monitoring hub!
