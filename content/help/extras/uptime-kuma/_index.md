---
title: Uptime Kuma
draft: false
layout: extra-service
no_toc: true
official_website: https://uptime.kuma.pet/
docs: https://github.com/louislam/uptime-kuma/wiki
code: https://github.com/louislam/uptime-kuma
support: https://github.com/louislam/uptime-kuma/issues
description: Uptime Kuma is a fancy self-hosted monitoring tool, offered as an add-on service.
---

![Uptime Kuma screenshot](/img/extra-services/uptime-kuma/uptime-kuma-screenshot.webp)

Image source: the [Uptime Kuma Github repository](https://github.com/louislam/uptime-kuma)

----

Uptime Kuma is a fancy monitoring tool like [Uptime Robot](https://uptimerobot.com/).

## Features

- Monitoring uptime for HTTP(s) / TCP / HTTP(s) Keyword / HTTP(s) Json Query / Ping / DNS Record / Push / Steam Game Server / Docker Containers
- Fancy, Reactive, Fast UI/UX
- Notifications via Matrix, Telegram, Discord, Gotify, Slack, Pushover, Email (SMTP), and [90+ notification services](https://github.com/louislam/uptime-kuma/tree/master/src/components/notifications)
- 20-second intervals
- [Multi-Language](https://github.com/louislam/uptime-kuma/tree/master/src/lang)
- Multiple status pages
- Map status pages to specific domains
- Ping chart
- Certificate info
- Proxy support
- 2FA support

You can [try an official demo instance](https://demo.uptime.kuma.pet/) of Uptime Kuma (sponsored by the [Uptime Kuma Sponsors](https://github.com/louislam/uptime-kuma#%EF%B8%8F-sponsors)) before deciding to get it installed on your server.

## Usage

We install your Uptime Kuma instance at a URL like: `https://kuma.your-server.com`

The first time you open the Uptime Kuma Web UI it will make you go through a setup wizard, which will set up your admin credentials.
