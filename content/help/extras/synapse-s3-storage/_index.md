---
title: Synapse S3 storage provider
draft: false
no_toc: true
layout: extra-service
code: https://github.com/matrix-org/synapse-s3-storage-provider
support: https://github.com/matrix-org/synapse-s3-storage-provider/issues
description: Synapse storage provider to fetch and store media in Amazon S3 and S3-compatible object storage. It's offered as an add-on service.
---

[Synapse S3 storage provider](https://github.com/matrix-org/synapse-s3-storage-provider) is a module for the [Synapse](https://github.com/element-hq/synapse) homeserver, which allows it to store its content repository files (audio/image/video/etc. chat attachments) in an object storage system like [Amazon S3](https://aws.amazon.com/s3/) or any other S3-compatible alternative ([Wasabi](https://wasabi.com/), [Backblaze B2](https://www.backblaze.com/cloud-storage), [Cloudflare R2](https://www.cloudflare.com/developer-platform/r2/), etc).

Moving media files off of the server's local filesystem and into an external storage (the S3 storage system) allows you to **free up disk space on the Matrix server** and **effectively gives you infinite storage** (for a relatively-cheap price).

To make your Matrix server use Synapse S3 storage provider, you first need to [choose an Object Storage provider](#choosing-an-object-storage-provider), then [create the S3 bucket](#bucket-creation-and-security-configuration) and finally send the details to us.

For new orders, you can provide the Synapse S3 Storage details right in our [order form](/order/). For migrating existing servers to S3, [contact us](/contacts/).


## Choosing an Object Storage provider

You can create [Amazon S3](https://aws.amazon.com/s3/) or another S3-compatible object store like [Backblaze B2](https://www.backblaze.com/b2/cloud-storage.html), [Wasabi](https://wasabi.com), [Digital Ocean Spaces](https://www.digitalocean.com/products/spaces), etc.

Amazon S3 and Backblaze S3 are pay-as-you with no minimum charges for storing too little data.

All these providers have different prices, with Backblaze B2 appearing to be the cheapest.

Wasabi has a minimum charge of 1TB if you're storing less than 1TB, which becomes expensive if you need to store less data than that.

Digital Ocean Spaces has a minimum charge of 250GB ($5/month as of 2022-10), which is also expensive if you're storing less data than that.

Important aspects of choosing the right provider are:

- a provider by a company you like and trust (or dislike less than the others)
- a provider which has a data region close to your Matrix server (if it's farther away, high latency may cause slowdowns)
- a provider which is OK pricewise
- a provider with free or cheap egress (if you need to get the data out often, for some reason) - likely not too important for the common use-case


## Bucket creation and Security Configuration

Now that you've [chosen an Object Storage provider](#choosing-an-object-storage-provider), you need to create a storage bucket.

How you do this varies from provider to provider, with Amazon S3 being the most complicated due to its vast number of services and complicated security policies.

Below, we provider some guides for common providers. If you don't see yours, look at the others for inspiration or read some guides online about how to create a bucket. Feel free to contribute to this documentation with an update!

## Amazon S3

You'll need an Amazon S3 bucket and some IAM user credentials (access key + secret key) with full write access to the bucket. Example IAM security policy:

```json
{
	"Version": "2012-10-17",
	"Statement": [
		{
			"Sid": "Stmt1400105486000",
			"Effect": "Allow",
			"Action": [
				"s3:*"
			],
			"Resource": [
				"arn:aws:s3:::your-bucket-name",
				"arn:aws:s3:::your-bucket-name/*"
			]
		}
	]
}
```

**NOTE**: This policy needs to be attached to an IAM user created from the **Security Credentials** menu. This is not a **Bucket Policy**.


## Backblaze B2

To use [Backblaze B2](https://www.backblaze.com/b2/cloud-storage.html) you first need to sign up.

You [can't easily change which region (US, Europe) your Backblaze account stores files in](https://old.reddit.com/r/backblaze/comments/hi1v90/make_the_choice_for_the_b2_data_center_region/), so make sure to carefully choose the region when signing up (hint: it's a hard to see dropdown below the username/password fields in the signup form).

After logging in to Backblaze:

- create a new **private** bucket through its user interface (you can call it something like `matrix-DOMAIN-media-store`)
- note the **Endpoint** for your bucket (something like `https://s3.us-west-002.backblazeb2.com`).
- adjust its Lifecycle Rules to: Keep only the last version of the file
- go to [App Keys](https://secure.backblaze.com/app_keys.htm) and use the **Add a New Application Key** to create a new one
  - restrict it to the previously created bucket (e.g. `matrix-DOMAIN-media-store`)
  - give it *Read & Write* access

The `keyID` value is your **Access Key** and `applicationKey` is your **Secret Key**.

## Other providers

For other S3-compatible providers, you may not need to configure security policies, etc. (just like for [Backblaze B2](#backblaze-b2)).

You most likely just need to create an S3 bucket and get some credentials (access key and secret key) for accessing the bucket in a read/write manner.
