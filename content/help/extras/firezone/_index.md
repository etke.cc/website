---
title: Firezone
draft: false
no_toc: true
layout: extra-service
official_website: https://firezone.dev
code: https://gitgub.com/firezone/firezone
support: https://discourse.firez.one/
docs: https://www.firezone.dev/docs
description: WireGuard®-based zero-trust access platform with OIDC auth and identity sync. 
---

[Firezone](https://www.firezone.dev/) is an open source platform to securely manage remote access for any-sized organization. 
Unlike most VPNs, Firezone takes a granular, least-privileged approach to access management with group-based policies that control access to individual applications, entire subnets, and everything in between.

## Usage

We host your Firezone instance at a URL like: `https://firezone.your-server.com` - just replace `your-server.com` with your actual server domain, and open the URL in your browser. Welcome to your Firezone instance!
