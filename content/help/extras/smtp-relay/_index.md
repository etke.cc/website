---
title: SMTP Relay
draft: false
layout: extra-service
no_toc: true
description: Explore our SMTP Relay service, an optional enhancement for your Matrix server hosting. Learn when SMTP Relay is needed, such as for email confirmation, notifications, and email bridges. Discover the advantages of using an SMTP relay, especially in cases of network restrictions and IP address reputation concerns. Set up SMTP Relay by providing your SMTP server details or consider our Email Hosting service for a hassle-free solution. We offer the flexibility and support to ensure your Matrix server seamlessly handles email functionality.
---

**Introduction**

Our SMTP Relay service is an additional offering to enhance your Matrix server hosting experience.
While it's not critical to the core functionality of your Matrix server, there are specific scenarios in which it becomes valuable.

**When You Need SMTP Relay**

Sending outgoing emails may not be a core requirement for all Matrix servers.
However, there are instances where your Matrix homeserver may need to send out emails:

1. **Email Confirmation**: When users add email addresses to their profiles, the server sends an email confirmation link to verify mailbox ownership.
2. **Email Notifications**: Users who enable email notifications for unread rooms may receive periodic "Here's what you've missed" emails.
3. **Postmoogle Email Bridge**: If users utilize the [Postmoogle email bridge](/help/bridges/postmoogle), your server may be involved in sending outgoing emails through it.

**Why Use SMTP Relay**

By default, your Matrix server attempts to send outgoing emails directly. In many cases, this direct approach works seamlessly. However, there are specific situations where using an SMTP relay is advantageous:

1. **Network Restrictions**: Some networks and VPS providers may block outgoing port `25`, hindering the direct delivery of emails.
2. **IP Address Reputation**: If the IP address reputation of your server is less than optimal, it can affect email deliverability.

In such circumstances, opting for an SMTP relay offers a reliable solution.

**How to Set Up SMTP Relay**

If you find the need for your Matrix server to utilize an SMTP relay for sending outgoing emails, you have two options:

1. **Provide Your SMTP Server**: You can supply us with the address and credentials of an SMTP server you own, whether it's on your infrastructure or a service like [SendGrid](https://sendgrid.com), among others.
2. **Email Hosting Service**: Alternatively, you can leverage our [Email Hosting service](/help/extras/email-hosting). If you choose this option, we'll configure your Matrix server to use the SMTP relay through our email hosting service. In this case, SMTP Relay is included at no extra cost.

We aim to provide the flexibility and support you need to ensure your Matrix server operates seamlessly, even when it comes to email functionality.
