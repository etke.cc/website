---
title: GoToSocial
draft: false
no_toc: true
layout: extra-service
official_website: https://gotosocial.org
code: https://github.com/superseriousbusiness/gotosocial
support: https://matrix.to/#/#gotosocial:superseriousbusiness.org
docs: https://docs.gotosocial.org/
description: GoToSocial is an ActivityPub social network server, written in Golang. With GoToSocial, you can keep in touch with your friends, post, read, and share images and articles. All without being tracked or advertised to!
---

[GoToSocial](https://gotosocial.org) is an ActivityPub social network server, written in Golang.
With GoToSocial, you can keep in touch with your friends, post, read, and share images and articles.
All without being tracked or advertised to!
GoToSocial provides a lightweight, customizable, and safety-focused entryway into the [Fediverse](https://en.wikipedia.org/wiki/Fediverse), and is comparable to (but distinct from) existing projects such as [Mastodon](https://joinmastodon.org/), [Pleroma](https://pleroma.social/), [Friendica](https://friendica.net/), and [PixelFed](https://pixelfed.org/).

If you've ever used something like Twitter or Tumblr (or even Myspace!) GoToSocial will probably feel familiar to you: You can follow people and have followers, you make posts which people can favourite and reply to and share, and you scroll through posts from people you follow using a timeline. You can write long posts or short posts, or just post images, it's up to you. You can also, of course, block people or otherwise limit interactions that you don't want by posting just to your friends.

Key features of GoToSocial include:
* **Mastodon API compatibility**: GoToSocial implements and extends the Mastodon API, ensuring compatibility with popular Mastodon client apps like Tusky, Semaphore, and Feditext.
* **Granular post settings**: Users can choose who sees their posts with options including public, unlisted, friends-only, mutuals-only, and direct posts. Additionally, users can customize interactions on their posts such as reblogging, liking, and replying.
* **Customizability for admins**: Admins have access to plenty of configuration options, including adjustable post length and media upload size settings.
* **Easy to run**: With no external dependencies apart from a database, GoToSocial is easy to deploy and run. It's designed to work well even on lower-powered machines like small VPSes.
* **Safety + security features**: GoToSocial prioritizes security with built-in support for secure HTTPS using Let's Encrypt, strict privacy enforcement, and HTTP signature authentication.
* **Various federation modes**: Users can choose from different federation modes, including 'blocklist' mode (default), 'allowlist' mode (experimental), and 'zero' federation mode (not yet implemented).
* **Backend-first design**: Unlike other federated server projects, GoToSocial doesn't include an integrated client front-end. Instead, it provides a backend server implementation with a well-documented API, allowing developers to build their own front-end implementations or mobile applications.


## Usage

We host your GoToSocial instance at a URL like: `https://social.your-server.com` - just replace `your-server.com` with your actual server domain, and open the URL in your browser. Welcome to your GoToSocial instance!

## Optional S3 storage

GoToSocial supports S3-compatible providers, which allows it to store videos, photos, etc. in an object storage system like [Amazon S3](https://aws.amazon.com/s3/) or any other S3-compatible alternative ([Wasabi](https://wasabi.com/), [Backblaze B2](https://www.backblaze.com/cloud-storage), [Cloudflare R2](https://www.cloudflare.com/developer-platform/r2/), etc).

Moving media files off of the server's local filesystem and into an external storage (the S3 storage system) allows you to **free up disk space on the server** and **effectively gives you infinite storage** (for a relatively-cheap price).

To make your GoToSocial server use an S3 storage provider, you first need to [choose an Object Storage provider](#choosing-an-object-storage-provider), then [create the S3 bucket](#bucket-creation-and-security-configuration) and finally send the details to us.

For new orders, you can provide the S3 Storage details right in our [order form](/order/).


### Choosing an Object Storage provider

You can create [Amazon S3](https://aws.amazon.com/s3/) or another S3-compatible object store like [Backblaze B2](https://www.backblaze.com/b2/cloud-storage.html), [Wasabi](https://wasabi.com), [Digital Ocean Spaces](https://www.digitalocean.com/products/spaces), etc.

Amazon S3 and Backblaze S3 are pay-as-you with no minimum charges for storing too little data.

All these providers have different prices, with Backblaze B2 appearing to be the cheapest.

Wasabi has a minimum charge of 1TB if you're storing less than 1TB, which becomes expensive if you need to store less data than that.

Digital Ocean Spaces has a minimum charge of 250GB ($5/month as of 2022-10), which is also expensive if you're storing less data than that.

Important aspects of choosing the right provider are:

- a provider by a company you like and trust (or dislike less than the others)
- a provider which has a data region close to your Matrix server (if it's farther away, high latency may cause slowdowns)
- a provider which is OK pricewise
- a provider with free or cheap egress (if you need to get the data out often, for some reason) - likely not too important for the common use-case


### Bucket creation and Security Configuration

Now that you've [chosen an Object Storage provider](#choosing-an-object-storage-provider), you need to create a storage bucket.

How you do this varies from provider to provider, with Amazon S3 being the most complicated due to its vast number of services and complicated security policies.

Below, we provider some guides for common providers. If you don't see yours, look at the others for inspiration or read some guides online about how to create a bucket. Feel free to contribute to this documentation with an update!

### Amazon S3

You'll need an Amazon S3 bucket and some IAM user credentials (access key + secret key) with full write access to the bucket. Example IAM security policy:

```json
{
	"Version": "2012-10-17",
	"Statement": [
		{
			"Sid": "Stmt1400105486000",
			"Effect": "Allow",
			"Action": [
				"s3:*"
			],
			"Resource": [
				"arn:aws:s3:::your-bucket-name",
				"arn:aws:s3:::your-bucket-name/*"
			]
		}
	]
}
```

**NOTE**: This policy needs to be attached to an IAM user created from the **Security Credentials** menu. This is not a **Bucket Policy**.


### Backblaze B2

To use [Backblaze B2](https://www.backblaze.com/b2/cloud-storage.html) you first need to sign up.

You [can't easily change which region (US, Europe) your Backblaze account stores files in](https://old.reddit.com/r/backblaze/comments/hi1v90/make_the_choice_for_the_b2_data_center_region/), so make sure to carefully choose the region when signing up (hint: it's a hard to see dropdown below the username/password fields in the signup form).

After logging in to Backblaze:

- create a new **private** bucket through its user interface (you can call it something like `peertube-DOMAIN-media-store`)
- note the **Endpoint** for your bucket (something like `https://s3.us-west-002.backblazeb2.com`).
- adjust its Lifecycle Rules to: Keep only the last version of the file
- go to [App Keys](https://secure.backblaze.com/app_keys.htm) and use the **Add a New Application Key** to create a new one
  - restrict it to the previously created bucket (e.g. `peertube-DOMAIN-media-store`)
  - give it *Read & Write* access

The `keyID` value is your **Access Key** and `applicationKey` is your **Secret Key**.

### Other providers

For other S3-compatible providers, you may not need to configure security policies, etc. (just like for [Backblaze B2](#backblaze-b2)).

You most likely just need to create an S3 bucket and get some credentials (access key and secret key) for accessing the bucket in a read/write manner.
