---
title: Sygnal reference Push Gateway for Matrix
draft: false
no_toc: true
layout: extra-service
code: https://github.com/matrix-org/sygnal
support: https://github.com/matrix-org/sygnal/issues
docs: https://github.com/matrix-org/sygnal/tree/main/docs
description: Sygnal is a reference Push Gateway useful to Matrix client application developers, which is offered as an add-on service.
---

Sygnal is a reference Push Gateway for Matrix, allowing Matrix **application developers** to make their custom Matrix application deliver push notifications (to [Android](https://www.android.com/), [iOS](https://www.apple.com/ios/)) via their own push gateway.

## Who is it for?

**Most Matrix users don't need to install their own push gateway**. As Sygnal's [Notes for application developers](https://github.com/matrix-org/sygnal/blob/master/docs/applications.md) documentation says:

> It is not feasible to allow end-users to configure their own Sygnal instance, because the Sygnal instance needs the appropriate FCM or APNs secrets that belong to the application.

However, if you're developing your own application, be it from scratch or by forking on of the existing [Matrix client applications](https://matrix.org/ecosystem/clients/), you may need a Sygnal push gateway. We can [configure](#configuration) and host Sygnal for you, and you can then [integrate it into your own Matrix application](#integrating-into-your-application).


## Configuration

You can use Sygnal to push notifications (via your custom Matrix application) to:

- [Android](https://www.android.com/) (and others), via [Firebase Cloud Messaging (previously Google Cloud Messaging)](#firebase-cloud-messaging-previously-google-cloud-messaging)
- [iOS](https://www.apple.com/ios/), via [Apple Push Notification service](#apple-push-notification-service)

For your Sygnal instance, you can **enable one or both of these push modules** depending on your needs.

### Firebase Cloud Messaging (previously Google Cloud Messaging)

You can deliver push notifications to [Android](https://www.android.com/) devices (and also to iOS, web, etc.) using [Firebase Cloud Messaging](https://firebase.google.com/docs/cloud-messaging). For iOS push notifications, you can also use [Apple Push Notification service](#apple-push-notification-service) directly.

To obtain an API key:

- go to the [Firebase Console](https://console.firebase.google.com/)
- create a new project
- go to **All products** -> **Cloud Messaging** (e.g. URL: `https://console.firebase.google.com/project/<PROJECT NAME>/settings/cloudmessaging/`)
- create an Android application and obtain the API key

Finally, send over **your application ID** (e.g. `com.example.messenger`) and **this API key** over to us, so we can configure your Sygnal instance.

### Apple Push Notification service

You can deliver push notifications to [iOS](https://www.apple.com/ios/) devices using the [Apple Push Notification service](https://developer.apple.com/notifications/).

For this, you will need to [enroll](https://developer.apple.com/programs/enroll/) into the [Apple Developer Program](https://developer.apple.com/programs/).

The APNS module for Sygnal can be configured in 2 ways:

1. (Legacy) Certificate-based authentication: Uses a certificate file that Apple signs based on a Certificate Signing Request. Things you will need to send over to us:
  - `certfile`: a `.pem` file containing the APNS certificate and unencrypted private key
2. (More recent) Token-based authentication.
  - `keyfile`: the key file - a `.p8` file
  - `key_id`: a string like `XF32BUG6NW`
  - `team_id`: a string like `42G2X6393F`
  - `topic`: a string like `com.example` - this is most commonly the 'Bundle Identifier' for your iOS application
  - `platform`: a string, either `production` and `sandbox` - it determines whether the production or sandbox APNS environment is used. This is optional and defaults to `production`
  - `push_type`: optional string, which determines what value to set for the `apns-push-type` header sent to APNs. Read more [when to use push types](https://developer.apple.com/documentation/usernotifications/setting_up_a_remote_notification_server/sending_notification_requests_to_apns#4294485) and what values are available.

We **recommend** that you go with the newer **Token-based authentication** method, but you can choose either one.

For iOS, we can configure **multiple APNs push instances** with different `push_type` configuration (e.g. `alert`, `voip`, etc.)

Whichever method you choose, send over **your application ID** (e.g. `com.example.messenger`) and **configuration** (as documented above), so we can configure your Sygnal instance.


## Integrating into your application

Our job ends with configuring Sygnal with the [Firebase Cloud Messaging](#firebase-cloud-messaging-previously-google-cloud-messaging) and [Apple Push Notification service](#apple-push-notification-service) settings described above.

We will deploy your Sygnal instance at a URL like: `https://sygnal.your-server.com`.

You then need to continue and actually make use of your push gateway in your custom Matrix application.

Your Push Gateway may be used with your own Matrix server (the one we host for you) or with any other Matrix server accessed through your application - as long as people are using your application, they can use your push gateway wih any server they access through it.

For more details about integrating a push gateway into your custom Matrix application, see the [applications documentation page](https://github.com/matrix-org/sygnal/blob/main/docs/applications.md) in the Sygnal project.

