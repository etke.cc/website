---
title: Miniflux
draft: false
no_toc: true
layout: extra-service
official_website: https://miniflux.app/
docs: https://miniflux.app/docs/
code: https://github.com/miniflux/v2
support: https://github.com/orgs/miniflux/discussions
description: Miniflux is a minimalist and opinionated feed reader. It's offered as an add-on service.
---

[Miniflux](https://miniflux.app/) is a minimalist and opinionated [feed](https://en.wikipedia.org/wiki/Web_feed) reader.

It's very lightweight when it comes to server resources. With its minimal use of [CSS](https://en.wikipedia.org/wiki/CSS), it's also very fast and efficient.

![Miniflux screenshot](/img/extra-services/miniflux/miniflux-screenshot.webp)

## Usage

**URL**: `https://miniflux.your-server.com`

You can visit your instance and log in with the credentials we have provided to you.

You can then proceed to add various [RSS](https://en.wikipedia.org/wiki/RSS) and [Atom](https://en.wikipedia.org/wiki/Atom_(web_standard)) feeds. Miniflux would periodically crawl them and make them available for reading in its web interface.