---
title: Frequently Asked Questions
draft: false
layout: singletoc
description: Learn the importance of .well-known redirects for shorter, user-friendly Matrix usernames and the required open ports for Matrix server components. Understand how to have multiple administrators on your server, modify the registration flow, and troubleshoot common issues. Get answers to frequently asked questions about Matrix servers.
---

## Can I migrate my server or its data to etke.cc?

### I want to get a new Matrix server from etke.cc based on exported data from my current provider

**The short answer is: no - we do not support migration**. It may be technically possible some of the time (see details above), but it's too much manual work for us and doesn't guarantee a 100% migration.

If another provider is currently hosting your Matrix server and provides a data export, you may be wondering if you can just migrate the data and continue running your server with us.

Depending on various things, migration may or may not be technically possible or financially justified.

- If your [homeserver](https://matrix.org/ecosystem/servers/) is based on software other than [Synapse](https://github.com/element-hq/synapse), migration is not possible. We only support Synapse right now and migrating data between homeserver implementations is not supported.
- If you are not using your own domain (e.g. `example.com`), but rather a subdomain (`example.hosting-provider.com`), migration is not possible. Using the old domain (owned by your current provider) is not possible and migrating data between domains is not supported in Matrix yet.
- If your data export contains a Postgres database, it may possible to import it into our system with manual work on our side
- If your data export contains a `.signing.key` file, we can easily make use of it for your new server hosted with us
- If your data export uses the default/native media store for the [Synapse](https://github.com/element-hq/synapse) homeserver, it can be migrated with manual work on our side. However, certain hosting providers (e.g. EMS) use an alternative media store ([matrix-media-repo](https://github.com/turt2live/matrix-media-repo)) and we cannot migrate media files stored that way.

Because there are a lot of variables in this and manual work is necessary, **it may be impossible to achieve a 100% migration**. Even if it is possible, we do not have automated processes for this. As such, it will require going over the exported data and manually importing it into our system, which is **a non-trivial amount of manual work**.

For these reasons, **we cannot afford to support migration** and **recommend starting fresh**.

When starting fresh, you have **2 options**:

1. It's **easiest if you use a different domain**:
  - this way, there won't be Matrix Federation conflicts with your previous server
  - you can get your brand new server by submitting an [Order](/order/) and following the steps there

2. You may **re-use your existing custom domain** (and start with empty data on it) with this flow:
  - while still using the server from your existing provider, leave all federated rooms from it
  - get a data export and extract the `.signing.key` file from it
  - submit our [Order](/order/) form
  - in the email communication with us (before paying for your order), send over the `.signing.key` file

Re-using an existing domain can work reasonably well if you leave all federated rooms before switching to a brand new server (having a fresh empty database). If you fail to leave federated rooms, other servers on the Matrix federated network may think you are still part of some rooms while your server thinks it's not (given its brand new database). Such a state mismatch may cause federation issues.

### I want to have an existing Matrix server installed with matrix-docker-ansible-deploy to be maintained by etke.cc

If you have a **relatively up-to-date** server installation done with the [matrix-docker-ansible-deploy](https://github.com/spantaleev/matrix-docker-ansible-deploy) Ansible playbook, it may be possible to migrate your setup to us.

However, it will require prior reviewing of your existing configuration, as components offered on etke.cc (which you can see on our [Order Form](/order/)) differ from those offered by the Ansible playbook.

Please, [contact us](/contacts/) and send over a cleaned up (no passwords and secrets) version your `vars.yml` file, so we can let you know if:

- we can migrate your setup as-is immediately
- or, we're planning on adding support for the services that you're missing

### I want to have existing Matrix servers installed using any other method to be maintained by etke.cc

Unfortunately, that is technically impossible.

## Can I have one-time setup without a subscription?

We do **not** offer one-time setups.
[Our services](/services) are subscription-based and we do **not** provide "just setup" service.

We are committed to providing enhancements and upgrades on a regular basis, 
and we believe that a subscription model is the best way to ensure that 
we can continue to provide the best possible service to our customers.

And so, we are unable to provide one-time setups.

## Why are .well-known redirects on the base domain important?

Moved to the [Delegation Redirects](/order/status/#delegation-redirects)

## What are the base Matrix components installed on the server?

We offer a lot of optional Matrix [bridges](/help/bridges/), [bots](/help/bots/) and [extra services](/help/extras/), but all Matrix servers come installed with a set of core Matrix components:

- the [Synapse](https://github.com/element-hq/synapse) Matrix homeserver software. At this moment, this is the most complete and compatible homeserver implementation. We currently do **not** offer [alternative homeserver software](https://matrix.org/ecosystem/servers/) like [Conduit](https://conduit.rs/), [Construct](https://github.com/matrix-construct/construct) or [Dendrite](https://github.com/matrix-org/dendrite).

- the [matrix-synapse-shared-secret-auth](https://github.com/devture/matrix-synapse-shared-secret-auth) module for Synapse, which assists various bridges and bots with authentication

- the [synapse_auto_compressor](https://github.com/matrix-org/rust-synapse-compress-state/#automated-tool-synapse_auto_compressor) tool, which runs in the background and periodically compresses the database for the Synapse homeserver, so that it runs optimally.

- the [sliding-sync](https://github.com/matrix-org/sliding-sync) software which assists next-generation clients like [Element X](https://element.io/labs/element-x) in talking to the homeserver in an optimized manner. In the future, it's expected that the homeserver itself (e.g. Synapse) will handle these tasks, but for now a separate component is necessary.

- the [Synapse-Admin](/help/extras/synapse-admin/) web UI tool for homeserver management

- the [Coturn](https://github.com/coturn/coturn/) TURN server, to assist audio/video calls

- a [PostgreSQL](https://www.postgresql.org/) database server, storing the data for Synapse and other services

- (only for [Bring-your-own-server](/order/#byos) types of orders), the [docker-postgres-backup-local](https://github.com/prodrigestivill/docker-postgres-backup-local) software which makes periodic local backups of your PostgreSQL database - we store **7 daily database dumps** by default. For [Turnkey](/order/#turnkey) orders (hosted on [Hetzner Cloud](https://hetzner.cloud/?ref=1yg3ZZmtrg5A) VPS servers rented by us), we do not enable local Postgres database dumps, because we enable [Hetzner's server backups feature](https://docs.hetzner.com/cloud/servers/getting-started/enabling-backups/), which stores the last 7 daily full-disk snapshots.

- optionally, the [Element](https://github.com/vector-im/element-web) web application for chatting on your Matrix server, but we also support some alternative client applications like [Cinny](https://cinny.in/) and [SchildiChat](https://schildi.chat/). Regardless of the web application we may install on your server, you can connect to your server via any compatible [Matrix client application](https://matrix.org/ecosystem/clients/) on any platform.

- a [Traefik](https://doc.traefik.io/traefik/) reverse-proxy server, which obtains free [Let's Encrypt](https://letsencrypt.org/) SSL certificates for all domains used in your setup

- a [Prometheus Node Exporter](https://prometheus.io/docs/guides/node-exporter/) agent for basic monitoring and alerting. Metrics data is collected by our own external [Prometheus](https://prometheus.io/) system which also does alerting. In the event of trouble, alerts go out to us and to you via email and Matrix.

## What ports should be open?

Moved to the [Ports and Firewalls](/order/status/#ports-and-firewalls)

## Can I have multiple administrator accounts on my server?

Yes, you can have multiple administrator accounts on your Matrix server.
However, there are some important details to consider:

1. **Matrix Homeserver Administration**: By default, our order form on the website configures a single administrator user account. This user has full access to the [synapse-admin](/help/extras/synapse-admin/) tool and various administration APIs of your Matrix server. You can create additional Matrix administrator accounts using the [synapse-admin](/help/extras/synapse-admin/) tool without needing to contact us.
2. **etke.cc Service Management**: To manage your server using our [scheduler bot](/help/extras/scheduler/) for maintenance scheduling and other commands, you need to prepare Matrix accounts for additional administrators. You can create these accounts (e.g., using [synapse-admin](/help/extras/synapse-admin/)) and provide us with their Matrix IDs (e.g., `@someone:YOUR_SERVER`). We will grant them access to the bot. These accounts must be on your server's domain and not on other Matrix servers across federation.
3. **Matrix Bridge Administration**: If you require additional administrators for your Matrix bridges, whether on your server or other servers, provide us with their Matrix IDs (e.g., `@someone:YOUR_SERVER` or `@someone:ANOTHER_SERVER`). We will grant them administrator access to your bridges.

## Can I change the registration (sign-up) flow?

By default, we configure [invite-based registration](#default-invite-based-registration) on all servers we set up. Enabling completely open registration is generally not recommended due to potential spam and abuse.

Check the supported registration types for your Matrix server below.

### (Default) Invite-Based Registration

The Matrix protocol supports [Token-authenticated registration](https://spec.matrix.org/latest/client-server-api/#token-authenticated-registration). This means registration is closed by default, allowing only those with a valid invite token to register. Admins can issue invite tokens using the [synapse-admin](/help/extras/synapse-admin/) tool, and each token can be configured for multiple registrations with various options, including expiration dates.

### Closed Registration

Closed registration is similar to [invite-based registration](#default-invite-based-registration). Admins can still manually register users using the [synapse-admin](/help/extras/synapse-admin/) tool, but users cannot register themselves with invitation tokens.

### Open Registration

Completely open registration allows anyone on the internet to register on your server without restrictions. However, this can lead to spam and abuse. To enhance security, you can enable mandatory email verification, ensuring users confirm their email addresses. An [SMTP relay](/help/extras/smtp-relay/) is recommended for this setup to prevent emails from landing in spam.

### Single Sign-On (SSO)

You can use Single Sign-On (SSO) with an OpenID Connect provider to allow registration for specific groups of people. SSO can work alongside other registration types, including [invite-based](#default-invite-based-registration), [closed registration](#closed-registration), or [open registration](#open-registration) with or without domain restrictions. For instance, you can link your Matrix server to a provider like [Google Workspace](https://workspace.google.com) for one-click registration for your users while restricting access for others.

## My newly-installed server joins new rooms slowly

Newly set up Matrix servers lack knowledge about other servers. When you join a room over federation, your server contacts other servers and retrieves data about the room and its members. This process can be slow, especially in larger rooms, as each step involves rate limiting and potential network issues. Over time, your server builds a network of federated servers, and room joins become faster. Be patient, and room joins will improve with time.

## Bridge bot doesn't respond

If your bridge bot isn't responding, there are two common issues to check:

1. **Wrong Bridge MXID**: Ensure you're contacting the correct bridge bot associated with your server. Make sure to use the correct Matrix IDs, which are listed in your onboarding materials or [bridge documentation pages](/help/bridges/).

2. **Encrypted Room**: We typically set up bridges with encryption support disabled as it can be unstable. Some client apps may automatically create encrypted rooms when trying to chat with a bridge bot. To avoid this, create a new room and disable encryption. Then invite the bridge bot to the room

If you encounter other issues, please refer to our [additional documentation](/help/) resources.

## Payments

We've prepared a [dedicated page with payment guides and instructions](/help/payments).

## How to migrate to the By Complexity subscription?

If you're on an old, deprecated, subscription tier (Maintenance, Maintenance + Email or one of the Turnkeys), you should migrate to the new **By Complexity** subscription plan. This new plan is based on the infrastructure, maintenance, and support complexity of specific components, ensuring fair pricing, otherwise you loose access to [etke.cc support](/services/support/) ([more details](#when-will-old-subscriptions-maintenance-and-turnkeys-be-deprecated)).

**How to verify your current subscription tier**:

* Either your server was installed before December 2023 and you didn't switch to the new subscription tier yet
* Or your subscription tier on [your Ko-Fi account page](https://ko-fi.com/account/) is **not** "By Complexity"

**How to migrate**:

Just obtain the price for your specific server by using [`run price` command of the Scheduler](/help/extras/scheduler/#price) and [follow the step-by-step guide](/help/payments#how-to-become-a-subscriber) to update your payment details.

## When will old subscriptions (Maintenance and Turnkeys) be deprecated?

~~Since December 2023, we operate on a new pay-by-complexity service model under which customers pay a different monthly subscription fee - based on the services we are providing to them. Customers who order a basic server with few components pay minimally (e.g. $5/mo), while others who order tens of services pay more (e.g. $100/mo). People on this new *By Complexity* subscription tier are always charged according to the services we run for them and this FAQ entry does not apply to them.~~

~~Customers from before December 2023 are on an old subscription tier (Maintenance, Maintenance + Email or Turnkey) which has a fixed price. Such customers can keep their existing subscription under certain conditions.~~

~~**Until 1st of July 2024**, existing customers can remain on their old subscription tier as long as the subscription is active (not overdue) and the services we provide to them remain unchanged. That is:~~

- ~~if customers subscription is overdue (i.e., inactive), we'll ask them to switch to the new *By Complexity* subscription tier~~
- ~~if customers continue to use the same services, they can remain on the same subscription tier and we won't charge them differently~~
- ~~if customers request additional services from us, we'll need to ask them to switch to the new *By Complexity* subscription tier, so we can charge them fairly according to our new pricing model~~

**Update**: after the 6 months of the grace period, starting from **1st of July 2024**, any existing customer who [contacts our support](/contacts/) will be asked to switch to the new *By Complexity* subscription plan. If customers wish to continue using our services without support, they can remain on the same subscription tier for longer. We have not yet established a final date after which we'll completely abandon the old subscription tiers.

[Announcement with details](/news/lnyorh4ggeh9xadrob_rtuuqu_jj-s-ojfvvfzlmxpm/), [Migration guide](#how-to-migrate-to-the-by-complexity-subscription)

## What about deprecated components?

We've prepared a [dedicated page with information about removed components](/help/removed-components).

## Can I modify the server configuration or run additional services?

With us, you get a managed Matrix server. Be it on-premises, or hosted by us, we take care of the server's configuration and maintenance. This means you can focus on using the server and not worry about its upkeep.

### Modifying Matrix configuration manually on the server is prohibited

Because:
- it often causes breakage and increases our support burden
- will typically be rolled-back by our automation ([scheduler bot](/help/extras/scheduler/))

Please, [contact us](/contacts/) if you need any changes to your server configuration. We'll be happy to help.

### Running other services along our stack is OK

**But**:
- may cause breakage
- may break at any time in the future due to service upgrades performed by us
- comes without any support from us
