---
title: Removed Components
draft: false
layout: help
aliases:
    - /help/bridges/appservice-kakaotalk/
    - /help/bridges/mx-puppet-groupme/
    - /help/bridges/mx-puppet-steam/
---

**You were likely redirected here from a removed or deprecated page. You may wish to check our full list of [bridges](/help/bridges/), [bots](/help/bots/), or [extra services](/help/extras/) for the latest information.**

It's been a long time since we deprecated certain components and replaced them with better-working alternatives.
Even though the old components were deprecated and considered unmaintained by us, some of our customers continued using them for a long time.

Despite us proclaiming these components as unmaintained, we still put effort into supporting them - trying to fix problems our customers sometimes encountered.
In the end, such components still misbehaved once in a while and we couldn't always make them fully work.
If a component is unmaintained by its developers, we cannot help maintaining it ourselves because eventually it becomes incompatible with modern APIs, and there is nobody to fix the component's code and make it compatible again.

This has prompted us to re-evaluate the current status of our deprecated components.
To avoid having to invest effort maintaining something which we proclaimed as unmaintained many months ago, we've completely eliminated all deprecated components after a long grace period.

Here is the list of the removed or deprecated components:

* **Dimension integration manager** which is unmaintained by its developers since November 2022. There is no free-software alternative integration manager
* **Wireguard** (and dnsmasq) which has been superseded by [Firezone](/help/extras/firezone/)
* **Discord bridge (mx-puppet-discord)** which has been unmaintained by its developers since May 2022. It got superseded by [Discord bridge (mautrix-discord)](/help/bridges/mautrix-discord/)
* **Facebook bridge (mautrix-facebook)** which has been archived by its developers since March 2024. It got superseded by [Facebook bridge (mautrix-meta)](/help/bridges/mautrix-meta-messenger/)
* **GroupMe Bridge (mx-puppet-groupme)** which has been unmaintained by its developers since November 2021. There is no free-software alternative GroupMe bridge
* **Instagram bridge (mautrix-instagram)** which has been archived by its developers since March 2024. It got superseded by [Instagram bridge (mautrix-meta)](/help/bridges/mautrix-meta-instagram/)
* **Slack bridge (mx-puppet-slack)** which has been unmaintained by its developers since May 2022. It got superseded by [Slack bridge (mautrix-slack)](/help/bridges/mautrix-slack/)
* **Steam Bridge (mx-puppet-steam)** which uses obsolete framework/base unmaintained by its developers since May 2022. There is no free-software alternative Steam bridge.
* **Webhooks bridge (appservice-webhooks)** which has been unmaintained by its developers since November 2022. It got superseded by [Webhooks bridge (hookshot)](/help/bridges/hookshot/)
