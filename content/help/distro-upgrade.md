---
title: Linux Distribution Upgrade
layout: single
draft: false
description: Learn how to upgrade the Linux Distribution of your Matrix server. This step-by-step guide covers the process of upgrading your server's OS, including the necessary preparations, the upgrade itself, and post-upgrade steps.
---

If you ever need to upgrade your Matrix server's host OS (distribution) from one major version to another, you can [contact us](/contacts/) and we will handle the upgrade for you for a one-time fee. We will take care of the entire process, ensuring your server is fully operational.

However, if you prefer to do it yourself at no extra cost, we will guide you through the process.

## Ubuntu

Please, use **LTS** (Long Term Support) versions of Ubuntu only.
We recommend using the [latest LTS version](https://wiki.ubuntu.com/Releases#List_of_releases) available at the time of installation.

### 1. Backup

**Hosting customers**: [contacts us](/contacts/) and ask us to perform a full server backup (snapshot)

**On-premises customers**: Make sure to back up your server before proceeding. All cloud providers have some kind of backup functionality and make snapshots either automatically or on-demand (manually)

### 2. Fallback port

During the upgrade process, the SSH process on your host may be restarted and you may lose connectivity.

The upgrade process usually opens another fallback SSH port (`1022`) that may be used to get back to the upgrade process.
The port will be opened automatically, but you need to update firewall(-s) to allow incoming connections to this port.

Ensure the `screen` utility is installed on your system to be able to [resume the session](https://serverfault.com/a/427004).

**Hosting customers**:

1. [Contacts us](/contacts/) and ask to allow SSH connections to the fallback port
2. Update the on-server firewall to allow incoming connections to the fallback port: `ufw allow 1022/tcp`

**On-premises customers**:

1. Update your provider-level firewall rules to allow connectivity to the `1022` (TCP) port, if you have one
2. Update the on-server firewall to allow incoming connections to the fallback port: `ufw allow 1022/tcp`

### 3. Update the system

Before performing the actual distribution upgrade, make sure your system is up-to-date:

```bash
# Update the package list
apt-get update
# Upgrade the system packages
apt-get dist-upgrade
```

You _may_ reboot your server after this step, if apt output suggests so.

### 4. Upgrade to the new release

Finally, run the upgrade command:

```bash
do-release-upgrade
```

Follow the on-screen instructions - they are quite straightforward.

## Post-upgrade steps

After the upgrade is complete, we strongly suggest to [run maintenance](/help/extras/scheduler/#maintenance) to ensure your server is properly configured. If your Matrix server happens to be down after the upgrade, [contact us](/contacts/) and we will help you bring it back online.
