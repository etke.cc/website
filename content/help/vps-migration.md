---
title: VPS to VPS migration
layout: single
draft: false
description: Learn how to migrate your Matrix server from one VPS to another. This step-by-step guide covers DNS setup, SSH configuration, data migration, and post-migration steps.
---

If you ever need to migrate your Matrix server from one host/VPS/VM to another, you can [contact us](/contacts/) and we will handle the migration for you for a one-time fee. We will take care of the entire process, including setting up the new server, transferring the data, and ensuring your server is fully operational.

However, if you prefer to do it yourself at no extra cost, we will guide you through the process.

1. Set DNS TTLs to 5 minutes for all Matrix-related DNS records.
2. Add etke.cc's SSH keys ([etke.cc/keys.txt](https://etke.cc/keys.txt)) to the new server, and share the SSH login information with us, along with the sudo password (if set) and the new server's IP address.
3. Generate a new SSH key on the new server using `ssh-keygen -t ed25519 -N '' -C migration`. Add the generated public key to the old server's `.ssh/authorized_keys` file (preferably using the `root` user).
4. Stop and disable all Matrix services from auto-starting on the old server by running `cd /etc/systemd/system/ && systemctl disable --now matrix-*`. Ensure they are stopped using `systemctl | grep matrix-` (a failed state is acceptable).
5. Update all Matrix-related DNS records to point to the new server.
6. Start a new [screen](https://www.gnu.org/software/screen/) session on the new server by running `screen -R`.
7. Migrate the data on the new server by running `rsync -arP OLD_SERVER_USER@OLD_SERVER_IP:/matrix/ /matrix` (replace `OLD_SERVER_USER` and `OLD_SERVER_IP` with the appropriate values).
8. Inform us once the data migration is complete. We will then run a full setup process to properly configure the new server.

If you have any Matrix client app with an active session, it will reconnect after the migration. Expect a few minutes of instability after the initial connection.

We recommend completing steps 1 and 2 first, so we can ensure connectivity to your new server is working.

The subsequent steps involve stopping services on the old server. Matrix services will be down until the data is copied over to the new server, you contact us, and we complete the full setup process. This final cooperation step may take some time, especially over weekends or during night hours (in Europe), which can lead to extended downtime.

It's best to perform the last steps at a mutually agreed-upon time with us to ensure a quicker response. Also, be aware that your Matrix services will be down once you stop them, so you'll need to contact us using any of the other methods listed on our [contacts page](https://etke.cc/contacts/).
