---
title: Google Chat
draft: false
layout: bridge
network: https://chat.google.com
package: mautrix-googlechat
docs: https://docs.mau.fi/bridges/python/googlechat/
code: https://github.com/mautrix/googlechat
support: https://matrix.to/#/%23googlechat:maunium.net
---

## Usage

Start a chat with `@googlechatbot:your-server.com`. When it joins, type `help` in the chat to see instructions

1. Send `login` to the bridge bot
2. Follow the link sent by the bot, click `Start`, and follow the instructions
3. Recent chats should now get portals automatically. Other chats will get portals as you receive messages.
