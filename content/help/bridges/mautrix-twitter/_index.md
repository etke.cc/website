---
title: Twitter
draft: false
layout: bridge
network: https://twitter.com
package: mautrix-twitter
docs: https://docs.mau.fi/bridges/python/twitter/
code: https://github.com/mautrix/twitter
support: https://matrix.to/#/%23twitter:maunium.net
---

## Usage

Start a chat with `@twitterbot:your-server.com`. When it joins, type `help` in the chat to see instructions

1. Send `login-cookie` to the bridge bot
2. The bot will send you these instructions to extract required cookies. Follow the steps to log into the bridge
3. Log into the [Twitter](https://twitter.com) in private/incognito browser window
4. Press `F12` to open developer tools
5. Select the `Application` (Chrome, Safari, Edge) or `Storage` (Firefox) tab
6. In the sidebar, expand `Cookies` and select `https://twitter.com`
7. In the cookie list, find the `auth_token` row and double click on the value then copy the value and send it to the chat
8. In the cookie list, find the `ct0` row and double click on the value then copy the value and send it to the chat
9. Recent chats should now get portals automatically. Other chats will get portals as you receive messages.
