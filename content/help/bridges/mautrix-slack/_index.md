---
title: Slack
draft: false
layout: bridge
network: https://slack.com
package: mautrix-slack
docs: https://docs.mau.fi/bridges/go/slack/index.html
code: https://github.com/mautrix/slack/
support: https://matrix.to/#/%23slack:maunium.net
aliases:
    - /help/bridges/mx-puppet-slack/
---

## Usage

Start a chat with `@slackbot:your-server.com`. When it joins, type `help` in the chat to see instructions.

### Option 1: Password login

1. Send `login-password your@email.here slack-team-domain.here your-password.here` (e.g., `login-password me@example.com example.slack.com superSecretPassword` to the bridge bot.
2. Recent chats should now get portals automatically. Other chats will get portals as you receive messages.

Password login isn't guaranteed to always be fully supported, but you can switch to token-based login at any time afterwards by following the instructions in the section below without signing out.

### Option 2: Token login

1. Open a slack workspace in a browser
2. Open browser dev tools (chrome dev tools) and go to the Network tab
3. Filter requests by type WS/WebSocket
4. Find `xoxc` token as a parameter of one of the URLs
5. Go to the dev tools Storage tab to get a list of cookies
6. Find `d` cookie and copy its contents
7. Send `login-token your.xoxc-token your.d-cookie` to the bridge bot
8. After login using a token, all your joined channels will automatically be bridged into Matrix, but DMs will only appear once you receive messages in them.
