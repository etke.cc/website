---
title: Skype
draft: false
layout: bridge
network: https://www.skype.com
package: go-skype-bridge
docs: https://github.com/kelaresg/go-skype-bridge
code: hhttps://github.com/kelaresg/go-skype-bridge
support: https://matrix.to/#/%23goskypebridge:matrix.org
---

## Usage

Start a chat with `@skypebridgebot:your-server.com`. When it joins, type `help` in the chat to see instructions.
