---
title: Hookshot
draft: false
layout: bridge
network: https://en.wikipedia.org/wiki/Webhook
package: hookshot
docs: https://matrix-org.github.io/matrix-hookshot
code: https://github.com/matrix-org/matrix-hookshot
support: https://matrix.to/#/%23hookshot:half-shot.uk
aliases:
    - /help/bridges/appservice-webhooks/
---

Hookshot supports integration with popular software project management services such as [GitHub](https://github.com), [GitLab](https://gitlab.com), [JIRA](https://www.atlassian.com/software/jira), and [Figma](https://www.figma.com/), as well as [generic webhooks](https://en.wikipedia.org/wiki/Webhook).

Below, we describe the basic Hookshot usage for:

- creating webhooks to receive arbitrary data in Matrix rooms
- subscribing to RSS/Atom feeds to receive updates in Matrix rooms


## Usage

Create a room and invite `@hookshot:your-server.com`. When it joins, type `!hookshot help` in the chat to see instructions.

Make sure the bot is able to send state events (usually the Moderator power level in clients).


### Webhooks

Now that you've invited the bot to the room and ensured the permission level is adequate, send a `!hookshot webhook yourwebhook` room message and you will receive a webhook link in a private message.

You can use this link with external systems which support webhooks.

To verify that the webhook link works, or to make use of it from your own systems, send a new message via the webhook link.
You will need to send a JSON body payload to it which looks like this:

```json
{
    "text": "Hello world!",
    "username": "MyBot"
}
```

You can test this via `curl` like so:

```sh
curl --header "Content-Type: application/json" \
--data '{
"text": "Hello world!",
"username": "MyBot"
}' \
THE_WEBHOOK_LINK_YOU_RECEIVED_EARLIER
```

For more information about webhook payloads and handling, see the [Hookshot documentation about Webhooks](https://matrix-org.github.io/matrix-hookshot/latest/setup/webhooks.html).


### Feeds (RSS, Atom)

Now that you've invited the bot to the room and ensured the permission level is adequate, send a `!hookshot feed URL_HERE` message to register a new URL feed. (`URL_HERE` needs to point to an [RSS](https://en.wikipedia.org/wiki/RSS) or [Atom](https://en.wikipedia.org/wiki/Atom_(web_standard)) feed).

To list feeds that the bot currently subscribes to, use `!hookshot feed list`.

To remove feeds from the list, use `!hookshot feed remove URL_HERE`.

For more information about feed handling, see the [Hookshot documentation about Feeds](https://matrix-org.github.io/matrix-hookshot/latest/setup/feeds.html).
