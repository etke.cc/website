---
title: WeChat
draft: false
layout: bridge
network: https://www.wechat.com
package: matrix-wechat
docs: https://github.com/duo/matrix-wechat/blob/master/README.md
code: https://github.com/duo/matrix-wechat
support: https://github.com/duo/matrix-wechat/issues
---

## Usage

Start a chat with `@wechat:your-server.com`. When it joins, type `help` in the chat to see instructions

1. Send `login` to the bridge bot
2. Open WeChat on your phone
3. Tap plus icon in the top right corner and select `Scan`
4. Point your phone at the QR code sent by the bot to scan it
5. Recent chats should now get portals automatically. Other chats will get portals as you receive messages.
