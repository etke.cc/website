---
title: WhatsApp
draft: false
layout: bridge
network: https://www.whatsapp.com
package: mautrix-whatsapp
docs: https://docs.mau.fi/bridges/go/whatsapp/index.html
code: https://github.com/mautrix/whatsapp
support: https://matrix.to/#/%23whatsapp:maunium.net
---

## Usage

Start a chat with `@whatsappbot:your-server.com`. When it joins, type `help` in the chat to see instructions

1. Send `login` to the bridge bot
2. Open WhatsApp on your phone
3. Tap `Menu` or `Settings` and select `WhatsApp Web`
4. Point your phone at the image sent by the bot to capture the code
5. Recent chats should now get portals automatically. Other chats will get portals as you receive messages.

Please note that the bridge uses the web API, so your phone must be connected to the internet for the bridge to work.
The WhatsApp app doesn't need to be running all the time, but it needs to be allowed to wake up when receiving messages.
The web API is used instead of the main client API to avoid getting banned.
