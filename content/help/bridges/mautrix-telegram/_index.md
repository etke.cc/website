---
title: Telegram
draft: false
layout: bridge
network: https://telegram.org
package: mautrix-telegram
docs: https://docs.mau.fi/bridges/python/telegram/
code: https://github.com/mautrix/telegram
support: https://matrix.to/#/%23telegram:maunium.net
---

## Usage

Start a chat with `@telegrambot:your-server.com`. When it joins, type `help` in the chat to see instructions

1. Send `login` to the bridge bot
2. Click on the link bot sent to you and fill your phone number (without spaces, dots, etc), and click on `Request Code`
3. Enter your code and click `Sign in`.
4. If you have 2FA enabled, enter your password and click on `Sign in` again.
5. Recent chats should now get portals automatically. Other chats will get portals as you receive messages.
