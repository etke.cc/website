---
title: Signal
draft: false
layout: bridge
network: https://signal.org
package: mautrix-signal
docs: https://docs.mau.fi/bridges/go/signal/index.html
code: https://github.com/mautrix/signal
support: https://matrix.to/#/%23signal:maunium.net
aliases:
    - /help/bridges/mautrix-signal-python/
---

## Usage

Start a chat with `@signalbot:your-server.com`. When it joins, type `help` in the chat to see instructions

1. Go to `Linked Devices` in Signal app and add a new device
2. Send `login` to the bridge bot
3. Scan the QR code the bridge bot sends you
