---
title: IRC
name: IRC (Heisenbridge)
draft: false
layout: bridge
network: https://en.wikipedia.org/wiki/Internet_Relay_Chat
package: heisenbridge
docs: https://github.com/hifi/heisenbridge
code: https://github.com/hifi/heisenbridge
support: https://matrix.to/#/%23heisenbridge:vi.fi
---

Heisenbridge brings IRC to Matrix by creating an environment where every user connects to each network individually like they would with a traditional IRC bouncer.

## Usage

Start a chat with `@heisenbridge:your-server.com`. When it joins, type `help` in the chat to see instructions

1. Send `addnetwork network.name` to the bridge bot, eg: `addnetwork libera.chat`
2. Send `addserver network.name irc-server.address port --tls` to the bridge bot, eg: `addserver libera.chat irc.libera.chat 6697 --tls`
3. Send `open network.name` to the bridge bot, eg: `open libera.chat`. The bridge bot will create a control room for that IRC network and invite you
4. In the new room, read the bot instructions in the first room message and configure it as you wish, after that send `connect`
5. Send `join #room-name` to the bot to bridge into an IRC room

> **Pro Tip!** You can allow other users (even from other homeservers) to use your bridge by sending `addmask @user:some-server.com` or even `addmask *:other-server.com` to the bot
