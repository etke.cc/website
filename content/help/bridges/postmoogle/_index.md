---
title: Email
draft: false
layout: bridge
network: https://en.wikipedia.org/wiki/Email
package: postmoogle
docs: https://gitlab.com/etke.cc/postmoogle
code: https://gitlab.com/etke.cc/postmoogle
support: https://matrix.to/#/%23postmoogle:etke.cc
---

With Postmoogle, you can:

- **receive** emails to custom addresses (like `something@matrix.example.com`) and have these emails delivered to Matrix rooms
- **send** outgoing emails
- and [many more things](https://gitlab.com/etke.cc/postmoogle#roadmap)


## Usage

Create a room and invite `@emailbot:your-server.com` (or `@postmoogle:your-server.com` if your server was installed before October 2023).
When it joins, type `!pm help` in the chat to see instructions

1. Send `!pm mailbox yourmailbox` to the bridge bot
2. Now you can receive emails to `yourmailbox@matrix.your-server.com` and send emails using the `!pm send` command
3. Send `!pm dkim` to the bridge bot and copy the signature from the bot response
4. Add new DNS record with type = `TXT`, key (subdomain/from): `postmoogle._domainkey.matrix` and value: signature copied from the `!pm dkim` response

After that we recommend to verify email deliverability using online services like [mail-tester.com](https://mail-tester.com), [dkimvalidator.com](https://dkimvalidator.com/). 
Note that even if your configuration is perfect, you still will have deliverability issues due to high centralization around big email services (e.g., Gmail, Outlook, Yahoo, etc.)
