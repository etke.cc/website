---
title: Facebook Messenger
draft: false
layout: bridge
network: https://messenger.com
package: mautrix-meta
docs: https://docs.mau.fi/bridges/go/meta/index.html
code: https://github.com/mautrix/meta
support: https://matrix.to/#/%23meta:maunium.net
aliases:
    - /help/bridges/mautrix-facebook/
---

## Usage

The `mautrix-meta` bridge can communicate with Facebook Messenger via [Facebook](https://www.facebook.com/), Facebook over [Tor](https://www.torproject.org/) and [Messenger](https://www.messenger.com/).

We configure the bridge to use [Messenger](https://www.messenger.com/), **not** [Facebook](https://www.facebook.com/), because that's most universal - all Facebook users have an associated Messenger account, but not all Messenger users have a Facebook account.

Start a chat with `@messengerbot:your-server.com`. When it joins, type `help` in the chat to see instructions

1. **Send a `login` message to the bridge bot**. The bot will reply with a link to its official [Authentication](https://docs.mau.fi/bridges/go/meta/authentication.html) instructions. **Ignore this** message. Our instructions below are similar, but simpler.

3. **Open a private window in your browser** ([Firefox](https://www.mozilla.org/en-US/firefox/new/) or any [Chromium](https://www.chromium.org/chromium-projects/)-based browser like [Google Chrome](https://www.google.com/chrome/), [Microsoft Edge](https://www.microsoft.com/en-us/edge), [Brave](https://brave.com/), etc.). It's important to use a private window, because we'd like your new Messenger login session to be separate from your regular session.

4. **Log into [Messenger](https://www.messenger.com/)** as you would usually do. We'll reiterate that it's important to log into [Messenger](https://www.messenger.com/), **not** [Facebook](https://www.facebook.com/)

5. **Open your browser's Developer Tools**. You can do this by **right-clicking anywhere** on the Messenger page and then clicking on the **Inspect** button. You may also open Developer Tools using a keyboard shortcut - `Ctrl + Shift + K` for Firefox and `Ctrl + Shift + J` for Chromium-based browsers.

6. Click on the **Network** tab in the Developer Tools window. **See a screenshot** of some of the steps below [for Firefox](/img/bridges/mautrix-meta/firefox-devtools.webp) or [for Chrome](/img/bridges/mautrix-meta/chrome-devtools.webp).

7. In the **Filter** textbox, enter this text: `graphql`

8. Ensure that the **All** button or the **XHR** button next to the *Filter* textbox **is activated**.

9. **Reload the page** using the reload button in the browser's toolbar or with the `F5` keyboard shortcut

10. You will **see one or more `graphql/` entries** in the list below the **Filter** textbox

11. **Right-click** on any of the `graphql/` entries, go to the **Copy** submenu and then click the **Copy as cURL** button (for Mac and Linux) or **Copy as cURL (POSIX)** button (for Windows). This copies the `graphql/` request in a [curl](https://curl.se/)-compatible format.

12. **Paste this copied information into your existing chat with the bridge bot** and send the message. The `mautrix-meta` bridge will extract some login [cookies](https://developer.mozilla.org/en-US/docs/Web/HTTP/Cookies) from there. The bot will automatically delete your message to prevent this sensitive information from leaking.

13. **Close the private window WITHOUT logging out**. It's important that you just close the window without actually logging out of Messenger. Logging out of Messenger will invalidate the login session and the bridge will stop working.
