---
title: Help | Matrix Bridges to Other Communication Networks
name: Bridges
heading: Matrix Bridges to Other Communication Networks
draft: false
layout: help-section
description: Explore Matrix Bridges, the essential connectors that facilitate seamless communication between Matrix and other chat and non-chat networks. Connect with people and bots from various networks using your Matrix server and Matrix client. Installation and support are available for bridges like Discord, Email, Facebook Messenger, Google Chat, Google Messages, and more.
aliases:
    - /help/bridges
    - /help/bridges/
    - /bridges
---

Matrix Bridges are essential connectors that enable communication between Matrix and other chat and non-chat networks. With these bridges, you can seamlessly interact with individuals and bots from various networks using your Matrix server and supported Matrix client.

We offer installation and support for the following bridges on your Matrix server:

- [**Discord Bridge**](/help/bridges/mautrix-discord/): Powered by [mautrix-discord](https://github.com/mautrix/discord)
- [**Email Bridge**](/help/bridges/postmoogle/): Powered by our own [Postmoogle](https://gitlab.com/etke.cc/postmoogle) bridge
- [**Facebook Messenger Bridge**](/help/bridges/mautrix-meta-messenger/): Powered by [mautrix-meta](https://github.com/mautrix/meta)
- [**Google Chat Bridge**](/help/bridges/mautrix-googlechat/): Powered by [mautrix-googlechat](https://github.com/mautrix/googlechat)
- [**Google Messages Bridge**](/help/bridges/mautrix-gmessages/): Powered by [mautrix-gmessages](https://github.com/mautrix/gmessages)
- [**IRC Bridge**](/help/bridges/heisenbridge/): Powered by [Heisenbridge](https://github.com/hifi/heisenbridge)
- [**Instagram Bridge**](/help/bridges/mautrix-meta-instagram/): Powered by [mautrix-meta](https://github.com/mautrix/meta)
- [**LinkedIn Bridge**](/help/bridges/beeper-linkedin/): Powered by [beeper-linkedin](https://github.com/beeper/linkedin)
- [**Signal Bridge**](/help/bridges/mautrix-signal/): Powered by [mautrix-signal](https://github.com/mautrix/signal)
- [**Skype Bridge**](/help/bridges/go-skype-bridge/): Powered by [go-skype-bridge](https://github.com/kelaresg/go-skype-bridge)
- [**Slack Bridge**](/help/bridges/mautrix-slack/): Powered by [mautrix-slack](https://github.com/mautrix/slack)
- [**Telegram Bridge**](/help/bridges/mautrix-telegram/): Powered by [mautrix-telegram](https://github.com/mautrix/telegram)
- [**Twitter Bridge**](/help/bridges/mautrix-twitter/): Powered by [mautrix-twitter](https://github.com/mautrix/twitter)
- [**Webhooks Bridge**](/help/bridges/hookshot/): Powered by [hookshot](https://github.com/matrix-org/matrix-hookshot)
- [**WeChat Bridge**](/help/bridges/wechat/): Powered by [matrix-wechat](https://github.com/duo/matrix-wechat)
- [**WhatsApp Bridge**](/help/bridges/mautrix-whatsapp/): Powered by [mautrix-whatsapp](https://github.com/mautrix/whatsapp)

In addition to these bridges, we offer various [Bots](/help/bots/) and [Extra Services](/help/extras/) to enhance your Matrix server. Discover how these tools can expand the capabilities of your Matrix server and streamline communication across different networks.
