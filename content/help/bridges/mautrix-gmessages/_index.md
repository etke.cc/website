---
title: Google Messages
draft: false
layout: bridge
network: https://messages.google.com/
package: mautrix-gmessages
docs: https://docs.mau.fi/bridges/go/gmessages/index.html
code: https://docs.mau.fi/bridges/go/gmessages/index.html
support: https://matrix.to/#/%23gmessages:maunium.net
---

## Usage

Start a chat with `@gmessagesbot:your-server.com`. When it joins, type `help` in the chat to see instructions

1. Send `login` to the bridge bot
2. On your phone, open the `Messages by Google` app
3. Tap Menu from your conversation list and select `Device pairing`
4. Tap `QR code scanner` and point your phone at the image sent by the bot.
5. Recent chats should now get portals automatically. Other chats will get portals as you receive messages.

All messages are proxied through the phone app, your phone has to be connected to the internet for the bridge to work.
