---
title: Discord
draft: false
layout: bridge
network: https://discord.com
package: mautrix-discord
docs: https://docs.mau.fi/bridges/go/discord/
code: https://github.com/mautrix/discord
support: https://matrix.to/#/%23discord:maunium.net
aliases:
    - /help/bridges/mx-puppet-discord/
---

## Usage

Start a chat with `@discordbot:your-server.com`. When it joins, type `help` in the chat to see instructions.

1. Send `login-qr` to the bridge bot
2. Open the Discord app on your phone
3. Point your phone at the image sent by the bot to capture the code, [official docs](https://support.discord.com/hc/en-us/articles/360039213771-QR-Code-Login-FAQ)
4. After scanning the code, you'll need to approve the login on the mobile app
5. Recent chats should now get portals automatically. Other chats will get portals as you receive messages.

You may uninstall the Discord app from your mobile phone after that.

Useful commands:

* `guild status` to see the list of guilds
* `guilds bridge GUILD_ID --entire` bridge a guild
