---
title: LinkedIn
draft: false
layout: bridge
network: https://www.linkedin.com
package: beeper-linkedin
docs: https://github.com/beeper/linkedin
code: https://github.com/beeper/linkedin
support: https://matrix.to/#/%23linkedin-matrix:nevarro.space
---

## Usage

Start a chat with `@linkedinbot:your-server.com`. When it joins, type `help` in the chat to see instructions

### Login (recommended)

1. Send `login your@email.here` to the bridge bot
2. Send your password in the next message
3. If you have 2FA enabled, the bot will ask you to send the 2FA token.
4. Recent chats should now get portals automatically. Other chats will get portals as you receive messages.

Note: If you don't have 2FA enabled and are logging in from an unknown/strange IP for the first time,
LinkedIn will send an email with a one-time code. You can use this code to authorize the bridge session.
Usually, once the IP is authorized, you will not be asked again.

### Login (manual)

In case of recommended login didn't work, you can login manually using `li_at` and `jsessionID` cookies.

1. Open linkedin website and login to your account.
2. Go to the dev tools Storage tab to get a list of cookies
3. Find the `li_at` and `jsessionID` cookies' values
4. Send `login-manual <li_at> <jsessionID>`
5. Recent chats should now get portals automatically. Other chats will get portals as you receive messages.
