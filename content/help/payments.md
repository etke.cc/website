---
title: Payments
layout: singletoc
draft: false
description: Explore payment instructions and answers to payment-related questions, such as becoming an etke.cc customer, updating subscription details, cancelling your subscription or renewing a failed subscription.
---

etke.cc uses the [Ko-Fi](https://etke.cc/ko-fi) payment and subscription management platform, which itself uses [PayPal](https://www.paypal.com/) for payment processing. At the moment, we don't support any other subscription or payment options.

While Ko-Fi is a robust tool, some aspects of it might not be immediately intuitive.
To ensure a seamless experience, we've prepared a set of step-by-step guides to assist you in various payment scenarios.

## How to Become a Subscriber

To access etke.cc's services, you'll need to set up a subscription with recurring payments on the [Ko-Fi](https://ko-fi.com/) subscription platform.

You will need a Ko-Fi account to manage your subscription later (to [update its price](#how-to-update-your-subscription-price), to [fix it if payments are failing](#how-to-fix-a-failing-subscription) or to [cancel it](#how-to-cancel-your-subscription)). For this reason:

- it's **recommended** that you [sign up for Ko-Fi](https://ko-fi.com/account/register) now (before making your subscription), so that the subscription will be associated with you automatically. When signing up for Ko-Fi, make sure to use the same email address that you had provided when placing your etke.cc order. If you use another email address, we won't be able to automatically match it to your order, so you'd need to [contact us](/contacts/) about it.

- alternatively, you sign up for Ko-Fi at any time later on, after having created a subscription. However, it's important that you sign up with the same email address that you used for your subscription (see below).

To become an etke.cc subscriber:

1. [Sign into your Ko-Fi account](https://ko-fi.com/account/login), if you have one already. If not, you can also subscribe without a Ko-Fi account, as discussed above.

2. Visit the [etke.cc membership page on Ko-Fi](https://etke.cc/membership).

3. Select the **By Complexity** tier.

4. Enter the following details:
   - **Pay what you want:** Use the total price based on the components you've ordered.
   - **Email address:** This field only appears when ordering without a Ko-Fi account. Use the **same email address** you had provided when placing your etke.cc order. If you use another email address, we won't be able to automatically match it to your order, so you'd need to [contact us](/contacts/) about it.
   - **Your name or nickname:** (optional) Use any name you wish

   ![Payment form](/img/ko-fi/payment-form.webp)

5. Press the **PayPal** button to proceed to the PayPal checkout page.

6. Select your preferred payment method or add a new one and click on **Agree & Continue**.

   ![PayPal payment](/img/ko-fi/payment-paypal.webp)

7. If the payment is successful, you'll be redirected to the payment confirmation page on Ko-Fi.

   ![Payment confirmation](/img/ko-fi/payment-confirmation.webp)

Congratulations! You're all set. 🎉

We'll receive a notification from Ko-Fi and will begin preparing your Matrix server.
**Keep an eye on your email for updates**.

If you had created a Ko-Fi account, you can also review your account's [Payments and Orders](https://ko-fi.com/manage/supportreceived?src=sidemenu) page on Ko-Fi.
If you don't have a Ko-Fi account yet, perhaps now is a good time to [sign up for Ko-Fi](https://ko-fi.com/account/register) - just make sure to sign up with the same email address that you used for your subscription, so that it will be associated with your new account.

![Payments list](/img/ko-fi/payment-list.webp)

---

## How to Update Your Subscription Price

If you subsequently order new services from us or cancel some services we were providing until now, you may need to adjust the price of your subscription to align it with the new cost.

You will need a [Ko-Fi](https://ko-fi.com/) account to manage your etke.cc subscription on the Ko-Fi platform. If you haven't created a Ko-Fi account early on, you can create one now by going to [Ko-Fi's signup page](https://ko-fi.com/account/register). When signing up, make sure to use the same email address that you used for your already-existing subscription - this way, Ko-Fi will automatically attribute this subscription to your new account.

To update your subscripion's price:

1. Log in to your Ko-Fi account

2. Visit your [Ko-Fi account page](https://ko-fi.com/account/)

3. Locate your **etke.cc** subscription in the list.

   ![Subscriptions list](/img/ko-fi/subscriptions-list.webp)

4. Click on the three-dots dropdown menu and press the **Update Pledge** button

   ![Update Pledge button on the subscriptions list](/img/ko-fi/subscriptions-adjust.webp)

5. Enter the new, recalculated price into the input field and click **Update**

   ![Adjusted price input on the subscriptions list](/img/ko-fi/subscriptions-adjust-input.webp)

6. Confirm the changes by clicking **Yes, update pledge**

7. You'll be charged using the new subscription price **during the next payment period** (e.g. next month), not immediately. To **make up for the difference** during the current payment period, make [a one-time Ko-Fi donation](https://etke.cc/kofi) with a custom price equal to the price difference between the new price and the old price. For example, if going from $10/mo subscription to a $25/mo subscription, make a one-time payment of $15.

Congratulations, you've successfully updated your subscription price! 🎉

Your [Ko-Fi account page](https://ko-fi.com/account/) should now reflect these changes:

![Subscriptions list after the price adjustments](/img/ko-fi/subscriptions-list-adjusted.webp)

---

## How to Cancel Your Subscription

If, for any reason, you don't wish to continue with your subscription, you can cancel it.

You will need a [Ko-Fi](https://ko-fi.com/) account to manage your etke.cc subscription on the Ko-Fi platform. If you haven't created a Ko-Fi account early on, you can create one now by going to [Ko-Fi's signup page](https://ko-fi.com/account/register). When signing up, make sure to use the same email address that you used for your already-existing subscription - this way, Ko-Fi will automatically attribute this subscription to your new account.

To cancel your subscription:

1. Log in to your Ko-Fi account

2. Visit your [Ko-Fi account page](https://ko-fi.com/account/)

3. Locate your **etke.cc** subscription in the list.

   ![Subscriptions list](/img/ko-fi/subscriptions-list.webp)

4. Click on the three-dots dropdown menu and press the **Don't Renew** button.

   ![Don't renew button on the subscriptions list](/img/ko-fi/subscriptions-cancel.webp)

5. Confirm the changes by clicking **Yes, cancel it**

Your canceled subscription will now appear on your Ko-Fi account page.

> **Note**: Depending on the automatic renewal status and the number of retry attempts, the canceled subscription may not be immediately visible or may appear differently.

![Subscriptions list after the cancellation](/img/ko-fi/subscriptions-list-canceled.webp)

At this point, you are no longer an etke.cc subscriber.
Nevertheless, we'll continue providing the service you were paying for until the end period you paid for.
If you wish for us to stop providing the service immediately, please [contact us](/contacts/).

---

## How to Fix a Failing Subscription

If, for any reason, your subscription fails to renew automatically, you may need to resubscribe manually.

To resubscribe:

1. **Cancel your existing subscription**. See the [How to Cancel Your Subscription](#how-to-cancel-your-subscription) guide
2. **Subscribe again**. See the [How to Become a Subscriber](#how-to-become-a-subscriber) guide
