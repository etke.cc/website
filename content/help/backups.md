---
title: Restoring servers from backups
layout: singletoc
draft: false
description: Learn how to restore your server from backups using BorgBackup and local Postgres backups. This guide provides step-by-step instructions for restoring specific files or full server snapshots.
---

Sometimes a server can crash due to hardware failure or a software bug. In such cases, having a backup is crucial for restoring the server to a working state. This guide will show you how to restore a backup of your server.

The best way is to [contact support](/contacts/) and let us handle it for you. However, if you decide to do it yourself, we will guide you through the process and mention other options and caveats. Please keep in mind that [with our hosting plans](/services/hosting/), we handle backups for you by default with daily full server snapshots (7 days retention).

## [BorgBackup](/help/extras/borgbackup/)

If your server has BorgBackup installed, follow these steps to restore a backup:

1. Get the list of all available backups using `/matrix/backup-borg/bin/borgmatic rlist`.
2. List the files in the archive. Usually, you need specific files from the archive, and extracting a full backup may require a lot of disk space. However, if you need to extract the full archive, skip this step. To find a specific file (e.g., Twitter bridge DB dump), use the following command: `/matrix/backup-borg/bin/borgmatic list --archive <ARCHIVE> --find .borgmatic/postgresql_databases/matrix-postgres/` (replace `<ARCHIVE>` with the archive name from the first step). Example: `/matrix/backup-borg/bin/borgmatic list --archive matrix-2023-04-04-021409 --find .borgmatic/postgresql_databases/matrix-postgres/`.
3. Extract the needed files from the archive using the command `/matrix/backup-borg/bin/borgmatic extract --archive <ARCHIVE> --destination /restore --path <LIST OF PATHS>` (replace `<ARCHIVE>` with the archive name from the first step and `<LIST OF PATHS>` with a space-separated list of paths without leading slash. If you want to extract the full archive, omit the `--path` parameter). Example: `/matrix/backup-borg/bin/borgmatic extract --archive matrix-2023-04-04-021409 --destination /restore --path root/.borgmatic/postgresql_databases/matrix-postgres/matrix_mautrix_twitter`.
4. The extracted files will be in the `/matrix/backup-borg/restored` directory. Do whatever you need with them, and don't forget to remove them when you're done.

## Local Postgres Backups

Enabled by default **only** for on-premises setup without any other backup solution. If you'd like to enable local Postgres backups on your server, please [contact us](/contacts/).

1. Find a usable backup in `/matrix/postgres-backup/data/last`; it is usually the latest one (e.g., `synapse-latest.sql.gz`).
2. Replace placeholders and run the following script on the server:

```bash
/usr/bin/env docker run --rm --name matrix-postgres-import \
--log-driver=none --user=998:1001 --cap-drop=ALL --network=matrix \
--env-file=/matrix/postgres/env-postgres-psql \
--mount type=bind,src=./DATABASE_DUMP.sql.gz,dst=/dump.sql.gz,ro \
--entrypoint=/bin/sh docker.io/postgres:POSTGRES_VERSION-alpine \
-c "cat /synapse_dump.sql | gunzip -c | grep -vE '^(CREATE|ALTER) ROLE (matrix)(;| WITH)' | grep -vE '^CREATE DATABASE (matrix)\s' | psql -v ON_ERROR_STOP=1 -h matrix-postgres --dbname=DATABASE_NAME"
```

- `DATABASE_DUMP.sql.gz` - file name on the host filesystem you want to import.
- `POSTGRES_VERSION` - Postgres version; you can find the actual version by running `cat /etc/systemd/system/matrix-postgres.service | grep -o "postgres:.*-alpine"`.
- `DATABASE_NAME` - the actual database name (e.g., `synapse` or `postmoogle`). Usually, the backup filename uses the format: `<database_name-timestamp>.sql.gz`.
