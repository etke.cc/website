---
title: Help
layout: help-base
draft: false
description: Explore answers to frequently asked questions for new homeserver owners and learn about additional components, such as bridges and bots, for your Matrix server. Discover the Scheduler, a maintenance system for server control, and Synapse-Admin, a web-based tool for managing your Synapse homeserver. If you need more assistance, don't hesitate to contact us.
---

## [Frequently Asked Questions](/help/faq)

Explore answers to common questions frequently asked by new homeserver owners.

## Additional Components

- [Bridges](/help/bridges/) - Discover how to bridge Matrix to other communication networks.
- [Bots](/help/bots/) - Explore bots you can use on your Matrix server.
- [Extra Services](/help/extras/) - Learn about additional services we can install on your Matrix server.

### [The Scheduler](/help/extras/scheduler/)

The Scheduler is a new maintenance system developed specifically for etke.cc customers.
It empowers you with control over server maintenance and addresses common issues, such as disk utilization and service restarts.

### [Synapse-Admin](/help/extras/synapse-admin/)

Synapse-Admin is a web-based tool for administrating your (Matrix) Synapse homeserver. Manage user accounts, delete media, and clear chat history to reclaim disk space. Find detailed how-to instructions for using synapse-admin on this page.

## Guides

We've prepared a series of guides to help you get started with your Matrix server

### [Payments](/help/payments)

Get help you with anything related to subscription-management (Ko-Fi) and payment-processing (Paypal).

### [Restoring Backups](/help/backups)

Learn how to restore your server from backups using BorgBackup and local Postgres backups.
This guide provides step-by-step instructions for restoring specific files or full server snapshots.

### [VPS to VPS migration](/help/vps-migration)

Learn how to migrate your Matrix server from one VPS to another. This step-by-step guide covers DNS setup, SSH configuration, data migration, and post-migration steps.

### [Linux Distributive Upgrades](/help/distro-upgrade)

Learn how to upgrade your Matrix server's host OS (distributive) from one major version to another. This step-by-step guide covers the process of upgrading your server's OS, including the necessary preparations, the upgrade itself, and post-upgrade steps.


## Need More Assistance?

If the information on these pages doesn't resolve your issue, please feel free to [contact us](/contacts/).
