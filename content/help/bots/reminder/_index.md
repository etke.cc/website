---
title: Reminder Bot
draft: false
layout: bot
package: matrix-reminder-bot
docs: https://github.com/anoadragon453/matrix-reminder-bot#usage
code: https://github.com/anoadragon453/matrix-reminder-bot
support: https://matrix.to/#/%23matrix-reminder-bot:matrix.org
---

With Reminder Bot, you can schedule **one-off** or **recurring** reminders by sending messages in a Matrix room.

The bot will keep track of the reminder and **send a message back to you** when the time comes.

### Example reminders

Example **one-off** reminders:

- `!reminmdme at 18:00; Pick up groceries`
- `!remindme tomorrow at 10:45; Head to that 11:00 meeting`
- `!remindme on 12th of September; Wish John a Happy Birthday`

Example **recurring** reminders:

- `!remindme every 1w; on Tuesday at 07:30; Take out the trash` (the first reminder will be next Tuesday at 07:30; it will repeat every 1 week after that)
- `!remindme every 5m; 1m; You are loved` (the first reminder will be after 1 minute; it will repeat every 5 minutes after that)

The [full documentation](https://github.com/anoadragon453/matrix-reminder-bot#usage) for the bot shows more examples.

## Usage

To use the bot, start a chat with `@reminder:your-server.com`.

You can also add the bot to any existing Matrix room by inviting it to the room.

See [Example Reminders](#example-reminders) above (or the [full documentation](https://github.com/anoadragon453/matrix-reminder-bot#usage)) for some example reminders you can schedule.

Send `!help reminders` to the room to see the bot's help menu for additional commands (listing reminders, canceling reminders, advanced usage, etc).
