---
title: Buscarron
draft: false
layout: bot
package: buscarron
docs: https://gitlab.com/etke.cc/buscarron/
code: https://gitlab.com/etke.cc/buscarron/
support: https://matrix.to/#/%23buscarron:etke.cc
---

# Buscarron - Matrix Bot for Secure Form Submissions

Buscarron is a Matrix bot developed by [etke.cc](https://etke.cc), which allows [forms](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/form) you add to your website (static or dynamic) to be submitted to a Matrix room of your choice. Submission happens via [HTTP POST](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST) to a URL endpoint that the Buscarron bot exposes.

Buscarron provides a robust and customizable solution for securely handling form submissions within the Matrix ecosystem.

## Features

### Secure Communication

Buscarron ensures end-to-end encryption, providing a secure channel for data transmission and maintaining user privacy.

### Form Submission Handling

Receive [HTTP POST](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST) forms and JSON submissions within Matrix rooms, and then redirect successful and failed submissions to different target pages.

### Spam prevention

Various spam prevention mechanisms are in place, starting from standard rate limiting and even doing SMTP validation on email fields in the form payload.

### Customizable matrix messages

Supporting [Go templates](https://pkg.go.dev/text/template) to define your own messages format

### Confirmation emails using Postmark

Say "Thank you!" automatically ;)

### Prometheus metrics

The bot exposes a [Prometheus](https://prometheus.io/)-compatible `/metrics` endpoint, protected with HTTP basic auth and an IP whitelist.
