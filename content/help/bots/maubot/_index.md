---
title: Maubot
draft: false
layout: bot
package: maubot
docs: https://docs.mau.fi/maubot/usage/basic.html
code: https://github.com/maubot/maubot
support: https://matrix.to/#/%23maubot:maunium.net
---

Maubot is a plugin-based Matrix bot system that can be used to create and manage bots for Matrix rooms. 
It is written in Python and can be extended with custom plugins to provide additional functionality. 
Maubot is designed to be easy to use and configure, making it a great choice for developers and non-developers alike.

By default, we configure Maubot's web interface to be accessible at `htts://matrix.your-server.com/_matrix/maubot`.
Your login is your Matrix ID (`@user:your-server.com`), and password sent in the onboarding list.
If you'd like to add or modify Maubot's admin users, please, [contact us](/contacts/).

To get started with Maubot, login into the web interface and follow the steps below:
1. Create one or more clients - a client is a matrix account which the bot will use to message. By default, the `maubot` (`@maubot:your-server.com`) account is created, you only need to obtain an access token for it
2. Upload plugins - you can get them from [official plugins repository](https://plugins.mau.bot/) or write your own
3. Create an instance - an instance is the actual bot. You have to specify a client which the bot instance will use and the plugin (how the bot will behave)

## Command-line Interface (advanced)

You can use Maubot cli to manage your Maubot instance, if you have SSH access to the server.

First, you need to get into the Maubot's container, with the following command: `docker exec -it matrix-bot-maubot sh`.

Then, you can use the `mbc` command to manage your Maubot instance:

```bash
$ mbc --help
Usage: python -m maubot.cli [OPTIONS] COMMAND [ARGS]...

Options:
  --help  Show this message and exit.

Commands:
  auth    Log into a Matrix account via the Maubot server
  build   Build a maubot plugin
  init    Initialize a new maubot plugin
  login   Log in to a Maubot instance
  logs    View the logs of a server
  upload  Upload a maubot plugin
```
