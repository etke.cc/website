---
title: ChatGPT
draft: false
layout: bot
package: matrix-chatgpt-bot
docs: https://github.com/matrixgpt/matrix-chatgpt-bot
code: https://github.com/matrixgpt/matrix-chatgpt-bot
support: https://matrix.to/#/%23matrix-chatgpt-bot:matrix.org
---

# ChatGPT Matrix Bot - Conversational AI in Your Matrix Rooms

![ChatGPT Matrix Bot](/img/bots/chatgpt.webp)

The ChatGPT Matrix Bot, brings the power of conversational AI (powered by [OpenAI](https://openai.com/)'s [ChatGPT](https://openai.com/blog/chatgpt)) into your own Matrix rooms, offering a seamless and interactive experience for users within the Matrix ecosystem.

To use this component, you will need to sign up for [OpenAI](https://openai.com/)'s platform and [obtain an API key](https://help.openai.com/en/articles/4936850-where-do-i-find-my-api-key). Send over this API key to us and we will configure your ChatGPT bot to use it - this way, your bot usage (through Matrix) will be billed to your own OpenAI account.

By default, we configure the bot to use the [ChatGPT 3.5 Turbo model](https://platform.openai.com/docs/models/gpt-3-5), but [other models are supported](#different-models-are-supported).

## Usage

Start a chat with `@chatgpt:your-server.com` and send the bot a message (example: `What are the benefits of decentralized networks?`).

Note that **the bot behaves differently based on the type of room it's in** (1-on-1 chat or group conversation), as described in [Matrix Integration](#matrix-integration) below.

## Matrix Integration

- Participate in Matrix rooms, providing real-time responses to user queries and engagement.
- Enhance collaboration by having ChatGPT present to facilitate discussions, answer questions, and provide information.

The ChatGPT Matrix Bot introduces a new dimension to communication within Matrix rooms, combining the capabilities of ChatGPT with the collaborative features of the Matrix platform. Enjoy natural language interactions and enhance the overall user experience with ChatGPT in your Matrix community.

Each conversation with the bot in your rooms happens within a separate thread. The bot behaves differently based on the room it's in:

- **in 1-on-1 rooms**, the bot considers every chat message as an inquiry and starts a chta thread to provide an answer
- **in rooms with multiple people (group chats)**, the bot does not consider every chat message as an inquiry meant for it to handle. This is to allow regular conversation (between people) to happen in the room without triggering the bot. In such rooms, you need to prefix all your questions to the bot with `!chatgpt`. Example message that would trigger the bot: `!chatgpt What are the benefits of decentralized networks?`

## Different models are supported

The bot could be powered by one of the [ChatGPT models](https://platform.openai.com/docs/models). For now, we support:

- (default) [ChatGPT 3.5 Turbo](https://platform.openai.com/docs/models/gpt-3-5) - a reasonably capable and fairly inexpensive model
- [gpt-4](https://platform.openai.com/docs/models/gpt-4-and-gpt-4-turbo) - a more capable (but ~10x more expensive and also slower) model
- [gpt-4-1106-preview](https://platform.openai.com/docs/models/gpt-4-and-gpt-4-turbo) - a more capable and up-to-date version of the `gpt-4` model

We do not support the [gpt-4-vision-preview](https://platform.openai.com/docs/models/gpt-4-and-gpt-4-turbo) model yet, because it's [not yet supported by matrix-chagpt-bot](https://github.com/matrixgpt/matrix-chatgpt-bot/issues/247).
