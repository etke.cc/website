---
title: Honoroit
draft: false
layout: bot
package: honoroit
docs: https://gitlab.com/etke.cc/honoroit/
code: https://gitlab.com/etke.cc/honoroit/
support: https://matrix.to/#/%23honoroit:etke.cc
---

# Honoroit - Matrix Bot for Efficient Helpdesk Support

Honoroit, developed by [etke.cc](https://etke.cc), is a specialized helpdesk bot designed to streamline support interactions within the Matrix ecosystem. By emulating the capabilities of website chat systems like Intercom and Jivosite inside Matrix (no web), Honoroit provides a seamless and user-friendly experience for both operators and customers.

## Key Features

### Chat-Based Configuration

- **Intuitive Setup:** Configure Honoroit effortlessly through chat interactions, making it easy for operators to adapt and manage settings on-the-fly.

### Prometheus Metrics

- **Comprehensive Monitoring:** Access detailed metrics through the `/metrics` endpoint, offering insights into Honoroit's performance and health.

### End-to-End Encryption

- **Secure Communication:** Ensure the privacy and security of conversations with end-to-end encryption, maintaining data confidentiality.

### Threaded Conversations

- **Efficient Communication:** Proxy messages from any Matrix user to a dedicated room, creating threaded conversations for efficient collaboration.
- **Collaborative Discussions:** Engage with users through Honoroit within a dedicated room, enabling multiple team members to participate in discussions.

### Request Fulfillment Workflow

- **Request Closure:** Mark requests as done using the `!ho done` command. Honoroit renames the thread topic and notifies the user, gracefully leaving their room with a special notice.

## Commands

### Available Commands in Threads

- `!ho done`: Close the current request, mark it as done, and notify the customer. The bot will gracefully leave the 1:1 chat with the customer.
- `!ho rename TXT`: Rename the thread topic title.
- `!ho note NOTE`: Keep private notes for operations in a thread with a customer.
- `!ho invite`: Invite yourself into the customer's 1:1 room.
- `!ho start MXID`: Start a conversation with a Matrix ID from the Honoroit bot.
- `!ho count MXID`: Count a request from a Matrix ID without creating a room or inviting them.
- `!ho config`: View all configuration options.
- `!ho config KEY`: View a specific configuration option and its current value.
- `!ho config KEY VALUE`: Update the value of a specific configuration option.

Honoroit offers a robust and user-centric solution for helpdesk support, seamlessly integrating into the Matrix platform for efficient and secure communication
