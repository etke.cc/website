---
title: Help | Matrix Bots
name: Bots
heading: Bots
draft: false
layout: help-section
description: Information about the various bots that we can install to your Matrix server
---

In addition to the various [Bridges](/help/bridges/) and [Extra Services](/help/extras/), we can also install and configure various bots on your Matrix server.

We offer installation and support for the following bots on your Matrix server:

- [Buscarron](/help/bots/buscarron/)
- [ChatGPT](/help/bots/chatgpt/)
- [Honoroit](/help/bots/honoroit/)
- [Maubot](/help/bots/maubot/)
- [Reminder Bot](/help/bots/reminder/)
