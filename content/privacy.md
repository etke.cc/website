---
title: Privacy Policy
draft: false
layout: singletoc
description: Explore our Privacy Policy to learn how etke.cc collects and manages your data and understand the involvement of every component and third party in our service operations. Protecting your privacy is our utmost priority.
---
At etke.cc, we are deeply committed to protecting your privacy. This page is currently under construction, but it provides important information about how your data is collected, used, and retained.

This Privacy Policy outlines how etke.cc collects, processes, and retains data, and it provides insights into the role of each component and third party involved in our service operations. By using our services, you consent to the practices described in this policy.

### Order Forms

Order forms are processed by our [Buscarron](https://gitlab.com/etke.cc/buscarron) bot on the Matrix server.

**Collected information**: The data you enter in the order form.

**Data retention**: up to 30 days

### Email communication

Email communication are built on top of the [Postmoogle](https://gitlab.com/etke.cc/postmoogle) bot on the Matrix server and [Postmark](https://postmarkapp.com/) email service.

**Collected information**: whole email message

**Data retention**: 45 days

### Managed hosting services

**Collected information**: the data you enter in the order form and additional technical information depending on the selected options in the order form (e.g., Telegram app ID and Hash).

**Data retention**: data is retained while the subscription is active.

### Third-Party Services

#### CDN

- [bunny.net Privacy Policy](https://bunny.net/privacy)

#### Email Service

- [Migadu.com Privacy Policy](https://www.migadu.com/privacy/)
- [Postmarkapp.com Privacy Policy](https://wildbit.com/privacy-policy)

#### Payment Services

- [Ko-Fi.com Privacy Policy](https://more.ko-fi.com/privacy)
- [PayPal Privacy Policy](https://www.paypal.com/us/webapps/mpp/ua/privacy-full) (used by Ko-Fi.com)
