---
title: Security Explained
draft: false
layout: singletoc
description: Explore the security measures and policies applied by etke.cc to protect customer data and privacy. Learn what data can be accessed and audited by customers. Our commitment to security ensures a safe and secure environment for your data.
---

# Security Explained

At etke.cc, we prioritize the security and privacy of our customers. This page provides insights into the security measures and policies we have in place.

## Security Measures

We have implemented the following security measures:

- **Firewall**: Protecting against unauthorized access.
- **Intrusion Prevention**: Detecting and mitigating potential threats.
- **SSH Daemon Hardening**: Enhancing the security of SSH access.
- **OS-Level Permissions**: Matrix components run under separate Linux cgroups and namespaces.
- **Docker Containers Hardening**: Ensuring the security of our containerized environments.
- **Single Source of Truth**: Any modifications to Matrix component configurations are wiped and replaced during each maintenance run.

## Data Access

### What Data Can Be Accessed by etke.cc?

We can access the following types of data:

- Any system file (due to SSH access with sudo permissions).
- Any plaintext (unencrypted) information, such as unencrypted text messages from bridged chats.

### What Data Can NOT Be Accessed by etke.cc?

We cannot access encrypted data, including encrypted messages, files, and more.

## Audit

### How Customers Can Check Server Activity?

Customers can monitor server activity using the following methods:

- **/var/log/auth.log**: This log is available by default. Additional archived logs may also be present, such as `/var/log/auth.log.1`, `/var/log/auth.log.2`, and so on.
- [The Linux Audit](https://wiki.archlinux.org/title/Audit_framework): Customers can leverage this audit framework for in-depth monitoring.
- Source Code Availability: In nearly all actions performed by our automation system on customers' servers, the source code is accessible on [gitlab.com/etke.cc](https://gitlab.com/etke.cc).
