import { defineConfig } from 'vite'
import { resolve } from 'path'
import { svelte } from '@sveltejs/vite-plugin-svelte'

// https://vitejs.dev/config/
//
// We support generation of multiple JS bundles from different access points.
// However, modules that are shared between them would be split out into separate files.
// See: https://github.com/rollup/rollup/issues/2756
export default defineConfig({
  // base: '/svelte',
  plugins: [svelte()],
  build: {
    rollupOptions: {
      input: {
        'order-form': resolve(__dirname, 'entrypoints/order-form.html'),
        'order-form-status': resolve(__dirname, 'entrypoints/order-form-status.html'),
        // 'another-form': resolve(__dirname, 'entrypoints/another-form.html'),
      },
      output: {
        entryFileNames: `assets/[name].js`,
        chunkFileNames: `assets/[name].js`,
        assetFileNames: `assets/[name].[ext]`
      }
    }
  }
})
