import AppOrderForm from './AppOrderForm.svelte'

let element = document.getElementById('js-container-svelte-order-form');

// We add some "Loading.." stuff to the initial container.
// Let's remove it all before mounting.
element.innerHTML = '';

const app = new AppOrderForm({
  target: element,
})

export default app
