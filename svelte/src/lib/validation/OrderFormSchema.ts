import * as yup from 'yup';
import { DomainNameType, HostingType } from '../types';
import { SshAccessType } from '../types';
import  turnkeyInstanceTypes, { instanceIds } from '../provider/TurnkeyInstanceTypes';
import  turnkeyLocationTypes from '../provider/TurnkeyLocationTypes';
import { supportOptions } from '../provider/SupportOptions';
import { EXAMPLE_DOMAIN, EXAMPLE_SUBDOMAIN_PREFIX } from '../entity/OrderForm';
import { isIPv4 } from 'is-ip';


const locationIds: string[] = turnkeyLocationTypes.map(location => location.id);
const supportTypes: string[] = supportOptions.map(support => support.id);


const patterns = {
	"domain": /^(?!:\/\/)([a-zA-Z0-9-_]+\.)*[a-zA-Z0-9][a-zA-Z0-9-_]+\.[a-zA-Z]{2,11}?$/,
	"cyrillicDomain": /^((http|https):\/\/)?[a-zа-я0-9]+([\-\.]{1}[a-zа-я0-9]+)*\.[a-zа-я]{2,5}(:[0-9]{1,5})?(\/.*)?$/i,
};

yup.addMethod(yup.string, 'domain', function pattern(name, message = "Domain is not valid.") {
	const domainRules = [patterns.domain, patterns.cyrillicDomain];

	return this.test({
		message,
		test: value => (value === null || value === '' || value === undefined) || domainRules.some(regex => regex.test(value)),
	});
});

let OrderFormSchema = yup.object({
	hostingType: yup.mixed<HostingType>().oneOf(Object.values(HostingType)).required(),
	turnkeyInstanceTypeId: yup.string()
		.when("hostingType", {
			is: HostingType.Turnkey,
			then: (schema) => schema.oneOf(instanceIds, (err) => {
				return new yup.ValidationError("Please choose server size.",  err.value, "server-size")
			}).required(),
		}
	),
	turnkeyInstanceLocationId: yup.string()
		.when("hostingType", {
			is: HostingType.Turnkey,
			then: (schema) => schema.oneOf(locationIds, (err) => {
				return new yup.ValidationError("Please choose server location.",  err.value, "server-location")
			}).required(),
		}
	),
	turnkeySshAccessConfig: yup.object({
		enabled: yup.boolean().required(),
		publicKey: yup.string().when("enabled", {
			is: true,
			then: (schema) => schema.required((err) => {
				return new yup.ValidationError("Please provide SSH Public key.",  err.value, "ssh-access-turnkey")
			}).matches(/^(ssh-ed25519|ssh-rsa)\s(.+)/, "Invalid SSH Public key format")
		}).nullable(),
		protectSshPortWithFirewall: yup.boolean(),
		whitelistedIpAddresses: yup.string().when(["enabled", "protectSshPortWithFirewall"], {
			is: true,
			then: (schema) => schema.required((err) => {
				return new yup.ValidationError("Please provide whitelisted IP addresses",  err.value, "ssh-access-turnkey")
			})
		}).nullable()
	}),
	byosSshAccessConfig: yup.object().when("hostingType", {
		is: HostingType.BYOS,
		then: (schema) => yup.object({
			accessType: yup.mixed<SshAccessType|null>().oneOf(Object.values(SshAccessType)).required((err) => {
				return new yup.ValidationError("Please choose access type.",  err.value, "ssh-access-byos")
			}),
			loginUsername: yup.string().when("accessType", {
				is: (val: string) => val === SshAccessType.SudoWithPassword || val === SshAccessType.SudoWithoutPassword,
				then: (schema) => {
					return schema.required("Please provide SSH username").notOneOf(["root"], (err) => {
						return new yup.ValidationError("Please use non root SSH login name.",  err.value, "ssh-access-byos")
					});
				},
			}),
			sudoPassword: yup.string().when("accessType", {
				is: SshAccessType.SudoWithPassword,
				then: (schema) => schema.required("Please provide sudo password")
			}),
			hostName: yup.string().test("ipAddress", "Please provide a valid SSH server IP", (value, context) => {
				if (!isIPv4(value)) {
					return context.createError({
						message: "Please provide a valid SSH server IP.",
						path: "ssh-access-byos",
					})
				}
				return true;
			}),
			portsOpened: yup.boolean().oneOf([true], (err) => {
				return new yup.ValidationError("Please make sure you have opened the needed ports.",  err.value, "ssh-access-byos")
			}).required(),
			publicKeyAdded: yup.boolean().when(["accessType", "loginUsername"], {
				is: (accessType: string, loginUsername: string) => {
					return (accessType !== null) || (loginUsername.trim().length > 0);
				},
				then: (schema) => schema.isTrue((err) => {
					return new yup.ValidationError("Please make sure you have granted access to our SSH key.",  err.value, "ssh-access-byos")
				})
			})
		})
	}),
	domainType: yup.mixed().oneOf(Object.values(DomainNameType)).required((err) => {
		return new yup.ValidationError("Please choose domain type.",  err.value, "domain-type")
	}),
	fullyQualifiedDomainName: yup.string().test("domain-type", "Please provide a domain name.", (value, context) => {
		// TODO: disable due to reported issue with validation
		if (value === EXAMPLE_DOMAIN || value.indexOf(EXAMPLE_SUBDOMAIN_PREFIX) === 0) {
			return context.createError({
				message: "Please use an actual domain name, not the example one.",
				path: "domain-type",
			})
		}
		const domainRules = [patterns.domain, patterns.cyrillicDomain];
		const domainIsValid = domainRules.some(regex => regex.test(value))
		if (!domainIsValid) {
			return context.createError({
				message: "Please provide a valid domain name.",
				path: "domain-type",
			})
		}

		return true;
	}),
	contactEmail: yup.string().email("Please provide valid email address.").required((err) => {
		return new yup.ValidationError("Please provide email address.", err.value, "contact-info")
	}),
	username: yup.string().required((err) => {
		return new yup.ValidationError("Please provide username.", err.value, "contact-info")
	}),
	supportType: yup.mixed().oneOf(supportTypes, (err) => {
		return new yup.ValidationError("Please choose support type.", err.value, "support-type")
	}).required()
});

export default OrderFormSchema;
