import { HostingType, DomainNameType, SshAccessType, type MatrixBotInterface, type MatrixBridgeInterface, type MatrixServiceInterface, type SupportTypeType } from '../types';
import { bases, support } from "../provider/dataConfig.json";
import { additionalServices, additionalMatrixServices } from "../provider/MatrixServices";
import turnkeyInstanceTypes from '../provider/TurnkeyInstanceTypes';
import turnkeyLocationTypes from '../provider/TurnkeyLocationTypes';

export interface OrderFormInterface {
	hostingType: HostingType | null;
	turnkeyInstanceTypeId: string;
	turnkeyInstanceLocationId: string;
	domainType: DomainNameType;
	fullyQualifiedDomainName: string;
	contactEmail: string;
	selectedApps: string[];
	selectedBridges: Partial<MatrixBridgeInterface>[];
	selectedBots: Partial<MatrixBotInterface>[];
	selectedServices: Partial<MatrixServiceInterface>[];
	selectedMatrixServices: Partial<MatrixServiceInterface>[];
	supportType: SupportTypeType;
	username: string;
	byosSshAccessConfig: ByosSshAccessConfig;
	turnkeySshAccessConfig: TurnkeySshAccessConfig;
	toServer(): URLSearchParams;
}

interface FormServerRequestInterface {
	type: string;
	username: string;
	email: string;
	domain: string;
}

let recommendedInstance = turnkeyInstanceTypes.find((instanceType) => instanceType.recommended === true);
let defaultLocation = turnkeyLocationTypes[0];

export const EXAMPLE_SUBDOMAIN_PREFIX: string = "mybusiness";
export const EXAMPLE_DOMAIN: string = "domain.com";

export default class OrderForm implements OrderFormInterface {

	public hostingType: HostingType | null = HostingType.Turnkey;

	public turnkeyInstanceTypeId: string = recommendedInstance.id;
	public turnkeyInstanceLocationId: string = defaultLocation.id;

	public domainType: DomainNameType | null = DomainNameType.ManagedSubdomain;
	public fullyQualifiedDomainName: string = "";
	public subdomainError: string = "";
	public domainNameError: string = "";

	public contactEmail: string = "";
	public selectedApps: string[] = ["element-web"];

	public selectedBridges: MatrixBridgeInterface[] = [];

	public bridgeEncryptionEnabled: boolean = false;

	public selectedBots: MatrixBotInterface[] = [];

	public selectedServices: Partial<MatrixServiceInterface>[] = additionalServices.filter((service) => service.checked);
	public selectedMatrixServices: Partial<MatrixServiceInterface>[] = additionalMatrixServices.filter((service) => service.checked);

	public supportType: SupportTypeType = "basic";

	public username: string = "";

	public byosSshAccessConfig: ByosSshAccessConfig;

	public turnkeySshAccessConfig: TurnkeySshAccessConfig;

	constructor() {
		this.byosSshAccessConfig = new ByosSshAccessConfig();
		this.turnkeySshAccessConfig = new TurnkeySshAccessConfig();
	}

	domainInfoIsComplete() {
		if (!this.domainType || !this.fullyQualifiedDomainName) {
			return false;
		}

		if (this.domainType === DomainNameType.OwnDomain && this.fullyQualifiedDomainName.trim() === EXAMPLE_DOMAIN) {
			return false;
		}

		if (this.domainType === DomainNameType.ManagedSubdomain && this.fullyQualifiedDomainName.indexOf(EXAMPLE_SUBDOMAIN_PREFIX) === 0) {
			return false;
		}

		return true;
	}

	contactInfoIsComplete() {
		if (this.contactEmail.trim() !== '' && this.username.trim() !== '') {
			return true;
		}

		return false;
	}

	toServer() {
		const serverRequest: FormServerRequestInterface = {
			[bases[0].id]: "on",
			type: this.hostingType,
			username: this.username,
			email: this.contactEmail,
			domain: this.fullyQualifiedDomainName,
		};

		if (this.hostingType === HostingType.Turnkey) {
			serverRequest[HostingType.Turnkey] = this.turnkeyInstanceTypeId;
			serverRequest[`${this.hostingType}-location`] = this.turnkeyInstanceLocationId;
		}

		if (this.turnkeySshAccessConfig) {
			if (this.turnkeySshAccessConfig.enabled) {
				serverRequest["ssh-client-key"] = this.turnkeySshAccessConfig.publicKey;
				serverRequest["ssh-client-ips"] = (this.turnkeySshAccessConfig.protectSshPortWithFirewall ? this.turnkeySshAccessConfig.whitelistedIpAddresses : 'N/A');
			} else {
				serverRequest["ssh-client-key-disabled"] = "on";
			}
		}

		if (this.byosSshAccessConfig && this.byosSshAccessConfig.isComplete()) {
			serverRequest["ssh-host"] = this.byosSshAccessConfig.hostName;
			serverRequest["ssh-user"] = this.byosSshAccessConfig.loginUsername;
			serverRequest["ssh-password"] = this.byosSshAccessConfig.sudoPassword;
			serverRequest["ssh-port"] = this.byosSshAccessConfig.portNumber;
		}

		if (this.supportType) {
			serverRequest[support.id] = this.supportType;
		}

		if (this.selectedApps && this.selectedApps.length > 0) {
			for (let app of this.selectedApps) {
				serverRequest[app] = "on";
			}
		}

		if (this.selectedBridges && this.selectedBridges.length > 0) {
			for (let bridge of this.selectedBridges) {
				serverRequest[bridge.id] = "on";
				if (bridge.additionalFields && bridge.additionalFields.length > 0) {
					for (let bridgeAdditionalField of bridge.additionalFields) {
						serverRequest[bridgeAdditionalField.id] = bridgeAdditionalField.value;
					}
				}
			}

			if (this.bridgeEncryptionEnabled) {
				serverRequest["bridges-encryption"] = "on";
			}
		}

		if (this.selectedBots && this.selectedBots.length > 0) {
			for (let bot of this.selectedBots) {
				serverRequest[bot.id] = "on";
				if (bot.additionalFields && bot.additionalFields.length > 0) {
					for (let botAdditionalField of bot.additionalFields) {
						serverRequest[botAdditionalField.id] = botAdditionalField.value;
					}
				}
			}
		}

		if (this.selectedServices && this.selectedServices.length > 0) {
			for (let service of this.selectedServices) {
				serverRequest[service.id] = "on";
				if (service.additionalFields && service.additionalFields.length > 0) {
					for (let serviceAdditionalField of service.additionalFields) {
						serverRequest[serviceAdditionalField.id] = serviceAdditionalField.value;
					}
				}
			}
		}

		if (this.selectedMatrixServices && this.selectedMatrixServices.length > 0) {
			for (let service of this.selectedMatrixServices) {
				serverRequest[service.id] = "on";
				if (service.additionalFields && service.additionalFields.length > 0) {
					for (let serviceAdditionalField of service.additionalFields) {
						serverRequest[serviceAdditionalField.id] = serviceAdditionalField.value;
					}
				}
			}
		}

		const formBody = new URLSearchParams();
		for (let requestField of Object.keys(serverRequest)) {
			formBody.append(requestField, serverRequest[requestField]);
		}

		return formBody;
	}

}

class TurnkeySshAccessConfig {

	public enabled: boolean = false;

	public publicKey: string | null = null;

	public protectSshPortWithFirewall: boolean = true;

	public whitelistedIpAddresses: string|null = null;
}

class ByosSshAccessConfig {

	public accessType: SshAccessType|null = null;

	public portNumber: number|null = 22;

	public publicKeyAdded: boolean = false;
	public portsOpened: boolean = false;

	// This is the username of the regular user (which elevates to root).
	// Applicable when `accessType != SshAccessType.Root`
	public loginUsername: string = '';

	public hostName: string = '';

	// This is the sudo password for elevating to root.
	// Applicable when `accessType === SshAccessType.SudoWithPassword`
	public sudoPassword: string = '';

	getHomeDirectoryPath(): string {
		if (this.accessType === null) {
			// if nothing is selected, root will be used
			return "/root";
		}

		if (this.accessType === SshAccessType.Root) {
			return '/root';
		}

		if (this.loginUsername === '') {
			return '/home/etke';
		}

		return '/home/' + this.loginUsername;
	}

	isComplete(): boolean {
		if (this.hostName.trim() === '') {
			return false;
		}

		if (this.accessType !== null && !(this.publicKeyAdded && this.portsOpened)) {
			return false;
		}

		if (this.accessType === SshAccessType.Root) {
			return true;
		}

		if (this.loginUsername.trim() === '') {
			return false;
		}

		if (this.accessType === SshAccessType.SudoWithoutPassword) {
			return true;
		}

		return (this.sudoPassword.trim() !== '');
	}

}
