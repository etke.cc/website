export default class TurnkeyInstanceType {

	id: string;
	name: string;
	cpusCount: number;
	ramSizeGb: number;
	diskSizeGb: number;
	trafficTb: number;
	capacityDescription: string;
	price: number;
	recommended: boolean;

	public constructor(
		id: string,
		name: string,
		cpusCount: number,
		ramSizeGb: number,
		diskSizeGb: number,
		trafficTb: number,
		capacityDescription: string,
		price: number,
		recommended: boolean,
	) {
		this.id = id;
		this.name = name;
		this.cpusCount = cpusCount;
		this.ramSizeGb = ramSizeGb;
		this.diskSizeGb = diskSizeGb;
		this.trafficTb = trafficTb;
		this.capacityDescription = capacityDescription;
		this.price = price;
		this.recommended = recommended;
	}

}
