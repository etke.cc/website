export default class TurnkeyLocationType {

	id: string;
	name: string;
	countryName: string;
	countryCode: string;

	public constructor(
		id: string,
		name: string,
		countryName: string,
		countryCode: string,
	) {
		this.id = id;
		this.name = name;
		this.countryName = countryName;
		this.countryCode = countryCode;
	}

}
