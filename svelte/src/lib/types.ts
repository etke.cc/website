export enum HostingType {
	BYOS = "byos",
	Turnkey = "turnkey",
};

export type HostingTypeLiteral = `${HostingType}`;

export enum DomainNameType {
	OwnDomain = "domain",
	ManagedSubdomain = "subdomain"
};

export enum SshAccessType {
	Root = "root",
	SudoWithoutPassword = "sudo-without-password",
	SudoWithPassword = "sudo-with-passsword",
};

export interface ClientAppInterface {
	id: string,
	iid: string,
	archived?: boolean;
	name: string,
	description: string;
	additionalInfo: string;
	price: number;
};

export interface MatrixBridgeInterface extends ServerMinTypeInterface {
	id: string;
	iid: string;
	archived?: boolean;
	checked: boolean;
	name: string;
	package: string;
	help: string;
	helpText?: string;
	additionalFields?: any;
};

export interface MatrixServiceInterface extends ServerMinTypeInterface {
	id: string;
	iid: string;
	archived?: boolean;
	checked: boolean;
	name: string;
	description: string;
	price: number;
	default?: boolean;
	help?: string;
	helpText?: string;
	additionalFields?: any;
};

export interface MatrixBotInterface extends ServerMinTypeInterface {
	id: string;
	iid: string;
	archived?: boolean;
	checked: boolean;
	name: string;
	description: string;
	help?: string;
	helpText?: string;
	price: number;
	additionalFields?: any;
};

export interface ServerMinTypeInterface {
	server_min_size?: string;
}

export type SupportTypeType = "basic" | "dedicated";
