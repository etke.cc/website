import { writable } from 'svelte/store';
import type { HostingTypeLiteral } from '../types';

export type TEntityStore = {
	instanceType: string,
	hostingType: HostingTypeLiteral
};
export const entityStore = writable<TEntityStore>();