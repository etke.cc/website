import turnkeyInstanceTypes from "../provider/TurnkeyInstanceTypes";
import { HostingType, type ServerMinTypeInterface } from "../types";

let scrollIntoView = (el: Element) => {
	let position = el.getBoundingClientRect();
	window.scrollTo(position.left, position.top + window.scrollY - 90);
};

let timeoutId = null;

let debounce = function (callback: () => void, delay: number) {
	return function () {
		let args = arguments;

		clearTimeout(timeoutId);

		timeoutId = setTimeout(function () {
			callback.apply(null, args);
		}, delay);
	};
};

let isServerEnoughForComponent = (component: ServerMinTypeInterface, entityStore: any): boolean => {
	if (entityStore.hostingType === HostingType.BYOS) {
		return true;
	}

	if (!component?.server_min_size) {
		return true;
	}

	const selectedInstanceIndex = turnkeyInstanceTypes.findIndex((instance) => {
		return instance.id === entityStore.instanceType
	});
	const componentMinInstanceIndex = turnkeyInstanceTypes.findIndex((instance) => {
		return instance.id === component?.server_min_size
	});

	return selectedInstanceIndex >= componentMinInstanceIndex;
};

export {
	scrollIntoView,
	isServerEnoughForComponent,
	debounce,
}