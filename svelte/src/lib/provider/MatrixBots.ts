import type { MatrixBotInterface } from "../types";
import * as dataConfig from "./dataConfig.json";

export let matrixBots: MatrixBotInterface[] = [
	...dataConfig.matrixBots
];