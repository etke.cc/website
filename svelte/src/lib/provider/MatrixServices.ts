import type { MatrixServiceInterface } from "../types";
import { additionalServices as configAdditionalServices, additionalMatrixServices as configAdditionalMatrixServices } from "./dataConfig.json";

export let additionalServices: MatrixServiceInterface[] = [
	...configAdditionalServices
];

export let additionalMatrixServices: MatrixServiceInterface[] = [
	...configAdditionalMatrixServices
];
