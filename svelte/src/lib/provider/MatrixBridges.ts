import type { MatrixBridgeInterface } from "../types";
import { matrixBridges, matrixBridgesPrice } from "./dataConfig.json";

export let bridgesPrice = matrixBridgesPrice;

export let bridges: MatrixBridgeInterface[] = [
	...matrixBridges
];