import TurnkeyInstanceType from '../entity/TurnkeyInstanceType';
import { instances} from "./dataConfig.json";

const turnkeyInstanceTypes: TurnkeyInstanceType[] = [];

instances.options.forEach(element => {
	if (element.disabled) {
		return;
	}
	turnkeyInstanceTypes.push(new TurnkeyInstanceType(
		element.id,
		element.name,
		element.cpusCount,
		element.ramSizeGb,
		element.diskSizeGb,
		element.trafficTb,
		element.capacityDescription,
		element.price,
		element.recommended,
	));
});

export const UNDERPOWERED_INSTANCE_ID = "cpx11";

export const instanceIds: string[] = turnkeyInstanceTypes.map(instance => instance.id);

export default turnkeyInstanceTypes;
