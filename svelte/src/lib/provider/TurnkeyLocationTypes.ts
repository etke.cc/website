import TurnkeyLocationType from '../entity/TurnkeyLocationType';
import { locations } from "./dataConfig.json";

const turnkeyLocationTypes: TurnkeyLocationType[] = [];
locations.forEach(element => {
	turnkeyLocationTypes.push(new TurnkeyLocationType(
		element.id,
		element.name,
		element.countryName,
		element.countryCode,
	));
});


export default turnkeyLocationTypes;
