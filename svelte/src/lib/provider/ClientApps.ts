import type { ClientAppInterface } from "../types";
import { matrixApps } from "./dataConfig.json";

export let clientApps: ClientAppInterface[] = [
	...matrixApps
];