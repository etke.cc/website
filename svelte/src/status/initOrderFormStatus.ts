import AppOrderFormStatus from './AppOrderFormStatus.svelte'

let element = document.getElementById('js-container-svelte-order-form-status');

// We add some "Loading.." stuff to the initial container.
// Let's remove it all before mounting.
element.innerHTML = '';

const app = new AppOrderFormStatus({
  target: element,
})

export default app
