export interface OrderStatusInterface {
	domain: string;
	error: string;
	installable: boolean;
	reasons: string[];
}

export default class OrderStatus implements OrderStatusInterface {
	public domain: string = "";
	public error: string = "";
	public installable: boolean = false;
	public reasons: string[] = [];

	constructor(data: OrderStatusInterface) {
		Object.keys(this).forEach(key => {
			if (data[key]) {
				this[key] = data[key];
			}
		});
	}
}